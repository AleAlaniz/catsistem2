<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users Controller Class
 *
 * Controller Users Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Users
 * @author 		Ivan
*/
class Users extends CI_Controller {

	/**
	 * Class constructor
	 *
	 * Load Model To Use.
	 *
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		//Load User Model
		$this->load->model('User_model', 'User');
	}

	// --------------------------------------------------------------------

	/**
	 * User - Index Page
	 *
	 * @return	mixed
	 */
	public function index()
	{
		//Check Permissions
		if ($this->Identity->Validate('users/index'))
		{

			//Load View
			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('users/users');
			$this->load->view(PRELAYOUTFOOTER);
			
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * User - Create Page
	 *
	 * @return	mixed
	 */
	public function create()
	{
		//Check Permissions
		if ($this->Identity->Validate('users/create'))
		{
			//Call Corresponding Method
			return $this->User->CreateUser();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * User - Delete Page
	 *
	 * @return	mixed
	 */
	public function delete()
	{
		//Check Permissions
		if($this->Identity->Validate('users/delete'))
		{
			//Call Corresponding Method
			return $this->User->DeleteUser();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * User - Details Page
	 *
	 * @return	mixed
	 */
	public function details()
	{
		//Check Permissions
		if($this->Identity->Validate('users/details'))
		{
			//Call Corresponding Method
			return $this->User->DetailsUser();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * User - Edit Page
	 *
	 * @return	mixed
	 */
	public function edit()
	{
		//Check Permissions
		if($this->Identity->Validate('users/edit'))
		{
			//Call Corresponding Method
			return $this->User->EditUser();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * User - Config Page
	 *
	 * @return	mixed
	 */
	public function config()
	{
		//Check Permissions
		if($this->Identity->Validate('users/config'))
		{
			//Check $_GET['cmd'] Variable Value
			if ($this->Identity->Validate('users/config/photo') && $this->input->get('cmd') && $this->input->get('cmd') == 'photo')
			{
				//Call Corresponding Method
				return $this->User->ConfigPhoto();
			}
			elseif ($this->Identity->Validate('users/config/data') && $this->input->get('cmd') && $this->input->get('cmd') == 'data')
			{
				//Call Corresponding Method
				return $this->User->ConfigUser();
			}
			elseif ($this->Identity->Validate('users/config/password') && $this->input->get('cmd') && $this->input->get('cmd') == 'password')
			{
				//Call Corresponding Method
				return $this->User->ConfigPassword();
			}
			elseif($this->Identity->Validate('users/config/select_photo') && $this->input->get('cmd') && $this->input->get('cmd')=='selectphoto')
			{
				return $this->User->getUserphotos();
			}
			elseif($this->Identity->Validate('users/config/background') && $this->input->get('cmd') && $this->input->get('cmd')=='background')
			{
				return $this->User->SelectBackground();
			}
			else
			{
				//Call Corresponding Method
				return $this->User->Config();
			}
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	public function getcitiesbystate()
	{
		if($this->Identity->Validate('users/config') && $this->input->get('state')) {
			$data 			= new StdClass();
			$data->status 	= 'success';
			$data->data 	= $this->User->GetCitiesByState($this->input->get('state'));
			echo escapeJsonString($data, FALSE);
		}
		else {
			echo '{"status" : "invalid"}';
		}
	}

	// --------------------------------------------------------------------

	/**
	 * User - Get Profile Photo Page
	 *
	 * @return	mixed
	 */
	public function profilephoto()
	{
		//Check Permissions
		if($this->Identity->Validate('users/profilephoto'))
		{
			//Call Corresponding Method
			return $this->User->GetProfilePhoto();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * User - Reset Password
	 *
	 * @return	mixed
	 */
	public function resetpassword()
	{
		//Check Permissions
		if($this->Identity->Validate('users/resetpassword'))
		{
			//Call Corresponding Method
			return $this->User->ResetPassword();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}


	public function userimages()
	{
		//Check Permissions
		if($this->Identity->Validate('users/config/config_photo'))
		{
			//Call Corresponding Method
			return $this->User->addImageToUser();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}

	}

	public function selectimage()
	{
		if($this->Identity->Validate('users/config/select_photo'))
		{
			//Call Corresponding Method
			return $this->User->selectImage();
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}

	}

	public function getprofilephotos()
	{
		return $this->User->GetProfilePhotos();
	}

	public function deletephoto()
	{
		if($this->Identity->Validate('users/config/config_photo'))
		{
			//Call Corresponding Method
			return $this->User->DeletePhoto();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}

	}

	public function resetphoto()
	{
		return $this->User->ResetPhoto();
	}
	
	public function searchusers()
	{
		$request = json_decode($this->input->raw_input_stream,true);
		$like = $request['like'];
		$users = $this->User->SearchUsers($like);
		if($users == 'empty'){
			echo $users;
		}
		else {
			echo json_encode($users);
		}
	}
	
	public function getusersbysite()
	{
		$request = json_decode(file_get_contents('php://input'), true);
		if(isset($request['usersIds'])){
			return $this->User->GetUsersBySite($request['usersIds']);
		}
	}

	public function getUsers()
	{

		$rolepermisson = "&& sectionId = ".$this->session->sectionId;
		if ($this->Identity->Validate('sections/viewall')) {
			$rolepermisson = '';
		}

		$sqlcount 	= "SELECT COUNT(u.userId) AS usuarios FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON u.userId = up.userId INNER JOIN (SELECT name, roleId FROM roles) AS r ON u.roleId = r.roleId WHERE true ".$rolepermisson." ";
		$sql 		= "SELECT u.userId, up.lastName, up.name, u.userName, u.active, r.name AS 'role' FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON u.userId = up.userId INNER JOIN (SELECT name, roleId FROM roles) AS r ON u.roleId = r.roleId WHERE true ".$rolepermisson." ";

		if (strlen($_POST['search']['value'])>0){

			$search_value 	= $_POST['search']['value'];
			$sql 			= $sql.'AND (concat_ws(" ", up.name, lastName) LIKE "%'.$search_value.'%" || userName LIKE "%'.$search_value.'%") ';
			$sqlcount 		= $sqlcount.'AND (concat_ws(" ", up.name, lastName) LIKE "%'.$search_value.'%" || userName LIKE "%'.$search_value.'%") ';
		}

		$sql = $sql.'ORDER BY lastName, up.name LIMIT ' .$_POST['start']. ',' .$_POST['length']. '   ';

		$users 		= $this->db->query($sql)->result();
		$usersCount = $this->db->query($sqlcount)->row();

		$users_data = array();

		for ($i=0; $i < count($users) ; $i++) { 

			$userData = array();
			
			$userData[] = $users[$i]->userId;
			$userData[] = $users[$i]->name;
			$userData[] = $users[$i]->lastName;
			$userData[] = $users[$i]->userName;
			$userData[] = $users[$i]->role;
			$userData[] = ($users[$i]->active == 1)? 'si' : 'no';

			$users_data[] = $userData;
		}

		$data = array(
			"draw" => intval($_POST["draw"]),
			"recordsTotal"      => $usersCount->usuarios,
			"recordsFiltered"   => $usersCount->usuarios,
			"data"              => $users_data
		);

		echo json_encode($data);
	}
}
    