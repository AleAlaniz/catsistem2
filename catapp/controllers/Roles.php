<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Roles Controller Class
 * 
 * Controller Roles Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Roles
 * @author 		Ivan
*/
class Roles extends CI_Controller {

	/**
	 * Class constructor
	 *
	 * Load Model To Use.
	 *
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		//Load Role model
		$this->load->model('Role_model', 'Roles');
	}

	// --------------------------------------------------------------------

	/**
	 * Roles - Index Page
	 *
	 * @return	mixed
	 */
	public function index()
	{
		//Check Permissions
		if ($this->Identity->Validate('roles/index'))
		{
			//Call Corresponding Method
			$this->Roles->GetRoles();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	public function getRoles()
	{
		$data = $this->Roles->GetRolesForComponent();
		echo json_encode($data);		
	}
	// --------------------------------------------------------------------

	/**
	 * Roles - Create Page
	 *
	 * @return	mixed
	 */
	public function create()
	{	
		//Check Permissions
		if ($this->Identity->Validate('roles/create')) 
		{
			//Call Corresponding Method
			return $this->Roles->Create();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Roles - Delete Page
	 *
	 * @return	mixed
	 */
	public function delete()
	{
		//Check Permissions
		if($this->Identity->Validate('roles/delete'))
		{
			//Call Corresponding Method
			return $this->Roles->Delete();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Roles - Details Page
	 *
	 * @return	mixed
	 */
	public function details()
	{
		//Check Permissions
		if($this->Identity->Validate('roles/details'))
		{
			//Call Corresponding Method
			return $this->Roles->DetailsRole();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Roles - Edit Page
	 *
	 * @return	mixed
	 */
	public function edit()
	{
		//Check Permissions
		if($this->Identity->Validate('roles/edit'))
		{
			//Call Corresponding Method
			return $this->Roles->Edit();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}
}