<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teaming extends CI_Controller {

	public function __construct(){

		parent::__construct();
		$this->load->library('Pdf');
		$this->load->library('export_excel');
		$this->load->model('Teamingaudits_model', 'Teamingaudits');
	}

	public function loadModal(){

		$res 			= new StdClass();
		$res->status 	= 'fail';

		$query = 
		'SELECT sectionId
		FROM teamingsectionavailable
		WHERE isAvailable = 1 AND sectionId = ?';

		if($this->db->query($query, $this->session->sectionId)->row() !== NULL)
		{
			$query = 
			'SELECT teamingShowPopupId
			FROM usershowpopupteaming
			WHERE userId = ? AND sectionId = ?';

			$query = $this->db->query($query, array($this->session->UserId, $this->session->sectionId));

			if($query->row() === NULL)
			{
				$this->db->insert('usershowpopupteaming', array('userId' => $this->session->UserId, 'sectionId' => $this->session->sectionId));
				$res->status = 'success';
			}
		}

		echo json_encode($res);
	}

	public function index(){

		if($this->Identity->divisionAvailable()){
			$this->load->view('teaming/teaming', array('header' => $this->load->view('teaming/_teaming_header')));
		}
		else{
			show_404();
		}
	}

	public function agreement(){
		$this->load->view('teaming/agreement');
	}

	public function update(){
		$this->load->view('teaming/update', array("query" => $this->Layout->GetUser()));
	}

	public function agreementTerms(){
		$this->load->view('teaming/agreementTerms');
	}

	public function acceptedForm(){
		$this->load->view('teaming/acceptedForm');
	}

	public function pendingApprove(){
		$this->load->view('teaming/pendingApprove');
	}

	public function noForm(){
		$this->load->view('teaming/noForm');
	}

	public function alreadySent(){
		$this->load->view('teaming/alreadySent');
	}

	public function pendingUpdateApprove(){
		$this->load->view('teaming/pendingUpdateApprove');
	}

	public function reports(){

		if($this->Identity->Validate('teaming/reports')){

			$this->load->view('teaming/reports', array('navbar' => $this->load->view('teaming/_navbar', array('title' => $this->lang->line('chat_uniquetitle')), TRUE), 'header' => $this->load->view('teaming/_teaming_header') ));
		}
		else{
			show_404();
		}
	}

	public function remainingReports(){
		if($this->Identity->Validate('teaming/reports')){
			$this->load->view('teaming/remainingReports', array('navbar' => $this->load->view('teaming/_navbar', array('title' => $this->lang->line('chat_uniquetitle')), TRUE), 'header' => $this->load->view('teaming/_teaming_header')));
		}
		else{
			show_404();
		}
	}

	public function manualAgreementLoad(){

		if($this->Identity->Validate('teaming/manual')){
			$this->load->view('teaming/manualAgreementLoad', array('navbar' => $this->load->view('teaming/_navbarmanual', array('title' => $this->lang->line('chat_uniquetitle')), TRUE), 'header' => $this->load->view('teaming/_teaming_header')));
		}
		else{
			show_404();
		}
	}

	public function manualUpdateLoad(){

		if($this->Identity->Validate('teaming/manual')){
			$this->load->view('teaming/manualUpdateLoad', array('navbar' => $this->load->view('teaming/_navbarmanual', array('title' => $this->lang->line('chat_uniquetitle')), TRUE), 'header' => $this->load->view('teaming/_teaming_header')));
		}
		else{
			show_404();
		}
	}

	public function divisions(){

		if($this->Identity->Validate('teaming/divisions')){
			$this->load->view('teaming/divisions', array('header' => $this->load->view('teaming/_teaming_header')));
		}
		else{
			show_404();
		}
	}

	public function joinedUsers(){

		if($this->Identity->Validate('teaming/joinedUsers')){
			$this->load->view('teaming/joinedUsers', array('header' => $this->load->view('teaming/_teaming_header')));
		}
		else{
			show_404();
		}
	}

	public function records(){

		if($this->Identity->Validate('teaming/records')){
			$this->load->view('teaming/records', array('header' => $this->load->view('teaming/_teaming_header')));
		}
		else{
			show_404();
		}
	}

	public function pendingForms(){

		if($this->Identity->Validate('teaming/pendingforms')){
			$this->load->view('teaming/pendingForms', array('header' => $this->load->view('teaming/_teaming_header')));
		}
		else{
			show_404();
		}
	}

	public function communicationReports(){

		if($this->Identity->Validate('teaming/comunnicationreports')){
			$this->load->view('teaming/communicationReports',array('navbar' => $this->load->view('teaming/_navbarcommunication', array('title' => $this->lang->line('chat_uniquetitle')), TRUE), 'header' => $this->load->view('teaming/_teaming_header')));
		}
		else{
			show_404();
		}
	}

	public function newsletterReports(){

		if($this->Identity->Validate('teaming/comunnicationreports')){
			$this->load->view('teaming/newsletterReports',array('navbar' => $this->load->view('teaming/_navbarcommunication', array('title' => $this->lang->line('chat_uniquetitle')), TRUE), 'header' => $this->load->view('teaming/_teaming_header')));
		}
		else{
			show_404();
		}
	}

	public function messageReports(){

		if($this->Identity->Validate('teaming/comunnicationreports')){
			$this->load->view('teaming/messageReports',array('navbar' => $this->load->view('teaming/_navbarcommunication', array('title' => $this->lang->line('chat_uniquetitle')), TRUE), 'header' => $this->load->view('teaming/_teaming_header')));
		}
		else{
			show_404();
		}
	}

	public function surveyReports(){

		if($this->Identity->Validate('teaming/comunnicationreports')){
			$this->load->view('teaming/surveyReports',array('navbar' => $this->load->view('teaming/_navbarcommunication', array('title' => $this->lang->line('chat_uniquetitle')), TRUE), 'header' => $this->load->view('teaming/_teaming_header')));
		}
		else{
			show_404();
		}
	}

	public function details(){

		if($this->Identity->Validate('teaming/records')){
			$this->load->view('teaming/details');
		}
		else{
			show_404();
		}
	}

	public function sendSuggestion(){

		$this->load->view('teaming/sendSuggestion');
	}

	public function acceptedSuggestion(){

		$this->load->view('teaming/acceptedSuggestion');
	}

	public function institutions(){

		$this->load->view('teaming/institutions', array('header' => $this->load->view('teaming/_teaming_header')));
	}

	public function getUserStatus(){

		$res   = new StdClass();
		$sql   = new StdClass();
		$query = new StdClass();

		$alreadyJoined 		= false;
		$PendingUpdate 		= false;
		$petitionApproved	= false;

		$sql   = 
		"SELECT COUNT(*) AS c
		FROM teamingforms
		WHERE userId = ?";
		$query = $this->db->query($sql, array('userId'=>$this->session->UserId))->row();

		if($query->c > 0){
			$alreadyJoined = true;
		}

		$sql   = 
		"SELECT COUNT(*) AS c
		FROM teamingforms
		WHERE userId = ?
		AND agreementDate IS NOT NULL
		AND isApproved = 1";
		$query = $this->db->query($sql, array('userId'=>$this->session->UserId))->row();

		if($query->c > 0){
			$petitionApproved = true;
		}

		$sql   = 
		"SELECT COUNT(*) AS c
		FROM teamingupdateforms
		WHERE userId = ?
		AND isUpdateApproved = 0";
		$query = $this->db->query($sql, array('userId'=>$this->session->UserId))->row();

		if($query->c > 0){
			$PendingUpdate = true;
		}

		$res->status = 'ok';
		$res->message = array(	
			"alreadyJoined"		=>	$alreadyJoined,
			"PendingUpdate"		=>	$PendingUpdate,
			"petitionApproved" 	=>  $petitionApproved
		);

		echo json_encode($res);
	}
	
	function repeated_amount($field,$oldValue){
		
		if(strcmp($field, $oldValue) == 0){
			$this->form_validation->set_message('repeated_amount', 'El campo %s debe tener un valor distinto al monto anterior');
			return FALSE;	
		}
		else{
			return TRUE;	
		}

	}

	function repeated_amountCharacters($field,$oldValue){
		
		if(strcmp($field, $oldValue) == 0){
			$this->form_validation->set_message('repeated_amountCharacters', 'El campo %s debe tener un valor distinto al anterior');
			return FALSE;	
		}
		else{
			return TRUE;	
		}

	}

	function cel_number_validation($celnumber){

		if(! preg_match('/^(\+)*[0-9]+$/', $celnumber)) {
			$this->form_validation->set_message('cel_number_validation', 'El campo %s solamente puede contener un símbolo + al inicio, seguido de dígitos numéricos');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}

	function alpha_special($alpha){

		if(! preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚ\s]+$/', $alpha)) {

			$this->form_validation->set_message('alpha_special', 'El campo %s solamente puede contener letras.');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}

	function date_validator($date) {

		if (preg_match("^\d{2}/\d{2}/\d{4}^", $date)){

			$date_array = explode('/', $date);

			if (! checkdate($date_array[1], $date_array[0], $date_array[2])){

				$this->form_validation->set_message('date_validator','el campo %s debe contener el siguiente formato: dd/mm/aaaa.');
				return false;
			}
		}
		else{

			$this->form_validation->set_message('date_validator','el campo %s debe contener el siguiente formato: dd/mm/aaaa.');
			return false;
		}
		return true;
	}

	function empty_object_validator($object){

		if(is_null($object)){

			$this->form_validation->set_message('empty_object_validator', 'El campo %s debe estar definido');
			return FALSE;	
		}
		else{
			return TRUE;	
		}
	}

	public function GetUnreadForms(){
		$data = new StdClass();
		$data->status = 'ok';
		$data->unread = $this->db->select('userId')->from('teamingforms')->where('agreementDate',NULL)->where('isApproved',0)->count_all_results();
		$mod = $this->db->select('userId')->from('teamingupdateforms')->where('updateDate',NULL)->where('isUpdateApproved',0)->count_all_results();
		$data->unread = $data->unread + $mod;
		echo json_encode($data);
	}

	public function sendAgreeForm(){

		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$_POST = json_decode(file_get_contents('php://input'), true);

		$sql    = 
		"SELECT COUNT(*) AS alreadyJoined
		FROM teamingforms
		WHERE userId = ?";
		$query 	= $this->db->query($sql, array('userId'=>$this->input->post("userId")))->row();
		$res 	= new StdClass();

		if($query->alreadyJoined == 0){

			$this->form_validation->set_rules('dni', $this->lang->line('teaming_form_id'), 'required|numeric|min_length[8]|max_length[8]');
			$this->form_validation->set_rules('amount', $this->lang->line('teaming_form_amount'), 'required|numeric|greater_than_equal_to[25]|max_length[4]');
			$this->form_validation->set_rules('amountCharacters', $this->lang->line('teaming_form_amount_letters'), 'required|callback_alpha_special|max_length[200]');
			$this->form_validation->set_rules('areaCampaign', $this->lang->line('teaming_form_area_campaign'), 'required|max_length[136]|regex_match[/^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ ,.+]*$/u]');
			$this->form_validation->set_rules('mobileNumber', $this->lang->line('teaming_form_celnumber'), 'required|max_length[20]|callback_cel_number_validation');
			$this->form_validation->set_rules('acceptterms', $this->lang->line('teaming_form_agree_terms'), 'required|max_length[150]');
			$this->form_validation->set_rules('loadType', $this->lang->line('teaming_form_load_type'), 'required');
			$this->form_validation->set_rules('institutionName', $this->lang->line('teaming_suggest_institution'), 'max_length[150]|regex_match[/^[0-9a-zñáéíóúüA-ZÑÁÉÍÓÚÜ ,.]*$/u]');

			if ($this->form_validation->run() == FALSE){

				$res->status 	= 'invalid';
				$res->message 	= validation_errors();
			}
			else{

				$res->status 	 = 'success';
				$institutionName = ($this->input->post("institutionName") == null ? " " : $this->input->post("institutionName"));
				$id 			 = ($this->input->post("id") == null ? " " : $this->input->post("id"));

				$formData = array(
					'userId' 		   => $this->input->post("userId"),
					'dni' 	 		   => $this->input->post("dni"),
					'amount' 		   => $this->input->post("amount"),
					'amountCharacters' => $this->input->post("amountCharacters"),
					'areaCampaign' 	   => $this->input->post("areaCampaign"),
					'mobileNumber'	   => $this->input->post("mobileNumber"),
					'institutionName'  => $institutionName,
					'agreementDate'    => null,
					'isApproved' 	   => false,
					'id'			   => $id,
					'loadType'		   => $this->input->post("loadType")
				);
				$this->db->insert('teamingforms',$formData);

				$timestamp = time() - (3600*3);
				$createEntry = array(
					'date'	=> gmdate("Y-m-d H:i:s",$timestamp),
					'userId'=> $this->input->post("userId"),
					'action'=> "send".$this->input->post("loadType"),
					'data'	=> $this->input->post("amount")
				);
				$this->db->insert('teamingaudits',$createEntry);
			}

		}
		else{
			$res->status  = 'alreadyJoined';
			$res->message = 'El usuario que intenta agregar ya existe';
		}

		echo json_encode($res);
	}

	public function sendUpdateForm(){

		$query 	= new StdClass();
		$res 	= new StdClass();
		$_POST = json_decode(file_get_contents('php://input'), true);

		$sql   	= 
		"SELECT COUNT(*) AS alreadyUpdated
		FROM teamingupdateforms
		WHERE userId = ?
		AND isUpdateApproved = 0";
		$query 	= $this->db->query($sql, array('userId'=>$this->input->post("userId")))->row();
		
		if($query->alreadyUpdated == 0){

			$old   = $_POST["oldForm"];
			$_POST = $_POST["form"];

			$this->form_validation->set_rules('dni', $this->lang->line('teaming_form_id'), 'required|numeric|min_length[8]|max_length[8]');
			$this->form_validation->set_rules('amount', $this->lang->line('teaming_form_amount'), 'required|numeric|greater_than_equal_to[25]|max_length[4]|callback_repeated_amount['.$old["amount"].']');
			$this->form_validation->set_rules('amountCharacters', $this->lang->line('teaming_form_amount_letters'), 'required|callback_alpha_special|max_length[200]|callback_repeated_amountCharacters['.$old["amountCharacters"].']');
			$this->form_validation->set_rules('areaCampaign', $this->lang->line('teaming_form_area_campaign'), 'required|max_length[136]|regex_match[/^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ ,.+]*$/u]');
			$this->form_validation->set_rules('mobileNumber', $this->lang->line('teaming_form_celnumber'), 'required|max_length[20]|callback_cel_number_validation');
			$this->form_validation->set_rules('acceptterms', $this->lang->line('teaming_form_agree_terms'), 'required|max_length[150]');
			$this->form_validation->set_rules('loadType', $this->lang->line('teaming_form_load_type'), 'required');
			$this->form_validation->set_rules('institutionName', $this->lang->line('teaming_suggest_institution'), 'max_length[150]|regex_match[/^[0-9a-zñáéíóúüA-ZÑÁÉÍÓÚÜ ,.]*$/u]');

			if ($this->form_validation->run() == FALSE){

				$res->status 	= 'invalid';
				$res->message 	= validation_errors();
			}
			else{

				$res->status 	= 'success';

				$updateForm = array(
					'userId' 					=> $this->input->post("userId"),
					'dni' 						=> $this->input->post("dni"),
					'updatedAmount' 			=> $this->input->post("amount"),
					'updatedAmountCharacters' 	=> $this->input->post("amountCharacters"),
					'areacampaign' 				=> $this->input->post("areaCampaign"),
					'mobileNumber'				=> $this->input->post("mobileNumber"),
					'institutionName' 			=> $this->input->post("institutionName"),
					'updateDate' 				=> NULL,
					'isUpdateApproved' 			=> FALSE,
					'id'			   			=> $this->input->post("id"),
					'loadType'		   			=> $this->input->post("loadType")
				);
				$this->db->insert('teamingupdateforms', $updateForm);

				$timestamp = time() - (3600*3);
				$createEntry = array(
					'date'	=> gmdate("Y-m-d H:i:s",$timestamp),
					'userId'=> $this->input->post("userId"),
					'action'=> "sendUpdate".$this->input->post("loadType"),
					'data'	=> $this->input->post("amount")
				);
				$this->db->insert('teamingaudits',$createEntry);
			}
		}
		else{
			$res->status 	= 'alreadyUpdated';
			$res->message 	= 'El usuario que intenta actualizar el monto ya tiene un formulario pendiente';
		}

		echo json_encode($res);
	}

	public function getUserLastData(){

		$_POST = json_decode($this->input->raw_input_stream);

		$actualuser 	= 	$_POST->updateId;
		$query 			=	new StdClass();
		$res 			=	new StdClass();
		$actualData 	=	new StdClass();
		$res->status 	= 	'invalid';

		$sql 	=
		"SELECT  *
		FROM teamingupdateforms
		INNER JOIN 
			(
			SELECT 
			MAX(teamingupdateforms.updateDate),
			MAX(teamingupdateforms.teamingUpdateFormId) AS maxid
			FROM teamingupdateforms
			WHERE teamingupdateforms.updateDate IS NOT NULL
			AND teamingupdateforms.isUpdateApproved = 1
			AND teamingupdateforms.userId = ?
			GROUP BY teamingupdateforms.userId
		) AS latestUpdate ON latestUpdate.maxid = teamingupdateforms.teamingUpdateFormId";
		$query = $this->db->query($sql,array($actualuser->userId))->row();

		if (count($query) == 0) {
	
			$sql 	=
			"SELECT 
			teamingforms.dni AS agreeDni,
			teamingforms.areaCampaign AS agreeAreaCampaign,
			teamingforms.mobileNumber AS agreeMobileNumber,
			teamingforms.amount AS agreeAmount,
			teamingforms.amountCharacters AS agreeAmountCharacters,
			teamingforms.institutionName AS agreeInstitutionName,
			teamingforms.id AS agreeId
			FROM teamingforms
			WHERE teamingforms.userId = ?";
			$query = $this->db->query($sql,array($actualuser->userId))->row();
	
			$actualData = array(
				"updateDni"					=>	$query->agreeDni,
				"updateAreaCampaign"		=>	$query->agreeAreaCampaign,
				"updateMobileNumber"		=>	$query->agreeMobileNumber,
				"updateInstitutionName"		=>	$query->agreeInstitutionName,
				"updateAmount"				=>	$query->agreeAmount,
				"updateAmountCharacters"	=>	$query->agreeAmountCharacters,
				"updateId"					=>	$query->agreeId
			);
		}
		else{

			$actualData = array(
				"updateDni"					=>	$query->dni,
				"updateAreaCampaign"		=>	$query->areaCampaign,
				"updateMobileNumber"		=>	$query->mobileNumber,
				"updateInstitutionName"		=>	$query->institutionName,
				"updateAmount"				=>	$query->updatedAmount,
				"updateAmountCharacters"	=>	$query->updatedAmountCharacters,
				"updateId"					=>	$query->id
			);
		}
	
		$res->message = array("latestUpdate" => $actualData);
	
		if (count($query) != 0){
			$res->status = 'ok';
		}
	
		echo json_encode($res);
	}

	public function getDivisionsStatus(){
	
		$query 	=	 new StdClass();
		$res 	=	 new StdClass();
		$res->status 	= 'invalid';
	
		$sql 	=	 
		"SELECT sections.sectionId,
		sections.name, 
		(CASE teamingsectionavailable.isAvailable 
		WHEN (NULL) THEN false 
		ELSE teamingsectionavailable.isAvailable 
		END) AS sectionValue
		FROM sections 
		LEFT JOIN teamingsectionavailable ON sections.sectionId = teamingsectionavailable.sectionId 
		ORDER BY sections.sectionId";
		$query = $this->db->query($sql)->result();
		if (count($query)!= 0){
			$res->status 	= 'success';
			foreach ($query as $section) {
				$section->sectionValue = (bool)$section->sectionValue;
			}
		}
	
		$res->message 	= $query;
		echo json_encode($res);
	}

	public function updateDivisionsStatus(){
	
		$data			 = json_decode($this->input->raw_input_stream,false);

		$query			 = new StdClass();
		$sectionToInsert = new StdClass();
		$res 			 = new StdClass();
		$sql 			 = new StdClass();

		$dataNotSet		 = false;
		$res->status 	 = 'invalid';
		
		if(count($data)	> 0)
		{
			foreach ($data as $section)
			{
				if ((isset($section->sectionValue)) && (isset($section->sectionId)))
				{
					$sql = "SELECT * 
					FROM teamingsectionavailable
					where teamingsectionavailable.sectionId = ?";
					$query = $query = $this->db->query($sql,$section->sectionId)->row();
	
					if (count($query) == 0)
					{
						$sectionToInsert = array(
							'sectionId'   => $section->sectionId,
							'isAvailable' => $section->sectionValue
						);
	
						$this->db->insert('teamingsectionavailable', $sectionToInsert);
					}
					else
						$this->db->set("isAvailable",$section->sectionValue)->where("sectionId",$section->sectionId)->update("teamingsectionavailable");
	
					if ($section->sectionValue == FALSE)
						$this->db->where("sectionId",$section->sectionId)->delete('usershowpopupteaming');
				}
				else
				{
					$dataNotSet = true;
				}		
			}	
		}
		
		if($dataNotSet == false){
			$res->status 	= 'success';
		}
		
		echo json_encode($res);
	}

	public function getFormsAdmin(){
	
		$res 			=	 new StdClass();
		$resAgreeForms 	=	 new StdClass();
		$resUpdateForms =	 new StdClass();
		$res->status 	=	 'ok';
	
		$sql 			= 
		"SELECT 
		teamingforms.*,
		userPersonalData.name,
		userPersonalData.lastName,
		u.sectionId,
		u.siteId
		FROM teamingforms 
		INNER JOIN userPersonalData ON teamingforms.userId = userPersonalData.userId
		INNER JOIN users u ON teamingforms.userId = u.userId
		WHERE agreementDate IS NULL 
		AND isApproved = 0";
		$resAgreeForms  = $this->db->query($sql)->result();
	
		$sql 			= 
		"SELECT teamingupdateforms.*,
		userPersonalData.name,
		userPersonalData.lastName,
		u.sectionId,
		u.siteId
		FROM teamingupdateforms 
		INNER JOIN userPersonalData ON teamingupdateforms.userId = userPersonalData.userId 
		INNER JOIN users u ON teamingupdateforms.userId = u.userId
		WHERE updateDate IS NULL 
		AND isUpdateApproved = 0";
		$resUpdateForms = $this->db->query($sql)->result();
	
		$res->message 	= array(
			"agreeForms"	=>	$resAgreeForms,
			"updateForms"	=>	$resUpdateForms
		);
	
		echo json_encode($res);
	}

	public function saveForm(){
	
		$_POST 	=	json_decode($this->input->raw_input_stream,true);
		$res 	= 	new StdClass();
	
		if(count($_POST["agree"]) == 0){
	
			$_POST = $_POST["update"];
			$this->form_validation->set_rules('dni', $this->lang->line('teaming_form_id'), 'required|numeric|min_length[8]|max_length[8]');
			$this->form_validation->set_rules('updatedAmount', $this->lang->line('teaming_form_amount'), 'required|greater_than_equal_to[25]|max_length[14]');
			$this->form_validation->set_rules('updatedAmountCharacters', $this->lang->line('teaming_form_amount_letters'), 'required|callback_alpha_special|max_length[200]');
			$this->form_validation->set_rules('areaCampaign', $this->lang->line('teaming_form_area_campaign'), 'required|max_length[136]|regex_match[/^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ 	,.+]*$/u]');
			$this->form_validation->set_rules('mobileNumber', $this->lang->line('teaming_form_celnumber'), 'required|max_length[20]|callback_cel_number_validation');
			$this->form_validation->set_rules('institutionName', $this->lang->line('teaming_suggest_institution'), 'max_length[150]|regex_match[/^[0-9a-zñáéíóúüA-ZÑÁÉÍÓÚÜ 	,.]*$/u]');
		}
		else{
	
			$_POST = $_POST["agree"];
			$this->form_validation->set_rules('dni', $this->lang->line('teaming_form_id'), 'required|numeric|min_length[8]|max_length[8]');
			$this->form_validation->set_rules('amount', $this->lang->line('teaming_form_amount'), 'required|greater_than_equal_to[25]|max_length[14]');
			$this->form_validation->set_rules('amountCharacters', $this->lang->line('teaming_form_amount_letters'), 'required|callback_alpha_special|max_length[200]');
			$this->form_validation->set_rules('areaCampaign', $this->lang->line('teaming_form_area_campaign'), 'required|max_length[136]|regex_match[/^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ 	,.+]*$/u]');
			$this->form_validation->set_rules('mobileNumber', $this->lang->line('teaming_form_celnumber'), 'required|max_length[20]|callback_cel_number_validation');
			$this->form_validation->set_rules('institutionName', $this->lang->line('teaming_suggest_institution'), 'max_length[150]|regex_match[/^[0-9a-zñáéíóúüA-ZÑÁÉÍÓÚÜ 	,.]*$/u]');
		}
	
		if ($this->form_validation->run() == FALSE){

			$res->status 	= 'invalid';
			$res->message 	= validation_errors();
		}
		else{

			$res->status 	= 'ok';
			$res->message 	= 'validacion correcta';
		}
	
		echo json_encode($res);
	}
	
	public function sendForm(){
	
		$_POST = json_decode($this->input->raw_input_stream,true);
		$validate = $_POST["is_accepted"];

		if(isset($_POST['observation'])){

			$observation = $_POST['observation'];
			$observation = (filter_var($observation,FILTER_SANITIZE_EMAIL));
		}
	
		$res 		= new StdClass();
		$old 		= new StdClass();
		$modified 	= array();
		$userFound	= new StdClass();
		
		if(count($_POST["agree"]) == 0){
	
			$old   = $_POST["oldUpdateform"];
			$_POST = $_POST["update"];
	
			$continue = true;
			$query 	  = 
			"SELECT *
			FROM teamingupdateforms
			WHERE teamingUpdateFormId = ?";
			$userFound = $this->db->query($query,$_POST["teamingUpdateFormId"])->row();
	
			if((count($userFound)==0) && ($validate == false)){

				$continue = false;
			}
			elseif ((count($userFound)!=0) && ($validate == true) && ($userFound->isUpdateApproved == 1)){

				$continue = false;
			}

			if($continue == true){
	
				$this->form_validation->set_rules('dni', $this->lang->line('teaming_form_id'), 'required|numeric|min_length[8]|max_length[8]');
				$this->form_validation->set_rules('updatedAmount', $this->lang->line('teaming_form_amount'), 'required|greater_than_equal_to[25]|max_length[14]');
				$this->form_validation->set_rules('updatedAmountCharacters', $this->lang->line('teaming_form_amount_letters'), 'required|callback_alpha_special|max_length[200]'	);
				$this->form_validation->set_rules('areaCampaign', $this->lang->line('teaming_form_area_campaign'), 'required|max_length[136]|regex_match[/^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ ,.+]*$/u]');
				$this->form_validation->set_rules('mobileNumber', $this->lang->line('teaming_form_celnumber'), 'required|max_length[20]|callback_cel_number_validation');
				$this->form_validation->set_rules('updateDate', $this->lang->line('teaming_form_date'), 'required|callback_date_validator');

				if(isset($_POST['agree']['id'])){

					$this->form_validation->set_rules('id', $this->lang->line('teaming_form_date'), 'callback_cel_number_validation');
				}
	
				if ($this->form_validation->run() == FALSE){
	
					$res->status 	= 'invalid';
					$res->message 	= validation_errors();
				}
				else{
	
					$newdate = join('-',array_reverse(explode('/',$_POST["updateDate"])));
					$res->status 	= 'ok';
					$res->message 	= 'validacion correcta';
	
					if($validate == true){

						$entries  = array();

						foreach ($old as $key => $value) {
	
							$timestamp  	= time() - (3600*3);
							$TranslatedKey  = "";
							
							if ($key != 'userId' && $key != 'loadType'){
	
								switch ($key) {
									
									case "updatedAmount":
									$TranslatedKey = "Monto";
									break;
									case "updatedAmountCharacters":
									$TranslatedKey = "Monto en letras";
									break;
									case "areaCampaign":
									$TranslatedKey = "Área / Campaña";
									break;
									case "mobileNumber":
									$TranslatedKey = "teléfono / celular";
									break;
									case "institutionName":
									$TranslatedKey = "Nombre de Institución";
									break;
									case "dni":
									$TranslatedKey = "Nro documento";
									break;
									case "id":
									$TranslatedKey = "Nro legajo";
									break;
								}
	
								if ($value != $_POST[$key]){

									$adhesionEntry = array(
										'date'	=> gmdate("Y-m-d H:i:s",$timestamp),
										'userId'=> $_POST["userId"],
										'action'=> "update",
										'data'	=> $TranslatedKey."-".$value."-".$_POST[$key]
									);
									$entries[] = $adhesionEntry;
								}
							}	
						}

						$this->db->insert_batch('teamingaudits', $entries);

						$updateData = array(
							'dni' 						=> $_POST["dni"],
							'updatedAmount' 			=> $_POST["updatedAmount"],
							'updatedAmountCharacters'	=> $_POST["updatedAmountCharacters"],
							'areacampaign' 				=> $_POST["areaCampaign"],
							'mobileNumber'				=> $_POST["mobileNumber"],
							'institutionName' 			=> $_POST["institutionName"],
							'updateDate' 				=> $newdate,
							'isUpdateApproved' 			=> true,
							'id'						=> $_POST['id']
						);
	
						$this->db->where('teamingUpdateFormId', $_POST["teamingUpdateFormId"]);
						$this->db->update('teamingupdateforms', $updateData);
						
						$timestamp = time() - (3600*3);
	
						$acceptData = array(
							'date'	=> gmdate("Y-m-d H:i:s",$timestamp),
							'userId'=> $_POST["userId"],
							'action'=> "updateFormAppro",
							'data'	=> $_POST["updatedAmount"]
						);
	
						$this->db->insert('teamingaudits',$acceptData);
	
						if( $_POST["institutionName"]!= ''){
	
							$institutionData = array(
								'name'	  		=> $_POST["institutionName"],
								'address' 		=> '',
								'email' 		=> '',
								'number' 		=> '',
								'referrer' 		=> '',
								'description' 	=> '',
								'userId'		=> $_POST["userId"],
							);
							$res = $this->Teamingaudits->sendSuggestion((object)$institutionData);
						}
						
	
					}
					else{
	
						$sql = 
						"SELECT teamingformId AS RecoveryId
						FROM teamingforms
						WHERE userId = ?";
						$userDischargeId = $this->db->query($sql,array("userId" => $_POST["userId"]))->row();
	
						$updateData = array(
							'dni' => $_POST["dni"],
							'updatedAmount' => $_POST["updatedAmount"],
							'updatedAmountCharacters' => $_POST["updatedAmountCharacters"],
							'areacampaign' => $_POST["areaCampaign"],
							'mobileNumber'=> $_POST["mobileNumber"],
							'institutionName' => $_POST["institutionName"],
							'updateDate' => $newdate,
							'isUpdateApproved' => false,
						);
	
						$dischargeData = array(
							'userId' => $_POST["userId"],
							'joinDate' => $newdate,
							'dischargeDate' => $newdate,
							'dischargeReasonId' => 'r',
							'dischargeAgreementFormId' => $userDischargeId->RecoveryId,
							'dischargeAmount' => $_POST["updatedAmount"],
							'dischargeAmountCharacters' => $_POST["updatedAmountCharacters"],
							'dischargeMobileNumber' => $_POST["mobileNumber"],
							'dischargeAreaCampaign' => $_POST["areaCampaign"],
							'dischargeInstitutionName' => $_POST["institutionName"],
							'dischargeDni' 				=> $_POST["dni"],
							'dischargeCompanyid' 		=> $_POST["id"]
	
						);
	
						$this->db->insert('teamingdischargedupdateforms',$dischargeData);
						$this->db->where('teamingUpdateFormId', $_POST["teamingUpdateFormId"]);
						$this->db->delete('teamingupdateforms');
	
						$timestamp = time() - (3600*3);

						$rejectData = array(
							'date'	=> gmdate("Y-m-d H:i:s",$timestamp),
							'userId'=> $_POST["userId"],
							'action'=> "updateFormRejec",
							'data'	=> $_POST["updatedAmount"]."- r"."-".$observation
						);
						$this->db->insert('teamingaudits',$rejectData);
					}
				}
			}
			else{

				$res->status 	= 'fail';
				$res->message 	= 'El formulario ya fue aceptado por otro usuario.';
			}
		}
	
		else{
			
			$old   = $_POST["oldAgreeform"];
			$_POST = $_POST["agree"];
	
			$continue 	= true;
			$query 		= 
			"SELECT *
			FROM teamingforms
			WHERE teamingformId = ?";
			$userFound = $this->db->query($query,$_POST["teamingformId"])->row();
	
			if((count($userFound)==0)&&($validate == false))
			{
				$continue = false;
			}
			elseif ((count($userFound)!=0)&&($validate == true)&&($userFound->isApproved==1))
			{
				$continue = false;
			}
	
	
			if($continue == true){
				
				$this->form_validation->set_rules('dni', $this->lang->line('teaming_form_id'), 'required|numeric|min_length[8]|max_length[8]');
				$this->form_validation->set_rules('amount', $this->lang->line('teaming_form_amount'), 'required|greater_than_equal_to[25]|max_length[14]');
				$this->form_validation->set_rules('amountCharacters', $this->lang->line('teaming_form_amount_letters'), 'required|callback_alpha_special|max_length[200]');
				$this->form_validation->set_rules('areaCampaign', $this->lang->line('teaming_form_area_campaign'), 'required|max_length[136]|regex_match[/^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ ,.+]*$/u]');
				$this->form_validation->set_rules('mobileNumber', $this->lang->line('teaming_form_celnumber'), 'required|max_length[20]|callback_cel_number_validation');
				$this->form_validation->set_rules('agreementDate', $this->lang->line('teaming_form_date'), 'required|callback_date_validator');

				if(isset($_POST['agree']['id']))
				{
					$this->form_validation->set_rules('id', $this->lang->line('teaming_form_date'), 'callback_cel_number_validation');
				}
	
				if ($this->form_validation->run() == FALSE){
	
					$res->status 	= 'invalid';
					$res->message 	= validation_errors();
				}
				else{
	
					$newdate = join('-',array_reverse(explode('/',$_POST["agreementDate"])));
					$res->status 	= 'ok';
					$res->message 	= 'rechazado correcto';
	
					if($validate == true){

						$entries  		= array();
	
						foreach ($old as $key => $value) {
	
							$timestamp      = time() - (3600*3);
							$TranslatedKey  = "";
	
							if ($key != 'userId' && $key != 'loadType'){
	
								switch ($key) {
									case "amount":
									$TranslatedKey = "Monto";
									break;
									case "amountCharacters":
									$TranslatedKey = "Monto en letras";
									break;
									case "areaCampaign":
									$TranslatedKey = "Área / Campaña";
									break;
									case "mobileNumber":
									$TranslatedKey = "teléfono / celular";
									break;
									case "institutionName":
									$TranslatedKey = "Nombre de Institución";
									break;
									case "dni":
									$TranslatedKey = "Nro documento";
									break;
									case "id":
									$TranslatedKey = "Nro legajo";
									break;
								}
	
								if ($value != $_POST[$key]){

									$adhesionEntry = array(
										'date'	=> gmdate("Y-m-d H:i:s",$timestamp),
										'userId'=> $_POST["userId"],
										'action'=> "adhesionDataUpd",
										'data'	=> $TranslatedKey."-".$value."-".$_POST[$key]
									);
								}
								else{

									$adhesionEntry = array(
										'date'	=> gmdate("Y-m-d H:i:s",$timestamp),
										'userId'=> $_POST["userId"],
										'action'=> "adhesionData",
										'data'	=> $TranslatedKey."-".$value
									);
								}
								array_push($entries, $adhesionEntry);
							}	
						}

						$this->db->insert_batch('teamingaudits', $entries);
	
						$agreementData = array(
							'dni' 			  => $_POST["dni"],
							'amount' 		  => $_POST["amount"],
							'amountCharacters'=> $_POST["amountCharacters"],
							'areacampaign' 	  => $_POST["areaCampaign"],
							'mobileNumber'	  => $_POST["mobileNumber"],
							'institutionName' => $_POST["institutionName"],
							'agreementDate'   => $newdate,
							'id'			  => $_POST['id'],
							'isApproved' 	  => true,
						);

						$this->db->where('teamingformId', $_POST["teamingformId"]);
						$this->db->update('teamingforms',$agreementData);
	
						$timestamp = time() - (3600*3);
	
						$acceptData = array(
							'date'	=> gmdate("Y-m-d H:i:s",$timestamp),
							'userId'=> $_POST["userId"],
							'action'=> "agreeFormApprove",
							'data'	=> $_POST["amount"]
						);
						$this->db->insert('teamingaudits',$acceptData);	
	
						if ($_POST["institutionName"]!= ''){
	
							$institutionData = array(
								'name'			=> $_POST["institutionName"],
								'address' 		=> '',
								'email' 		=> '',
								'number' 		=> '',
								'referrer' 		=> '',
								'description' 	=> '',
								'userId'		=> $_POST["userId"],
							);
							$res = $this->Teamingaudits->sendSuggestion((object)$institutionData);
						}
					}
					else{

						$sql = 	
						"SELECT teamingformId AS RecoveryId
						FROM teamingforms
						WHERE userId = ?";
						$userDischargeId = $this->db->query($sql,array("userId" => $_POST["userId"]))->row();
	
						$agreementData = array(
							'dni' => $_POST["dni"],
							'amount' => $_POST["amount"],
							'amountCharacters' => $_POST["amountCharacters"],
							'areacampaign' => $_POST["areaCampaign"],
							'mobileNumber'=> $_POST["mobileNumber"],
							'institutionName' => $_POST["institutionName"],
							'agreementDate' => $newdate,
							'isApproved' => false,
						);

						$dischargeData = array(
							'userId' 					=> $_POST["userId"],
							'joinDate' 					=> $newdate,
							'dischargeDate' 			=> $newdate,
							'dischargeReasonId' 		=> 'r',
							'dischargeAgreementFormId' 	=> $userDischargeId->RecoveryId,
							'dischargeAmount' 			=> $_POST["amount"],
							'dischargeAmountCharacters' => $_POST["amountCharacters"],
							'dischargeMobileNumber' 	=> $_POST["mobileNumber"],
							'dischargeAreaCampaign' 	=> $_POST["areaCampaign"],
							'dischargeInstitutionName' 	=> $_POST["institutionName"],
							'dischargeDni' 				=> $_POST["dni"],
							'dischargeCompanyid' 		=> $_POST["id"],
							'observation'				=> $observation
						);
	
						$this->db->insert('teamingdischargedforms',$dischargeData);
						$this->db->where('teamingformId', $_POST["teamingformId"]);
						$this->db->delete('teamingforms');
	
						$timestamp = time() - (3600*3);
						$rejectData = array(
							'date'	=> gmdate("Y-m-d H:i:s",$timestamp),
							'userId'=> $_POST["userId"],
							'action'=> "agreeFormReject",
							'data'	=> $_POST["amount"]."- r"."-".$observation
						);
						$this->db->insert('teamingaudits',$rejectData);
					}
				}
			}
			else{

				$res->status  = 'fail';
				$res->message = 'El formulario ya fue rechazado por otro usuario.';
			}
		}

		echo json_encode($res);
	}

public function getApprovedForms(){

	$res 			= new StdClass();
	$res->status 	= 'ok';
	$resAgreeForms 	= new StdClass();
	$sql 			=
	"SELECT
	teamingforms.*,
	CONCAT_WS(' ',userPersonalData.name,userPersonalData.lastName) AS userName,
	teamingforms.amount AS lastAmount,
	DATE_FORMAT(teamingforms.agreementDate,\"%d/%m/%Y\") AS lastDate,
	DATE_FORMAT(teamingforms.agreementDate,\"%d/%m/%Y\") AS agreementDate,
	DATE_FORMAT(lastUpdateRows.updateDate,\"%d/%m/%Y\") AS updateDate,
	lastUpdateRows.updatedAmountCharacters,
	lastUpdateRows.institutionName AS lastInstitution,
	lastUpdateRows.mobileNumber AS lastmobileNumber,
	lastUpdateRows.id AS lastId,
	lastUpdateRows.areaCampaign AS lastCampaign,
	teamingforms.amountCharacters AS lastAmountCharacters,
	lastUpdateRows.updatedAmount,
	userPersonalData.name,
	userPersonalData.lastName,
	userPersonalData.civilState,
	userPersonalData.gender,
	DATE_FORMAT(userPersonalData.birthDate,\"%d/%m/%Y\") AS birthDate,
	userPersonalData.email,
	sites.siteId,
	sections.sectionId,
	sites.name AS site,
	userComplementaryData.turn AS turn,
	'true' as joinedUser 
	FROM teamingforms
	LEFT JOIN(
		SELECT *
		FROM teamingupdateforms
		INNER JOIN
		(
			SELECT MAX(teamingupdateforms.updateDate),
			MAX(teamingupdateforms.teamingUpdateFormId) AS maxid
			FROM teamingupdateforms
			WHERE teamingupdateforms.updateDate IS NOT NULL AND teamingupdateforms.isUpdateApproved = 1
			GROUP BY teamingupdateforms.userId
		) AS lastUpdate ON lastUpdate.maxid = teamingupdateforms.teamingUpdateFormId
	) AS lastUpdateRows ON lastUpdateRows.userId = teamingforms.userId
	INNER JOIN userPersonalData ON teamingforms.userId = userPersonalData.userId
	INNER JOIN users ON teamingforms.userId = users.userId
	INNER JOIN sites ON users.siteId = sites.siteId
	INNER JOIN sections ON users.sectionId = sections.sectionId
	INNER JOIN userComplementaryData ON teamingforms.userId = userComplementaryData.userId
	WHERE teamingforms.agreementDate IS NOT NULL AND teamingforms.isApproved = 1";
	$resAgreeForms = $this->db->query($sql)->result();

	$sql 	=
	"SELECT
	teamingdischargedforms.*,
	DATE_FORMAT(teamingdischargedforms.joinDate,\"%d/%m/%Y\") AS joinDate,
	lastDischargesUser.dischargeAmount AS updatedDischargeAmount,
	lastDischargesUser.dischargeAmountCharacters AS updatedDischargeAmountCharacters,
	lastDischargesUser.dischargeMobileNumber AS updatedDischargeMobileNumber,
	lastDischargesUser.dischargeAreaCampaign AS updatedDischargeAreaCampaign,
	lastDischargesUser.dischargeInstitutionName AS updatedDischargeInstitutionName,
	lastDischargesUser.dischargeCompanyId AS updatedDischargeCompanyId,
	lastDischargesUser.dischargeDni AS updatedDischargeDni,
	dischargereasons.reason,
	CONCAT_WS(' ',userPersonalData.name,userPersonalData.lastName) AS userName,
	userPersonalData.civilState,
	userPersonalData.gender,
	DATE_FORMAT(userPersonalData.birthDate,\"%d/%m/%Y\") AS birthDate,
	userPersonalData.email,
	sites.name AS site,
	sites.siteId,
	userComplementaryData.turn AS turn,
	'false' AS joinedUser 
	FROM teamingdischargedforms
	INNER JOIN(
		SELECT MAX(teamingdischargedforms.dischargeId) AS maxid,
		MAX(teamingdischargedforms.dischargeDate)
		FROM teamingdischargedforms
		GROUP BY teamingdischargedforms.userId
		) AS latestDischargesPerUser ON latestDischargesPerUser.maxid = teamingdischargedforms.dischargeId
	LEFT JOIN(
		SELECT *
		FROM teamingdischargedupdateforms
		INNER JOIN(
			SELECT MAX(teamingdischargedupdateforms.dischargeId) AS maxUpdateid
			FROM teamingdischargedupdateforms
			GROUP BY teamingdischargedupdateforms.userId
		) AS dischargeMaxUpdate ON dischargeMaxUpdate.maxUpdateid = teamingdischargedupdateforms.dischargeId
	)AS lastDischargesUser ON lastDischargesUser.userId = teamingdischargedforms.userId
	INNER JOIN dischargereasons ON dischargereasons.dischargeReasonId = teamingdischargedforms.dischargeReasonId
	INNER JOIN userPersonalData ON teamingdischargedforms.userId = userPersonalData.userId
	INNER JOIN users ON teamingdischargedforms.userId = users.userId
	INNER JOIN sites ON users.siteId = sites.siteId
	INNER JOIN userComplementaryData ON teamingdischargedforms.userId = userComplementaryData.userId
	WHERE teamingdischargedforms.userId NOT IN (
		SELECT teamingforms.userId
		FROM teamingforms
	)";
	$dischargedUsers = $this->db->query($sql)->result();

	foreach ($dischargedUsers as $dischargedUser) {

		if (count($dischargedUser->updatedDischargeAmount)!= 0){

			$dischargedUser->DischargeAmount 			= $dischargedUser->updatedDischargeAmount;
			$dischargedUser->dischargeAmountCharacters 	= $dischargedUser->updatedDischargeAmountCharacters;
			$dischargedUser->dischargeMobileNumber 		= $dischargedUser->updatedDischargeMobileNumber;
			$dischargedUser->dischargeAreaCampaign 		= $dischargedUser->updatedDischargeAreaCampaign;
			$dischargedUser->dischargeInstitutionName 	= $dischargedUser->updatedDischargeInstitutionName;
			$dischargedUser->dischargeCompanyId 		= $dischargedUser->updatedDischargeCompanyId;
			$dischargedUser->dischargeDni 				= $dischargedUser->updatedDischargeDni;
		}

		array_push($resAgreeForms, array(

			"userId" 			=> $dischargedUser->userId,
			"userName" 			=> $dischargedUser->userName,
			"dni" 				=> $dischargedUser->dischargeDni,
			"id" 				=> $dischargedUser->dischargeCompanyId,
			"amount" 			=> $dischargedUser->dischargeAmount,
			"areaCampaign" 		=> $dischargedUser->dischargeAreaCampaign,
			"mobileNumber" 		=> $dischargedUser->dischargeMobileNumber,
			"institutionName" 	=> $dischargedUser->dischargeInstitutionName,
			"agreementDate" 	=> $dischargedUser->joinDate,
			"email" 			=> $dischargedUser->email,
			"birthDate" 		=> $dischargedUser->birthDate,
			"gender" 			=> $dischargedUser->gender,
			"civilState" 		=> $dischargedUser->civilState,
			"site" 				=> $dischargedUser->site,
			"siteId" 			=> $dischargedUser->siteId,
			"turn"	 			=> $dischargedUser->turn,
			"joinedUser"		=> false
		));
	}

	$sql	=
	"SELECT
	DISTINCT LOWER(userPersonalData.gender) AS value
	FROM userPersonalData
	WHERE LENGTH(userPersonalData.gender) > 0
	ORDER BY userPersonalData.gender";
	$genders = $this->db->query($sql)->result();

	for ($i=0; $i < sizeof($genders) ; $i++) {

		switch ($genders[$i]->value) {
			case "f":
			$genders[$i]->name = "Femenino";
			break;
			case "m":
			$genders[$i]->name = "Masculino";
			break;
			case "o":
			$genders[$i]->name = "Otro";
			break;
		}
	}

	$sql 	=
	"SELECT
	DISTINCT LOWER(userPersonalData.civilState) AS value
	FROM userPersonalData
	WHERE LENGTH(userPersonalData.civilState) > 0
	ORDER BY userPersonalData.civilState";
	$civilStates = $this->db->query($sql)->result();

	for ($i=0; $i < sizeof($civilStates) ; $i++) {

		switch ($civilStates[$i]->value) {
			case "c":
			$civilStates[$i]->name = "Casado/a";
			break;
			case "d":
			$civilStates[$i]->name = "Divorciado/a";
			break;
			case "o":
			$civilStates[$i]->name = "Concubino/a";
			break;
			case "s":
			$civilStates[$i]->name = "Soltero/a";
			break;
			case "v":
			$civilStates[$i]->name = "Viudo/a";
			break;
		}
	}

	$sql =
	"SELECT
	DISTINCT LOWER(turns.name) AS name, LOWER(SUBSTRING(turns.name,1,1)) AS value
	FROM turns
	WHERE LENGTH(turns.name) > 0
	ORDER BY turns.name";
	$turns = $this->db->query($sql)->result();

	array_push($turns, array("name" => "noche","value" => "n"));
	array_push($turns, array("name" => "Full time","value" => "f"));

	$sql =
	"SELECT
	DISTINCT LOWER(sites.name) AS name, sites.name AS value
	FROM sites
	WHERE LENGTH(sites.name) > 0
	ORDER BY sites.name";
	$sites   = $this->db->query($sql)->result();

	$sql =
	"SELECT
	DISTINCT sites.siteId, 
	CASE sites.siteId
    	WHEN 2 THEN 'CAT Technologies Argentina S. A.' 
		WHEN 3 THEN 'CAT Technologies Argentina S. A.' 
		WHEN 4 THEN 'ADVAL S.A' 
		WHEN 6 THEN 'CAT Technologies Argentina S. A.' 
		WHEN 7 THEN 'CAT TECHNOLOGIES CUSTOMER EXPERIENCES S. A.'
    	ELSE NULL
	END AS name 
	FROM sites";
	$socialReasonsFilter   = $this->db->query($sql)->result();

	$socialReasons   	   = array();
	array_push($socialReasons, array('value'=>'CAT Technologies Argentina S. A.','name'=>'CAT Technologies Argentina S. A.'));
	array_push($socialReasons, array('value'=>'ADVAL S.A','name'=>'ADVAL S.A'));
	array_push($socialReasons, array('value'=>'CAT TECHNOLOGIES CUSTOMER EXPERIENCES S. A.','name'=>'CAT TECHNOLOGIES CUSTOMER EXPERIENCES S. A.'));

	$filters = array();

	array_push($filters, array("column"=>0,"show"=>false,"name"=>"Nombre y Apellido","type"=>"text","pressed" => true));
	array_push($filters, array("column"=>1,"show"=>false,"name"=>"DNI","type"=>"num","limit"=>"8","pressed" => true));
	array_push($filters, array("column"=>2,"show"=>false,"name"=>"Legajo","type"=>"num","limit"=>"6","pressed" => true));
	array_push($filters, array("column"=>3,"show"=>false,"name"=>"Monto a donar","type"=>"amount","limit"=>"4","pressed" => true));
	array_push($filters, array("column"=>4,"show"=>false,"name"=>"Área / Campaña","type"=>"text","pressed" => true));
	array_push($filters, array("column"=>5,"show"=>false,"name"=>"Teléfono / Celular","type"=>"num","limit"=>"20","pressed" => true));
	array_push($filters, array("column"=>6,"show"=>false,"name"=>"Institución","type"=>"text","pressed" => true));
	array_push($filters, array("column"=>7,"show"=>false,"name"=>"Fecha de adhesión","type"=>"date","pressed" => true));
	array_push($filters, array("column"=>8,"show"=>false,"name"=>"Correo Electrónico","type"=>"text","pressed" => false));
	array_push($filters, array("column"=>9,"show"=>false,"name"=>"Fecha de nacimiento","type"=>"date","pressed" => false));
	array_push($filters, array("column"=>10,"show"=>false,"name"=>"Género","type"=>"select","filterValues" => $genders,"pressed" => false));
	array_push($filters, array("column"=>11,"show"=>false,"name"=>"Estado civil","type"=>"select","filterValues" => $civilStates,"pressed" => false));
	array_push($filters, array("column"=>12,"show"=>false,"name"=>"Site","type"=>"select","filterValues" => $sites,"pressed" => false));
	array_push($filters, array("column"=>13,"show"=>false,"name"=>"Turno","type"=>"select","filterValues" => $turns,"pressed" => false));
	array_push($filters, array("column"=>14,"show"=>false,"name"=>"Razón social","type"=>"select","filterValues" => $socialReasons,"pressed" => false));

	$res->message 	= array(
		"agreeForms" 	=> $resAgreeForms,
		"filters" 	 	=> $filters,
		"socialReasons" => $socialReasonsFilter
	);

	echo json_encode($res);
}

public function dischargeTeamingUser(){

	$_POST 				= json_decode($this->input->raw_input_stream,true);
	$res 				= new StdClass();
	$res->status 		= 'invalid';
	$userDischargeId	= new StdClass();
	$userUpdates 		= new StdClass();

	$sql =
	"SELECT teamingformId AS RecoveryId
	FROM teamingforms
	WHERE userId = ?";
	$userDischargeId = $this->db->query($sql,array("userId" => $_POST["userId"]))->row();

	if(count($userDischargeId)!=0){

		$res->status = 'ok';
		$observation = $_POST['observation'];
		$observation = (filter_var($observation,FILTER_SANITIZE_EMAIL));

		$dischargeData 	= array(
			'userId' 					=> $_POST["userId"],
			'joinDate'			 		=> $_POST["agreementDate"],
			'dischargeDate' 			=> $_POST["todayDate"],
			'dischargeReasonId' 		=> $_POST["rejectReason"],
			'dischargeAgreementFormId' 	=> $userDischargeId->RecoveryId,
			'dischargeAmount' 			=> $_POST["amount"],
			'dischargeAmountCharacters' => $_POST["amountCharacters"],
			'dischargeMobileNumber' 	=> $_POST["mobileNumber"],
			'dischargeAreaCampaign' 	=> $_POST["areaCampaign"],
			'dischargeInstitutionName' 	=> $_POST["institutionName"],
			'dischargeDni' 				=> $_POST["dni"],
			'dischargeCompanyid' 		=> $_POST["id"],
			'observation'               => $observation 
		);

		$this->db->insert('teamingdischargedforms',$dischargeData);

		$this->db->where('userId', $_POST["userId"]);
		$return = $this->db->delete('teamingforms');

		$sql =
		"SELECT teamingupdateforms.updateDate,
		teamingupdateforms.updatedAmount,
		teamingupdateforms.updatedAmountCharacters,
		teamingupdateforms.mobileNumber,
		teamingupdateforms.areaCampaign,
		teamingupdateforms.institutionName,
		teamingupdateforms.id,
		teamingupdateforms.dni
		FROM teamingupdateforms
		WHERE userId = ?";
		$userUpdates = $this->db->query($sql,$_POST["userId"])->result();

		foreach ($userUpdates as $value) {

			$dischargeData 	= array(
				'userId' => $_POST["userId"],
				'joinDate' => $value->updateDate,
				'dischargeDate' => $_POST["todayDate"],
				'dischargeReasonId' => $_POST["rejectReason"],
				'dischargeAgreementFormId' => $userDischargeId->RecoveryId,
				'dischargeAmount' => $value->updatedAmount,
				'dischargeAmountCharacters' => $value->updatedAmountCharacters,
				'dischargeMobileNumber' => $value->mobileNumber,
				'dischargeAreaCampaign' => $value->areaCampaign,
				'dischargeInstitutionName' => $value->institutionName,
				'dischargeDni' => $value->dni,
				'dischargeCompanyid' => $value->id,
			);

			$this->db->insert('teamingdischargedupdateforms',$dischargeData);
		}

		$sql =
		"DELETE
		FROM teamingupdateforms
		WHERE userId = ?";
		$userUpdates = $this->db->query($sql,$_POST["userId"]);

		if ($_POST["lastAmount"]){
			$_POST["amount"] = $_POST["lastAmount"];
		};

		$timestamp = time() - (3600*3);
		$rejectData = array(
			'date'	=> gmdate("Y-m-d H:i:s",$timestamp),
			'userId'=> $_POST["userId"],
			'action'=> "dischargedFromT",
			'data'	=> $_POST["amount"]."-".$_POST["rejectReason"].'-'.$observation
		);
		$this->db->insert('teamingaudits',$rejectData);
	}

	echo json_encode($res);
}

public function getRemainingUsers(){

	$res = new StdClass();
	$res->status 	= 'invalid';
	$sql 		= 
	"SELECT 
	userPersonalData.userId,
	userPersonalData.gender,
	userPersonalData.dni,
	userPersonalData.email,
	CONCAT_WS(' ', userPersonalData.name,userPersonalData.lastName) AS userName,
	userPersonalData.civilState,
	DATE_FORMAT(userPersonalData.birthDate,\"%d/%m/%Y\") AS birthDate,
	userComplementaryData.turn AS turn,
	sites.name AS site,
	sites.siteId
	FROM userPersonalData 
	INNER JOIN users ON userPersonalData.userId = users.userId 
	INNER JOIN sites ON users.siteId = sites.siteId
	INNER JOIN userComplementaryData ON userPersonalData.userId = userComplementaryData.userId
	WHERE userPersonalData.userId NOT IN (SELECT userId from teamingforms)
	AND users.active = 1";
	$result = $this->db->query($sql)->result();

	$sql	=
	"SELECT
	DISTINCT LOWER(userPersonalData.gender) AS value
	FROM userPersonalData
	WHERE LENGTH(userPersonalData.gender) > 0
	ORDER BY userPersonalData.gender";
	$genders = $this->db->query($sql)->result();

	for ($i=0; $i < sizeof($genders) ; $i++) {

		switch ($genders[$i]->value) {
			case "f":
			$genders[$i]->name = "Femenino";
			break;
			case "m":
			$genders[$i]->name = "Masculino";
			break;
			case "o":
			$genders[$i]->name = "Otro";
			break;
		}
	}

	$sql 	=
	"SELECT
	DISTINCT LOWER(userPersonalData.civilState) AS value
	FROM userPersonalData
	WHERE LENGTH(userPersonalData.civilState) > 0
	ORDER BY userPersonalData.civilState";
	$civilStates = $this->db->query($sql)->result();

	for ($i=0; $i < sizeof($civilStates) ; $i++) {

		switch ($civilStates[$i]->value) {
			case "c":
			$civilStates[$i]->name = "Casado/a";
			break;
			case "d":
			$civilStates[$i]->name = "Divorciado/a";
			break;
			case "o":
			$civilStates[$i]->name = "Concubino/a";
			break;
			case "s":
			$civilStates[$i]->name = "Soltero/a";
			break;
			case "v":
			$civilStates[$i]->name = "Viudo/a";
			break;
		}
	}

	$sql =
	"SELECT
	DISTINCT LOWER(turns.name) AS name, LOWER(SUBSTRING(turns.name,1,1)) AS value
	FROM turns
	WHERE LENGTH(turns.name) > 0
	ORDER BY turns.name";
	$turns = $this->db->query($sql)->result();

	array_push($turns, array("name" => "noche","value" => "n"));
	array_push($turns, array("name" => "Full time","value" => "f"));

	$sql =
	"SELECT
	DISTINCT LOWER(sites.name) AS name, sites.name AS value
	FROM sites
	WHERE LENGTH(sites.name) > 0
	ORDER BY sites.name";
	$sites = $this->db->query($sql)->result();

	$sql =
	"SELECT
	DISTINCT sites.siteId, 
	CASE sites.siteId
    	WHEN 2 THEN 'CAT Technologies Argentina S. A.' 
		WHEN 3 THEN 'CAT Technologies Argentina S. A.' 
		WHEN 4 THEN 'ADVAL S.A' 
		WHEN 6 THEN 'CAT Technologies Argentina S. A.' 
		WHEN 7 THEN 'CAT TECHNOLOGIES CUSTOMER EXPERIENCES S. A.'
    	ELSE NULL
	END AS name 
	FROM sites";
	$socialReasonsFilter   = $this->db->query($sql)->result();

	$socialReasons   	   = array();
	array_push($socialReasons, array('value'=>'CAT Technologies Argentina S. A.','name'=>'CAT Technologies Argentina S. A.'));
	array_push($socialReasons, array('value'=>'ADVAL S.A','name'=>'ADVAL S.A'));
	array_push($socialReasons, array('value'=>'CAT TECHNOLOGIES CUSTOMER EXPERIENCES S. A.','name'=>'CAT TECHNOLOGIES CUSTOMER EXPERIENCES S. A.'));

	$filters	= array();
	array_push($filters, array("column"=>0,"show"=>false,"name"=>"Nombre y Apellido","type"=>"text","pressed" => true));
	array_push($filters, array("column"=>1,"show"=>false,"name"=>"DNI","type"=>"num","limit"=>"8","pressed" => true));
	array_push($filters, array("column"=>2,"show"=>false,"name"=>"Correo Electrónico","type"=>"text","pressed" => true));
	array_push($filters, array("column"=>3,"show"=>false,"name"=>"Fecha de nacimiento","type"=>"date","pressed" => true));
	array_push($filters, array("column"=>4,"show"=>false,"name"=>"Género","type"=>"select","filterValues" => $genders,"pressed" => true));
	array_push($filters, array("column"=>5,"show"=>false,"name"=>"Estado civil","type"=>"select","filterValues" => $civilStates,"pressed" => true));
	array_push($filters, array("column"=>6,"show"=>false,"name"=>"Site","type"=>"select","filterValues" => $sites,"pressed" => true));
	array_push($filters, array("column"=>7,"show"=>false,"name"=>"Turno","type"=>"select","filterValues" => $turns,"pressed" => true));
	array_push($filters, array("column"=>8,"show"=>false,"name"=>"Razón social","type"=>"select","filterValues" => $socialReasons,"pressed" => false));

	$res->status  = 'ok';
	$res->message = array(
		"remainingUsers" => $result,
		"filters" 		 => $filters,
		"socialReasons"  => $socialReasonsFilter
	);
	echo json_encode($res);
}

public function getTeamingUser(){

	$roleId = $this->session->Role;
	$res = new StdClass();

	$sql = 
	"SELECT 
	userPersonalData.name,
	userPersonalData.lastName,
	userPersonalData.userId  
	FROM userPersonalData
	INNER JOIN users ON userPersonalData.userId = users.userId
	INNER JOIN roles ON users.roleId = roles.roleId 
	WHERE roles.name like 'teaming'";
	$result = $this->db->query($sql)->row();

	$res->status = 'ok';
	$res->message = array("teaming" => $result);
	echo json_encode($res);
}

public function SearchUsers(){

	$recieve = new StdClass();
	$recieve = $this->db->escape_like_str($this->input->post('like'));
	$res = new StdClass();
	$users= new StdClass();

	$sql = "SELECT concat_ws(' ', up.name, up.lastName) AS completeName, u.userId, u.userName FROM ((SELECT userName, roleId, userId FROM users) AS u INNER JOIN (SELECT name, lastName,userId FROM userPersonalData) AS up ON u.userId = up.userId) WHERE (concat_ws(' ', name, lastName) LIKE '%$recieve%' || userName LIKE '%$recieve%') && u.userId != ? && u.userId NOT IN (SELECT userId from teamingforms) ORDER BY completeName LIMIT 20";
	$users = $this->db->query($sql, array($this->session->UserId))->result();

	$res->status = 'ok';
	$res->message = array("usersAlike"	=>	$users);
	echo json_encode($res);
}

public function SearchUsersUpdate(){

	$recieve = new StdClass();
	$recieve = $this->db->escape_like_str($this->input->post('like'));
	$res = new StdClass();
	$users= new StdClass();

	$sql = "SELECT concat_ws(' ', up.name, up.lastName) AS completeName, 
	u.userId, 
	u.userName FROM(
	(SELECT userName, roleId, userId FROM users) AS u 
	INNER JOIN 
	(SELECT name, lastName,userId FROM userPersonalData) AS up ON u.userId = up.userId) 
	WHERE (concat_ws(' ', name, lastName) LIKE '%$recieve%' || userName LIKE '%$recieve%') 
	AND u.userId IN (SELECT userId from teamingforms WHERE agreementDate IS NOT NULL AND isApproved = 1) 
	AND u.userId NOT IN (SELECT userId from teamingupdateforms WHERE updateDate IS NULL AND isUpdateApproved = 0)
	ORDER BY completeName LIMIT 20";
	$users = $this->db->query($sql, array($this->session->UserId))->result();

	$res->status = 'ok';
	$res->message = array("usersAlike"	=>	$users);
	echo json_encode($res);
}

public function getDni(){

	$res = new StdClass();
	$dni = new StdClass();
	$_POST = json_decode($this->input->raw_input_stream,true);

	$sql = "SELECT userPersonalData.dni FROM userPersonalData WHERE userPersonalData.userId = ?";
	$dni = $this->db->query($sql, $_POST["userId"])->row();

	$res->status = 'ok';
	$res->message = $dni;
	echo json_encode($res);
}

public function getUserId(){

	$res = new StdClass();
	$res->status = 'ok';
	$res->message = $this->session->UserId;
	echo json_encode($res);
}

public function getNameAndDni(){

	$res = new StdClass();
	$_POST = json_decode($this->input->raw_input_stream,true);

	$sql = "SELECT userPersonalData.dni,userPersonalData.name,userPersonalData.lastName FROM userPersonalData WHERE userPersonalData.userId = ?";
	$personalData = $this->db->query($sql, $this->session->UserId)->row();
	$personalData->userId = $this->session->UserId;

	$res->status = 'ok';
	$res->message = $personalData;
	echo json_encode($res);
}


public function getUserStatistics(){

	$_POST = json_decode($this->input->raw_input_stream,true);

	/*format of dates*/
	$startDate = new DateTime($_POST["startDate"]);
	$endDate = new DateTime($_POST["endDate"]);
	$startDate = $startDate->format('Y-m-d');
	$endDate = $endDate->format('Y-m-d');

	$res = new StdClass();
	$res->status = 'ok';
	$res->message = array();
	/*count of total users of teaming*/
	$query =
	"SELECT COUNT(tm.userId) AS totalUsers
	FROM teamingforms tm
	WHERE tm.agreementDate IS NOT NULL AND tm.isApproved = 1";
	$totalUsers = $this->db->query($query)->row();

	/*users charged today*/
	$query =
	"SELECT COUNT(tm.userId) AS usersPerLoadType
	FROM teamingforms tm
	WHERE tm.agreementDate IS NOT NULL
	AND tm.isApproved = 1
	AND tm.agreementDate BETWEEN ? AND ?
	AND tm.loadType= ?";
	$usersInDateRangeOnline = $this->db->query($query,array('startDate' => $startDate,'endDate' => $endDate,'loadType' => 'Online'))->row();
	$usersInDateRangeManual = $this->db->query($query,array('startDate' => $startDate,'endDate' => $endDate,'loadType' => 'Manual'))->row();

	/*users dischaged between date ingresed*/
	$query =
	"SELECT COUNT(discharges.dischargeId) AS dischargesPerReasonId
	FROM teamingdischargedforms discharges
	WHERE discharges.dischargeDate BETWEEN ? AND ?
	AND discharges.dischargeReasonId = ?
	GROUP BY discharges.dischargeReasonId
	ORDER BY discharges.dischargeReasonId";
	/*every list is the users discharged with the reason*/
	$dischargesF = $this->db->query($query,array('startDate' => $startDate,'endDate' => $endDate,"dischargeId" =>'f'))->row();
	$dischargesR = $this->db->query($query,array('startDate' => $startDate,'endDate' => $endDate,"dischargeId" =>'r'))->row();
	$dischargesD = $this->db->query($query,array('startDate' => $startDate,'endDate' => $endDate,"dischargeId" =>'d'))->row();

	/*user's amounts*/
	$query=
	"SELECT MAX(amount) AS max_amount
	FROM teamingforms
	WHERE agreementDate BETWEEN ? AND ?
	AND isApproved = 1";
	$maxAmount = $this->db->query($query,array('startDate' => $startDate, 'endDate' => $endDate))->row();

	$query=
	"SELECT MAX(updatedAmount) AS max_amount_updated
	FROM teamingupdateforms
	WHERE isUpdateApproved = 1
	AND updateDate BETWEEN ? AND ?";
	$maxAmoutUpdated = $this->db->query($query,array('startDate' => $startDate , 'endDate' => $endDate))->row();

	$query =
	"SELECT teamingforms.userId,amount,updatedAmount,agreementDate,updateDate from teamingforms
	LEFT JOIN teamingupdateforms
	ON teamingforms.userId = teamingupdateforms.userId
	WHERE agreementDate BETWEEN ? AND ? OR updateDate BETWEEN ? AND ?";
	$amounts = $this->db->query($query,array('startDate' => $startDate,'endDate' => $endDate,'startDate1' => $startDate,'endDate1' => $endDate))->result_array();

	/*users updated on the dates ingresed*/
	$query =
	"SELECT COUNT(tmupdate.userId) AS updatesPerLoadType
	FROM teamingupdateforms tmupdate
	WHERE tmupdate.updateDate BETWEEN ? AND ?
	AND tmupdate.isUpdateApproved = 1
	GROUP BY tmupdate.loadType";
	$updatesInDateRange = $this->db->query($query,array('startDate' => $startDate,'endDate' => $endDate))->result();

	$res->message = array(
		"users" 	  		 => $totalUsers->totalUsers,
		"joinedInDateRange"  => array("online" => $usersInDateRangeOnline->usersPerLoadType,"manual" => $usersInDateRangeManual->usersPerLoadType),
		"amounts" 			 => array('maxAmount' => $maxAmount, 'maxAmountUpdated' => $maxAmoutUpdated, 'amounts' => $amounts),
		"discharges" 		 => array("f" => $dischargesF,"r" => $dischargesR,"d" => $dischargesD),
		"updatesInDateRange" => $updatesInDateRange,
	);
	echo json_encode($res);
}

public function getNewsletterStatistics(){

	$_POST = json_decode($this->input->raw_input_stream,true);

	//format of dates
	$startDate 	= new DateTime($_POST["startDate"]);
	$endDate 	= new DateTime($_POST["endDate"]);
	$startDate 	= $startDate->format('Y-m-d');
	$endDate 	= $endDate->format('Y-m-d');

	$res 			= new StdClass();
	$res->status 	= 'ok';
	$res->message 	= array();
	$teamingUser 	= new StdClass();

	$totalNewsletters 		= new StdClass();
	$newslettersNotJoined 	= new StdClass();
	$newslettersJoined 		= new StdClass();
	$totalReceivers 		= new StdClass();
	$roleId = $this->session->Role;

	$query = 
	"SELECT userPersonalData.userId 
	FROM userPersonalData
	INNER JOIN users ON userPersonalData.userId = users.userId
	INNER JOIN roles ON users.roleId = roles.roleId 
	WHERE roles.roleId = $roleId";

	$teamingUser = $this->db->query($query)->row();

	$query = 	"SELECT COUNT(*) AS totalNews
	FROM newsletters
	WHERE newsletters.userId = ?";
	$totalNewsletters = $this->db->query($query,$teamingUser->userId)->row();

	//all newsletters
	$query =
	"SELECT newsletterId, newsletter, userId, newslettercategoryId, title, from_unixtime(timestamp,'%d/%m/%Y')as creation_date, file
	FROM newsletters
	WHERE userId = ?
	AND from_unixtime(timestamp,'%Y-%m-%d') BETWEEN ? AND ?";
			//array of newsletters
	$NewsLetters = $this->db->query($query,array($teamingUser->userId,$startDate,$endDate))->result();

	if($totalNewsletters->totalNews == 0){
		$res->status = 'invalid';
	}

	$res->message = array(
		"resOne" => $totalNewsletters,
		"resTwo" => $NewsLetters,
		"resThird" => $Recipients
	);
	echo json_encode($res);
}

public function getSurveyStatistics(){

	$res 			= new StdClass();
	$res->status 	= 'ok';
	$res->message 	= array();
	$roleId 		= $this->session->Role;

	$query = 	
	"SELECT userPersonalData.userId 
	FROM userPersonalData
	INNER JOIN users ON userPersonalData.userId = users.userId
	INNER JOIN roles ON users.roleId = roles.roleId 
	WHERE roles.roleId = $roleId";
	$teamingUser = $this->db->query($query)->row();

	$query =
	"SELECT surveys.surveyId,surveys.name
	FROM surveys
	WHERE surveys.userId = ?
	AND active = 'true'";
	$surveysteaming = $this->db->query($query,$teamingUser->userId)->result();

	$res->message = array(
		"resOne" => $surveysteaming,
	);
	echo json_encode($res);
}

public function getUserDataAndHistory($userId){

	$userId 			= $userId;
	$roleId = $this->session->roleId;

	$sql 	= 
	"SELECT userPersonalData.userId 
	FROM userPersonalData
	INNER JOIN users ON userPersonalData.userId = users.userId
	INNER JOIN roles ON users.roleId = roles.roleId 
	WHERE roles.roleId = ?";
	$admins	= $this->db->query($sql,$roleId)->result();
	$last = array_pop($admins);
	$adminList = $last->userId; 

	foreach ($admins as $admin) {
		$adminList = $adminList." , ".$admin->userId;
	}

	$sql =
	"SELECT
	userPersonalData.name,
	userPersonalData.lastName,
	userPersonalData.civilState,
	userPersonalData.gender,
	DATE_FORMAT(userPersonalData.birthDate,\"%d/%m/%Y\") AS birthDate,
	userPersonalData.email,
	sites.name AS site,
	userComplementaryData.turn AS turn
	FROM userPersonalData
	INNER JOIN users ON userPersonalData.userId = users.userId
	INNER JOIN sites ON users.siteId = sites.siteId
	INNER JOIN userComplementaryData ON userPersonalData.userId = userComplementaryData.userId
	WHERE userPersonalData.userId = ?";
	$userPersonalData = $this->db->query($sql,$userId)->row();

	switch ($userPersonalData->gender) {
		case "f":
		$userPersonalData->gender = "Femenino";
		break;
		case "m":
		$userPersonalData->gender = "Masculino";
		break;
		case "o":
		$userPersonalData->gender = "Otro";
		break;
	}

	switch ($userPersonalData->civilState) {
		case "c":
		$userPersonalData->civilState = "Casado/a";
		break;
		case "d":
		$userPersonalData->civilState = "Divorciado/a";
		break;
		case "o":
		$userPersonalData->civilState = "Concubino/a";
		break;
		case "s":
		$userPersonalData->civilState = "Soltero/a";
		break;
		case "v":
		$userPersonalData->civilState = "Viudo/a";
		break;
	}

	switch ($userPersonalData->turn) {
		case "m":
		$userPersonalData->turn = "Mañana";
		break;
		case "t":
		$userPersonalData->turn = "Tarde";
		break;
		case "n":
		$userPersonalData->turn = "Noche";
		break;
		case "f":
		$userPersonalData->turn = "Full time";
		break;
	}

	$sql =
	"SELECT
	teamingforms.*,
	DATE_FORMAT(teamingforms.agreementDate,\"%d/%m/%Y\") AS teamingDate
	FROM teamingforms
	WHERE teamingforms.agreementDate IS NOT NULL 
	AND teamingforms.isApproved = 1
	AND teamingforms.userId = ?";
	$teamingData = $this->db->query($sql,$userId)->row();

	if (count($teamingData) != 0){

		$sql 			=
		"SELECT
		teamingupdateforms.*,
		DATE_FORMAT(teamingupdateforms.updateDate,\"%d/%m/%Y\") AS teamingUpdateDate
		FROM teamingupdateforms
		WHERE teamingupdateforms.updateDate IS NOT NULL 
		AND teamingupdateforms.isUpdateApproved = 1
		AND teamingupdateforms.userId = ?
		ORDER BY teamingupdateforms.updateDate DESC ,teamingupdateforms.teamingUpdateFormId DESC
		LIMIT 1";
		$teamingUpdateData = $this->db->query($sql,$userId)->row();
	}

	else{

		$sql =
		"SELECT
		teamingdischargedforms.*,
		DATE_FORMAT(teamingdischargedforms.joinDate,\"%d/%m/%Y\") AS teamingDate
		FROM teamingdischargedforms
		WHERE teamingdischargedforms.userId = ?
		ORDER BY teamingdischargedforms.dischargeDate DESC,teamingdischargedforms.dischargeId DESC
		LIMIT 1";
		$teamingDischargedData = $this->db->query($sql,$userId)->row();

		$sql =
		"SELECT
		teamingdischargedupdateforms.*,
		DATE_FORMAT(teamingdischargedupdateforms.joinDate,\"%d/%m/%Y\") AS teamingDate
		FROM teamingdischargedupdateforms
		WHERE teamingdischargedupdateforms.userId = ?
		ORDER BY teamingdischargedupdateforms.dischargeDate DESC ,teamingdischargedupdateforms.dischargeId DESC
		LIMIT 1";
		$teamingDischargedUpdateData = $this->db->query($sql,$userId)->row();
	}

	$teamingLastData = new StdClass();

	if (count($teamingData)	!=	0){

		$teamingLastData = $teamingData;
		$teamingLastData->agreementDate	= $teamingData->teamingDate;
		$teamingLastData->isJoined 		= true;

		if (count($teamingUpdateData)	!=	0){

			$teamingLastData->dni 				=	$teamingUpdateData->dni;
			$teamingLastData->amount 			=	$teamingUpdateData->updatedAmount;
			$teamingLastData->amountCharacters 	=	$teamingUpdateData->updatedAmountCharacters;
			$teamingLastData->areaCampaign 		=	$teamingUpdateData->areaCampaign;
			$teamingLastData->mobileNumber 		=	$teamingUpdateData->mobileNumber;
			$teamingLastData->id 				=	$teamingUpdateData->id;
		}
	}

	elseif (count($teamingDischargedData)	!=	0) {

		$teamingLastData->userId 					=	$teamingDischargedData->userId;
		$teamingLastData->teamingDate				=	$teamingDischargedData->teamingDate;
		$teamingLastData->dni 						=	$teamingDischargedData->dischargeDni;
		$teamingLastData->amount 					=	$teamingDischargedData->dischargeAmount;
		$teamingLastData->amountCharacters 			=	$teamingDischargedData->dischargeAmountCharacters;
		$teamingLastData->areaCampaign 				=	$teamingDischargedData->dischargeAreaCampaign;
		$teamingLastData->mobileNumber 				=	$teamingDischargedData->dischargeMobileNumber;
		$teamingLastData->agreementDate 			=	$teamingDischargedData->teamingDate;
		$teamingLastData->id 						=	$teamingDischargedData->dischargeCompanyId;

		if (count($teamingDischargedUpdateData)	!=	0){

			$teamingLastData->dni 						=	$teamingDischargedUpdateData->dischargeDni;
			$teamingLastData->amount 					=	$teamingDischargedUpdateData->dischargeAmount;
			$teamingLastData->amountCharacters 			=	$teamingDischargedUpdateData->dischargeAmountCharacters;
			$teamingLastData->areaCampaign 				=	$teamingDischargedUpdateData->dischargeAreaCampaign;
			$teamingLastData->mobileNumber 				=	$teamingDischargedUpdateData->dischargeMobileNumber;
			$teamingLastData->agreementDate 			=	$teamingDischargedUpdateData->teamingDate;
			$teamingLastData->id 						=	$teamingDischargedUpdateData->dischargeCompanyId;
		}
		$teamingLastData->isJoined = false;
	} 

	$sql 			=
	"SELECT userOrderedHistory.*,
	DATE_FORMAT(userOrderedHistory.date,\"%d/%m/%Y %H:%i:%s\") AS date
	FROM(
	(SELECT * 
	FROM teamingaudits 
	WHERE userId = ? 
	ORDER BY teamingaudits.date ASC)
	UNION
	(SELECT * 
	FROM teamingaudits 
	WHERE userId IN (?) 
	HAVING LOCATE(?,teamingaudits.data) != 0)
	) AS userOrderedHistory
	ORDER BY userOrderedHistory.teamingauditId";
	$userTeamingHistory = $this->db->query($sql,array("userIdHistory"	=>	$userId,"teamingAdmins"	=>	$adminList,"idToLocateInComm"	=>	$userId))->result();

	$initialData 		= array();
	$updatesToData 		= array();
	$userPageHistory 	= array();

	foreach ($userTeamingHistory as $key => $historyEntry){

		switch ($historyEntry->action) {

			case 'admin_newslette':
			$communicationId = explode('-', $historyEntry->data);
			$sql 			 =
			"SELECT newsletters.title AS title
			FROM newsletters
			WHERE newsletterId = ?";
			$newsletterTitle = $this->db->query($sql,trim($communicationId[0]))->row();
			array_push($userPageHistory, array("date"	=>	$historyEntry->date,"action"	=>	"Se envió al usuario una novedad con título: ".$newsletterTitle->title));
			break;

			case 'admin_surveys':
			$communicationId = explode('-', $historyEntry->data);
			$sql 			 =
			"SELECT surveys.name AS name
			FROM surveys
			WHERE surveyId = ?";
			$surveyName = $this->db->query($sql,trim($communicationId[0]))->row();
			array_push($userPageHistory, array("date"	=>	$historyEntry->date,"action"	=>	"Se envió al usuario una encuesta con nombre: ".$surveyName->name));
			break;

			case 'sendmanual';
			case 'sendonline';
			array_push($userPageHistory, array("date"	=>	$historyEntry->date,"action"	=>	"El usuario envió un formulario de adhesión con monto ".$historyEntry->data.". Fue cargado de forma ". substr($historyEntry->action, 4, 6)."."));
			break;

			case 'adhesionData':
			$data = explode("-",$historyEntry->data);
			array_push($initialData, $data[0].": ".$data[1]);
			break;

			case 'adhesionDataUpd':
			$data = explode("-",$historyEntry->data);
			array_push($initialData, $data[0].": se modificó de ".$data[1]." a ".$data[2]);
			break;

			case 'agreeFormApprov':
			array_push($userPageHistory, array("date"	=>	$historyEntry->date,"action"	=>	"Se aprobó el formulario de adhesión con monto ".$historyEntry->data,"obtainedData" => $initialData));
			$initialData = 	array();
			break;

			case 'agreeFormReject':
			$rejectData = explode("-", $historyEntry->data);
			if(trim($rejectData[1])	==	'f'){
				$reason = "porque el usuario ya no pertenece a la empresa.";
			}
			else{
				$reason = $rejectData[2];
			}
			array_push($userPageHistory, array("date"	=>	$historyEntry->date,"action"	=>	"Se rechazó el formulario de adhesión con monto ".trim($rejectData[0]).$reason,"obtainedData" => $initialData));
			$initialData 				= 	array();
			break;

			case 'sendUpdatemanua';
			case 'sendUpdateonlin';
			if (substr($historyEntry->action, 10, 6) == 'manua'){
				$loadtype = "manual";
			}
			else{
				$loadtype = "online";
			}
			array_push($userPageHistory, array("date"	=>	$historyEntry->date,"action"	=>	"El usuario envió un formulario de actualización con monto ".$historyEntry->data.". Fue cargado de forma ".$loadtype."."));
			break;

			case 'update':
			$data = explode("-",$historyEntry->data);
			array_push($updatesToData, $data[0].": se modificó de ".$data[1]." a ".$data[2]);
			break;

			case 'updateFormAppro':
			array_push($userPageHistory, array("date"	=>	$historyEntry->date,"action"	=>	"Se aprobó el formulario de actualización con monto ".$historyEntry->data,"obtainedData" => $updatesToData));
			$updatesToData 			= 	array();
			break;

			case 'updateFormRejec':
			$rejectData = explode("-", $historyEntry->data);
			$reason = $rejectData[2];
			array_push($userPageHistory, array("date"	=>	$historyEntry->date,"action"	=>	"Se rechazó el formulario de actualización con monto ".trim($rejectData[0]).$reason));
			$updatesToData 			= 	array();
			break;

			case 'suggest':
			array_push($userPageHistory, array("date"	=>	$historyEntry->date,"action"	=>	"Sugirió una institución con los siguientes datos: ".$historyEntry->data));
			break;

			case 'dischargedFromT':
			$rejectData = explode("-", $historyEntry->data);
			if(trim($rejectData[1])	==	'f'){
				$reason = "el usuario ya no pertenece a la empresa.";
			}
			else{
				$reason = $rejectData[2];
			}
			array_push($userPageHistory, array("date"	=>	$historyEntry->date,"action"	=>	"Se rechazó el formulario de adhesión con monto ".trim($rejectData[0]).". Razón: ".$reason));
			$initialData 				= 	array();
			break;

			default:
			break;
		}
	}

	$userHistoryData = array(
		"teamingData" 		=> $teamingLastData,
		"personalData" 		=> $userPersonalData,
		"teamingHistory" 	=> $userPageHistory
	);
	return $userHistoryData;
}

public function dataToView(){
	$userId = json_decode($this->input->raw_input_stream,true);
	$res = new StdClass();
	$res->status 	= 'ok';
	$res->message 	= $this->getUserDataAndHistory($userId);
	echo json_encode($res);
}

public function institutionsPdf()
{
	$institutions = $this->uri->segment_array();
	$selected 	  = '';

	for ($i=1; $i <= sizeof($institutions); $i++) {

		if (intval($institutions[$i])!=0){

			$selected = $selected.$institutions[$i].',' ;
		}	
	}

	$selected    = substr($selected, 0, -1);
	$res  		 = new StdClass();
	$res->status = 'invalid';

	$this->load->model('Teamingaudits_model','Teamingaudits');
	$res->data   = $this->Teamingaudits->getSuggestions($selected);

	if (count($res->data) > 0){

		$res->status = 'ok';
	}

	$pdf 		= new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

	// remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);

	$pdf->SetAuthor('CAT Technologies');
	$pdf->SetDisplayMode('real', 'default');
	$pdf->SetTitle("Instituciones Sugeridas");
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// add a page
	

	// set JPEG quality
	$pdf->setJPEGQuality(80);

	// Add custom font
	$segoe 			= TCPDF_FONTS::addTTFfont($_SERVER['DOCUMENT_ROOT']."/".APPFOLDERADD.'/libraries/fonts/seguisym_1.ttf', 'TrueType','', 96);

	// get current vertical position
	
	if ($res->status == 'ok') {
		
		foreach ($res->data as $institution)
		{
			$pdf->AddPage();
			$y = $pdf->getY();

			// Images
			$pdf->Image("/".APPFOLDERADD."/libraries/images/logo_teaming_2.jpg",'10', '10',"46", "15", '', '', $align='L');
			$pdf->Image("/".APPFOLDERADD."/libraries/images/logo_2.jpg",'150', '10',"40", "15" , '', '', $align='R');
			$y = $pdf->getY();
			$pdf->writeHTML('', true, false, true, false, $align='C');
			$pdf->writeHTML('', true, false, true, false, $align='C');
			$pdf->writeHTML('', true, false, true, false, $align='C');
			$pdf->writeHTML('', true, false, true, false, $align='C');
			$pdf->SetFont($segoe, '', 18, '', false);

			$html = '<h1>'.$institution->name.'</h1>';
			$html = ($institution->description)? $html.'<div>'.$this->lang->line('administration_items_description').': '.$institution->description.'</div>' : $html;
			$pdf->writeHTML($html, true, false, true, false, $align='C');
			$pdf->writeHTML('', true, false, true, false, $align='C');
			$pdf->SetFont($segoe, '', 16, '', false);
			$pdf->writeHTML('<h3>'.$this->lang->line('teaming_suggestion_date').': '.$institution->date.'</h3>', true, false, true, false, $align='C');
			$pdf->writeHTML('', true, false, true, false, $align='C');
			$pdf->writeHTML('', true, false, true, false, $align='C');

			$y = $pdf->getY();
			// write the first column
			$pdf->writeHTMLCell(80, '', '', $y, '<h3>'.$this->lang->line('teaming_suggest_contact_data').'</h3>', 0, 0, 0, true, 'C', true);
			// write the second column
			$pdf->writeHTMLCell(100, '', '', '','<h3>'.$this->lang->line('teaming_suggest_adm_suggested_by').'</h3>', 0, 1, 0, true, 'C', true);

			$pdf->SetFont($segoe, '', 12, '', false);

			//write first column data
			$left_column = '<div>';
			$left_column = ($institution->province && sizeof($institution->province) > 0)? $left_column."<div><span>".$this->lang->line('administration_users_state').": </span>".$institution->province."</div>" : $left_column;
			$left_column = ($institution->locality && sizeof($institution->locality) > 0)? $left_column."<div><span>".$this->lang->line('administration_users_city').": </span>".$institution->locality."</div>" : $left_column;
			$left_column = ($institution->address && sizeof($institution->address) > 0)? $left_column."<div><span>".$this->lang->line('administration_users_address').": </span>".$institution->address."</div>" : $left_column;
			$left_column = ($institution->number && sizeof($institution->number) > 0)? $left_column."<div>".$this->lang->line('teaming_form_celnumber').": ".$institution->number."</div>" : $left_column;
			$left_column = ($institution->email && sizeof($institution->email) > 0)? $left_column."<div>".$this->lang->line('teaming_suggest_contact_data_social_network_web_page').": ".$institution->email."</div>" : $left_column;
			$left_column = ($institution->referrer && sizeof($institution->referrer) > 0)? $left_column."<div>".$this->lang->line('teaming_suggest_contact_data_referrer').": ".$institution->referrer."</div>" : $left_column;
			$left_column = $left_column.'</div>';
			switch($institution->turn){

				case 'm': $institution->turn = 'mañana';
				break;
				case 'n': $institution->turn = 'noche';
				break;
				case 't': $institution->turn = 'tarde';
				break;
				default:break;
			}

			//write second column data		
			$right_column = "<div><div><span>Nombre y apellido: </span>".$institution->suggestedFrom."</div><div><span>División: </span>".$institution->division."</div><div><span>Turno: </span>".$institution->turn."</div></div>";

			$y = $pdf->getY();

			// write the first column
			$pdf->writeHTMLCell(80, '', '', $y, $left_column, 0, 0, 0, true, 'C', true);
			// write the second column
			$pdf->writeHTMLCell(100, '', '', '',$right_column, 0, 1, 0, true, 'C', true);			
		}
	}
	else{

		$pdf->AddPage();
		$pdf->writeHTML('No hay instituciones sugeridas', true, false, false, false, $align='C');
	}

	$pdf->Output("InstitucionesSugeridas.pdf", 'D');
}

public function generatePdf(){

	$userId 		= $this->uri->segment(3);
	$dataAndHistory = $this->getUserDataAndHistory($userId);
	$personalData	= $dataAndHistory["personalData"];
	$teamingData	= $dataAndHistory["teamingData"];
	$teamingHistory	= $dataAndHistory["teamingHistory"];

	// create new PDF document
	$pdf 	= new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

	// remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);

	// set document information
	$pdf->SetAuthor('CAT Technologies');
	$pdf->SetDisplayMode('real', 'default');
	$pdf->SetTitle("Datos e historial");
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// add a page
	$pdf->AddPage();

	// set JPEG quality
	$pdf->setJPEGQuality(75);

	// Images
	$pdf->Image("/".APPFOLDERADD."/libraries/images/logo_teaming_2.jpg",'10', '10',"46", "15", '', '', $align='L');
	$pdf->Image("/".APPFOLDERADD."/libraries/images/logo_2.jpg",'150', '10',"40", "15" , '', '', $align='R');

	// Add custom font
	$segoe 			= TCPDF_FONTS::addTTFfont($_SERVER['DOCUMENT_ROOT']."/".APPFOLDERADD.'/libraries/fonts/seguisym_1.ttf', 'TrueType','', 96);
	$segoeNegrita 	= TCPDF_FONTS::addTTFfont($_SERVER['DOCUMENT_ROOT']."/".APPFOLDERADD.'/libraries/fonts/seguisym_1.ttf', 'TrueType','', 96);


	$pdf->SetFont($segoe, '', 32, '', false);
	$pdf->writeHTML('<h2>Detalles del usuario</h2>', true, false, false, false, $align='C');
		// get current vertical position
	$y = $pdf->getY();

	// write the first column
	$pdf->writeHTMLCell(80, 10, '', $y, '', 0, 0, 0, true, 'C', true);
	// write the second column
	$pdf->writeHTMLCell(100, 10, '', '','', 0, 1, 0, true, 'C', true);

	$y = $pdf->getY();
	// set color for background
	$pdf->SetFillColor(255, 255, 225);

	$pdf->SetFont($segoe, '', 14, '', false);
	// write the first column
	$pdf->writeHTMLCell(80, '', '', $y, '<h3>Datos de la iniciativa</h3>', 0, 0, 0, true, 'C', true);
	// write the second column
	$pdf->writeHTMLCell(100, '', '', '','<h3>Datos personales</h3>', 0, 1, 0, true, 'C', true);

	$pdf->SetFont($segoe, '', 10, '', false);

	$y = $pdf->getY();

	//write first column data
	$left_column="<div><div><span>Nombre y Apellido </span>".$personalData->name." ".$personalData->lastName."</div><div><span>DNI </span>".$teamingData->dni."</div><div><span>Legajo </span>".$teamingData->id."</div><div><span>Área / Campaña </span>".$teamingData->areaCampaign."</div><div><span>Teléfono / Celular </span>".$teamingData->mobileNumber."</div><div><span>Monto </span>".$teamingData->amount." (".$teamingData->amountCharacters.")</div><div><span>Fecha de adhesión </span>".$teamingData->teamingDate."</div></div>";

		//write second column data		
	$right_column = "<div><div><span>Género </span>".$personalData->gender."</div><div><span>Fecha de nacimiento </span>".$personalData->birthDate."</div><div><span>Correo Electrónico </span>".$personalData->email."</div><div><span>Estado civil </span>".$personalData->civilState."</div><div><span>Site </span>".$personalData->site."</div><div><span>Turno </span>".$personalData->turn."</div></div>";

	// write the first column
	$pdf->writeHTMLCell(80, '', '', $y, $left_column, 0, 0, 0, true, 'C', true);
		// write the second column
	$pdf->writeHTMLCell(100, '', '', '',$right_column, 0, 1, 0, true, 'C', true);

	$y = $pdf->getY();
	// write the first column
	$pdf->writeHTMLCell(80, 20, '', $y, '', 0, 0, 0, true, 'C', true);
	// write the second column
	$pdf->writeHTMLCell(100, 20, '', '','', 0, 1, 0, true, 'C', true);

	$pdf->SetFont($segoe, '', 32, '', false);
	$pdf->writeHTML('<h2>Historial del usuario</h2>', true, false, false, false, $align='C');

	$y = $pdf->getY();
	$pdf->writeHTMLCell(80, 10, '', $y, '', 0, 0, 0, true, 'C', true);
	// write the second column
	$pdf->writeHTMLCell(100, 10, '', '','', 0, 1, 0, true, 'C', true);

	$pdf->SetFont($segoe, '', 14, '', false);

	foreach ($teamingHistory as $entry) {

		$y = $pdf->getY();
			// write the first column
		$pdf->writeHTMLCell(40, 10, '', $y, '<span style="font-size:14px;">'.$entry["date"].'</span>', 0, 0, 0, true, 'L', true);
			// write the second column
		$pdf->writeHTMLCell(140, 10, '', '','<span style="font-size:14px;">'.$entry["action"].'</span>', 0, 1, 0, true, 'L', true);

		if (array_key_exists("obtainedData",$entry)){

			$y = $pdf->getY();
			$pdf->SetFont($segoe, '', 14, '', false);
			$pdf->writeHTMLCell(40, 10, '', $y, '', 0, 0, 0, true, 'L', true);
				// write the second column
			$pdf->writeHTMLCell(140, 10, '', '','<span style="font-size:14px;">Los datos ingresados fueron:</span>', 0, 1, 0, true, 'L', true);

			for ($i=0; $i < count($entry["obtainedData"]); $i++) { 
				$y = $pdf->getY();
				$pdf->writeHTMLCell(40, 10, '', $y, '', 0, 0, 0, true, 'L', true);
					// write the second column
				$pdf->writeHTMLCell(140, 10, '', '','<span style="font-size:14px;">'.$entry["obtainedData"][$i].'</span>', 0, 1, 0, true, 'L', true);
			}
		}
	}

	$pdf->Output($personalData->name." ".$personalData->lastName.".pdf", 'D');
}

public function getUserHistory($userId)
{
	$query =
	"SELECT agreementDate as 'Fecha de adhesión',name as 'Nombre',lastname as 'Apellido',teamingforms.dni,email as 'Mail',amount as 'Monto',amountCharacters as 'Monto en letras'
	FROM teamingforms
	JOIN userPersonalData
	ON teamingforms.userId = userPersonalData.userId
	WHERE isApproved = 1
	AND teamingforms.userId = ?";
	$agreementData = $this->db->query($query,$userId)->result();

	$query = 
	"SELECT joinDate as 'Fecha de adhesión',name as 'Nombre',lastname as 'Apellido',userPersonalData.dni,email as 'Mail',dischargeAmount as 'Monto',dischargeAmountCharacters as 'Monto en letras'
	FROM teamingdischargedforms
	JOIN userPersonalData
	ON teamingdischargedforms.userId = userPersonalData.userId
	WHERE teamingdischargedforms.userId = ?  
	ORDER BY `Fecha de adhesión` DESC LIMIT 1";
	$lastDischargedData = $this->db->query($query,$userId)->result();

	$query = 
	"SELECT updateDate as 'Fecha de actualización',name as 'Nombre',lastname as 'Apellido',userPersonalData.dni,email as 'Mail',updatedAmount as 'Monto actualizado',updatedAmountCharacters as 'Monto en letras'
	FROM teamingupdateforms 
	JOIN userPersonalData 
	ON teamingupdateforms.userId = userPersonalData.userId 
	WHERE isUpdateApproved = 1 
	AND teamingupdateforms.userId = ?";
	$updateData = $this->db->query($query,$userId)->result();

	$query =
	"SELECT `dischargeDate` as 'Fecha de baja de actualización',name as 'Nombre',lastname as 'Apellido',userPersonalData.dni,email as 'Mail',`dischargeAmount` as 'Monto actualizado dado de baja',`dischargeAmountCharacters` as 'Monto en letras'
	FROM teamingdischargedupdateforms 
	JOIN userPersonalData 
	ON teamingdischargedupdateforms.userId = userPersonalData.userId 
	WHERE teamingdischargedupdateforms.userId = ?";
	$lastDischargedUpdatedData = $this->db->query($query,$userId)->result();

	$query = 
	"SELECT dischargeDate as 'Fecha de baja',name as 'Nombre',lastname as 'Apellido',userPersonalData.dni,email as 'Mail',dischargeReasonId as 'Razón de baja',dischargeAmount as 'Monto',dischargeAmountCharacters as 'Monto en letras',observation as 'Observación' 
	FROM teamingdischargedforms 
	JOIN userPersonalData 
	ON teamingdischargedforms.userId = userPersonalData.userId 
	WHERE teamingdischargedforms.userId = ?";
	$dischargeData = $this->db->query($query,$userId)->result();

	if(!$agreementData)
	{
		$agreementData = $lastDischargedData;
	}

	$userTotalData = array(
		"agreementData" => $agreementData,
		"updateData"	=> $updateData,
		"lastDischargedUpdatedData" => $lastDischargedUpdatedData,
		"dischargeData"	=> $dischargeData
	);
	return $userTotalData;
}

public function generateExcel(){ 

	$userId     = $this->uri->segment(3); 
	$userHistory = $this->getUserHistory($userId); 
	$this->export_excel->to_excel($userHistory,'Historial de usuario'); 
} 

public function getAppFolderAdd(){

	$res = new StdClass();
	$res->status = 'ok';
	$res->message = APPFOLDERADD;
	echo json_encode($res);
}

public function getsign()
{
	$_POST =json_decode($this->input->raw_input_stream,true);
	
	$this->form_validation->set_rules('sectionId', '', 'required|numeric|greater_than[0]');
	$this->form_validation->set_rules('siteId', '', 'required|numeric|greater_than[0]');
	$res = new StdClass();

	if($this->form_validation->run() == TRUE)
	{
		$res->status  = 'ok';
		$res->title   = '<span class="bold">'.$this->lang->line('teaming_sign_title_section_'.$this->input->post('siteId', TRUE)).'</span>';
		$res->content = '<span class="bold">'.$this->lang->line('teaming_sign_title_section_'.$this->input->post('siteId', TRUE)).'</span><br>'.$this->lang->line('teaming_sign_body_site_'.$this->input->post('siteId', TRUE));
	}
	else
	{
		$res->status = 'error';
		$res->content = $this->lang->line('general_error');
	}

	echo json_encode($res);

}
public function suggestionDetails($value='')
{
	$this->load->view('teaming/suggestionDetails', array('header' => $this->load->view('teaming/_teaming_header')));
}



function institutionValidation($data)
{

	$res  		  = new StdClass();
	$res->status  = 'invalid';
	$res->message = array();

	$this->form_validation->set_data($data);

	$this->form_validation->set_rules('name', $this->lang->line('teaming_suggest_name'), 'required|max_length[150]|regex_match[/^[0-9a-zñáéíóúüA-ZÑÁÉÍÓÚÜ ,.]*$/u]');
	$this->form_validation->set_rules('chosenState', $this->lang->line('administration_users_state'), 'required');
	$this->form_validation->set_rules('chosenCity', $this->lang->line('administration_users_city'), 'required');
	

	if (strlen($data["address"]) >0){

		$this->form_validation->set_rules('address', $this->lang->line('administration_users_address'), 'max_length[150]'); 
	}

	if (strlen($data["email"]) >0){

		$this->form_validation->set_rules('email', $this->lang->line('teaming_suggest_contact_data_social_network_web_page'), 'max_length[100]'); 
	}

	if (strlen($data["number"]) >0){

		$this->form_validation->set_rules('number', $this->lang->line('teaming_form_celnumber'), 'callback_cel_number_validation|max_length[30]'); 
	}

	if (strlen($data["referrer"]) >0){

		$this->form_validation->set_rules('referrer', $this->lang->line('teaming_suggest_contact_data_referrer'), 'callback_alpha_special|max_length[150]'); 
	}

	if (strlen($data["description"]) >0){

		$this->form_validation->set_rules('description', $this->lang->line('administration_items_description'), 'max_length[150]'); 
	}

	if(($this->form_validation->run() == FALSE)){

		$res->message 	= validation_errors();
	}

	else{

		$res->status = 'ok';
		$res->message = '';
	}

	return $res;
}

public function sendUserSuggestion(){

	$data = json_decode($this->input->raw_input_stream,true);
	$res  = new StdClass();

	$res  = $this->institutionValidation($data);

	if ($res->status == 'ok'){
		$res = $this->Teamingaudits->sendSuggestion((object)$data);
	}

	echo json_encode($res);
}

public function getInstitutions(){
	
	$res  			= new StdClass();
	$res->status 	= 'invalid';
	$res->data  	= $this->Teamingaudits->getSuggestions(null);

	if(isset($res->data)){

		$res->status = 'ok';
	}
	else{

		$res->status = 'empty';
	}

	echo json_encode($res);
}

public function getInstitution(){

	$suggestionId 	= json_decode($this->input->raw_input_stream,true);

	$res  			= new StdClass();
	$res->status 	= 'invalid';
	$res->message  	= $this->Teamingaudits->getSuggestions($suggestionId);

	if(isset($res->message)){

		$res->status = 'ok';
	}
	else{

		$res->status  = 'invalid';
		$res->message = 'No se encontro la institución';
	}

	echo json_encode($res);
}

public function getLocations(){

	$res 	= new StdClass();

	$sql 	= "SELECT state as stateName,stateId FROM states WHERE stateId IN (1, 2, 19) ORDER BY state";
	$states = $this->db->query($sql)->result();

	$sql 	= "SELECT stateId,city as cityName,cityId FROM cities WHERE stateId IN (1, 2, 19) ORDER BY city";
	$cities = $this->db->query($sql)->result();

	$res->status = 'invalid';
	$res->states = $states;
	$res->cities = $cities;

	if (isset($states)&&isset($cities)){

		$res->status = 'ok';
	}

	echo json_encode($res);
}

public function updateInstitution()
{
	$res = new StdClass();
	$updateStatus = '';

	$updatedInstitution = json_decode($this->input->raw_input_stream,true);

	$res  = $this->institutionValidation($updatedInstitution);

	if ($res->status == 'ok'){

		$res = $this->Teamingaudits->updateSuggestion((object)$updatedInstitution);
	}

	echo json_encode($res);
}

	public function exportInstitutions()
	{
		if($this->Identity->Validate('teaming/reports'))
		{
			$post = $_POST;
			$date = date('d-m-y');
			$fileName = "Datos_de_las_instituciones"."_".$date.".xls";
			$fileName = (filter_var($fileName,FILTER_SANITIZE_EMAIL));

			header("Content-type: application/vnd.ms-excel; name='excel'");
			header("Content-Disposition: attachment; filename = $fileName");
			header("Pragma: no-cache");
			header("Expires: 0");
			
			//construccion de tabla
			
			$columns = explode(",",$post['columns']);
			$cColumns = count($columns);

			$institutionsId = $post['selectedInstitutions'];
			$institutions = $this->Teamingaudits->getSuggestions($institutionsId);
			$cInstitutions = count($institutions);
			
			$tableToExport = '<table><tr>';

			for ($i=1; $i < $cColumns; $i++) {
				$text = ucwords($columns[$i]);
				// $text = (filter_var($text,FILTER_SANITIZE_EMAIL));
				$tableToExport.= '<th style="border:1px #888;color: #FFF;background-color:rgb(66,166,42);">'.$text.'</th>';
			}
			$tableToExport.= '</tr>';

			for ($i=0; $i < $cInstitutions; $i++) { 
				
				$inst = $institutions[$i];
				
				$tableToExport.= '<tr>';
				
				$tableToExport.= '<td style="border:1px #888 solid;color:#555;">'.$inst->suggestedFrom.'</td>';
				$tableToExport.= '<td style="border:1px #888 solid;color:#555;">'.$inst->division.'</td>';
				$tableToExport.= '<td style="border:1px #888 solid;color:#555;">'.$inst->turn.'</td>';
				$tableToExport.= '<td style="border:1px #888 solid;color:#555;">'.$inst->name.'</td>';
				$tableToExport.= '<td style="border:1px #888 solid;color:#555;">'.$inst->province.'</td>';
				$tableToExport.= '<td style="border:1px #888 solid;color:#555;">'.$inst->locality.'</td>';
				$tableToExport.= '<td style="border:1px #888 solid;color:#555;">'.$inst->address.'</td>';
				$tableToExport.= '<td style="border:1px #888 solid;color:#555;">'.$inst->email.'</td>';
				$tableToExport.= '<td style="border:1px #888 solid;color:#555;">'.$inst->number.'</td>';
				$tableToExport.= '<td style="border:1px #888 solid;color:#555;">'.$inst->referrer.'</td>';
				$tableToExport.= '<td style="border:1px #888 solid;color:#555;">'.$inst->description.'</td>';
				$tableToExport.= '<td style="border:1px #888 solid;color:#555;">'.$inst->date.'</td>';
				
				$tableToExport.= '</tr>';
			}
			$tableToExport.= '</table>';
			echo $tableToExport;
		}
	}

	public function acceptSuggestion()
	{
		$suggestion  = json_decode($this->input->raw_input_stream,true);

		$res 		 = new StdClass();
		$res->status = 'invalid';

		if (isset($suggestion)){

			$updated = $this->Teamingaudits->acceptSuggestion($suggestion);

			if (isset($updated)){

				$res->status = 'ok';
				$res->message = $updated;
			}
		}

		echo json_encode($res);
	}
}
