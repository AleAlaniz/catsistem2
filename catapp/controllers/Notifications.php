<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Notification_model','Notifications');
        $this->load->model('Event_model','Events');
        $this->lang->load('notifications');
        $this->load->library('form_validation');
    }

    public function index()
    {
        if ($this->Identity->Validate('notifications/manage'))
		{
			$this->load->view('notifications/notifications');
		}
		else
		{
			show_404();	
		}
    }

    public function getnotifications()
    {
        if ($this->Identity->Validate('notifications/manage'))
        {
            $res = new StdClass();
            $res->status    = 'ok';
            
            $notifications  = $this->Notifications->getNotifications();
            for ($i=0; $i < count($notifications) ; $i++) 
            { 
                $notifications[$i]->events = array();
                $events = $this->Events->getEvents( $notifications[$i]->notificationId );
                foreach ($events as $event) 
                {
                    $notifications[$i]->events[] = $event;
                }
            }
            $res->notifications = $notifications;
            echo escapeJsonString($res,FALSE);
        }
        else {
            echo '{"status": "invalid"}';
        }
    }

    public function getnotification()
    {
        $id = $this->uri->segment(3);
        if(isset($id))
        {
            $notification = $this->Notifications->getNotification($id);
            if(isset($notification))
            {
              return $notification;
            }
        }
        else {
            echo "invalid";
        }
    }

    public function create()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        if ($this->Identity->Validate('notifications/manage')) 
        {
            $this->form_validation->set_rules('title', 'title', 'required');
            if ($this->form_validation->run() == FALSE)
            {
                $this->load->view('notifications/createNotification');
            }
            else
            {
                $this->Notifications->Create($_POST['title']);
            }
        } 
        else 
        {
            show_404();
        }
    }

    public function edit()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        if ($this->Identity->Validate('notifications/manage'))
		{
            if($this->uri->segment(3))
            {
                $exist = new StdClass();
                $exist = $this->db->get_where('notifications',array('notificationId' => $this->uri->segment(3)))->row();
                if(isset($exist))
                {
                    $this->form_validation->set_rules('title', 'title', 'required');
                    if ($this->form_validation->run() == FALSE) 
                    {
                        $this->load->view('notifications/editNotification');    
                    } 
                    else 
                    {
                        $exist->title = $_POST['title'];
                        $this->Notifications->Edit($exist);
                    }
                }
                else
                {
                    echo "invalid";
                }
            }
            else
            {
                echo "invalid";
            }
        }
        else
        {
            show_404();
        }
    }

    public function delete($notificationId)
    {
        if ($this->Identity->Validate('notifications/manage'))
		{
			$this->Notifications->Delete($notificationId);
		}
		else
		{
			echo "invalid";
		}
    }

    public function deleteEvent($eventId)
    {
        if ($this->Identity->Validate('notifications/manage'))
		{
            $this->Events->Delete($eventId);
        }
        else
        {
            echo "invalid";
        }
    }

}
    
?>