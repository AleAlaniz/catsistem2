<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Themes Controller Class
 * 
 * Controller Themes Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Themes
 * @author 		Ivan
*/
class Themes extends CI_Controller {

	/**
	 * Class constructor
	 *
	 * Load Model To Use.
	 *
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		//load Theme Model
		$this->load->model('Theme_model', 'Theme');
	}

	// --------------------------------------------------------------------

	/**
	 * Themes - Index Page
	 *
	 * @return	mixed
	 */
	public function index()
	{
		//Check Permissions
		if ($this->Identity->Validate('theme/index'))
		{
			//Call Corresponding Method
			$this->Theme->GetThemes();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Themes - Create Page
	 *
	 * @return	mixed
	 */
	public function create()
	{
		//Check Permissions
		if ($this->Identity->Validate('theme/create'))
		{
			//Call Corresponding Method
			return $this->Theme->CreateTheme();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Themes - Details Page
	 *
	 * @return	mixed
	 */
	public function details()
	{
		if($this->Identity->Validate('theme/details'))
		{
			//Check Permissions
			return $this->Theme->DetailsTheme();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Themes - Delete Page
	 *
	 * @return	mixed
	 */
	public function delete()
	{
		//Check Permissions
		if($this->Identity->Validate('theme/delete'))
		{
			//Call corresponding Method
			return $this->Theme->DeleteTheme();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Themes - Edit Page
	 *
	 * @return	mixed
	 */
	public function edit()
	{
		//Check Permissions
		if($this->Identity->Validate('theme/edit'))
		{
			//Call Corresponding Method
			return $this->Theme->Edit();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Themes - Delete Image Page
	 *
	 * @return	mixed
	 */
	public function deleteimage()
	{
		//Check Permissions
		if($this->Identity->Validate('theme/edit'))
		{
			//Call Corresponding Method
			return $this->Theme->DeleteImage();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}
}