<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Chat Controller Class
 * 
 * Controller Chat Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Chat
 * @author 		Ivan
*/
class Chat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Chat_model', 'Chat');
		$this->lang->load('chat');
	}


	public function unique()
	{
		if ($this->Identity->Validate('chat/index'))
		{
			$this->load->view('chat/unique', array('navbar' => $this->load->view('chat/_navbar', array('title' => $this->lang->line('chat_uniquetitle')), TRUE)));
		}
		else
		{
			show_404();
		}
	}

	public function getchatsu()
	{
		if ($this->Identity->Validate('chat/index'))
		{
			$data = new StdClass();
			$data->status 	= 'ok';
			$data->chats 	= $this->Chat->GetChatsU();
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getchatubyid()
	{
		if ($this->Identity->Validate('chat/index') && $this->input->get('id'))
		{
			$data 		= new StdClass();
			$data->chat = $this->Chat->GetChatUById($this->input->get('id'));
			if ($data->chat != NULL) {
				$data->status = 'ok';
			}
			else{
				$data->status = 'invalid';
			}
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getoldmessages()
	{
		if ($this->Identity->Validate('chat/unique/getolds') && $this->input->get('id') && $this->input->get('first'))
		{
			$data 			= new StdClass();
			$data->messages = $this->Chat->GetOldMessages($this->input->get('id'), $this->input->get('first'));
			if (isset($data->messages)) {
				if (count($data->messages) == 0) {
					$data->status = 'empty';
				}
				else
				{
					$data->status = 'ok';
				}
			}
			else{
				$data->status = 'invalid';
			}
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function deletechatu()
	{
		if ($this->Identity->Validate('chat/index') && $this->input->get('id'))
		{
			$data = $this->Chat->DeleteChatU($this->input->get('id'));
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function newmessageu()
	{
		if ($this->Identity->Validate('chat/unique/sendmessage'))
		{
			try {
				$data = json_decode(file_get_contents("php://input"));
			} catch (Exception $e) {	}

			if (isset($data) && $data->id && $data->message) {
				$response 			= new StdClass();
				$response->message = $this->Chat->newMessageU($data->id, $data->message);
				if (isset($response->message)) {
					$response->status = 'ok';
				}
				else{
					$response->status = 'invalid';
				}
				echo escapeJsonString($response, FALSE);
			}
			else
			{
				echo '{"status":"invalid"}';	
			}
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function addmessageuview()
	{
		if ($this->Identity->Validate('chat/index') && $this->input->get('id'))
		{
			$data 			= new StdClass();
			$data->status 	= $this->Chat->AddMessageUView($this->input->get('id'));
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getnewmessagesu()
	{
		
		if ($this->Identity->Validate('chat/unique/getnews') && $this->input->get('id') && $this->input->get('last') )
		{
			$data 			= new StdClass();
			$data->messages = $this->Chat->GetNewMessagesU($this->input->get('id'), $this->input->get('last'));
			if (isset($data->messages)) {
				if (count($data->messages) == 0) {
					$data->status = 'empty';
				}
				else
				{
					$data->status = 'ok';
				}
			}
			else{
				$data->status = 'invalid';
			}
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function newunique()
	{
		if ($this->Identity->Validate('chat/unique/new'))
		{
			$this->Chat->NewUnique();
		}
		else
		{
			show_404();
		}
	}

	public function multi()
	{
		if ($this->Identity->Validate('chat/multi'))
		{
			$this->load->view('chat/multi', array('navbar' => $this->load->view('chat/_navbar', array('title' => $this->lang->line('chat_multipletitle')), TRUE)));
		}
		else
		{
			show_404();
		}
	}

	public function getchatsm()
	{
		if ($this->Identity->Validate('chat/multi'))
		{
			$data = new StdClass();
			$data->status 	= 'ok';
			$data->chats 	= $this->Chat->GetChatsM();
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function deletechatm()
	{
		if ($this->Identity->Validate('chat/multi') && $this->input->get('id'))
		{
			$data 			= new StdClass();
			$data->status 	= $this->Chat->DeleteChatM($this->input->get('id'));
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getchatmbyid()
	{
		if ($this->Identity->Validate('chat/multi') && $this->input->get('id'))
		{
			$data 		= new StdClass();
			$data->chat = $this->Chat->GetChatMById($this->input->get('id'));
			if ($data->chat != NULL) {
				$data->status = 'ok';
			}
			else{
				$data->status = 'invalid';
			}
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getoldmmessages()
	{
		if ($this->Identity->Validate('chat/multi/getolds') && $this->input->get('id') && $this->input->get('first'))
		{
			$data 			= new StdClass();
			$data->messages = $this->Chat->GetOldMMessages($this->input->get('id'), $this->input->get('first'));
			if (isset($data->messages)) {
				if (count($data->messages) == 0) {
					$data->status = 'empty';
				}
				else
				{
					$data->status = 'ok';
				}
			}
			else{
				$data->status = 'invalid';
			}
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getnewmessagesm()
	{
		
		if ($this->Identity->Validate('chat/multi/getnews') && $this->input->get('id') && $this->input->get('last') )
		{
			$data 			= new StdClass();
			$data->messages = $this->Chat->GetNewMessagesM($this->input->get('id'), $this->input->get('last'));
			if (isset($data->messages)) {
				if (count($data->messages) == 0) {
					$data->status = 'empty';
				}
				else
				{
					$data->status = 'ok';
				}
			}
			else{
				$data->status = 'invalid';
			}
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function newmessagem()
	{
		if ($this->Identity->Validate('chat/multi/sendmessage'))
		{
			try {
				$data = json_decode(file_get_contents("php://input"));
			} catch (Exception $e) {	}

			if (isset($data) && $data->id && $data->message) {
				$response 			= new StdClass();
				$response->message = $this->Chat->NewMessageM($data->id, $data->message);
				if (isset($response->message)) {
					$response->status = 'ok';
				}
				else{
					$response->status = 'invalid';
				}
				echo escapeJsonString($response, FALSE);
			}
			else
			{
				echo '{"status":"invalid"}';	
			}
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function deleteallchatm()
	{
		if ($this->Identity->Validate('chat/multi') && $this->input->get('id'))
		{
			$data 			= new StdClass();
			$data->status 	= $this->Chat->DeleteAllChatM($this->input->get('id'));
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function deleteuser()
	{
		if ($this->Identity->Validate('chat/multi') && $this->input->get('id') && $this->input->get('user'))
		{
			$data 			= new StdClass();
			$data->status 	= $this->Chat->DeleteUser($this->input->get('id'), $this->input->get('user'));
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function newmulti()
	{
		if ($this->Identity->Validate('chat/multi/new'))
		{
			$this->Chat->NewMulti();
		}
		else
		{
			show_404();
		}
	}


	public function addmessagemview()
	{
		if ($this->Identity->Validate('chat/multi') && $this->input->get('id'))
		{
			$data 			= new StdClass();
			$data->status 	= $this->Chat->AddMessageMView($this->input->get('id'));
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function searchusers()
	{
		if ($this->Identity->Validate('chat/index'))
		{
			$likeuser = $this->db->escape_like_str($this->input->post('like'));
			if ($likeuser != '') {
				$data 			= new StdClass();
				$data->users = $this->Chat->SearchUsers($likeuser);
				if (isset($data->users)) {
					if (count($data->users) == 0) {
						$data->status = 'empty';
					}
					else
					{
						$data->status = 'ok';
					}
				}
				else{
					$data->status = 'invalid';
				}
				echo escapeJsonString($data, FALSE);
			}
			else
			{
				echo '{"status":"invalid"}';
			}
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getunreadu()
	{
		if ($this->Identity->Validate('chat/index'))
		{
			$data = new StdClass();
			$data->status = 'ok';
			$data->unread = $this->Chat->GetUnreadU();
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function getunreadm()
	{
		if ($this->Identity->Validate('chat/multi'))
		{
			$data = new StdClass();
			$data->status = 'ok';
			$data->unread = $this->Chat->GetUnreadM();
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function updateChatSubject($value='')
	{	$chatData = json_decode(file_get_contents('php://input'), false);
		if ($this->Identity->Validate('chat/multi'))
		{
			$data = new StdClass();
			$data = $this->Chat->updateSubject($chatData);
			echo escapeJsonString($data, FALSE);
		}
		else
		{
			echo '{"status":"invalid"}';
		}
	}

	public function addNewMember()
	{
		$request = json_decode($this->input->raw_input_stream,true);
		
		$chatId = $request['chatId'];

		$this->db->select('userId');
		$userId = $this->db->get_where('chatM',array('chatmId' => $chatId))->row();
		$userId = $userId->userId;
		
		if($userId == $this->session->UserId){
			
			$this->db->select('userId');
			$response = $this->db->get_where('chatmUsers',array('chatmId' => $chatId))->result();
			
			$users  = $request['users'];
			if(array_search($userId,$users) != FALSE){//si intenta agregarse a si mismo
				show_404();
			}
			foreach ($response as $value) {//llenado de array
				$chatUsers[] = $value->userId;
			}
			foreach ($users as $value) { // si intenta agregar a quienes ya estan
				if(array_search($value,$chatUsers) != FALSE){
					show_404();
				}
			}
			foreach ($users as $user) {
				$res = $this->Chat->addNewMember($user,$chatId);
			}
			print_r($res);
		}
		else {
			show_404();
		}
	}

	public function getChatUsers()
	{
		$request = json_decode($this->input->raw_input_stream,true);
		$chatId = $request['chatId'];
		$res = $this->Chat->getChatUsers($chatId);
		echo(json_encode($res));
	}

	public function searchChatUsers()
	{
		$request = json_decode($this->input->raw_input_stream,true);
		$chatId = $request['id'];
		$like = $request['like'];
		$res = $this->Chat->searchChatUsers($chatId,$like);
		if($res == 'empty'){
			echo $res;
		}
		else {
			echo json_encode($res);
		}
	}

}
