<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Subsections Controller Class
 * 
 * Controller Subsections Page
 *
 * @package 	CATNET
 * @subpackage 	Controllers
 * @category 	Subsections
 * @author 		Ivan
*/
class Subsections extends CI_Controller {

	/**
	 * Class constructor
	 *
	 * Load Model To Use.
	 *
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		//Load Subsection Model
		$this->load->model('Subsection_model', 'Subsection');
	}

	// --------------------------------------------------------------------

	/**
	 * Subsections - Index Page
	 *
	 * @return	mixed
	 */
	public function index()
	{
		//Check Permissions
		if ($this->Identity->Validate('subsections/index')) 
		{
			//Load View
			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('subsections/subsections', array('model' => $this->Subsection->GetSubsections()));
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * Subsections - Create Page
	 *
	 * @return	mixed
	 */
	public function create()
	{
		//Check Permissions
		if ($this->Identity->Validate('subsections/create'))
		{
			//Call Corresponding Method
			return $this->Subsection->CreateSubsection();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * Subsections - Delete Page
	 *
	 * @return	mixed
	 */
	public function delete(){
		//Check Permissions
		if($this->Identity->Validate('subsections/delete'))
		{
			//Calll Corresponding Method
			return $this->Subsection->DeleteSubsection();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * Subsections - Details Page
	 *
	 * @return	mixed
	 */
	public function details()
	{
		//Check Permissions
		if($this->Identity->Validate('subsections/details'))
		{
			//Call Corresponding Method
			return $this->Subsection->DetailsSubsection();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * Subsections - Edit Page
	 *
	 * @return	mixed
	 */
	public function edit()
	{
		//Check Permissions
		if($this->Identity->Validate('subsections/edit'))
		{
			//Call Corresponding Method
			return $this->Subsection->EditSubsection();
		}
		else
		{
			//Redirect To Home
			header('Location:/'.FOLDERADD);
		}
	}
}