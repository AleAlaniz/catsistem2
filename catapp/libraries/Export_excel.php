<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class export_excel{

    function to_excel($array, $filename) {
        header('Content-Disposition: attachment; filename='.$filename.'.xls');
        header('Content-type: application/force-download');
        header('Content-Transfer-Encoding: binary');
        header('Pragma: public');
        print "\xEF\xBB\xBF"; // UTF-8 BOM

        $index = 0;
        foreach ($array as $element) {
            if($element){
                $this->writeFormat($element,$index);
                $index++;
            }
        }        
    }

    function writeRow($val) {
        echo '<td style="border:1px #888 solid;color:#555;">'.$val.'</td>';
    }
    function writeFormat($array,$index)
    {
        $h = array();
        $arreglo = (array)$array;

        foreach($arreglo as $row){
            foreach($row as $key=>$val){
                if(!in_array($key, $h)){
                    $h[] = $key;
                }
            }
        }
        echo '<table><tr>';
        // switch ($index) {
        //     case 0:
        //         echo'<tr>Datos de adhesión actual en teaming</tr>';
        //         break;
        //     case 1:
        //         echo'<tr>Datos de actualización actual en teaming</tr>';
        //         break;
        //     case 2:
        //         echo'<tr>Datos de actualizaciones anteriores en teaming</tr>';
        //         break;
        //     case 3:
        //         echo'<tr>Datos de bajas en teaming</tr>';
        //         break;
        //     default;
        //         break;
        // }
        foreach($h as $key) {
            $key = ucwords($key);
            echo '<th style="border:1px #888;color: #FFF;background-color:rgb(66,166,42);">'.$key.'</th>';
        }
        echo '</tr>';

        foreach($arreglo as $row){
            echo '<tr>';
            foreach($row as $val){
                switch ($val) {
                    case 'd':
                        $this->writeRow('Desvinculación');
                        break;
                    case 'r':
                        $this->writeRow('Rechazado');
                        break;
                    case 'f':
                        $this->writeRow('Baja a teaming');
                        break;
                    default;
                        $this->writeRow($val);
                        break;
            }
        }              
        echo '</tr>';
        }

        echo '</table>';
    }
}
?>
