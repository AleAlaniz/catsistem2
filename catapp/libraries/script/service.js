var io = require('socket.io')(443);
var usersOnline = {};

function getOnlineUsersLenght() {
	var total = 0;
	for (user in usersOnline) {
		if (usersOnline[user]) {
			total++;
		}
	}
	return total;
}

io.use(function(socket, next){
	var userId = socket.handshake.query.id;
	if (userId) {
		if (usersOnline['id '+userId]) {
			usersOnline['id '+userId].push(socket.id);
		}
		else{
			usersOnline['id '+userId] = [socket.id];

			io.to('analitycs_online').emit('analitycs_online_change', getOnlineUsersLenght());

		}
		next();
	}
	else
	{
		next(new Error('Authentication error'));
	}
});

io.on('connection', function(socket){

	socket.realtime = {
		subscribe 	: function(chanel, params){
			var channel = socket.realtime.channels[chanel];
			if (channel)
			{
				if (params)
				{
					channel.subscribe(params);
				}
				else{
					channel.subscribe();
				}
			}
		},
		unsubscribe : function(chanel, params){
			var channel = socket.realtime.channels[chanel];
			if (channel)
			{
				if (params)
				{
					channel.unsubscribe(params);
				}
				else{
					channel.unsubscribe();
				}
			}
		},
		channels 	: {
			unread_suggestbox 	:  {
				subscribe 	: function() {
					socket.join('new_suggestbox');
				},
				unsubscribe : function() {
					socket.leave('new_suggestbox');
				},
				'new_suggestbox' : function() {
					io.to('new_suggestbox').emit('new_suggestbox');
				}
			},
			all_newsletter 		:  {
				subscribe 	: function() {
					socket.join('all_newsletter');
				},
				unsubscribe : function() {
					socket.leave('all_newsletter');
				}
			},
			section 			:  {
				subscribe 	: function(id) {
					socket.join('section '+id);
				},
				unsubscribe : function(id) {
					socket.leave('section '+id);
				}
			},
			site 				:  {
				subscribe 	: function(id) {
					socket.join('site '+id);
				},
				unsubscribe : function(id) {
					socket.leave('site '+id);
				}
			},
			role 				:  {
				subscribe 	: function(id) {
					socket.join('role '+id);
				},
				unsubscribe : function(id) {
					socket.leave('role '+id);
				}
			},
			user 				:  {
				subscribe 	: function(id) {
					socket.join('user '+id);
				},
				unsubscribe : function(id) {
					socket.leave('user '+id);
				}
			},
			press 				:  {
				subscribe 	: function() {
					socket.join('press');
				},
				unsubscribe : function() {
					socket.leave('press');
				},
				'new_press' : function() {
					io.to('press').emit('new_press');
				}
			},
			chatu 				:  {
				subscribe 		: function(id) {
					socket.join('chatu '+id);
				},
				unsubscribe 	: function(id) {
					socket.leave('chatu '+id);
				},
				chatu_message 	: function(data) {
					socket.broadcast.to('chatu '+data.id).emit('chatu_message', data.id, data.message);
					for (var i = 0; i < data.to.length; i++) {
						io.to('user '+data.to[i]).emit('new_chatu_message', data.id);
					}
				}
			},
			chatm 				:  {
				subscribe 		: function(id) {
					socket.join('chatm '+id);
				},
				unsubscribe 	: function(id) {
					socket.leave('chatm '+id);
				},
				chatm_message 	: function(data) {
					if (data.message) {
						socket.broadcast.to('chatm '+data.id).emit('chatm_message', data.id, data.message);
					}
					for (var i = 0; i < data.to.length; i++) {
						io.to('user '+data.to[i]).emit('new_chatm_message', data.id);
					}
				},
				chatm_delete 	: function(data) {
					for (var i = 0; i < data.to.length; i++) {
						io.to('user '+data.to[i]).emit('chatm_delete', data.id);
					}
				}
			},
			analitycs_online		:  {
				subscribe 		: function() {
					socket.join('analitycs_online');
					socket.emit('analitycs_online_change', getOnlineUsersLenght());

				},
				unsubscribe 	: function() {
					socket.leave('analitycs_online');
				}
			}
		}
	}

	socket.on('subscribe'		, socket.realtime.subscribe);
	socket.on('unsubscribe'		, socket.realtime.unsubscribe);
	socket.on('new_suggestbox'	, socket.realtime.channels.unread_suggestbox.new_suggestbox);
	socket.on('new_newsletter'	, new_newsletter);
	socket.on('new_press'		, socket.realtime.channels.press.new_press);
	socket.on('new_inbox'		, new_inbox);
	socket.on('chatu_message'	, socket.realtime.channels.chatu.chatu_message);
	socket.on('chatm_message'	, socket.realtime.channels.chatm.chatm_message);
	socket.on('chatm_delete'	, socket.realtime.channels.chatm.chatm_delete);
	socket.on('disconnect'		, disconnect);
	function new_newsletter(to) {
		var time = Math.floor(Date.now() / 1000);
		if (to.sections) {
			for (var i = 0; i < to.sections.length; i++) {
				io.to('section '+to.sections[i]).emit('new_newsletter', time);
			}
		}
		if (to.sites) {
			for (var i = 0; i < to.sites.length; i++) {
				io.to('site '+to.sites[i]).emit('new_newsletter', time);
			}
		}
		if (to.roles) {
			for (var i = 0; i < to.roles.length; i++) {
				io.to('role '+to.roles[i]).emit('new_newsletter', time);
			}
		}
		if (to.users) {
			for (var i = 0; i < to.users.length; i++) {
				io.to('user '+to.users[i]).emit('new_newsletter', time);
			}
		}
		io.to('all_newsletter').emit('new_newsletter', time);
	}

	function new_inbox(to) {
		if (to) {
			if (to == 'all') {
				socket.broadcast.emit('new_inbox');
			}
			else{
				for (var i = 0; i < to.length; i++) {
					io.to('user '+to[i]).emit('new_inbox');
				}
			}
		}
	}

	function disconnect() {
		var userId = socket.handshake.query.id;
		if (usersOnline['id '+userId]) {
			for (var i = 0; i < usersOnline['id '+userId].length; i++) {
				if (usersOnline['id '+userId][i] == socket.id) {
					usersOnline['id '+userId].splice(i,1);
					break;
				}
			}
			if (usersOnline['id '+userId].length <= 0) {
				usersOnline['id '+userId] = false;
				io.to('analitycs_online').emit('analitycs_online_change', getOnlineUsersLenght());
			}
		}
	}
});