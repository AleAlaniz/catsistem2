(function() {
	'use strict';

	angular
	.module('catnet')
	.config	   (ActivitiesRoutes						)
	.factory   ('activity_message'	,activity_message	)
	.controller('Cactivity' 		,Cactivity			)
	.controller('Ccreate'   		,Ccreate			)
	.controller('Cedit'   	  		,Cedit				)
	.controller('Creport'   	  	,Creport			);

	ActivitiesRoutes.$inject = ['$routeProvider'];
	function ActivitiesRoutes($routeProvider) {
		
		$routeProvider
		.when('/activities', {
			templateUrl: '/'+FOLDERADD+'/activity',
			controller: 'Cactivity'
		})
		.when('/activities/create',{
		templateUrl : '/'+FOLDERADD+'/activity/create',
		controller  : 'Ccreate'
		})
		.when('/activities/edit/:activityId',{
		templateUrl : '/'+FOLDERADD+'/activity/edit',
		controller  : 'Cedit'
		})
		.when('/activities/report/:activityId',{
		templateUrl : '/'+FOLDERADD+'/activity/report',
		controller  : 'Creport'
		})
	}

	activity_message.$inject = ['$rootScope'];
	function activity_message($rootScope) {
		var data = {}
		data.message = '';
		data.setMessage = function(message, auto_update) {
			this.message = message;
			if (auto_update) {
				$rootScope.$broadcast('activity_message');
			}
		}
		return data;
	}

	Cactivity.$inject = ['$http','nav','$scope','activity_message','$window','$location'];
	function Cactivity($http, nav, $scope,activity_message,$window,$location) {
		
		nav.setSelected('activities');
		$window.scrollTo(0, 0);
		$scope.activities = [];
		$scope.loading 	  = true;
		$scope.message 	  = activity_message.message; 

		var selectedActivity;

		getAllActivities();

		$scope.$on("activity_message",function () {
			$scope.message = activity_message.message;
		})

		$scope.$on("$destroy",function () {
			activity_message.setMessage('',false);
		})

		$scope.showEdit = function () {
			$('[role="endDate"]').datepicker({
				language	   : 'es',
                autoclose	   : true,
                todayHighlight : true, 
				todayBtn       : "linked",
				format		   : 'mm/dd/yyyy',
				startDate      : 'getDate()'					
			});
		}

		$scope.updateDate = function (activityId,endDate) {
			if(endDate){
				$http({
				method 	: 'POST',
				url 	: '/'+FOLDERADD+'/activity/updateDate',
				data 	: 
				{
					endDate	: endDate,
					activityId: activityId
				}
				}).success(function(data) {
					if(data == 'ok'){
						$scope.editDateSurvey = false;
						getAllActivities();
					}
				});
			}
			else{
				var filter = "#"+activityId;
				$(filter).show();
			}
		}

		function getAllActivities() {

			$scope.loading 	 = true;
			selectedActivity = null;
			$http
			.get('/'+FOLDERADD+'/activity/getActivities')
			.success(function(data) {
				if (data.status == 'ok') {
					$scope.activities = data.activities;
				}
				$scope.loading = false;
			});
		};

		$scope.active = function () {

			$http
			.get('/'+FOLDERADD+'/activity/activate/'+selectedActivity)
			.success(function(data) {

				getAllActivities();
				$('#confirm-create').modal('hide');
			})
		}

		$scope.delete = function () {

			$http
			.get('/'+FOLDERADD+'/activity/delete/'+selectedActivity)
			.success(function(data) {
				if(data.status == "ok"){
					$('#confirm-delete').modal('hide');
					$scope.message = data.message;
					getAllActivities();
				}
			})
		}

		$scope.showCreate = function(activityId) {

			selectedActivity = activityId;
			$('#confirm-create').modal('show');
		}

		$scope.showDelete= function(activityId) {

			selectedActivity = activityId;
			$('#confirm-delete').modal('show');
		}
	}

	Ccreate.$inject = ['$http','$scope','$location','activity_message'];
	function Ccreate($http,$scope,$location,activity_message) {

		$scope.users = [];
		$scope.sending		= false;

		$('[role="date"]').datepicker({
			language	: 'es',
			autoclose	: true
		});

		if(typeof String.prototype.trim !== 'function') {
			String.prototype.trim = function() {
				return this.replace(/^\s+|\s+$/g, ''); 
			}
		}

		//funcionalidad de agregar usuarios por busqueda
		$scope.removeUser = function(i) {
			$scope.users.splice(i, 1);
		};

		$scope.addUser = function(i) {
			$scope.users.push($scope.userOptions[i]);
			$scope.userOptions.splice(i, 1);
		};

		$scope.userOptions = [];

		$scope.findLike = '';


		$scope.searchNumber = 0;
		$scope.searchActual = 0;

		$scope.searchUsers = function() {
			if($scope.findLike != '') 
			{
				var thisSearch = $scope.searchNumber;
				$scope.searchNumber = $scope.searchNumber+1;
				$.ajax({
					method 	: "POST",
					url		: '/'+FOLDERADD+'/surveys/searchusers',
					data 	: {'like': $scope.findLike}
				}).done(function (data) {
					if(data == 'invalid'){

					}
					else if(data != 'empty' && thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply($scope.addUserOptions(data));
					}
					else if (thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply($scope.userOptions = []);
					}
				});
			}
			else
			{
				$scope.$apply($scope.userOptions = []);
			}
		};

		$scope.addUserOptions = function (users) {
			try{

				users = $.parseJSON(users);
			}catch(e){
				return;
			}

			$scope.userOptions = [];
			jQuery.each(users, function(i, user) {
				var exist = false;
				jQuery.each($scope.users, function(i, userExist) {
					if (userExist.userId == user.userId) {exist = true;}
				});
				if (!exist) {$scope.userOptions.push(user);}
			});


		};

		$scope.selectClick = function (e) {
			$('.select-input', e.currentTarget).focus();
		};
		//fin de funcionalidad de agregar usuarios


		$scope.submitActivity = function () {
			var incomplete = false;
			var haveTo     = false;

			jQuery.each($('input[name="sections[]"]'), function(i, section) {
				if(section.checked)
				{
					haveTo = true;
					return;
				}
			});

			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="sites[]"]'), function(i, site) {
					if(site.checked)
					{
						haveTo = true;
						return;
					}
				});
				if (!haveTo) {
					haveTo = false;
					jQuery.each($('input[name="roles[]"]'), function(i, role) {
						if(role.checked)
						{
							haveTo = true;
							return;
						}
					});

					if (!haveTo) {
						haveTo = false;
						jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
							if(userGroup.checked)
							{
								haveTo = true;
								return;
							}
						});

						if (!haveTo) {
							haveTo = false;
							if ($('input[name="users[]"]').length <= 0) {
								incomplete = true;
							}
						}
					}

				}

			}
			
			if ($('input[name="name"]').val().trim() == "") {incomplete=true;}
			if ($('#startDate').val().trim() == "") {incomplete=true;}
			if ($('#endDate').val().trim() == "") {incomplete=true;}
			if($('#userfile').val() == "") {incomplete=true;}
			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else{
				$scope.sending = true;
				if(!$scope.showCoupons){
					$('#coupons').val('');
				}
				$('#form-activity').ajaxSubmit({
					url  		: '/'+FOLDERADD+'/activity/create',
					dataType 	: 'json',
					success: function(data) {
						if (data.status == "ok") {
							activity_message.setMessage(data.message, false);
							$location.path('/activities');
							$scope.$apply()

						}
						$scope.sending = false;
						$scope.$apply()
					}
				});
			}
		}
	}

	Cedit.$inject = ['$http','$scope','$location','$routeParams','activity_message'];
	function Cedit($http,$scope,$location, $routeParams,activity_message) {

		$scope.users 		= [];
		$scope.sending		= false;
		$scope.showCoupons  = false;

		$('[role="date"]').datepicker({
			language	: 'es',
			autoclose	: true
		});

		if(typeof String.prototype.trim !== 'function') {
			String.prototype.trim = function() {
				return this.replace(/^\s+|\s+$/g, ''); 
			}
		}

		//funcionalidad de agregar usuarios por busqueda
		$scope.removeUser = function(i) {
			$scope.users.splice(i, 1);
		};

		$scope.addUser = function(i) {
			$scope.users.push($scope.userOptions[i]);
			$scope.userOptions.splice(i, 1);
		};

		$scope.userOptions = [];

		$scope.findLike = '';


		$scope.searchNumber = 0;
		$scope.searchActual = 0;

		$scope.searchUsers = function() {
			if($scope.findLike != '') 
			{
				var thisSearch = $scope.searchNumber;
				$scope.searchNumber = $scope.searchNumber+1;
				$.ajax({
					method 	: "POST",
					url		: '/'+FOLDERADD+'/surveys/searchusers',
					data 	: {'like': $scope.findLike}
				}).done(function (data) {
					if(data == 'invalid'){

					}
					else if(data != 'empty' && thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply($scope.addUserOptions(data));
					}
					else if (thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply($scope.userOptions = []);
					}
				});
			}
			else
			{
				$scope.$apply($scope.userOptions = []);
			}
		};

		$scope.addUserOptions = function (users) {
			try{

				users = $.parseJSON(users);
			}catch(e){
				return;
			}

			$scope.userOptions = [];
			jQuery.each(users, function(i, user) {
				var exist = false;
				jQuery.each($scope.users, function(i, userExist) {
					if (userExist.userId == user.userId) {exist = true;}
				});
				if (!exist) {$scope.userOptions.push(user);}
			});


		};

		$scope.selectClick = function (e) {
			$('.select-input', e.currentTarget).focus();
		};
		//fin de funcionalidad de agregar usuarios

		// obtener datos
		$scope.activity = [];
		
		function getData() {
			$http({
			method 	: 'GET',
			url 	: '/'+FOLDERADD+'/activity/getactivity/'+$routeParams.activityId
			}).success(function(data) {
				if(data != "error"){
					$scope.activity = data;
					
					// $scope.$apply(function(){
						if($scope.activity.capacity){
							$scope.showCoupons = true;
							$('#coupons').val($scope.activity.capacity);
						}
						$('#startDate').datepicker('update',$scope.activity.startDate);
						$('#endDate').datepicker('update',$scope.activity.endDate);

						$scope.required = ($scope.activity.required == "TRUE");

						jQuery.each($scope.activity.users,function (i,user) {
							$scope.users.push({ completeName : user.name, userId : user.userId, userName : user.userName});
						})
					// })
				}
				else{
					$location.path("#/");
					$scope.$apply();
				}
			});
		}
		getData();
		
		$scope.cancelCoupons = function () {
			$scope.showCoupons = false;
			$('#coupons').val(1);
		}

		$("#userfile").change(function(){openFile(event)});

		function openFile(newFile) {
			var input = newFile.target;
			
				var reader = new FileReader();
				reader.onload = function(){
					var dataURL = reader.result;
					var output = document.getElementById('imgPrev');
					output.src = dataURL;
				};
				reader.readAsDataURL(input.files[0]);
		}
		
		//enviar los datos al servidor
		$scope.submitActivity = function () {
			var incomplete = false;
			var haveTo     = false;

			jQuery.each($('input[name="sections[]"]'), function(i, section) {
				if(section.checked)
				{
					haveTo = true;
					return;
				}
			});

			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="sites[]"]'), function(i, site) {
					if(site.checked)
					{
						haveTo = true;
						return;
					}
				});
				if (!haveTo) {
					haveTo = false;
					jQuery.each($('input[name="roles[]"]'), function(i, role) {
						if(role.checked)
						{
							haveTo = true;
							return;
						}
					});

					if (!haveTo) {
						haveTo = false;
						jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
							if(userGroup.checked)
							{
								haveTo = true;
								return;
							}
						});

						if (!haveTo) {
							haveTo = false;
							if ($('input[name="users[]"]').length <= 0) {
								incomplete = true;
							}
						}
					}

				}

			}
			
			if ($('input[name="name"]').val().trim() == "") {incomplete=true;}
			if ($('#startDate').val().trim() == "") {incomplete=true;}
			if ($('#endDate').val().trim() == "") {incomplete=true;}
			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else{
				$scope.sending = true;
				
				$('#form-activity').ajaxSubmit({
					url  		: '/'+FOLDERADD+'/activity/editActivity/'+$routeParams.activityId,
					dataType 	: 'json',
					success: function(data) {
						if (data.status == 'ok') {
							activity_message.setMessage(data.message, false);
							$location.path('/activities');
							$scope.$apply()
						}
						$scope.sending = false;
						$scope.$apply()
					}
				});
			}
		}	
	}

	Creport.$inject = ['$http','nav','$scope','activity_message', '$routeParams','$window'];
	function Creport($http, nav, $scope,activity_message,$routeParams,$window) {
		
		nav.setSelected('activities');
		$scope.survey 		= null;
		$scope.survey 		= {};
		$scope.loading 		= true;
		$scope.openCtable   = true;
		$scope.openItable   = true;
		
		init_ctrl();
		function init_ctrl() {
			$http({
				method : 'GET',
				url    : '/'+FOLDERADD+'/activity/getactivityreport/'+$routeParams.activityId,
			}).success(function(data){
				if (data.status == 'ok'){
					$scope.loading = false;
					$scope.activity = data.activity;
				}
			})
		}
		$scope.displayTables = function (id) {
			$('#'+id).slideToggle();
			if(id == 'cTable'){
				$scope.openCtable = !$scope.openCtable;
			}
			if(id == 'iTable'){
				$scope.openItable = !$scope.openItable;
			}
		}

		$scope.exportData =function() {
			$window.open('/'+FOLDERADD+'/activity/exportActivityReport/'+$routeParams.activityId,'_blank')
		}
		
	}

})();