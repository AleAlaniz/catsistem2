(function () {
    'use strict';
    angular
    .module('catnet')
    .config(notificationConfig)
    .factory('notifications_message',notifications_message);
    
    notificationConfig.$inject = ['$routeProvider'];
    function notificationConfig($routeProvider) {
        $routeProvider
        .when('/notifications',{
            templateUrl : '/'+FOLDERADD+'/notifications',
            controller  : Cnotifications
        })
        .when('/notifications/createNotification',{
            templateUrl : '/'+FOLDERADD+'/notifications/create',
            controller  : CnotificationsCreate
        })
        .when('/notifications/editNotification/:id',{
        templateUrl : function(params){return '/'+FOLDERADD+'/notifications/edit/'+params.id;} ,
        controller  : CNotificationsEdit
        })
        .when('/notifications/createEvent/:id',{
        templateUrl : function(params){return '/'+FOLDERADD+'/event/create/'+params.id;} ,
        controller  : CEventsCreate
        })
        .when('/notifications/editEvent/:id',{
        templateUrl : function(params){return '/'+FOLDERADD+'/event/edit/'+params.id;},
        controller  : CEventEdit
        });
    }

    notifications_message.$inject = ['$rootScope'];
    function notifications_message($rootScope) {
        var data = {};
        data.message = '';
        data.setMessage = function (param_message) {
            this.message = param_message;
        }
        return data;
    }

    Cnotifications.$inject = ['$scope','nav','$http','notifications_message'];
    function Cnotifications($scope,nav,$http,notifications_message) {
        $scope.notifications = [];
        $scope.event         = {};
        $scope.loading    = true;
        $scope.message    = notifications_message;
        $scope.setDelete  = setDelete;
        $scope.toDelete   = toDelete;
        $scope.sendEvent  = sendEvent;
        $scope.getReport  = getReport;
        $scope.setModal   = setModal;

        nav.setSelected('notifications');

        $scope.getNotifications = getNotifications;

        function getNotifications() {
            $scope.notifications = [];
            
            $http({
                method  : 'GET',
                url     : '/'+FOLDERADD+'/Notifications/getnotifications'})
            .then(function (res) {
                $scope.notifications = res.data.notifications;
                $scope.loading = false;
            });
        }

        function setDelete(type, id) {
			$scope.deleting 	= false;
			$scope.deleteType 	= type;
			$scope.deleteId 	= id;
        }
        
        function toDelete() 
        {
            $scope.deleting = true;
			
			var url = $scope.deleteType == 'event' ? 'deleteEvent' : 'delete';

			$http
			.get('/'+FOLDERADD+'/notifications/'+url+'/'+ $scope.deleteId)
			.success(function(res) {
				if (res != 'invalid') {
					notifications_message.setMessage(res.message);
					$scope.deleting = false;
					$scope.getNotifications();
					$('#confirm-delete').modal('hide');
				}
			});
        }

        function setModal(event) {
            $scope.event = event;
        }

        function getReport(event,option) {
            
                $scope.reportOption = option;
                $scope.loadingReport = true;

                $http({
                method 	: 'POST',
                url 	: '/'+FOLDERADD+'/event/getreportdata',
                data 	: {
                    eventId : event.eventId,
                    option  : $scope.reportOption
                }
                }).success(function(data) {
                    event.report = data;
                    for (var index = 0; index < event.report.length; index++) {
                        var element = event.report[index];
                        switch (element.turn) {
                            case 'm':
                                event.report[index].turn = 'Mañana';
                                break;
                            case 't':
                                event.report[index].turn = 'Tarde';
                                break;
                            case 'n':
                                event.report[index].turn = 'Noche';
                                break;
                            case 'f':
                                event.report[index].turn = 'Full Time';
                                break;                                                            
                        }
                    }
                    $scope.loadingReport = false;
                });
        }

        function sendEvent(id) {
            $http({
            method 	: 'GET',
            url 	: '/'+FOLDERADD+'/event/sendevent/'+id,
            }).success(function(data) {
                notifications_message.setMessage(data.message);
                $scope.getNotifications();
            });
        }

        $scope.getNotifications();
        $('[data-toggle="tooltip"]').tooltip();
    }

    CnotificationsCreate.$inject = ['$scope','nav','$location','$http','notifications_message'];
    function CnotificationsCreate($scope,nav,$location,$http,notifications_message) {
        nav.setSelected('notifications');
        $scope.notification = {};
        $scope.notification.title = '';
        $scope.saving = false;

        $scope.createNotification = function(){
            $scope.saving = true;
            if(!notificationIsComplete($scope.notification.title)){
                $http({
                    method  : 'POST',
                    url     : '/'+FOLDERADD+'/notifications/create',
                    data    : $scope.notification
                    })
                .success(function(data) {
                    if(data != 'invalid'){
                        notifications_message.setMessage(data.message);
                        $location.path('/notifications');
                    }
                });
            }
            else
            {
                $scope.saving = false;
            }
        }
    }
    
    CNotificationsEdit.$inject = ['$scope','nav','$http','notifications_message','$routeParams','$location'];
    function CNotificationsEdit($scope,nav,$http,notifications_message,$routeParams,$location) {
        $scope.notification = {};
        nav.setSelected('notifications');

        $http({
        method 	: 'GET',
        url 	: '/'+FOLDERADD+'/notifications/getNotification/'+$routeParams.id,
        })
        .then(function(res) {
            $scope.notification = res.data;
        });

        $scope.editNotification = function () {
            $scope.saving = true;
            
            if(!notificationIsComplete($scope.notification.title))
            {
                $http({
                method 	: 'POST',
                url 	: '/'+FOLDERADD+'/notifications/edit/'+$scope.notification.notificationId,
                data    : $scope.notification
                }).success(function(data) {
                    if(data != 'invalid')
                    {
                        notifications_message.setMessage(data.message);
                        $location.path('/notifications');
                    }
                });
            }
            $scope.saving = false;
        }

        
    }

    CEventsCreate.$inject = ['$scope','nav','$http','notifications_message','$routeParams','$location'];
    function CEventsCreate($scope,nav,$http,notifications_message,$routeParams,$location) {
        $scope.users = [];
        $scope.searchUsersUrl = "/"+FOLDERADD+'/Users/searchusers/';
        $scope.hasDate = false;
        $scope.divisions = [];
        $scope.checked = false;
        $scope.divisions[0] = 'Sections';
        $scope.divisions[1] = 'Sites';
        $scope.divisions[2] = 'Roles';
        $scope.search = "";
        $scope.appliedFilters = {
            sites               : [],
            divisions           : [],
            roles               : [],
            civilState          : null,
            teaming             : false,
            birthday_startDate  : null,
            birthday_endDate    : null,
            gender              : null,
            turn                : null
        };

        $scope.getUsers;
        
        nav.setSelected('notifications');

        $scope.changed = function (value) {
            $scope.appliedFilters.teaming = value;
            $scope.checked = value;
        }

        $(document).ready(function(){

            $('[role="date"]').datepicker({
                language	   : 'es',
                autoclose	   : true,
                todayHighlight : true, 
                todayBtn       : "linked",
                startDate      : 'getDate()'
            });
    
            $('#reminders').datepicker({
                language	   : 'es',
                multidate      : true,
            });
            $('#tooltip').tooltip();

            var setedStart = false;
            var setedEnd   = false;
    
            // SETEAR EL MAXIMO Y MINIMO DE LA FECHA EN LOS RECORDATORIOS
            function setMaxDate() {
                var datef = $('#endDate').datepicker('getDate');
                $("#reminders").datepicker("setEndDate", datef);
                var dates = datesInRange();
                $("#reminders").datepicker("setDates", dates);
                if($('#startDate').datepicker('getDate')>$('#endDate').datepicker('getDate')){
                    $('#startDate').datepicker('setDate',$('#endDate').datepicker('getDate'));
                }
                setedStart = true;
                show();
            }

            function setInitDate() {
                var datef = new Date($('#startDate').datepicker('getDate'));
                datef.setDate(datef.getDate() + 1);
                $("#reminders").datepicker("setStartDate", datef);
                var dates = datesInRange();
                $("#reminders").datepicker("setDates", dates);
                if($('#startDate').datepicker('getDate')>$('#endDate').datepicker('getDate')){
                    $('#endDate').datepicker('setDate',$('#startDate').datepicker('getDate'));
                }
                setedEnd = true;
                show();
            }

            function show() {
                if(setedStart && setedEnd)
                {
                    $scope.$apply(function() {
                        $scope.showReminder = true;
                    })
                }
            }

            function datesInRange() {
                var dates = $("#reminders").datepicker("getDates");
                var sDate = $("#startDate").datepicker("getDate");
                var eDate = $("#endDate").datepicker("getDate");

                var rDates = new Array();
                for (var i = 0; i < dates.length; i++) {
                    if((dates[i]>sDate) && dates[i] <= eDate)    
                        rDates[rDates.length] = new Date(dates[i]);
                }
                return rDates;
            }

            $('#endDate').on('changeDate',setMaxDate)
            $('#startDate').on('changeDate',setInitDate)
        });

        $scope.refresh = function () {
            setTimeout(function() {
                if($scope.addedFilters.length > 0){
                    $scope.applyFilters();
                }
                else{
                    $scope.$apply(function () {
                        $scope.users = [];
                    })
                }
            }, 500);
        }

        $scope.applyFilters = function () {
            $scope.appliedFilters.civilState    = (document.getElementsByName('civilState')[0]) ? document.getElementsByName('civilState')[0].value : null;
            $scope.appliedFilters.gender        = ((document.getElementById('genre')) && (document.getElementById('genre').value.length == 1)) ? $scope.appliedFilters.gender = document.getElementById('genre').value : null;
            $scope.appliedFilters.turn          = ((document.getElementById('turn')) && (document.getElementById('turn').value.length == 1)) ? $scope.appliedFilters.turn = document.getElementById('turn').value : null;

            var arrayOfSelected = [];
            angular.forEach(document.getElementsByName('Sites[]'),function (sites) {
                if(sites.checked)
                {   
                   arrayOfSelected.push(sites.value); 
                }
            })
            $scope.appliedFilters.sites = arrayOfSelected;
            arrayOfSelected = [];
            angular.forEach(document.getElementsByName('Sections[]'),function (section) {
                if(section.checked)
                {   
                   arrayOfSelected.push(section.value); 
                }
            })
            $scope.appliedFilters.divisions = arrayOfSelected;
            arrayOfSelected = [];
            angular.forEach(document.getElementsByName('Roles[]'),function (roles) {
                if(roles.checked)
                {   
                   arrayOfSelected.push(roles.value); 
                }
            })
            $scope.appliedFilters.roles      = arrayOfSelected;
            $scope.appliedFilters.birthday_startDate    = $('#start').val() ? $('#start').val() : null;
            $scope.appliedFilters.birthday_endDate      = $('#end').val() ? $('#end').val() : null;
            $scope.appliedFilters.teaming               = document.getElementsByName("fromTeaming").length > 0 ? $scope.checked : null;
            
            $http({
            method 	: 'POST',
            url 	: '/'+FOLDERADD+'/event/getusers',
            data 	: {
                sections   : $scope.appliedFilters.divisions,
                sites      : $scope.appliedFilters.sites,
                roles      : $scope.appliedFilters.roles,
                teaming    : $scope.appliedFilters.teaming,
                civilState : $scope.appliedFilters.civilState,
                genre      : $scope.appliedFilters.gender,
                turn       : $scope.appliedFilters.turn,
                startDate  : $scope.appliedFilters.birthday_startDate,
                endDate    : $scope.appliedFilters.birthday_endDate
            }
            }).success(function(data) {
                $scope.users = data;
            });
        }

        $scope.submitEvent = function(){
            var incomplete = false;
            $scope.saving = true;
            
            if(typeof String.prototype.trim !== 'function') {
                String.prototype.trim = function() {
                    return this.replace(/^\s+|\s+$/g, ''); 
                }
            }

            if ($('input[name="users[]"]').length <= 0) {
                incomplete = true;
            }

            if ($('#title').val().trim() == "" || ($('#startDate').val().trim() == "" || $('#endDate').val().trim() == "")) {incomplete=true;}


            if(incomplete)
            {
                $('#no-complete').modal('show');
                $scope.saving=false;
            }
            else
            {
                var usersSelected = [];
                var i = 0;
                angular.forEach(document.getElementsByName('users[]'),function (users) {
                        usersSelected[i]=users.value; 
                        i++;
                })

                var fd = new FormData();
                var files = document.getElementById('userfile').files[0];
                fd.append('userfile',files);
                fd.append('title',$scope.event.title);
                fd.append('startDate',$('#startDate').val());
                fd.append('endDate',$('#endDate').val());
                fd.append("users[]",usersSelected);
                fd.append('reminders',$('#reminders').val());
                
                $http.post('/'+FOLDERADD+'/event/create/'+$routeParams.id, fd, {                
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(data) {
                    if (data.status == 'ok')
                    {
                        notifications_message.setMessage(data.message, false);
                        $location.path('/notifications');
                    }
                    else
                    {
                        $scope.saving=false;
                        $('#no-complete').modal('show');
                    }
                    $scope.$apply()
                });
            }
        }
    }

    CEventEdit.$inject = ['$scope','nav','$http','notifications_message','$routeParams','$location'];
    function CEventEdit($scope,nav,$http,notifications_message,$routeParams,$location) {
        nav.setSelected('notifications');
        $scope.searchUsersUrl = "/"+FOLDERADD+'/Users/searchusers/';
        $scope.event = {};
        $scope.users = {};
        $scope.url = "/"+FOLDERADD+'/Users/searchusers/';
        $scope.today = false;
        $scope.checked = false;
        $scope.divisions = [];
        $scope.divisions[0] = 'Sections';
        $scope.divisions[1] = 'Sites';
        $scope.divisions[2] = 'Roles';
        $scope.appliedFilters = {
            sites               : [],
            divisions           : [],
            roles               : [],
            civilState          : null,
            teaming             : false,
            birthday_startDate  : null,
            birthday_endDate    : null,
            gender              : null,
            turn                : null
        };

        $scope.changed = function (value) {
            $scope.appliedFilters.teaming = value;
            $scope.checked = value;
        }

        $(document).ready(function(){
            $('#startDate').datepicker({
                language	    : 'es',
                autoclose	    : true,
                todayHighlight  : true, 
                todayBtn        : "linked"
            });
            $('#endDate').datepicker({
                language	    : 'es',
                autoclose	    : true,
                todayHighlight  : true, 
                todayBtn        : "linked",
                startDate       : 'getDate()'
            })
    
            $('#reminders').datepicker({
                language	   : 'es',
                multidate      : true
            });
    
            $('[role="tooltip"]').tooltip();

            $('#startDate').on('changeDate',setInitDate)
            $('#endDate').on('changeDate',setMaxDate)
            $("#userfile").change(function(){openFile(event)});


            function openFile(newFile) {
                var input = newFile.target;
                
                    var reader = new FileReader();
                    reader.onload = function(){
                      var dataURL = reader.result;
                      var output = document.getElementById('imgPrev');
                      output.src = dataURL;
                    };
                    reader.readAsDataURL(input.files[0]);
            }

        });
        
        function setMaxDate() {
            var datef = $('#endDate').datepicker('getDate');
            $("#reminders").datepicker("setEndDate", datef);
            var dates = datesInRange();
            $("#reminders").datepicker("setDates", dates);
            if($('#startDate').datepicker('getDate')>$('#endDate').datepicker('getDate')){
                $('#startDate').datepicker('setDate',$('#endDate').datepicker('getDate'));
            }
        }

        function setInitDate(today) {
            var datef;
            if(today == true){
                datef = new Date();
            }
            else{
                datef = new Date($('#startDate').datepicker('getDate'));
            }
            datef.setDate(datef.getDate() + 1);
            $("#reminders").datepicker("setStartDate", datef);
            var dates = datesInRange();
            $("#reminders").datepicker("setDates", dates);
            if($('#startDate').datepicker('getDate')>$('#endDate').datepicker('getDate')){
                $('#endDate').datepicker('setDate',$('#startDate').datepicker('getDate'));
            }
        }

        function datesInRange() {
            var dates = $("#reminders").datepicker("getDates");
            var sDate = $("#startDate").datepicker("getDate");
            var eDate = $("#endDate").datepicker("getDate");

            var rDates = new Array();
            for (var i = 0; i < dates.length; i++) {
                if((dates[i]>sDate) && dates[i] <= eDate)    
                    rDates[rDates.length] = new Date(dates[i]);
            }
            return rDates;
        }
        
        function getData() {
            $http({
                method	: "GET",
                url 	: "/"+FOLDERADD+"/event/geteventbyid/"+$routeParams.id
            }).success(function(data) {
                if (data != 'invalid') {
                    $scope.event = data;
                    $scope.users = data.users;
                }
                //asignando las fechas a los datepickers
                var startDate = $scope.event.startDate;
                var endDate = $scope.event.endDate;
                $("#startDate").datepicker("update",startDate);
                $("#endDate").datepicker("update",endDate);
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1;
                var yyyy = today.getFullYear();
                if(dd<10){
                    dd='0'+dd;
                } 
                if(mm<10){
                    mm='0'+mm;
                } 
                var today = dd+'-'+mm+'-'+yyyy;
                if(today >= startDate)
                {
                    $scope.today = true;
                    $('#startDate').prop('disabled',true);
                }
                setInitDate($scope.today);
                setMaxDate();
                var dates = $scope.event.eventReminders;
                $("#reminders").datepicker("setDates",dates);
            });
        }
        getData();

        $scope.refresh = function () {
            setTimeout(function() {
                if($scope.addedFilters.length > 0){
                    $scope.applyFilters();
                    
                }
                else{
                    $scope.$apply(function () {
                        $scope.users = [];
                    })
                }
            }, 500);
        }

        $scope.applyFilters = function () {
            $scope.appliedFilters.civilState    = (document.getElementsByName('civilState')[0]) ? document.getElementsByName('civilState')[0].value : null;
            $scope.appliedFilters.gender        = ((document.getElementById('genre')) && (document.getElementById('genre').value.length == 1)) ? $scope.appliedFilters.gender = document.getElementById('genre').value : null;
            $scope.appliedFilters.turn          = ((document.getElementById('turn')) && (document.getElementById('turn').value.length == 1)) ? $scope.appliedFilters.turn = document.getElementById('turn').value : null;

            var arrayOfSelected = [];
            angular.forEach(document.getElementsByName('Sites[]'),function (sites) {
                if(sites.checked)
                {   
                   arrayOfSelected.push(sites.value); 
                }
            })
            $scope.appliedFilters.sites = arrayOfSelected;
            arrayOfSelected = [];
            angular.forEach(document.getElementsByName('Sections[]'),function (section) {
                if(section.checked)
                {   
                   arrayOfSelected.push(section.value); 
                }
            })
            $scope.appliedFilters.divisions = arrayOfSelected;
            arrayOfSelected = [];
            angular.forEach(document.getElementsByName('Roles[]'),function (roles) {
                if(roles.checked)
                {   
                   arrayOfSelected.push(roles.value); 
                }
            })
            $scope.appliedFilters.roles      = arrayOfSelected;
            $scope.appliedFilters.birthday_startDate    = $('#start').val() ? $('#start').val() : null;
            $scope.appliedFilters.birthday_endDate      = $('#end').val() ? $('#end').val() : null;
            $scope.appliedFilters.teaming               = document.getElementsByName("fromTeaming").length > 0 ? $scope.checked : null;
            
            $http({
            method 	: 'POST',
            url 	: '/'+FOLDERADD+'/event/getusers',
            data 	: {
                sections   : $scope.appliedFilters.divisions,
                sites      : $scope.appliedFilters.sites,
                roles      : $scope.appliedFilters.roles,
                teaming    : $scope.appliedFilters.teaming,
                civilState : $scope.appliedFilters.civilState,
                genre      : $scope.appliedFilters.gender,
                turn       : $scope.appliedFilters.turn,
                startDate  : $scope.appliedFilters.birthday_startDate,
                endDate    : $scope.appliedFilters.birthday_endDate
            }
            }).success(function(data) {
                $scope.users = data;
            });

        }

        $scope.submitEvent = function(){
            var incomplete = false;
            $scope.saving = true;

            if(typeof String.prototype.trim !== 'function') {
                String.prototype.trim = function() {
                    return this.replace(/^\s+|\s+$/g, ''); 
                }
            }

            if ($('input[name="users[]"]').length <= 0) {
                incomplete = true;
            }

            if ($('#title').val().trim() == "" || $('#endDate').val().trim() == "") {incomplete=true;}
            if(!$scope.today){
                if($('#startDate').val().trim() == "")
                {
                    incomplete = true;
                }
            }
            if(incomplete)
            {
                $('#no-complete').modal('show');
                $scope.saving=false;
            }
            else
            {
                //enviar el valor de la fecha de inicio, ya que si está inhabilitado no lo recibe el servidor
                if($scope.today){
                    $('#startDate').prop('disabled',false);
                    var startDate = $scope.event.startDate;
                    $("#startDate").datepicker("update",startDate);
                }
                var usersSelected = [];
                var i = 0;
                angular.forEach(document.getElementsByName('users[]'),function (users) {
                        usersSelected[i]=users.value; 
                        i++;
                })

                var fd = new FormData();
                var files = document.getElementById('userfile').files[0];
                fd.append('userfile',files);
                fd.append('title',$scope.event.title);
                fd.append('startDate',$('#startDate').val());
                fd.append('endDate',$('#endDate').val());
                fd.append("users[]",usersSelected);
                fd.append('reminders',$('#reminders').val());
                
                $http.post('/'+FOLDERADD+'/event/edit/'+$routeParams.id, fd, {                
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                }).success(function(data) {
                    if (data.status == 'ok')
                    {
                        notifications_message.setMessage(data.message, false);
                        $location.path('/notifications');
                    }
                    else
                    {
                        $scope.saving=false;
                        $('#no-complete').modal('show');
                    }
                    $scope.$apply()
                });
            }
        }

    }
    function notificationIsComplete(title) {
        var incomplete = false;
        if(title == "" || !title) { incomplete = true;}

        if(incomplete){
            $('#no-complete').modal('show');
        }
        return incomplete;
    }
})();
