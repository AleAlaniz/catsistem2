(function() {
	'use strict';
	angular
	.module('catnet')
	.config(corporatedocsConfig)
	.factory('corporatedocs_message',corporatedocs_message);

	corporatedocsConfig.$inject = ['$routeProvider'];
	function corporatedocsConfig($routeProvider) {
		$routeProvider.
		when('/corporatedocs', {
			templateUrl: '/'+FOLDERADD+'/corporatedocs',
			controller: Ccorporatedocs
		}).
		when('/corporatedocs/create', {
			templateUrl: '/'+FOLDERADD+'/corporatedocs/create',
			controller: Ccorporatedocs_create
		}).
		when('/corporatedocs/createcategory', {
			templateUrl: '/'+FOLDERADD+'/corporatedocs/createcategory',
			controller: Ccorporatedocs_createcategory
		}).
		when('/corporatedocs/editcategory/:id', {
			templateUrl: function(params){ return '/'+FOLDERADD+'/corporatedocs/editcategory/'+params.id;},
			controller: Ccorporatedocs_editcategory
		}).
		when('/corporatedocs/edit/:id', {
			templateUrl: '/'+FOLDERADD+'/corporatedocs/edit',
			controller: Ccorporatedocs_edit
		});;
	}

	corporatedocs_message.$inject = ['$rootScope'];
	function corporatedocs_message($rootScope) {
		var data = {}
		data.message = '';
		data.setMessage = function(message, auto_update) {
			this.message = message;
			if (auto_update) {
				$rootScope.$broadcast('corporatedocs_message');
			}
		}
		return data;
	}
	
	Ccorporatedocs.$inject = ['$scope','nav', 'corporatedocs_message', '$http'];
	function Ccorporatedocs($scope,nav, corporatedocs_message, $http)
	{
		nav.setSelected('corporatedocs');
		$scope.message 		= corporatedocs_message;
		$scope.loading 		= true;
		$scope.categories 	= [];
		$scope.docs 		= [];
		$scope.setDelete 	= setDelete;

		$scope.getCorporateDocs = getCorporateDocs;
		$scope.setSelected 		= setSelected;
		$scope.checkView 		= checkView;
		$scope.getReport 		= getReport;
		$scope.toDelete 		= toDelete;

		$scope.reportOption = '';

		$scope.$on('corporatedocs_message', function () {
			$scope.message = corporatedocs_message;
		});

		$scope.$on("$destroy", function(){
			corporatedocs_message.setMessage('', false);
		});

		function setDelete(type, id) {
			$scope.deleting 	= false;
			$scope.deleteType 	= type;
			$scope.deleteId 	= id;
		}

		function getCorporateDocs() {
			$scope.loading 		= true;
			$scope.categories 	= [];	
			$scope.docs 		= [];	
			
			$http
			.get('/'+FOLDERADD+'/corporatedocs/getcorporatedocs')
			.success(function(res) {
				if (res.status == 'ok') {
					$scope.categories 	= res.categories;
					$scope.docs 		= res.docs;
					$scope.loading 		= false;
				}
			});
		}
		
		function setSelected(doc) {
			doc.report 				= null;
			$scope.loadingView 		= false;
			$scope.loadingReport	= false;
			$scope.doc 				= doc;
			$scope.reportOption = '';
		}

		function checkView(doc) {
			$scope.loadingView = true;

			$http
			.get('/'+FOLDERADD+'/corporatedocs/checkview/'+ doc.corporatedocId)
			.success(function(res) {
				if (res.status == 'ok') {
					doc.isView = 1;
					$scope.loadingView 	= false;
				}
			});
		}

		function getReport(doc,option) {

			if(option != $scope.reportOption)
			{
				$scope.reportOption = option;
				$scope.loadingReport = true;

				$http.get('/'+FOLDERADD+'/corporatedocs/getreport/'+ doc.corporatedocId+'/'+$scope.reportOption)
				.success(function(res) {
					if (res.status == 'ok') {
						doc.report = res.data;
						$scope.loadingReport 	= false;
					}
				});
			}


		}

		function toDelete() {
			$scope.deleting = true;
			
			var url = $scope.deleteType == 'category' ? 'deletecategory' : 'delete';

			$http
			.get('/'+FOLDERADD+'/corporatedocs/'+url+'/'+ $scope.deleteId)
			.success(function(res) {
				if (res != 'invalid') {
					corporatedocs_message.setMessage(res);
					$scope.deleting = false;
					$scope.getCorporateDocs();
					$('#confirm-delete').modal('hide');
				}
			});
		}

		$scope.getCorporateDocs();



		/*$('[role="delete"]').click(function(e) {
			var btn = $(this);
			$.ajax({
				method	: "GET",
				url 	: btn.data('url')
			}).done(function(data) {
				if (data != 'invalid') {
					$('#confirm-delete').modal('hide');
					corporatedocs_message.setMessage(data, true);
					$scope.initCtrl();
				}
			});
});*/
}

Ccorporatedocs_create.$inject = ['$scope','nav', '$location', 'corporatedocs_message'];
function Ccorporatedocs_create($scope, nav, $location, corporatedocs_message)
{
	nav.setSelected('corporatedocs');
	$scope.formData = {};
	$scope.formData.title = ''; 
	$scope.saving=false;

	if(typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, ''); 
		}
	}

	$scope.submitcorporatedocs = function () {

		var incomplete = false;
		var haveTo = false;
		$scope.saving=true;

		jQuery.each($('input[name="sections[]"]'), function(i, section) {
			if(section.checked)
			{
				haveTo = true;
				return;
			}
		});

		if (!haveTo) {
			haveTo = false;
			jQuery.each($('input[name="sites[]"]'), function(i, site) {
				if(site.checked)
				{
					haveTo = true;
					return;
				}
			});
			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="roles[]"]'), function(i, role) {
					if(role.checked)
					{
						haveTo = true;
						return;
					}
				});

				if (!haveTo) {
					haveTo = false;
					jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
						if(userGroup.checked)
						{
							haveTo = true;
							return;
						}
					});

					if (!haveTo) {
						haveTo = false;
						if ($('input[name="users[]"]').length <= 0) {
							incomplete = true;
						}
					}
				}

			}

		}

		if ($('#name').val().trim() == "" || $('#description').val().trim() == "") {incomplete=true;}

		if(incomplete)
		{
			$('#no-complete').modal('show');
			console.log('incompleto');
			$scope.saving=false;
		}
		else
		{

			$('#corporatedocsForm').ajaxSubmit({
				url : '/'+FOLDERADD+'/corporatedocs/create',
				dataType : 'json',
				success: function(data) {
					if (data.status == 'ok')
					{
						corporatedocs_message.setMessage(data.message, false);
						$location.path('/corporatedocs');
					}
					else
					{
						$scope.saving=false;
						$('#no-complete').modal('show');
					}
					$scope.$apply()
				}
			});
		}
	};

	$scope.finding = false;
	$scope.users = [];
	$scope.removeUser = function(i) {
		$scope.users.splice(i, 1);
	};
	$scope.addUser = function(i) {
		$scope.users.push($scope.userOptions[i]);
		$scope.userOptions.splice(i, 1);
	};
	$scope.userOptions = [];
	$scope.findLike = '';
	$scope.searchNumber = 0;
	$scope.searchActual = 0;
	$scope.searchUsers = function() {
		if($scope.findLike != '') 
		{
			$scope.finding = true;
			var thisSearch = $scope.searchNumber;
			$scope.searchNumber = $scope.searchNumber+1;
			$.ajax({
				method 	: "POST",
				url		: '/'+FOLDERADD+'/corporatedocs/searchusers',
				data 	: {'like': $scope.findLike}
			}).done(function (data) {
				if(data == 'invalid'){
					$(location).attr('href', '/'+FOLDERADD+'');
				}
				else if(data != 'empty' && thisSearch >= $scope.searchActual && $scope.findLike != '')
				{
					$scope.searchActual = thisSearch;
					$scope.$apply(function() {
						$scope.addUserOptions(data);
						$scope.finding = false;
					});
				}
				else if (thisSearch >= $scope.searchActual)
				{
					$scope.searchActual = thisSearch;
					$scope.$apply(function() {
						$scope.finding = false;
						$scope.userOptions = [];	
					});
				}
			});
		}
		else
		{
			$scope.userOptions = [];
			$scope.finding = false;
		}
	};

	$scope.addUserOptions = function (users) {
		try{
			users = $.parseJSON(users);
		}catch(e){
			return;
		}
		$scope.userOptions = [];
		jQuery.each(users, function(i, user) {
			var exist = false;
			jQuery.each($scope.users, function(i, userExist) {
				if (userExist.userId == user.userId) {exist = true;}
			});
			if (!exist) {$scope.userOptions.push(user);}
		});
	};

	$scope.selectClick = function (e) {
		$('.select-input', e.currentTarget).focus();
	};
}

Ccorporatedocs_createcategory.$inject = ['$scope','nav', '$location', 'corporatedocs_message',];
function Ccorporatedocs_createcategory($scope, nav, $location, corporatedocs_message)
{
	nav.setSelected('corporatedocs');
	$scope.categorydata = {};
	$scope.categorydata.name = '';
	$scope.createCategory = function() {
		var incomplete = false;
		if ($scope.categorydata.name == "") {incomplete=true;}

		if(incomplete)
		{
			$('#no-complete').modal('show');
		}
		else
		{
			$('#corporatedocs_createcategory').attr('disabled', 'disabled');
			$('#corporatedocs_createcategory').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
			$.ajax({
				method 	: "POST",
				url 	: "/"+FOLDERADD+"/corporatedocs/createcategory",
				data 	: $scope.categorydata
			}).done(function(data) {
				if (data != 'invalid') {
					corporatedocs_message.setMessage(data, false);
					$location.path('/corporatedocs');
					$scope.$apply()
				}
			});
		}
	}
}

Ccorporatedocs_editcategory.$inject = ['$scope','nav', '$location', 'corporatedocs_message', '$routeParams'];
function Ccorporatedocs_editcategory($scope, nav, $location, corporatedocs_message, $routeParams)
{
	nav.setSelected('corporatedocs');
	$scope.categorydata 		= null;
	$scope.editCategory = function() {
		var incomplete = false;
		if ($scope.categorydata.name == "" || typeof $scope.categorydata.name == "undefined") {incomplete=true;}

		if(incomplete)
		{
			$('#no-complete').modal('show');
		}
		else
		{
			$('#corporatedocs_editcategory').attr('disabled', 'disabled');
			$('#corporatedocs_editcategory').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
			$.ajax({
				method 	: "POST",
				url 	: "/"+FOLDERADD+"/corporatedocs/editcategory/"+$routeParams.id,
				data 	: $scope.categorydata
			}).done(function(data) {
				if (data != 'invalid') {
					corporatedocs_message.setMessage(data, false);
					$location.path('/corporatedocs');
					$scope.$apply()
				}
			});
		}
	}

	$.ajax({
		method	: "GET",
		url 	: "/"+FOLDERADD+"/corporatedocs/getcategory/"+$routeParams.id
	}).done(function(data) {
		if (data != 'invalid') {
			try{
				console.log(data);
				data = $.parseJSON(data);
				$scope.$apply(function() {
					$scope.categorydata = data;
				});

			}catch(e){
				return;
			}
		}
	});
}

Ccorporatedocs_edit.$inject = ['$scope','nav', '$location', 'corporatedocs_message', '$routeParams', 'tinyMCE'];
function Ccorporatedocs_edit($scope,nav, $location, corporatedocs_message, $routeParams, tinyMCE)
{
	nav.setSelected('corporatedocs');
	$scope.corporatedocdata = null;
	$scope.sections = [];
	$scope.sites = [];
	$scope.roles = [];

	$scope.users = [];
	$scope.removeUser = function(i) {
		$scope.users.splice(i, 1);
	};

	$scope.addUser = function(i) {
		$scope.users.push($scope.userOptions[i]);
		$scope.userOptions.splice(i, 1);
	};
	$scope.userOptions = [];
	$scope.findLike = '';
	$scope.searchNumber = 0;
	$scope.searchActual = 0;
	$scope.searchUsers = function() {
		if($scope.findLike != '') 
		{
			var thisSearch = $scope.searchNumber;
			$scope.searchNumber = $scope.searchNumber+1;
			$.ajax({
				method 	: "POST",
				url		: '/'+FOLDERADD+'/corporatedocs/searchusers',
				data 	: {'like': $scope.findLike}
			}).done(function (data) {
				if(data == 'invalid'){
					$(location).attr('href', '/'+FOLDERADD+'');
				}
				else if(data != 'empty' && thisSearch >= $scope.searchActual)
				{
					$scope.searchActual = thisSearch;
					$scope.$apply($scope.addUserOptions(data));
				}
				else if (thisSearch >= $scope.searchActual)
				{
					$scope.searchActual = thisSearch;
					$scope.$apply($scope.userOptions = []);
				}
			});
		}
		else
		{
			$scope.userOptions = [];
		}
	};

	$scope.addUserOptions = function (users) {
		try{
			users = $.parseJSON(users);
		}catch(e){
			return;
		}
		$scope.userOptions = [];
		jQuery.each(users, function(i, user) {
			var exist = false;
			jQuery.each($scope.users, function(i, userExist) {
				if (userExist.userId == user.userId) {exist = true;}
			});
			if (!exist) {$scope.userOptions.push(user);}
		});
	};

	$scope.selectClick = function (e) {
		$('.select-input', e.currentTarget).focus();
	};

	$.ajax({
		method	: "GET",
		url 	: "/"+FOLDERADD+"/corporatedocs/getcorporatedocsbyid/"+$routeParams.id
	}).done(function(data) {
		if (data != 'invalid') {
			try{
				data = $.parseJSON(data);
			}catch(e){
				return;
			}
			$scope.$apply(function() {
				jQuery.each(data.links, function(i, link){
					if (link.sectionId != null) {
						$scope.sections['id-'+link.sectionId] = true;
					}
					else if(link.siteId != null){
						$scope.sites['id-'+link.siteId] = true;

					}
					else if(link.roleId != null){
						$scope.roles['id-'+link.roleId] = true;
					}
					else if(link.userId != null){
						$scope.users.push({ completeName : link.name, userId : link.userId, userName : link.userName});
					}
				});

				$scope.corporatedocdata = {
					name 		: data.name,
					category 	: data.corporatedoccategoryId || '',
					description : data.description
				};

				$scope.required=(data.required == '1');
			});
		}
	});

	$scope.editCorporatedocs = function () {
		var incomplete = false;
		var haveTo = false;

		jQuery.each($('input[name="sections[]"]'), function(i, section) {
			if(section.checked)
			{
				haveTo = true;
				return;
			}
		});

		if (!haveTo) {
			haveTo = false;
			jQuery.each($('input[name="sites[]"]'), function(i, site) {
				if(site.checked)
				{
					haveTo = true;
					return;
				}
			});
			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="roles[]"]'), function(i, role) {
					if(role.checked)
					{
						haveTo = true;
						return;
					}
				});

				if (!haveTo) {
					haveTo = false;
					jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
						if(userGroup.checked)
						{
							haveTo = true;
							return;
						}
					});

					if (!haveTo) {
						haveTo = false;
						if ($('input[name="users[]"]').length <= 0) {
							incomplete = true;
						}
					}
				}

			}
		}
		if (!$scope.corporatedocdata.name || !$scope.corporatedocdata.description) {incomplete=true;}
		if(incomplete)
		{
			$('#no-complete').modal('show');
		}
		else
		{
			$('#corporatedocs_edit').attr('disabled', 'disabled');
			$('#corporatedocs_edit').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
			$('#editCorporatedocs').ajaxSubmit({
				url : '/'+FOLDERADD+'/corporatedocs/edit/'+$routeParams.id,
				success: function(data) {
					if (data != 'invalid') {
						corporatedocs_message.setMessage(data, false);
						$location.path('/corporatedocs');
						$scope.$apply()
					}
				}
			});
		}
	};
}
})();
