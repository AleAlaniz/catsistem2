(function() {
	'use strict';

	angular
	.module 	('catnet'			)
	.controller ('Cnav', Cnav		);

	Cnav.$inject = ['$scope','nav','$location', 'realTime', 'sugestboxService', 'newsletterService', 'pressService', 'chatsu', 'chatsm', '$http', '$rootScope', '$timeout', 'teamingAgreeService','sectionService'];
	function Cnav($scope, nav,$location, realTime, sugestboxService, newsletterService, pressService, chatsu, chatsm, $http, $rootScope, $timeout, teamingAgreeService,sectionService) {
		$scope.unread = {
			suggestbox 		: sugestboxService.unread,
			newsletter 		: newsletterService.unread,
			press 			: pressService.unread,
			chats 			: chatsu.unread + chatsm.unread,
			teamingforms	: teamingAgreeService.unread,
			section			: sectionService.unread
		};

		$scope.$on("accepted_suggestion", function() {
			console.log('eventoRecibido');
			$('#suggestionModal').modal('show');
		})

		$http
		.get('/'+FOLDERADD+'/home/getunread')
		.success(function(data) {
			if (data.status == 'ok') {
				if (permissions['suggestbox/view']) {
					sugestboxService.unread = data.Suggestbox;
					$rootScope.$broadcast("update_unread_suggestbox");
				}
				if (permissions['newsletter/index']) {
					newsletterService.unread = data.Newsletter;
					$rootScope.$broadcast("update_unread_newsletter");
				}
				if (permissions['press/index']) {
					pressService.unread = data.Press;
					$rootScope.$broadcast("update_unread_press");
				}
				if (permissions['chat/index']) {
					chatsu.unread = data.ChatU;
					$rootScope.$broadcast("update_unread_chatu");
				}
				if (permissions['chat/multi']) {
					chatsm.unread = data.ChatM;
					$rootScope.$broadcast("update_unread_chatm");
				}
				if(permissions['teaming/pendingforms']) {
					teamingAgreeService.unread = data.Forms;
					$rootScope.$broadcast("update_unread_forms");
				}
				if(permissions['sections/index']) {
					sectionService.unread = data.Section;
					$rootScope.$broadcast("update_unread_sections");
				}
			}
		})


		if (permissions['suggestbox/view']) {
			realTime.suscribe('unread_suggestbox');
			$scope.$on("update_unread_suggestbox", function() {
				$scope.unread.suggestbox = sugestboxService.unread;
			});
		}
		
		if (permissions['newsletter/index']) {
			$scope.$on("update_unread_newsletter", function() {
				$scope.unread.newsletter = newsletterService.unread;
			});
		}

		if (permissions['newsletter/create']) {
			realTime.suscribe('all_newsletter');
		}

		if (permissions['press/index']) {
			realTime.suscribe('press');
			$scope.$on("update_unread_press", function() {
				$scope.unread.press = pressService.unread;
			});
		}

		if (permissions['chat/index']) {
			$scope.$on("update_unread_chatu", function() {
				$scope.unread.chats = parseInt(chatsu.unread) + parseInt(chatsm.unread);
			});
		}
		
		if (permissions['chat/multi']) {
			$scope.$on("update_unread_chatm", function() {
				$scope.unread.chats = parseInt(chatsu.unread) + parseInt(chatsm.unread);
			});
			$scope.$on('chatm_delete', function(e,id) {
				chatsm.getUnread();
			});
		}

		if (permissions['teaming/pendingforms']) {
			realTime.suscribe('teaming');
			$scope.$on("update_unread_forms", function() {
				$scope.unread.teamingforms = teamingAgreeService.unread;
			});
		}

		if(permissions['sections/index']){
			realTime.suscribe('section');
			$scope.$on("update_unread_sections",function () {
				$scope.unread.section = sectionService.unread;
			});
		}

		realTime.suscribe('section'	, userData.sectionId 	);
		realTime.suscribe('role'	, userData.roleId 		);
		realTime.suscribe('site'	, userData.siteId 		);
		realTime.suscribe('user'	, userData.userId		);
		realTime.suscribe('campaign', userData.campaignId	);

		$scope.$on('updateselected', function () {
			$scope.navSelected = nav.selected;
		});

		$scope.$on('$routeChangeStart', function() {
			$('#layout_loading').show();
		});

		$scope.$on('$routeChangeSuccess', function() {
			$('#layout_loading').hide();
		});
	}
})();
