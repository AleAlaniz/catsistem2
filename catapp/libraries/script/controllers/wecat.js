(function() {
	'use strict';

	angular
	.module 	('catnet'			)
	.controller ('Cwecat', Cwecat	);

	Cwecat.$inject = ['$scope' ,'nav'];
	function Cwecat($scope, nav) {
		nav.setSelected('wecat');
	}
})();
