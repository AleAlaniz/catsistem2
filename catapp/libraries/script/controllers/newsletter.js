(function() {
	'use strict';
	angular
	.module('catnet')
	.config(NewsletterConfig)
	.factory('newsletter_message',newsletter_message)
	.factory('newsletterService',newsletterService)
	.controller('Cnewsletter',Cnewsletter)
	.controller('Cnewsletter_create',Cnewsletter_create)
	.controller('Cnewsletter_createcategory',Cnewsletter_createcategory)
	.controller('Cnewsletter_editcategory',Cnewsletter_editcategory)
	.controller('Cnewsletter_edit',Cnewsletter_edit);

	newsletterService.$inject = ['$http', '$rootScope'];
	function newsletterService($http, $rootScope) {
		var sv =  {
			unread 	: 0,
			lastTime : 0,
			getUnread 	: function() {
				$http
				.get('/'+FOLDERADD+'/newsletter/getunread')
				.success(function(data) {
					if (data.status == 'ok') {
						sv.unread = data.unread;
						$rootScope.$broadcast('update_unread_newsletter');
					}
				})
			}
		}
		return sv;
	}
	NewsletterConfig.$inject = ['$routeProvider'];
	function NewsletterConfig($routeProvider) {
		$routeProvider.
		when('/newsletter', {
			templateUrl: '/'+FOLDERADD+'/newsletter',
			controller: 'Cnewsletter'
		}).
		when('/newsletter/create', {
			templateUrl: '/'+FOLDERADD+'/newsletter/create',
			controller: 'Cnewsletter_create'
		}).
		when('/newsletter/createcategory', {
			templateUrl: '/'+FOLDERADD+'/newsletter/createcategory',
			controller: 'Cnewsletter_createcategory'
		}).
		when('/newsletter/editcategory/:id', {
			templateUrl: function(params){ return '/'+FOLDERADD+'/newsletter/editcategory/'+params.id;},
			controller: 'Cnewsletter_editcategory',
			cache : false
		}).
		when('/newsletter/edit/:id', {
			templateUrl: function(params){ return '/'+FOLDERADD+'/newsletter/edit/'+params.id;},
			controller: 'Cnewsletter_edit',
			cache : false
		});
	}
	newsletter_message.$inject = ['$rootScope'];
	function newsletter_message($rootScope) {
		var data = {}
		data.message = '';
		data.setMessage = function(message, auto_update) {
			this.message = message;
			if (auto_update) {
				$rootScope.$broadcast('newsletter_message');
			}
		}
		return data;
	}
	Cnewsletter.$inject = ['$scope','nav', 'newsletter_message', 'newsletterService'];
	function Cnewsletter($scope,nav, newsletter_message, newsletterService)
	{
		nav.setSelected('newsletter');
		$scope.message = newsletter_message;
		$scope.$on('newsletter_message', function () {
			$scope.message = newsletter_message;
		});
		$scope.$on("$destroy", function(){
			newsletter_message.setMessage('', false);
		});

		$scope.categories = [];

		$('#confirm-delete').on('show.bs.modal', function (event) {
			var button 	= $(event.relatedTarget) 
			var type 	= button.data('type')
			var id 		= button.data('id')
			var modal 	= $(this)

			if (type == 'category') {
				modal.find('[role="message"]').text($scope.deletecategory);
				modal.find('[role=delete]').data('url', '/'+FOLDERADD+'/newsletter/deletecategory/'+id);
			}
			else if (type == 'newsletter')
			{
				modal.find('[role="message"]').text($scope.deletenewsletter);
				modal.find('[role="delete"]').data('url','/'+FOLDERADD+'/newsletter/delete/'+id);
			}
		});

		$scope.initCtrl = function() {
			$('[role="loading"]').show();
			$('[role="containt"]').hide();
			$scope.categories = [];
			$.ajax({
				method	: "GET",
				url 	: "/"+FOLDERADD+"/newsletter/getnewsletter"
			}).done(function(data) {
				if (data != 'invalid') {
					try{
						data = $.parseJSON(data);
						newsletterService.getUnread();
						$scope.$apply(function() {
							$scope.categories = data;
						});
						$('[role=toggle-body]').click(function() {
							$(this).next().slideToggle();
						});
					}catch(e){
						return;
					}

					$('[role="loading"]').hide();
					$('[role="containt"]').show();
				}
			});
		};

		$scope.initCtrl();

		$('[role="delete"]').click(function(e) {
			var btn = $(this);
			$.ajax({
				method	: "GET",
				url 	: btn.data('url')
			}).done(function(data) {
				if (data != 'invalid') {
					$('#confirm-delete').modal('hide');
					newsletter_message.setMessage(data, true);
					$scope.initCtrl();
				}
			});
		});
	}
	Cnewsletter_create.$inject = ['$scope','nav', '$location', 'newsletter_message', 'tinyMCE', 'realTime','newCommunication', '$http'];
	function Cnewsletter_create($scope, nav, $location, newsletter_message, tinyMCE, realTime, newCommunication, $http)
	{
		nav.setSelected('newsletter');
		tinyMCE.init('#newsletter');

		$scope.teamingMessageBody;
		$scope.communicationToTeamingUser = false;

		var teamingNewsletter = false;

		$scope.newsletterdata = {
			color : 'info'
		};

		if(typeof String.prototype.trim !== 'function') {
			String.prototype.trim = function() {
				return this.replace(/^\s+|\s+$/g, ''); 
			}
		}

		$scope.submitNewsletter = function () {
			var incomplete = false;
			var haveTo = false;

			jQuery.each($('input[name="sections[]"]'), function(i, section) {
				if(section.checked)
				{
					haveTo = true;
					return;
				}
			});

			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="sites[]"]'), function(i, site) {
					if(site.checked)
					{
						haveTo = true;
						return;
					}
				});
				if (!haveTo) {
					haveTo = false;
					jQuery.each($('input[name="roles[]"]'), function(i, role) {
						if(role.checked)
						{
							haveTo = true;
							return;
						}
					});

					if (!haveTo) {
						haveTo = false;
						jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
							if(userGroup.checked)
							{
								haveTo = true;
								return;
							}
						});

						if (!haveTo) {
							haveTo = false;
							if ($('input[name="users[]"]').length <= 0) {
								incomplete = true;
							}
						}
					}

				}

			}

			if ($('input[name="title"]').val().trim() == "" || $('#newsletter_ifr').contents().find('#tinymce').html() == '<p><br data-mce-bogus="1"></p>') {incomplete=true;}

			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				if($scope.communicationToTeamingUser == true){
					$scope.teamingMessageBody=  {"type"			: "newsletters",
												"colSearch"		: "userId", 
												"colResult"		: "newsletterId",
												"user"			: $scope.users,
												"data"			: $('input[name="title"]').val().trim()};
				}

				$('#newsletter_create').attr('disabled', 'disabled');
				$('#newsletter_create').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
				$('#newsletterForm').ajaxSubmit({
					url : '/'+FOLDERADD+'/newsletter/create',
					data: {"teamingData": $scope.teamingMessageBody},
					dataType : 'json',
					success: function(data) {
						if (data.status == 'ok') {

							realTime.socket.emit('new_newsletter', data.to);
							newsletter_message.setMessage(data.message, false);
							$location.path('/newsletter');
							$scope.$apply()
						}
					}
				});
			}
		};

		$scope.finding = false;
		$scope.users = [];
		
		var selected = newCommunication.getMessage();
		var keys = Object.keys(selected).length;

		if (keys > 0){
			$scope.communicationToTeamingUser = true;
			$scope.newsletterdata = {
				color : 'success'
			};
			var users = selected.data;
			teamingNewsletter = true;

			for (var i = 0; i < users.length; i++) {
				if (users[i].value == true){
					$scope.users.push({
						completeName : users[i].userName,
						userId : users[i].userId,
						userName : users[i].userName
					});
				}				
			}
		}

		newCommunication.setMessage({});
		
		$scope.removeUser = function(i) {
			$scope.users.splice(i, 1);
		};
		$scope.addUser = function(i) {
			$scope.users.push($scope.userOptions[i]);
			$scope.userOptions.splice(i, 1);
		};
		$scope.userOptions = [];
		$scope.findLike = '';
		$scope.searchNumber = 0;
		$scope.searchActual = 0;
		$scope.searchUsers = function() {
			if($scope.findLike != '') 
			{
				$scope.finding = true;
				var thisSearch = $scope.searchNumber;
				$scope.searchNumber = $scope.searchNumber+1;
				$.ajax({
					method 	: "POST",
					url		: '/'+FOLDERADD+'/newsletter/searchusers',
					data 	: {'like': $scope.findLike}
				}).done(function (data) {
					if(data == 'invalid'){
						$(location).attr('href', '/'+FOLDERADD+'');
					}
					else if(data != 'empty' && thisSearch >= $scope.searchActual && $scope.findLike != '')
					{
						$scope.searchActual = thisSearch;
						$scope.$apply(function() {
							$scope.addUserOptions(data);
							$scope.finding = false;
						});
					}
					else if (thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply(function() {
							$scope.finding = false;
							$scope.userOptions = [];	
						});
					}
				});
			}
			else
			{
				$scope.userOptions = [];
				$scope.finding = false;
			}
		};

		$scope.addUserOptions = function (users) {
			try{
				users = $.parseJSON(users);
			}catch(e){
				return;
			}
			$scope.userOptions = [];
			jQuery.each(users, function(i, user) {
				var exist = false;
				jQuery.each($scope.users, function(i, userExist) {
					if (userExist.userId == user.userId) {exist = true;}
				});
				if (!exist) {$scope.userOptions.push(user);}
			});
		};

		$scope.selectClick = function (e) {
			$('.select-input', e.currentTarget).focus();
		};
	}
	Cnewsletter_createcategory.$inject = ['$scope','nav', '$location', 'newsletter_message',];
	function Cnewsletter_createcategory($scope,nav, $location, newsletter_message)
	{
		nav.setSelected('newsletter');
		$scope.categorydata = {};
		$scope.categorydata.color = 'info';
		$scope.categorydata.name = '';
		$scope.createCategory = function() {
			var incomplete = false;
			if ($scope.categorydata.name == "" ||$scope.categorydata.color == "") {incomplete=true;}

			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$('#newsletter_createcategory').attr('disabled', 'disabled');
				$('#newsletter_createcategory').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
				$.ajax({
					method 	: "POST",
					url 	: "/"+FOLDERADD+"/newsletter/createcategory",
					data 	: $scope.categorydata
				}).done(function(data) {
					if (data != 'invalid') {
						newsletter_message.setMessage(data, false);
						$location.path('/newsletter');
						$scope.$apply()
					}
				});
			}
		}
	}
	Cnewsletter_editcategory.$inject = ['$scope','nav', '$location', 'newsletter_message', '$routeParams'];
	function Cnewsletter_editcategory($scope,nav, $location, newsletter_message, $routeParams)
	{
		nav.setSelected('newsletter');
		$scope.categorydata 		= null;
		$scope.editCategory = function() {
			var incomplete = false;
			if ($scope.categorydata.name == "" || typeof $scope.categorydata.name == "undefined" || $scope.categorydata.color == "") {incomplete=true;}

			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$('#newsletter_editcategory').attr('disabled', 'disabled');
				$('#newsletter_editcategory').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
				$.ajax({
					method 	: "POST",
					url 	: "/"+FOLDERADD+"/newsletter/editcategory/"+$routeParams.id,
					data 	: $scope.categorydata
				}).done(function(data) {
					if (data != 'invalid') {
						newsletter_message.setMessage(data, false);
						$location.path('/newsletter');
						$scope.$apply()
					}
				});
			}
		}
		$.ajax({
			method	: "GET",
			url 	: "/"+FOLDERADD+"/newsletter/getcategory/"+$routeParams.id
		}).done(function(data) {
			if (data != 'invalid') {
				try{
					data = $.parseJSON(data);
					$scope.$apply(function() {
						$scope.categorydata = data;
					});

				}catch(e){
					return;
				}
			}
		});
	}
	Cnewsletter_edit.$inject = ['$scope','nav', '$location', 'newsletter_message', '$routeParams', 'tinyMCE', '$timeout'];
	function Cnewsletter_edit($scope,nav, $location, newsletter_message, $routeParams, tinyMCE, $timeout)
	{
		nav.setSelected('newsletter');
		tinyMCE.init('#newsletter');
		
		$scope.newsletterdata = null;
		$scope.sections = [];
		$scope.sites 	= [];
		$scope.roles 	= [];
		$scope.users 	= [];

		$scope.removeUser = function(i) {
			$scope.users.splice(i, 1);
		};

		$scope.addUser = function(i) {
			$scope.users.push($scope.userOptions[i]);
			$scope.userOptions.splice(i, 1);
		};
		$scope.userOptions 	= [];
		$scope.findLike 	= '';
		$scope.searchNumber = 0;
		$scope.searchActual = 0;

		$scope.searchUsers = function() {
			if($scope.findLike != '') 
			{
				var thisSearch = $scope.searchNumber;
				$scope.searchNumber = $scope.searchNumber+1;
				$.ajax({
					method 	: "POST",
					url		: '/'+FOLDERADD+'/newsletter/searchusers',
					data 	: {'like': $scope.findLike}
				}).done(function (data) {
					if(data == 'invalid'){
						$(location).attr('href', '/'+FOLDERADD+'');
					}
					else if(data != 'empty' && thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply($scope.addUserOptions(data));
					}
					else if (thisSearch >= $scope.searchActual)
					{
						$scope.searchActual = thisSearch;
						$scope.$apply($scope.userOptions = []);
					}
				});
			}
			else
			{
				$scope.userOptions = [];
			}
		};

		$scope.addUserOptions = function (users) {
			try{
				users = $.parseJSON(users);
			}catch(e){
				return;
			}
			$scope.userOptions = [];
			jQuery.each(users, function(i, user) {
				var exist = false;
				jQuery.each($scope.users, function(i, userExist) {
					if (userExist.userId == user.userId) {exist = true;}
				});
				if (!exist) {$scope.userOptions.push(user);}
			});
		};

		$scope.selectClick = function (e) {
			$('.select-input', e.currentTarget).focus();
		};


		$.ajax({
			method	: "GET",
			url 	: "/"+FOLDERADD+"/newsletter/getnewsletterbyid/"+$routeParams.id
		}).done(function(data) {
			if (data != 'invalid') {
				try{
					data = $.parseJSON(data);
				}catch(e){
					return;
				}
				$scope.$apply(function() {
					jQuery.each(data.links, function(i, link){
						if (link.sectionId != null) {
							$scope.sections['id-'+link.sectionId] = true;
						}
						else if(link.siteId != null){
							$scope.sites['id-'+link.siteId] = true;

						}
						else if(link.roleId != null){
							$scope.roles['id-'+link.roleId] = true;
						}
						else if(link.userId != null){
							$scope.users.push({ completeName : link.name, userId : link.userlinkId, userName : link.userName});
						}
					});

					$scope.newsletterdata = {
						title 		: data.title,
						category 	: data.newslettercategoryId,
						newsletter 	: data.newsletter,
						color 		: data.color
					};

					$timeout(function() { 
						tinymce.get('newsletter').setContent(data.newsletter);
						$('#editNewsletter').show();
					});
					
				});
			}
		});

		$scope.editNewsletter = function () {
			var incomplete = false;
			var haveTo = false;

			jQuery.each($('input[name="sections[]"]'), function(i, section) {
				if(section.checked)
				{
					haveTo = true;
					return;
				}
			});

			if (!haveTo) {
				haveTo = false;
				jQuery.each($('input[name="sites[]"]'), function(i, site) {
					if(site.checked)
					{
						haveTo = true;
						return;
					}
				});
				if (!haveTo) {
					haveTo = false;
					jQuery.each($('input[name="roles[]"]'), function(i, role) {
						if(role.checked)
						{
							haveTo = true;
							return;
						}
					});

					if (!haveTo) {
						haveTo = false;
						jQuery.each($('input[name="userGroups[]"]'), function(i, userGroup) {
							if(userGroup.checked)
							{
								haveTo = true;
								return;
							}
						});

						if (!haveTo) {
							haveTo = false;
							if ($('input[name="users[]"]').length <= 0) {
								incomplete = true;
							}
						}
					}

				}
			}
			if ($('input[name="title"]').val().trim() == "" || tinymce.get('newsletter').getContent() == '') {incomplete=true;}
			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$('#newsletter_create').attr('disabled', 'disabled');
				$('#newsletter_create').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
				$('#editNewsletter').ajaxSubmit({
					url : '/'+FOLDERADD+'/newsletter/edit/'+$routeParams.id,
					success: function(data) {
						if (data != 'invalid') {
							newsletter_message.setMessage(data, false);
							$location.path('/newsletter');
							$scope.$apply()
						}
					}
				});
			}
		};
	}
})();
