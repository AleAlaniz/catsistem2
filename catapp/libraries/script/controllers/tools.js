(function() {
	'use strict';

	angular
	.module 	('catnet'								)
	.config 	(ToolsConfig 							)
	.controller ('Ctools'			, Ctools			)
	.controller ('CNotes'			, CNotes			)
	.controller ('CFaq'				, CFaq			    )
	.controller ('CAnalitycs'		, CAnalitycs 		)
	.controller ('CAnalitycs_users'	, CAnalitycs_users  )
	.controller ('CAnalitycs_online', CAnalitycs_online )
	.controller ('CAnalitycs_login', CAnalitycs_login   );

	ToolsConfig.$inject = ['$routeProvider'];
	function ToolsConfig($routeProvider) {
		$routeProvider
		.when('/tools',{
			templateUrl:'/'+FOLDERADD+'/tools',
			controller:'Ctools'
		})
		.when('/tools/notes', {
			templateUrl	: '/'+FOLDERADD+'/tools/notes',
			controller 	: 'CNotes'
		})
		.when('/tools/analitycs', {
			templateUrl	: '/'+FOLDERADD+'/tools/analitycs',
			controller 	: 'CAnalitycs'
		})
		.when('/tools/analitycsusers', {
			templateUrl	: '/'+FOLDERADD+'/tools/analitycsusers',
			controller 	: 'CAnalitycs_users'
		})
		.when('/tools/analitycsonline', {
			templateUrl	: '/'+FOLDERADD+'/tools/analitycsonline',
			controller 	: 'CAnalitycs_online'
		})
		.when('/tools/analitycslogin', {
			templateUrl	: '/'+FOLDERADD+'/tools/analitycslogin',
			controller 	: 'CAnalitycs_login'
		})
		.when('/tools/faq',{
			templateUrl : '/'+FOLDERADD+'/tools/faq',
			controller  : 'CFaq'
		});
	}

	Ctools.$inject = ['$scope' ,'nav','$http'];
	function Ctools($scope, nav,$http) {
		nav.setSelected('tools');
	}

	CNotes.$inject = ['$scope' ,'nav'];
	function CNotes($scope, nav) {
		nav.setSelected('tools');

		var originalpositions;

		$("#stack").on("click",function(){
			originalpositions = [];
			jQuery.each($("[note].note"), function(i, note) {
				note = $(note);
				originalpositions.push({ 
					'note' : note.attr('note'),
					'x' : note.css('left'),
					'y' : note.css('top')
				});

			});
			$(".note").css({'left':'15px','top':'15px'});
			$("[role=revert]").fadeIn(500);
			setTimeout(function() {$("[role=revert]").fadeOut(1000)}, 2000);
		});

		$("[role=revert]").click(function() {
			jQuery.each(originalpositions, function(i, note) {
				$("[note='"+note.note+"']").css({'left': note.x, 'top': note.y});
			});
			$(this).fadeOut(100);
		});


		$('#nav_tools').addClass('active');
		var noteWidth = 150, noteHeight = 170;

		var a = 3;

		function restartProperties(first)
		{
			var notes;
			if (first) {
				notes = $(".sticky-note").last();
			}
			else
			{
				notes = $(".sticky-note");
			}

			notes.draggable({
				containment: ".sticky-note-box",
				scroll: true,
				stop: function(e,i){
					var thisNote = $(this);
					var dataAjax = {
						axisX: i.position.left,
						axisY: i.position.top,
						note: parseInt(thisNote.attr('note')),	
					};

					$.ajax({
						method: "POST",
						url: '/'+FOLDERADD+'/tools/savenoteposition',
						data: dataAjax,
						beforeSend: function()
						{
							$("#saving").show(); 

						}
					}).done(function (data)
					{
						$("#saving").hide();
						if(data == 'invalid'){
							$(location).attr('href', '/'+FOLDERADD);
						}
						else if(data == 'done')
						{	
						}
					});
				},
				start: function(event, ui)
				{
					$(this).css("z-index", a++);
				}
			}).resizable({
				containment: ".sticky-note-box",
				minHeight: 45,
				minWidth: 150,
				stop:function(e,i)
				{
					var thisNote = $(this);
					var dataAjax = {
						note: parseInt(thisNote.attr('note')),
						width: i.helper.context.clientWidth,
						height: i.helper.context.clientHeight,
					};
					$.ajax({
						method: "POST",
						url: '/'+FOLDERADD+'/tools/savenotesize',
						data: dataAjax,
						beforeSend: function()
						{
							$("#saving").show(); 

						}
					}).done(function (data)
					{
						$("#saving").hide();

						if(data == 'invalid'){
							$(location).attr('href', '/'+FOLDERADD);
						}
						else if(data == 'done')
						{	
						}
					});

				},
				start: function(event, ui)
				{
					$(this).css("z-index", a++);
				}
			});

			$('.savenote', notes).click(function(e) {
				var thisNote = $(this);
				var dataAjax = {
					note: parseInt(thisNote.parent().attr('note')),
					text: thisNote.parent().find('textarea').first().val()
				};

				$.ajax({
					method: "POST",
					url: '/'+FOLDERADD+'/tools/savenote',
					data: dataAjax,
					beforeSend: function()
					{
						$("#saving").show(); 
					}
				}).done(function (data) {
					$("#saving").hide(); 
					if(data == 'invalid'){
						$(location).attr('href', '/'+FOLDERADD);
					}
				});
			});

			$('.deletenote').click(function(e) {
				$('#confirm-delete').find('[role=delete]').attr('note', $(this).parent().attr('note'));
				$('#confirm-delete').modal('show');
			});

			if (first) {
				notes.fadeIn();
			}
		}

		
		$('#confirm-delete [role=delete]').click(function (e) {
			var thisNote = $(this);
			$("[note='"+thisNote.attr('note')+"'].sticky-note .fa-trash").addClass('fa-pulse fa-spinner').removeClass('fa-trash');
			$('#confirm-delete').modal('hide');

			$.ajax({
				method: "POST",
				url: '/'+FOLDERADD+'/tools/deletenote',
				data: {note: parseInt(thisNote.attr('note'))},
				beforeSend: function()
				{
					$("#saving").show(); 
				}
			}).done(function (data) {
				$("#saving").hide(); 

				if(data == 'invalid'){
					$(location).attr('href', '/'+FOLDERADD);
				}
				else if(data == 'done')
				{	
					$("[note='"+thisNote.attr('note')+"'].sticky-note").fadeOut();
				}
				else
				{
					$("[note='"+thisNote.attr('note')+"'].sticky-note .fa-pulse").addClass('fa-trash').removeClass('fa-pulse fa-spinner');
				}
			});
		});


		$.ajax({
			method 	: 'GET',
			url 	: '/'+FOLDERADD+'/tools/getnotes',
			dataType: 'json'
		}).done(function (data) {
			if (data.status == 'ok') {
				for (var i = 0; i < data.notes.length; i++) {	
					var newNote=$('<div  note="'+data.notes[i].stickynoteId+'" class="sticky-note sticky-note-'+data.notes[i].color+' note"><span style="margin-top:5px;margin-right:30px" class="sticky-note-close savenote"><i class="fa fa-floppy-o"></i></span><span style="margin-top:5px" id="savenote" class="sticky-note-close deletenote"><i class="fa fa-trash"></i></span><div class="sticky-note-body note"><textarea class="sticky-note-'+data.notes[i].color+'">'+data.notes[i].note+'</textarea></div></div>');
					newNote.css({
						'top'	: data.notes[i].axisY+'px' ,
						'left' 	: data.notes[i].axisX+'px',
						'width'	: data.notes[i].width+'px',
						'height': data.notes[i].height+'px'
					});
					newNote.fadeIn();
					$('.sticky-note-box').append(newNote);
				}

				restartProperties(false);
			}
		});

		$("#addNote a").on("click",function()
		{
			var color=$(this).attr('color');

			$.ajax({
				method: "POST",
				url: '/'+FOLDERADD+'/tools/createnote',
				data: {'color':color},
				beforeSend: function()
				{
					$("#saving").show(); 
				}
			}).done(function (data) {
				$("#saving").hide(); 
				if(data == 'invalid'){
					$(location).attr('href', '/'+FOLDERADD);
				}
				else
				{	
					var newNote = $('<div  note="'+data+'" class="sticky-note sticky-note-'+color+' note"  style="display:none;"><span style="margin-top:5px;margin-right:30px" class="sticky-note-close savenote"><i class="fa fa-floppy-o"></i></span><span style="margin-top:5px" id="savenote" class="sticky-note-close deletenote"><i class="fa fa-trash"></i></span><div class="sticky-note-body note"><textarea class="sticky-note-'+color+'"></textarea></div></div>');
					$('.sticky-note-box').append(newNote);
					restartProperties(true);
				}
				
			});
		});
	}

	CAnalitycs.$inject = ['$scope' ,'nav', '$http'];
	function CAnalitycs($scope, nav, $http) {
		nav.setSelected('tools');
		$scope.sending 	= false;
		$scope.width 	= 1000;
		$scope.height 	= 500;
		$scope.divisions = 'Sites';
		
		$scope.getData 	= getData;

		function getData() {

			
			if ($scope.date == "" || $scope.width == "" ||$scope.height == "" || $scope.width == null ||$scope.height == null || $('input[name="Sites[]"]:checked').length == 0)
			{
				$('#no-complete').modal('show');
			}
			else
			{

				$scope.sending = true;
				var sitesSelected = new Array();
				angular.forEach(document.getElementsByName('Sites[]'),function (sites) {
					if(sites.checked)
					{   
						sitesSelected.push(sites.value);
					}
				})
				$http
				.post('/'+FOLDERADD+'/tools/getdiarydata', {
					date	: $scope.date,
					sites	: sitesSelected
				})
				.success(function(data) {
					if (data.status == 'ok') {
						$scope.data = data.data;
						var cht 			= document.createElement('canvas');
						cht.style.width 	= $scope.width+'px';
						cht.style.height 	= $scope.height+'px';

						var Cdata = {
							labels 		: data.data.names,
							datasets 	: [{
								fillColor 		: "rgba(151,187,205,0.5)",
								strokeColor 	: "rgba(151,187,205,0.8)",
								highlightFill 	: "rgba(151,187,205,0.75)",
								highlightStroke : "rgba(151,187,205,1)",
								data 			: data.data.values
							}]
						};
						var chartBox = document.getElementById("chart");
						while (chartBox.firstChild) {
							chartBox.removeChild(chartBox.firstChild);
						}
						chartBox.appendChild(cht);
						var ctx = cht.getContext("2d");
						new Chart(ctx).Line(Cdata);
					}
					$scope.sending = false;
				});
			}	
		}

		$('.input-date').datepicker({language: 'es',autoclose: true});
	}

	CAnalitycs_users.$inject = ['$scope' ,'nav', '$http'];
	function CAnalitycs_users($scope, nav, $http) {
		nav.setSelected('tools');
		$scope.sending 	= false;
		$scope.width 	= 1000;
		$scope.height 	= 500;
		$scope.divisions = 'Sites';

		$scope.getData 	= getData;

		function getData() {
			if ($scope.dateStart == "" ||$scope.dateFinish == "" || $scope.width == "" ||$scope.height == "" || $scope.width == null ||$scope.height == null || $('input[name="Sites[]"]:checked').length == 0)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$scope.sending = true;
				var sitesSelected = new Array();
				angular.forEach(document.getElementsByName('Sites[]'),function (sites) {
					if(sites.checked)
					{   
						sitesSelected.push(sites.value);
					}
				})
				$http
				.post('/'+FOLDERADD+'/tools/getusersdata', {
					dateStart	: $scope.dateStart,
					dateFinish	: $scope.dateFinish,
					sites		: sitesSelected
				})
				.success(function(data) {
					if (data.status == 'ok') {
						$scope.data = data.data;
						var cht 			= document.createElement('canvas');
						cht.style.width 	= $scope.width+'px';
						cht.style.height 	= $scope.height+'px';
						var Cdata = {
							labels 		: data.data.names,
							datasets 	: [{
								fillColor 		: "rgba(151,187,205,0.5)",
								strokeColor 	: "rgba(151,187,205,0.8)",
								highlightFill 	: "rgba(151,187,205,0.75)",
								highlightStroke : "rgba(151,187,205,1)",
								data 			: data.data.values
							}]
						};
						var chartBox = document.getElementById("chart");
						while (chartBox.firstChild) {
							chartBox.removeChild(chartBox.firstChild);
						}
						chartBox.appendChild(cht);
						var ctx = cht.getContext("2d");
						new Chart(ctx).Bar(Cdata);
					}
					$scope.sending = false;
				});
			}	
		}
		$('.input-daterange input').datepicker({language: 'es',autoclose: true});
	}

	CAnalitycs_online.$inject = ['$scope' ,'nav', 'realTime','$http'];
	function CAnalitycs_online($scope, nav, realTime,$http) {
		nav.setSelected('tools');
		$scope.onlineUsers = 0;
		$scope.count = [];

		$scope.$on("analitycs_online_change", analitycs_online_change);
		$scope.$on("$destroy", destroy);

		function analitycs_online_change(e, n) {
			$scope.onlineUsers = n.total;
			getSitesCount(n.userIds);
			$scope.$apply();
		}

		function destroy() {
			realTime.unsuscribe('analitycs_online');
		}
		realTime.suscribe('analitycs_online');

		function getSitesCount(usersIds) {
			$http({
			method 	: 'POST',
			url 	: '/'+FOLDERADD+'/Users/getusersbysite',
			data 	: {usersIds : usersIds}
			}).success(function(data) {
				$scope.count = data;
			});
		}
		
	}



	CAnalitycs_login.$inject = ['$scope' ,'nav', '$http', '$filter'];
	function CAnalitycs_login($scope, nav, $http, $filter) {
		nav.setSelected('tools');

		$scope.searchUsersUrl = "/"+FOLDERADD+'/Users/searchusers/';
		$scope.users = [];
		$scope.sending 	= false;
		$scope.someCheked = true;

		$scope.sites 	= [];
		$scope.getData 	= getData;
		$scope.data 	= {
			date 	: null,
			users 	: null
		};
			
		$scope.change = function () {
			if($filter('filter')($scope.sites , {active : true}).length > 0){
				$scope.someChecked = true;
			}
			else{
				$scope.someChecked = false;
			}
		}
		function getData() {
			var incomplete = false;
			
			if (!$scope.dateStart && !$scope.dateFinish) {
				incomplete = true;
			}

			if($filter('filter')($scope.sites , {active : true}).length == 0 && $('input[name="users[]"]').length <= 0){
				incomplete = true;
			}
			
			if(incomplete){
				$('#no-complete').modal('show');
			}
			else
			{
				var sites = [], sitesObject = $filter('filter')($scope.sites , {active : true});

				for (var i = 0; i < sitesObject.length; i++) {
					sites.push(sitesObject[i].siteId);
				}

				var userSelected = [];
				var i = 0;
				angular.forEach(document.getElementsByName('users[]'),function(users){
					userSelected[i] = users.value;
					i++;
				})

				$scope.sending = true;
				$http
				.post('/'+FOLDERADD+'/tools/getuserslogin', 
					$.param({
						dateStart	: $scope.dateStart, 
						dateFinish	: $scope.dateFinish, 
						'sites[]'	: sites,
						users 		: userSelected
					}), 
					{ 
						headers : { 
							'Content-Type' : 'application/x-www-form-urlencoded'
						}
					})
				.success(function(data) {
					if (data.status == 'ok') {
						$scope.data.date 	= data.date;
						$scope.data.users 	= data.users;

						
					}
					$scope.sending = false;
				});
			}	
		}
		$('.input-daterange input').datepicker({language: 'es',autoclose: true});

		$scope.exportData  = function () {
			var sitesSelected = new Array();
			angular.forEach(document.getElementsByName('sites[]'),function (sites) {
				if(sites.checked)
				{   
					sitesSelected.push(sites.value);
				}
			})
			
			$('#sites').val(sitesSelected);
			$('#date').val($scope.date);
			$("#sendData").val( $("<div>").append( $("#reportTable").eq(0).clone()).html());

			$("#exportForm").submit();
		}
		//configuracion visual de la busqueda de usuarios.
		setTimeout(() => {
			$('label[for="find_user"]').css('margin-left','9em');
			$('label[for="find_user"]').css('margin-right','-0.5em');
			document.querySelector('label[for="find_user"]').innerHTML+= '<span class="text-danger"><strong> *</strong></span>';
		}, 100);
	}

	CFaq.$inject = ['$scope','$http','nav'];
	function CFaq($scope,$http,nav) {
		nav.setSelected('faqs');
		
		$scope.questions = [];
		$scope.question = {};
		$scope.loading = true;
		$scope.editing = false;
		$scope.deleteId;
		$scope.notifType = '';

		function getQuestions () {
			$http({
			method 	: 'POST',
			url 	: '/'+FOLDERADD+'/tools/getfaqs',
			}).success(function(data) {
				if(data.status == 'success'){
					$scope.questions = data.questions;
					$scope.loading = false;
				}
			});
		}
		getQuestions();

		$scope.createQuestion = function () {
			var incomplete = false;
			if($scope.question.title == "" || !$scope.question.title){incomplete = true;}
			if($scope.question.message == "" || !$scope.question.message){incomplete = true;}

			if(incomplete){
				$('#no-complete').modal('show');				
			}
			else{
				$scope.loading = true;
				$http({
				method 	: 'POST',
				url 	: '/'+FOLDERADD+'/tools/createquestion',
				data 	: {
					title 	: $scope.question.title,
					message : $scope.question.message
				}
				}).success(function(data) {
					if (data == 'success') {
						getQuestions();
						$scope.success = true;
						$scope.loading = false;
						$scope.question.title 	= "";
						$scope.question.message = "";
						$scope.notifType = 'created';
					}
				});
			}
		}

		$scope.closePanel = function () {
			$scope.question.title = "";
			$scope.question.message = "";
			$scope.showCreate = false;
			$scope.success = false;
		}

		$scope.setDelete = function (Id) {
			$scope.deleteId = Id;
			$('#confirm-delete').modal('show');
		}

		$scope.toDelete = function () {
			$scope.deleting = true;
			
			$http({
			method 	: 'POST',
			url 	: '/'+FOLDERADD+'/tools/deletequestion',
			data 	: {id : $scope.deleteId}
			}).success(function(data) {
				if(data == 'success'){
					$scope.deleting = false;
					getQuestions();
					$('#confirm-delete').modal('hide');
					if($scope.showCreate)
					{
						$scope.closePanel();
					}
					$scope.notifType = 'deleted';
				}
			});
		}

		$scope.editQuestion = function name(qId,title,message) {
			var incomplete = false;
			if(qId == '' || !qId){incomplete = true;}			
			if(title == '' || !title){incomplete = true;}			
			if(message == '' || !message){incomplete = true;}

			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else{
				$scope.editing = true;
				$http({
				method 	: 'POST',
				url 	: '/'+FOLDERADD+'/tools/editquestion',
				data 	: {
					qId 	: qId,
					title 	: title,
					message : message
				}
				}).success(function(data) {
					if(data == 'success')
					{
						$scope.editing = false;
						getQuestions();
						$('#confirm-delete').modal('hide');
						if($scope.showCreate)
						{
							$scope.closePanel();
						}
						$scope.notifType = 'edited';
					}
				});
			}
		}
	}
})();
