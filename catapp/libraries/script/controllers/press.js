(function() {
	'use strict';

	angular
	.module('catnet')
	.config 	(PressConfig 									)
	.factory 	('press_message'		, FactoryPressMessage 	)
	.factory 	('pressService'			, pressService 			)
	.controller ('Cpress'				, Cpress 				)
	.controller ('Cpress_editclipping'	, Cpress_editclipping 	)
	.controller ('Cpress_createclipping', Cpress_createclipping )
	.controller ('Cpress_create'		, Cpress_create 		)
	.controller ('Cpress_edit'			, Cpress_edit 			);


	pressService.$inject = ['$http', '$rootScope'];
	function pressService($http, $rootScope) {
		var sv =  {
			unread 	: 0,
			getUnread 	: function() {
				$http
				.get('/'+FOLDERADD+'/press/getunread')
				.success(function(data) {
					if (data.status == 'ok') {
						sv.unread = data.unread;
						$rootScope.$broadcast('update_unread_press');
					}
				})
			}
		}
		return sv;
	}

	PressConfig.$inject = ['$routeProvider'];
	function PressConfig($routeProvider) {
		$routeProvider
		.when('/press', {
			templateUrl: '/'+FOLDERADD+'/press',
			controller: 'Cpress'
		})
		.when('/press/createclipping', {
			templateUrl: '/'+FOLDERADD+'/press/createclipping',
			controller: 'Cpress_createclipping'
		})
		.when('/press/create', {
			templateUrl: '/'+FOLDERADD+'/press/create',
			controller: 'Cpress_create'
		})
		.when('/press/editclipping/:id', {
			templateUrl: function(params){ return '/'+FOLDERADD+'/press/editclipping/'+params.id;},
			controller: 'Cpress_editclipping'
		})
		.when('/press/edit/:id', {
			templateUrl: function(params){ return '/'+FOLDERADD+'/press/edit/'+params.id;},
			controller: 'Cpress_edit'
		});
	}

	FactoryPressMessage.$inject = ['$rootScope'];
	function FactoryPressMessage($rootScope) {
		var data = {}
		data.message = '';
		data.setMessage = function(message, auto_update) {
			this.message = message;
			if (auto_update) {
				$rootScope.$broadcast('press_message');
			}
		}
		return data;
	}

	Cpress.$inject = ['$scope','nav', 'press_message', 'pressService', 'realTime'];	
	function Cpress($scope,nav, press_message, pressService, realTime) {
		nav.setSelected('press');
		$scope.message 		= press_message;
		$scope.clippings	= [];
		$scope.notes		= [];

		$scope.$on('press_message', function () {
			$scope.message = press_message;
		});

		$scope.$on("$destroy", function(){
			press_message.setMessage('', false);
		});
		
		var unread = 0;

		$scope.getallclipping 	= function() {
			$('#press_clipping_loading').show();
			$('#press_clipping_container').hide();
			$scope.clippings 	= [];
			$.ajax({
				method		: "POST",
				url 		: "/"+FOLDERADD+"/press/getallclipping",
				data		: {unread: unread},
				dataType 	: 'json'
			}).done(function(data) {
				if (data.status == 'ok') {
					$scope.$apply(function() {
						$scope.clippings = data.clippings;
					});
					$('#press_clipping_loading').hide();
					$('#press_clipping_container').show();
				}
			});
		};

		$scope.getallnote 	= function() {
			$('#press_note_loading').show();
			$('#press_note_container').hide();
			$scope.notes 	= [];
			$.ajax({
				method		: "POST",
				url 		: "/"+FOLDERADD+"/press/getallnote",
				data		: {unread: unread},
				dataType 	: 'json'
			}).done(function(data) {
				if (data.status == 'ok') {

					pressService.getUnread();
					$scope.$apply(function() {
						$scope.notes = data.notes;
					});
					$('#press_note_loading').hide();
					$('#press_note_container').show();
				}
			});
		};
		$scope.initCtrl = function() {
			unread = $('.fa-camera .messagesnumber').html();
			$scope.getallclipping();
			$scope.getallnote();
		};
		$scope.initCtrl();

		$scope.loadOldClipping = function() {
			$('#press_clipping_loadold').hide();
			$('#press_clipping_loadingold').show();
			var data = {};
			if ($scope.clippings.length > 0) {
				data.last = $scope.clippings[$scope.clippings.length-1].pressclippingId
			}
			else
			{
				data.last = 0;
			}
			$.ajax({
				method		: "POST",
				url 		: "/"+FOLDERADD+"/press/getoldpressclipping",
				data 		: data,
				dataType 	: 'json'
			}).done(function(data) {
				if (data.status == 'ok') {
					$scope.$apply(function() {
						$scope.clippings = $scope.clippings.concat(data.clippings);
					});
					$('#press_clipping_loadingold').hide();
					$('#press_clipping_loadold').show();
				}
				else if (data.status == 'empty')
				{
					$('#press_clipping_loadingold').hide();
					$scope.$apply(function() {
						$scope.clippingfull = true;
					});
				}
			});
		}

		$scope.loadOldNote = function() {
			$('#press_note_loadold').hide();
			$('#press_note_loadingold').show();
			var data = {};
			if ($scope.notes.length > 0) {
				data.last = $scope.notes[$scope.notes.length-1].pressnoteId
			}
			else
			{
				data.last = 0;
			}
			$.ajax({
				method		: "POST",
				url 		: "/"+FOLDERADD+"/press/getoldpressnotes",
				data 		: data,
				dataType 	: 'json'
			}).done(function(data) {
				if (data.status == 'ok') {
					$scope.$apply(function() {
						$scope.notes = $scope.notes.concat(data.notes);
					});
					$('#press_note_loadingold').hide();
					$('#press_note_loadold').show();
				}
				else if (data.status == 'empty')
				{
					$('#press_note_loadingold').hide();
					$scope.$apply(function() {
						$scope.notefull = true;
					});
				}
			});
		}

		$('#confirm-delete').on('show.bs.modal', function (event) {
			$('#delete_button_yes').show();
			$('#delete_button_loading').hide();

			var button 	= $(event.relatedTarget) 
			var type 	= button.data('type')
			var id 		= button.data('id')
			var modal 	= $(this)

			if (type == 'clipping') {
				modal.find('#delete_message').text($scope.deleteclippingconfirm);
				modal.find('#delete_button_yes').data('url', '/'+FOLDERADD+'/press/deleteclipping/'+id);
			}
			else if (type == 'note')
			{
				modal.find('#delete_message').text($scope.deletenoteconfirm);
				modal.find('#delete_button_yes').data('url','/'+FOLDERADD+'/press/delete/'+id);
			}
		});

		$('#delete_button_yes').click(function() {
			$('#delete_button_yes').hide();
			$('#delete_button_loading').show();
			$.ajax({
				method	: "GET",
				url 	: $(this).data('url')
			}).done(function(data) {
				try{data = $.parseJSON(data);}catch(e){return;};
				if (data.status == 'ok') {
					realTime.socket.emit('new_press');
					$('#confirm-delete').modal('hide');
					press_message.setMessage(data.message, true);
					$scope.initCtrl();
				}
			});
		});
	}

	Cpress_editclipping.$inject = ['$scope','nav', 'press_message', 'tinyMCE', '$routeParams', '$location'];
	function Cpress_editclipping($scope, nav, press_message, tinyMCE, $routeParams, $location) {
		nav.setSelected('press');
		tinyMCE.init('#clipping');
		$scope.title = '';
		$.ajax({
			method		: "GET",
			url 		: "/"+FOLDERADD+"/press/getclippingbyid/"+$routeParams.id,
			dataType 	: 'json'
		}).done(function(data) {
			if (data.status == 'ok') {
				$scope.$apply(function() {
					$scope.title = data.clipping.title;
					$('#clipping_ifr').contents().find('#tinymce').html(data.clipping.clipping);
					$('#press_clipping_editform').show();
					$('#press_clipping_loading').hide();
				});
			}
		});

		$scope.editclipping = function () {
			var incomplete = false;
			if ($scope.title == '' || !$scope.title || $('#clipping_ifr').contents().find('#tinymce').html() == '<p><br data-mce-bogus="1"></p>') {incomplete=true;}
			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$('#press_clipping_edit').attr('disabled', 'disabled');
				$('#press_clipping_edit').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
				$.ajax({
					method 		: "POST",
					url 		: "/"+FOLDERADD+"/press/editclipping/"+$routeParams.id,
					data 		: { title : $scope.title, clipping : $('#clipping_ifr').contents().find('#tinymce').html() },
					dataType 	: 'json'
				}).done(function(data) {
					if (data.status == 'ok') {
						press_message.setMessage(data.message, false);
						$location.path('/press');
						$scope.$apply()
					}
				});
			}
		};
	}

	Cpress_createclipping.$inject = ['$scope','nav', '$location', 'press_message', 'tinyMCE', 'realTime'];
	function Cpress_createclipping($scope, nav, $location, press_message, tinyMCE, realTime)
	{
		nav.setSelected('press');
		tinyMCE.init('#clipping');
		$scope.formData = {};
		$scope.formData.title = ''; 

		$scope.createclipping = function () {
			var incomplete = false;

			if ($scope.formData.title == "" || !$scope.formData.title || $('#clipping_ifr').contents().find('#tinymce').html() == '<p><br data-mce-bogus="1"></p>') {incomplete=true;}

			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$('#press_clipping_create').attr('disabled', 'disabled');
				$('#press_clipping_create').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
				$.ajax({
					method 		: "POST",
					url 		: "/"+FOLDERADD+"/press/createclipping/",
					data 		: { title : $scope.formData.title, clipping : $('#clipping_ifr').contents().find('#tinymce').html() },
					dataType 	: 'json'
				}).done(function(data) {
					if (data.status == 'ok') {
						realTime.socket.emit('new_press');
						press_message.setMessage(data.message, false);
						$location.path('/press');
						$scope.$apply()
					}
				});
			}
		};
	}

	Cpress_create.$inject = ['$scope','nav', '$location', 'press_message', 'tinyMCE', 'realTime'];
	function Cpress_create($scope, nav, $location, press_message, tinyMCE, realTime)
	{
		nav.setSelected('press');
		tinyMCE.init('#pressnote');

		$scope.create = function () {
			var incomplete = false;

			if ($('#pressnote_ifr').contents().find('#tinymce').html() == '<p><br data-mce-bogus="1"></p>') {incomplete=true;}

			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$('#press_note_create').attr('disabled', 'disabled');
				$('#press_note_create').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
				$.ajax({
					method 		: "POST",
					url 		: "/"+FOLDERADD+"/press/create/",
					data 		: { pressnote : $('#pressnote_ifr').contents().find('#tinymce').html() },
					dataType 	: 'json'
				}).done(function(data) {
					if (data.status == 'ok') {
						realTime.socket.emit('new_press');
						press_message.setMessage(data.message, false);
						$location.path('/press');
						$scope.$apply()
					}
				});
			}
		};
	}

	Cpress_edit.$inject = ['$scope','nav', 'press_message', 'tinyMCE', '$routeParams', '$location'];
	function Cpress_edit($scope, nav, press_message, tinyMCE, $routeParams, $location) {
		nav.setSelected('press');
		tinyMCE.init('#pressnote');
		$.ajax({
			method		: "GET",
			url 		: "/"+FOLDERADD+"/press/getnotebyid/"+$routeParams.id,
			dataType 	: 'json'
		}).done(function(data) {
			if (data.status == 'ok') {
				$scope.$apply(function() {
					$('#pressnote_ifr').contents().find('#tinymce').html(data.note.pressnote);
					$('#press_note_editform').show();
					$('#press_note_loading').hide();
				});
			}
		});
		$scope.editnote = function () {
			var incomplete = false;
			if ($('#pressnote_ifr').contents().find('#tinymce').html() == '<p><br data-mce-bogus="1"></p>') {incomplete=true;}
			if(incomplete)
			{
				$('#no-complete').modal('show');
			}
			else
			{
				$('#press_note_edit').attr('disabled', 'disabled');
				$('#press_note_edit').html("<i class='fa fa-refresh fa-spin fa-lg fa-fw'></i>");
				$.ajax({
					method 		: "POST",
					url 		: "/"+FOLDERADD+"/press/edit/"+$routeParams.id,
					data 		: { pressnote : $('#pressnote_ifr').contents().find('#tinymce').html() },
					dataType 	: 'json'
				}).done(function(data) {
					if (data.status == 'ok') {
						press_message.setMessage(data.message, false);
						$location.path('/press');
						$scope.$apply()
					}
				});
			}
		};
	}
}
)()