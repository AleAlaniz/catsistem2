(function () {
    'use strict';
    angular
    .module('catnet')
    .config(referedConfig);
    
    referedConfig.$inject = ['$routeProvider'];
    function referedConfig($routeProvider,){
        $routeProvider
        .when('/referred',{
        templateUrl : '/'+FOLDERADD+'/referred',
        controller  : Creferred
        })
        .when('/referred/sendReferred',{
        templateUrl : '/'+FOLDERADD+'/referred/sendReferredV',
        controller  : CreferredSend
        })
        .when('/referred/acceptedReferred',{
        templateUrl : '/'+FOLDERADD+'/referred/acceptedReferred',
        controller  : Creferred
        })
        .when('/referred/referredList',{
        templateUrl : '/'+FOLDERADD+'/referred/referredList',
        controller  : CreferredSended
        })
    }

    Creferred.$inject= ['nav'];
    function Creferred(nav,) {
        nav.setSelected('referred');
	}
	CreferredSend.$inject= ['$scope','nav','$http','$location'];
    function CreferredSend($scope,nav,$http,$location) {
		$scope.referred = {
            dni : "",
            name: "", 
            numberPhone: ""
        };

        $scope.sendReferred = function () {
            if(
                ($scope.referred.dni == "") || ($scope.referred.name == "") || ($scope.referred.numberPhone =="")
            ){
                $('#no-complete').modal('show');
            }
            else
            {
                $http({
                    method 	: 'POST',
                    url 	: '/'+FOLDERADD+'/referred/sendReferred',
                    data 	: $scope.referred
                    }).success(function(data) {
                        if(data == 'success'){
                            $location.path('/referred/acceptedReferred');
                        }
                        else{
                            $scope.errors = data;
                        }
                    });
            }
            
        }
	}

    CreferredSended.$inject = ['$scope', "$http" ,'$window','nav', '$timeout', '$location'];
    function CreferredSended($scope, $http, $window, nav, $timeout, $location) {
	   
		$scope.loading = true;
        $scope.candidatesList 		= null;
        $scope.savedCandidateData 	= {
            dni  		: "",
            name 		: "", 
            numberPhone : "",
            mail  		: "",
        };

        $scope.saveState = function(referedId){

        	for (var i = $scope.candidatesList.length - 1; i >= 0; i--) {

				if($scope.candidatesList[i].referedId == referedId){

					$scope.savedCandidateData.dni 			= $scope.candidatesList[i].dni;
					$scope.savedCandidateData.name 			= $scope.candidatesList[i].name;
					$scope.savedCandidateData.numberPhone 	= $scope.candidatesList[i].numberPhone;
					$scope.savedCandidateData.mail 			= $scope.candidatesList[i].mail;
				}
			}
		}
		
		$scope.restoreData	= function(referedId) {

			for (var i = $scope.candidatesList.length - 1; i >= 0; i--) {

				if($scope.candidatesList[i].referedId == referedId){
					
					$scope.candidatesList[i].dni 			= $scope.savedCandidateData.dni;
					$scope.candidatesList[i].name 			= $scope.savedCandidateData.name;
					$scope.candidatesList[i].numberPhone 	= $scope.savedCandidateData.numberPhone;
					$scope.candidatesList[i].mail 			= $scope.savedCandidateData.mail;
				}
			}

			$scope.savedCandidateData 	= {
           		dni  		: "",
           		name 		: "", 
           		numberPhone : "",
           		mail  		: "",
        	};
		}

		getCandidates();

		function getCandidates(){

			$http({
				method  : 'POST',
				url     : 'referred/getAllCandidates'
			})
			.success(function(data) {

				if (data.status == 'ok'){

					$scope.candidatesList = data.message;

					$timeout(function() {

						$('.datatable').DataTable({

							"columnDefs": [
							    { "orderable": false, "targets": 6 }
							],

							language: {

								"sProcessing":     "Procesando...",
								"sLengthMenu":     "Mostrar _MENU_ referidos",
								"sZeroRecords":    "No se encontraron resultados",
								"sInfo":           "Mostrando referidos del _START_ al _END_ de un total de _TOTAL_ referidos",
								"sInfoEmpty":      "Mostrando referidos del 0 al 0 de un total de 0 referidos",
								"sInfoFiltered":   "(filtrado de un total de _MAX_ referidos)",
								"sInfoPostFix":    "",
								"sSearch":         "Buscar:",
								"sUrl":            "",
								"sInfoThousands":  ",",
								"sLoadingRecords": "Cargando...",
								
								"oPaginate": {
									"sFirst":    "Primero",
									"sLast":     "Último",
									"sNext":     "Siguiente",
									"sPrevious": "Anterior"
								},

								"oAria": {
									"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
									"sSortDescending": ": Activar para ordenar la columna de manera descendente"
								}
							}
						});
					});
				}
				else{
					$scope.isEmpty = true;
				}

				$scope.loading = false;
			});
		}

		$scope.hasMail = function(mail){

			var currentMail = "";

			if (mail){
				currentMail += mail;
			}
			else{
				currentMail+="ninguno";
			}

			return currentMail;
		}

		$scope.saveEdition = function(referedId) {

			$scope.errors = "";
			$scope.successful  = null;

			var referrerToEdit = null;

			for (var i = $scope.candidatesList.length - 1; i >= 0; i--) {
				if($scope.candidatesList[i].referedId == referedId){
					referrerToEdit = $scope.candidatesList[i];
				}
			}

			$http({
				method  : 'POST',
				url     : 'referred/editCandidate',
				data 	: referrerToEdit,
			}).success(function(data) {

				if( data.status == 'invalid' || data.status == 'validation_error'){
					$scope.errors = data.message.message; 
				}
				else{

					$scope.successful = 'Se ha modificado al usuario de manera exitosa'; 
				}
			})
		}

		$scope.currentMonth = function(month){

			var monthInSpanish = "";

			switch(month){

				case "1":
					monthInSpanish = "Enero";
					break;
				case "2":
					monthInSpanish = "Febrero";
					break;
				case "3":
					monthInSpanish = "Marzo";
					break;
				case "4":
					monthInSpanish = "Abril";
					break;
				case "5":
					monthInSpanish = "Mayo";
					break;
				case "6":
					monthInSpanish = "Junio";
					break;
				case "7":
					monthInSpanish = "Julio";
					break;
				case "8":
					monthInSpanish = "Agosto";
					break;
				case "9":
					monthInSpanish = "Septiembre";
					break;
				case "10":
					monthInSpanish = "Octubre";
					break;
				case "11":
					monthInSpanish = "Noviembre";
					break;
				case "12":
					monthInSpanish = "Diciembre";
					break;
			}

			return monthInSpanish;
		}
    }
})();