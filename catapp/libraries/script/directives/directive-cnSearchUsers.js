(function() {
	'use strict';
	angular
	.module('catnet')
	.directive('cnSearchUsers'	, DsearchUsers);
	
	DsearchUsers.$inject = ['$http', '$timeout'];
	function DsearchUsers($http, $timeout) {
		return {
			'restrict' 	: 'A',
			'scope'		: {
				'users'			: '=?',
				'placeholder'	: '@?',
				'url'			: '@'
			},
			link: function ($scope, element) {
				
				$scope.folder = FOLDERADD;

				!$scope.users ? $scope.users = []: false;
				
				$scope.finding = false;
				
				$scope.removeUser = function(i) {
					$scope.users.splice(i, 1);
				};

				$scope.addUser = function(i) {
					$scope.users.push($scope.userOptions[i]);
					$scope.userOptions.splice(i, 1);
				};

				$scope.userOptions = [];
				$scope.findLike = '';
				$scope.searchNumber = 0;
				$scope.searchActual = 0;
				$scope.searchTimeout = null;

				$scope.searchUsers = function() {
					if($scope.findLike != '') 
					{
						$scope.searchTimeout ? $timeout.cancel($scope.searchTimeout) : false;
						$scope.searchTimeout = $timeout(gosearch, 250);
					}
					else
					{
						$scope.userOptions = [];
						$scope.finding = false;
					}
				};

				function gosearch() {
					$scope.finding = true;
					var thisSearch = $scope.searchNumber;
					$scope.searchNumber = $scope.searchNumber+1;

					$http
					.post($scope.url, $.param({'like': $scope.findLike}), { headers : { 'Content-Type' : 'application/x-www-form-urlencoded'}})
					.success(function(data) {
						if(data.status == 'ok' && thisSearch >= $scope.searchActual && $scope.findLike != '')
						{
							$scope.searchActual = thisSearch;
							$scope.addUserOptions(data.users);
							$scope.finding = false;
						}
						else if (thisSearch >= $scope.searchActual)
						{
							$scope.searchActual = thisSearch;
							$scope.finding = false;
							$scope.userOptions = [];	
						}
					});
				}

				$scope.addUserOptions = function (users) {
					$scope.userOptions = [];
					jQuery.each(users, function(i, user) {
						var exist = false;
						jQuery.each($scope.users, function(i, userExist) {
							if (userExist.userId == user.userId) {exist = true;}
						});
						if (!exist) {$scope.userOptions.push(user);}
					});
				};


				$scope.selectClick = function (e) {
					$('.select-input', e.currentTarget).focus();
				};


			},
			'templateUrl'	: 	'/'+APPFOLDERADD+'/libraries/script/directives/directive-cnSearchUsers.html'
		}
	}
})();