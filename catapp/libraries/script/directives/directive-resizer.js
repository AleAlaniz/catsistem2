(function() {
	angular
	.module('cd.resizer', [])
	.directive('resizer', ['$document' , function($document) {

		return function($scope, $element, $attrs) {
			var resizerWidth = 15;

			$element.append(angular.element("<span/>"));

			if ($attrs.resizer == 'vertical') {
				var initialWidth = angular.element($attrs.resizerLeft).width();

				angular.element($attrs.resizerRight).css("left", (initialWidth + resizerWidth) + 'px');
				$element.css("left", initialWidth+"px");
			}
			if ($attrs.resizer == 'horizontal') {
				var initialWidth = angular.element($attrs.resizerTop).height();

				angular.element($attrs.resizerBottom).css("height", (($document.height() - initialWidth) - resizerWidth) + 'px');
				$element.css("bottom", ($document.height() - initialWidth - resizerWidth)+"px");
			}


			function mousemove(event) {
				event.stopPropagation();
				if ($attrs.resizer == 'vertical') {
					// Handle vertical resizer
					var x = event.pageX;
					var maxWidth = $element.attr('resizer-max-width');
					maxWidth = maxWidth ? parseInt(maxWidth) : null;
					if (x > maxWidth) { x = maxWidth;}

					//console.log(maxWidth);

					$element.css("left", (x - resizerWidth / 2) + 'px');
					angular.element($attrs.resizerLeft).css("width", (x - resizerWidth / 2) + 'px');
					angular.element($attrs.resizerRight).css("left", (x + resizerWidth / 2) + 'px');

				} else {
					var y = $document.height() - event.pageY;

					$element.css("bottom", (y - resizerWidth / 2) + 'px');

					$($attrs.resizerTop).css("bottom",  (y + resizerWidth / 2 ) + 'px');
					$($attrs.resizerBottom).css("height", (y - resizerWidth / 2) + 'px');

				}
			}

			$element.on('mousedown', mousedown);

			function mousedown(event) {
				angular.element($attrs.resizerHelper).css("display", "block");
				$document.on('mousemove', mousemove);
				$document.on('mouseup', mouseup);
			}

			function mouseup() {
				angular.element($attrs.resizerHelper).css("display", "none");
				$document.unbind('mousemove', mousemove);
				$document.unbind('mouseup', mouseup);
			}
		};
	}]);
})();