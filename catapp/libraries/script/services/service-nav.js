(function() {
	'use strict';
	
	angular
	.module('catnet')
	.factory('nav', nav);

	nav.$inject = ['$rootScope'];
	function nav($rootScope) {
		var nav = {};
		nav.selected = '';
		nav.setSelected = function (name) {
			this.selected = name;
			this.update()
		};
		nav.update = function() {
			$rootScope.$broadcast('updateselected');
		};
		return nav;
	}
})();
