<?php
if (!defined( 'BASEPATH')) exit('No direct script access allowed'); 
class Activities_hook
{
    private $ci;
    public function __construct(){
        $this->ci =& get_instance();
        !$this->ci->load->library('session') ? $this->ci->load->library('session') : false;
        !$this->ci->load->helper('url') ? $this->ci->load->helper('url') : false;
    }

    public function complete_activities(){
        if($this->ci->session->Loged && (($this->ci->uri->segment(1) == NULL && $this->ci->uri->segment(2) == NULL) || ($this->ci->uri->segment(1) == 'home' && $this->ci->uri->segment(2) == 'index')) && $this->ci->Identity->Validate('activities/view') && !($this->ci->uri->segment(1) == 'activities' && $this->ci->uri->segment(2) == 'completeactivity') && !($this->ci->uri->segment(1) == 'users' && $this->ci->uri->segment(2) == 'profilephoto')  && !($this->ci->uri->segment(1) == 'users' && $this->ci->uri->segment(2) == 'config') ){
            $timestamp = time();
            
            $sql  = "SELECT turn FROM userComplementaryData WHERE userId = ?";
			$turn = $this->ci->db->query($sql,$this->ci->session->UserId)->row();
            
            $sql=
            "SELECT activities.activityId, activities.name, activities.description
            FROM activityLinks INNER JOIN activities ON activityLinks.activityId = activities.activityId
            WHERE activities.required = 'TRUE' && (capacity > 0 || capacity IS NULL) 
            && (activityLinks.userId = ? || activityLinks.sectionId = ? || activityLinks.siteId = ? || activityLinks.roleId = ?)
            && (activityLinks.turns = 'NO' || FIND_IN_SET(?,activityLinks.turns))
            && activities.active = 'TRUE'
            && (activities.startDate <= ? || activities.startDate IS NULL) 
            && (activities.endDate >= ? || activities.endDate IS NULL)
            && (SELECT count(*) FROM activityCompletes WHERE activityCompletes.activityId = activities.activityId && activityCompletes.userId = ?) = 0
            GROUP BY activityLinks.activityId ORDER BY activities.activityId ASC";

            $activity = $this->ci->db->query($sql, 
            array($this->ci->session->UserId, 
                $this->ci->session->sectionId, 
                $this->ci->session->siteId, 
                $this->ci->session->roleId,
                $turn->turn,
                $timestamp, 
                $timestamp,
                $this->ci->session->UserId))->row();
            
            if(isset($activity))
            {
                header('Location:/'.FOLDERADD.'/activity/completeActivity');
            }
        }
    }
}
?>