<?php
if (!defined( 'BASEPATH')) exit('No direct script access allowed'); 
class CorporateDocs_hook
{
    private $ci;
    public function __construct()
    {
        $this->ci =& get_instance();
        !$this->ci->load->library('session') ? $this->ci->load->library('session') : false;
        !$this->ci->load->helper('url') ? $this->ci->load->helper('url') : false;
        !$this->ci->load->model("Identity_model") ? $this->ci->load->model("auth") :false;
    }

    public function read_doc()
    {
        if($this->ci->Identity->Validate('corporatedocs/index'))
        {

            if
                ($this->ci->session->Loged 
                && (
                    ($this->ci->uri->segment(1) == NULL && $this->ci->uri->segment(2) == NULL) || ($this->ci->uri->segment(1) == 'home' && $this->ci->uri->segment(2) == 'index')
                )
                && !($this->ci->uri->segment(1) == 'corporatedocs'
                && $this->ci->uri->segment(2)   == 'readdoc')       && !($this->ci->uri->segment(1) == 'users'
                && $this->ci->uri->segment(2)   == 'profilephoto')  && !($this->ci->uri->segment(1) == 'users' 
                && $this->ci->uri->segment(2)   == 'config') 
            )
            {

                $sql=
                "SELECT corporatedocs.corporatedocId,corporatedocs.name,corporatedocs.description
                FROM corporatedoclinks links INNER JOIN corporatedocs ON links.corporatedocId = corporatedocs.corporatedocId
                WHERE corporatedocs.required = 1 && (links.userId = ? || links.siteId = ? || (case when links.sectionId is null then links.roleId = ? when links.roleId is null then links.sectionId = ? else links.sectionId = ? && links.roleId = ? end))
                && (SELECT count(*) FROM corporatedocviews views WHERE views.corporatedocId = corporatedocs.corporatedocId && views.userId = ?) = 0
                GROUP BY links.corporatedocId ORDER BY corporatedocs.corporatedocId ASC";

                $doc = $this->ci->db->query($sql, 
                    array($this->ci->session->UserId, 
                        $this->ci->session->siteId, 
                        $this->ci->session->Role,
                        $this->ci->session->sectionId, 
                        $this->ci->session->sectionId, 
                        $this->ci->session->Role,
                        $this->ci->session->UserId))->row();

                if (isset($doc)) 
                {
                    header('Location:/'.FOLDERADD.'/corporatedocs/readdoc');
                }
            }
        }
    }
}
