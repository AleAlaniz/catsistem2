<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_hook extends CI_Controller {

    private $ci;
    public function __construct()
    {
        $this->ci =& get_instance();
        !$this->ci->load->library('session') ? $this->ci->load->library('session') : false;
        !$this->ci->load->helper('url') ? $this->ci->load->helper('url') : false;
        !$this->ci->load->model("Identity_model") ? $this->ci->load->model("auth") :false;
        !$this->ci->load->model('Event_model','Event') ? $this->ci->load->model('Event_model','Events') : false;
    }

    public function show_notification()
    { 

        if($this->ci->session->Loged &&
        ( ($this->ci->uri->segment(1) == NULL && $this->ci->uri->segment(2) == NULL) || 
         ($this->ci->uri->segment(1) == 'home' && $this->ci->uri->segment(2) == 'index'))
        && !($this->ci->uri->segment(1) == 'event'
           && $this->ci->uri->segment(2) == 'showEvent') && !($this->ci->uri->segment(1) == 'users'
           && $this->ci->uri->segment(2) == 'profilephoto')  && !($this->ci->uri->segment(1) == 'users' 
           && $this->ci->uri->segment(2) == 'config'))
        {
            $session = $this->ci->session;
            $event = $this->ci->Event->getEventsForUser($session->UserId);
            if(count($event)>0)
            {
                header('Location:/'.FOLDERADD.'/event/showEvent');
            }
        }
    }
}

/* End of file Notification_hook.php */

?>