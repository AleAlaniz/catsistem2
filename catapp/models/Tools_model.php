<?php
Class Tools_model extends CI_Model {

	public function GamificationInput()
	{
		$this->load->view(PRELAYOUTHEADER);
		$this->load->view('tools/input');
		$this->load->library('export_excel');
		$this->load->view(PRELAYOUTFOOTER);
	}


	public function PrizeValidator()
	{

		$this->form_validation->set_rules('voucherId', 'voucher', 'required|numeric');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view(PRELAYOUTHEADER);
			$this->load->view('tools/validator');
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$sql 	= "SELECT voucherId FROM vouchers WHERE voucherId = ? && redeemed = 'FALSE' ";
			$query 	= $this->db->query($sql, $this->input->post('voucherId'))->row();
			if ($query) {
				$object = array(
					'redeemed' 			=> 'TRUE',
					'redeemeduserId' 	=> $this->session->UserId,
					'redeemedDate'		=> time()
					);
				$this->db->where('voucherId', $query->voucherId);
				$this->db->update('vouchers', $object);	

				$this->session->set_flashdata('flashMessage', 'redeemed');
				header('Location:/'.FOLDERADD.'/tools/prizevalidator');
			}
			else
			{
				show_404();
			}
		}

	}

	public function GetNotes()
	{
		$sql = "SELECT * FROM stickyNotes WHERE userId = ?";
		return $this->db->query($sql, $this->session->UserId)->result();
	}

	function SaveNotePosition(){
		if (($this->input->post('axisX') || $this->input->post('axisX') == 0) && ($this->input->post('axisY') || $this->input->post('axisY') == 0) && $this->input->post('note')) {
			$axisX = (int)$this->input->post('axisX');
			$axisY = (int)$this->input->post('axisY');
			

			if ((is_int($axisY) || $axisY === 0) && (is_int($axisX) || $axisX === 0)) {
				$sql = "SELECT * FROM stickyNotes WHERE stickynoteId = ? && userId = ?";
				$query = $this->db->query($sql,array($this->input->post('note'), $this->session->UserId))->row();
				if (isset($query))
				{
					$objectEdit = array(
						'axisX' => $axisX,
						'axisY' => $axisY
						);

					$this->db->where('stickynoteId', $query->stickynoteId);
					$this->db->update('stickyNotes', $objectEdit);	
					return 'done';
				}
				else 
				{
					return 'invalid';
				}
			}
			else 
			{
				return 'invalid';
			}
		}
		else 
		{
			return 'invalid';
		}
	}

	function SaveNoteSize(){
		if (($this->input->post('width') || $this->input->post('width') > 0) && ($this->input->post('height') || $this->input->post('height') > 0) && $this->input->post('note')) {
			
			$width =(int)$this->input->post('width');
			$height =(int)$this->input->post('height');
			
			if (is_int($width) && is_int($height)) {
				$sql = "SELECT * FROM stickyNotes WHERE stickynoteId = ? && userId = ?";
				$query = $this->db->query($sql,array($this->input->post('note'), $this->session->UserId))->row();
				if (isset($query))
				{
					$objectEdit = array(
						'width'=>$width,
						'height'=>$height		
						);

					$this->db->where('stickynoteId', $query->stickynoteId);
					$this->db->update('stickyNotes', $objectEdit);	
					return 'done';
				}
				else 
				{
					return 'invalid';
				}
			}
			else 
			{
				return 'invalid';
			}
		}
		else 
		{
			return 'invalid';
		}
	}

	function SaveNote(){

		if ($this->input->post('note'))
		{
			
			$text=$this->input->post('text');

			$sql = "SELECT * FROM stickyNotes WHERE stickynoteId = ? && userId = ?";
			$query = $this->db->query($sql,array($this->input->post('note'), $this->session->UserId))->row();
			if (isset($query))
			{
				$objectEdit = array(
					'note'=>$text
					);

				$this->db->where('stickynoteId', $query->stickynoteId);
				$this->db->update('stickyNotes', $objectEdit);	
				return 'done';
			}
			else 
			{	
				return 'invalid';
			}

		}
		else 
		{
			return 'invalid';
		}
	}

	function DeleteNote(){
		if ($this->input->post('note')) {
			$sql = "SELECT * FROM stickyNotes WHERE stickynoteId = ? && userId = ?";
			$query = $this->db->query($sql,array($this->input->post('note'), $this->session->UserId))->row();
			if (isset($query))
			{	
				$objectDelete = array(
					'stickynoteId' => $query->stickynoteId
					);
				$this->db->delete('stickyNotes', $objectDelete);
				return 'done';
			}
			else 
			{
				return 'invalid';
			}
			
		}
		else 
		{
			return 'invalid';
		}
	}

	function CreateNote(){
		$validate=FALSE;
		$color=$this->input->post('color');

		if($color =='pink' || $color == 'yelow' || $color == 'blue' || $color =='gray' || $color == 'green')
		{
			$validate=TRUE;
		}

		if (!$validate)
		{
			echo 'invalid';
		}
		else
		{
			$objectInsert = array(
				'note' => '',
				'color' => $this->input->post('color'),
				'axisY' => 15,
				'axisX' => 15,
				'timestamp' => time(),
				'userId' => $this->session->UserId,
				'width'=>150,
				'height'=>170
				);
			$this->db->insert('stickyNotes', $objectInsert);
			
			echo $this->db->insert_id();
		}
	}

	public function GetDiaryData($date,$sites)
	{
		$origDate = $date;
		$data = new StdClass();
		$data->names  = array();
		$data->values = array();
		for ($i=0; $i < 24; $i++) {
			$date = $origDate + (3600 * $i);
			$sql = 
			"SELECT	count(*) access 
			FROM access_logs 
			LEFT JOIN users
			ON access_logs.userId = users.userId
			WHERE timestamp >= ? && timestamp <= ?
			AND (";
			for ($j=0; $j < count($sites); $j++) { 
				$sql .= "siteId = $sites[$j] ";
				if(count($sites)>1 && $j < count($sites)-1){
					$sql.=" OR ";
				}
			}
			$sql .=")";
			$query = $this->db->query($sql, array($date, $date+3600-1))->row();
			$data->names[$i] = $i." Hs";
			$data->values[$i] = $query->access;
		}

		$sql = "SELECT	count(*) total 
		FROM access_logs 
		LEFT JOIN users
		ON access_logs.userId = users.userId
		WHERE timestamp >= ? && timestamp <= ?
		AND (";
		for ($j=0; $j < count($sites); $j++) { 
			$sql .= "siteId = $sites[$j] ";
			if(count($sites)>1 && $j < count($sites)-1){
				$sql.=" OR ";
			}
		}
		$sql .=")";
		$data->totalLogins = $this->db->query($sql, array($origDate, $origDate+86400-1))->row();

		$sql = "SELECT	count(*) total, access_logs.userId 
		FROM access_logs 
		LEFT JOIN users
		ON access_logs.userId = users.userId
		WHERE timestamp >= ? && timestamp <= ? GROUP BY access_logs.userId
		AND (";
		for ($j=0; $j < count($sites); $j++) { 
			$sql .= "siteId = $sites[$j] ";
			if(count($sites)>1 && $j < count($sites)-1){
				$sql.=" OR ";
			}
		}
		$sql .=")";
		$todayUsers = $this->db->query($sql, array($origDate, $origDate+86400-1))->result();

		$data->newUsers = 0;
		foreach ($todayUsers as $user) {

			$sql = "SELECT access_logs.userId 
			FROM access_logs 
			LEFT JOIN users
			ON access_logs.userId = users.userId
			WHERE timestamp < ? && access_logs.userId = ? 
			AND (";
			for ($j=0; $j < count($sites); $j++) { 
				$sql .= "siteId = $sites[$j] ";
				if(count($sites)>1 && $j < count($sites)-1){
					$sql.=" OR ";
				}
			}
			$sql .=")";
			$temp = $this->db->query($sql, array($origDate, $user->userId))->row();
			if ($temp == NULL) {
				$data->newUsers++;
			}

		}
		$data->totalUsers = count($todayUsers);
		return $data;
	}

	public function GetUsersData($start, $finish,$sites)
	{
		
		$sql = "SELECT 
		CONCAT(up.name, ' ', up.lastName) name, count(access_logs.userId) access
		FROM access_logs 
		JOIN userPersonalData AS up 
		ON (access_logs.userId = up.userId)
		JOIN users 
		ON (access_logs.userId = users.userId)
		WHERE
		access_logs.timestamp >= ? &&
		access_logs.timestamp <= ? &&
		up.userId = access_logs.userId
		AND (";
		for ($j=0; $j < count($sites); $j++) { 
			$sql .= "siteId = $sites[$j] ";
			if(count($sites)>1 && $j < count($sites)-1){
				$sql.=" OR ";
			}
		}
		$sql .=")";
		$sql.="GROUP BY access_logs.userId";
		$query = $this->db->query($sql, array($start, $finish+86400))->result();
		$data = new StdClass();
		$data->names  = array();
		$data->values = array();
		$data->totalLogins = 0;
		foreach ($query as $key => $dat) {
			$data->names[$key] 	= $dat->name;
			$data->values[$key] = $dat->access;
			$data->totalLogins += $dat->access;
		}
		
		$data->totalUsers = count($query);
		
		return $data;
	}

	public function GetUsersLogin($dateStart,$dateFinish,$sites,$users)
	{
		$sql = "SELECT s.name site, up.name, up.lastName, u.userName,up.dni, al.timestamp,FROM_UNIXTIME(al.timestamp,'%Y-%m-%d')as time, u.siteId, 
		(SELECT ll.timestamp FROM logout_logs AS ll WHERE ll.timestamp >= al.timestamp && ll.timestamp < al.timestamp+86400 && ll.userId = al.userID ORDER BY ll.timestamp DESC LIMIT 1) logoutTimestamp 
		FROM access_logs AS al 
		INNER JOIN (SELECT name, lastName, userId, dni FROM userPersonalData) AS up ON al.userId = up.userId 
		INNER JOIN (SELECT userName, siteId, userId FROM users) AS u ON al.userId = u.userId 
		INNER JOIN sites AS s ON u.siteId = s.siteId 
		WHERE al.timestamp >= ? && al.timestamp < ? ";

		if(count($sites)>0)
		{
			$sites2 = join(",",$sites);
			$sql.= "AND u.siteId IN ($sites2)";
		}

		if(count($users)>0)
		{
			$users2 = join(",",$users);
			$sql.= "AND al.userId IN ($users2)";
		}
		$sql.= " GROUP BY al.userId,time ORDER BY al.timestamp";
		
		return $this->db->query($sql, array($dateStart, $dateFinish+86400))->result();
			  
	}

	public function GetSites()
	{
		$sql = 'SELECT s.siteId, s.name FROM sites AS s;';
		return  $this->db->query($sql)->result();
	}

	function getData($userId)
	{
		$this->db->select('sections.name');
		$this->db->join('sections', 'sections.sectionId = users.sectionId');
		$section = $this->db->get_where('users',array('users.userId' => $userId))->row();	

		//query que trae todos  los usuarios comentar la otra 
		// $sql = ""SELECT * from users u join userPersonalData up on(u.userId = up.userId) join userComplementaryData uc on (u.userId = uc.userId) where u.active = 1;";"

		$sql =
		"SELECT u.`userId`, `Division`, `Site`, `Alquila`, `Personas a cargo`, `Turno`, `Estado Civil`, `Genero`, `Nombre`, `Apellido`,`DNI`, `Correo`, `Fecha de nacimiento`, `Provincia`,`Localidad`, `Codigo Postal`, `Direccion`, `Activo`, `Trabajo anteriormente`, `Hijos`, `Edad`,CONCAT (`Codigo de Area de Telefono`,'-',`Telefono`) 'Telefono', CONCAT (`Codigo de Area de Telefono Celular`,'-', `Telefono Celular`) as 'Celular',`Campaña` as 'Area',`Subcampaña` as 'Campania', `Nacionalidad`
		FROM `(BI) Usuarios` as u 
		WHERE Activo = 'Si'";

		$result = $this->db->query($sql)->result();
		foreach ($result as $user) {
			
			$sql = "SELECT `Nivel`, `Estado`, `Carrera / Orientación`as Carrera, `Institución` FROM `(BI) Estudios` WHERE `userId` = $user->userId";
			$user->studies = $this->db->query($sql)->result();

							 $this->db->select('Hobby');
							 $this->db->join("`(BI) Hobbies` as h", 'h.hobbyId = hpu.hobbyId');
			$user->hobbies = $this->db->get_where("`(BI) Hobbies por Usuario` as hpu",array('userId' => $user->userId))->result();
		}
		return $result;

	}

	public function GetFaqs()
	{
		$res = new StdClass();
		$res->questions = $this->db->get('faqs')->result();
		if(isset($res->questions))
			$res->status = 'success';
		return $res;
	}

	public function CreateQuestion($title,$message)
	{
		$userId = $this->session->UserId;
		$object = array(
			'title'   => $title, 
			'message' => $message,
			'userId'  => $userId
		);
		
		$this->db->insert('faqs', $object);
		echo "success";
	}

	public function DeleteQuestion($Id)
	{
		$this->db->where('faqId', $Id);
		$this->db->delete('faqs');
		echo "success";
	}

	public function EditQuestion($qId,$title,$message)
	{
		$userId = $this->session->UserId;
		$object = array(
			'title'   => $title, 
			'message' => $message,
			'userId'  => $userId
		);
		
		$this->db->set('title',$title);
		$this->db->set('message',$message);
		$this->db->where('faqId', $qId);
		$this->db->update('faqs', $object);
		
		echo('success');
	}

}
