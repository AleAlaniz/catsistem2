<?php

Class Press_model extends CI_Model {

	var $maxpressnotes = 5;

	public function GetAllClipping($unread = NULL)
	{
		$data = new stdClass();
		$sql = 'SELECT * FROM pressClippings ORDER BY pressclippingId DESC LIMIT '.$this->maxpressnotes;
		$data->clippings = $this->GetClipping($sql);
		$data->status = 'ok';

		if(isset($unread))
		{
			$sql = "SELECT pc.pressclippingId FROM pressClippings AS pc WHERE NOT EXISTS (SELECT 1 FROM pressclippingViews AS pcv WHERE pc.pressclippingId = pcv.pressclippingId && pcv.userId = ?)";
			$unreads = $this->db->query($sql, $this->session->UserId)->result();
	
			foreach ($unreads as $clipping) {
				$objectInsert = array(
					'timestamp' 		=> time(),
					'userId' 			=> $this->session->UserId,
					'pressclippingId' 	=> $clipping->pressclippingId
					);
				$this->db->insert('pressclippingViews', $objectInsert);
			}
		}
		return escapeJsonString($data, FALSE);
	}

	public function GetAllNote($unread = NULL)
	{
		$data = new stdClass();
		$sql = "SELECT * FROM pressNotes ORDER BY pressnoteId DESC LIMIT ".$this->maxpressnotes;
		$data->notes = $this->GetNotes($sql);
		$data->status = 'ok';

		if(isset($unread))
		{
			$sql = "SELECT pn.pressnoteId FROM pressNotes AS pn WHERE NOT EXISTS (SELECT 1 FROM pressnoteViews AS pnv WHERE pn.pressnoteId = pnv.pressnoteId && pnv.userId = ?)";
			$unreads = $this->db->query($sql, $this->session->UserId)->result();
	
			foreach ($unreads as $note) {
				$objectInsert = array(
					'timestamp' 	=> time(),
					'userId' 		=> $this->session->UserId,
					'pressnoteId' 	=> $note->pressnoteId
					);
				$this->db->insert('pressnoteViews', $objectInsert);
			}
		}
		return escapeJsonString($data, FALSE);
	}

	function Create(){
		$this->form_validation->set_rules('pressnote', 'lang:press_pressnote', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('press/create');
		}
		else
		{
			$objectInsert = array(
				'pressnote' => $this->input->post('pressnote'),
				'userId' => $this->session->UserId,
				'timestamp' => time()
				);
			$this->db->insert('pressNotes', $objectInsert);
			$response = new StdClass();
			$response->status 	= 'ok';
			$response->message 	= $this->lang->line('press_successmessage');
			echo escapeJsonString($response, FALSE);
		}
	}

	function CreateClipping(){

		$this->form_validation->set_rules('clipping', 'lang:press_clippingpress', 'required');
		$this->form_validation->set_rules('title', 'lang:press_notetitle', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('press/createclipping');
		}
		else
		{
			$objectInsert = array(
				'clipping' => $this->input->post('clipping'),
				'title' => $this->input->post('title'),
				'userId' => $this->session->UserId,
				'timestamp' => time()
				);
			$this->db->insert('pressClippings', $objectInsert);
			$response = new StdClass();
			$response->status 	= 'ok';
			$response->message 	= $this->lang->line('press_clippingsuccessmessage');
			echo escapeJsonString($response, FALSE);
		}
	}

	function EditClipping(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM pressClippings WHERE pressclippingId  = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$this->form_validation->set_rules('clipping', 'lang:press_clippingpress', 'required');
				$this->form_validation->set_rules('title', 'lang:press_notetitle', 'required');

				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view('press/editclipping', $query);
				}
				else
				{
					$objectEdit = array(
						'clipping' => $this->input->post('clipping'),
						'title' => $this->input->post('title'),
						);
					$this->db->where('pressclippingId', $query->pressclippingId);
					$this->db->update('pressClippings', $objectEdit);
					$response = new StdClass();
					$response->status 	= 'ok';
					$response->message 	= $this->lang->line('press_clippingeditmessage');
					echo escapeJsonString($response, FALSE);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function GetClippingById(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT title, clipping FROM pressClippings WHERE pressclippingId  = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$response = new StdClass();
				$response->status 	= 'ok';
				$response->clipping = $query;
				return escapeJsonString($response, FALSE);
			}
			else
			{
				return '{"status":"invalid"}';
			}
		}
		else{
			return '{"status":"invalid"}';
		}
	}

	function GetNoteById(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM pressNotes WHERE pressnoteId  = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$response = new StdClass();
				$response->status 	= 'ok';
				$response->note = $query;
				return escapeJsonString($response, FALSE);
			}
			else
			{
				return '{"status":"invalid"}';
			}
		}
		else{
			return '{"status":"invalid"}';
		}
	}

	function DeleteClipping(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM pressClippings WHERE pressclippingId  = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$objectDelete = array(
					'pressclippingId' => $query->pressclippingId
					);
				$this->db->delete('pressClippings', $objectDelete);
				$response = new StdClass();
				$response->status 	= 'ok';
				$response->message 	= $this->lang->line('press_clippingdeletemessage');
				return escapeJsonString($response , FALSE);
			}
			else
			{
				return '{"status":"invalid"}';
			}
		}
		else{
			return '{"status":"invalid"}';
		}
	}

	function Delete(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM pressNotes WHERE pressnoteId  = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$objectDelete = array(
					'pressnoteId' => $query->pressnoteId
					);
				$this->db->delete('pressNotes', $objectDelete);
				$response = new StdClass();
				$response->status 	= 'ok';
				$response->message 	= $this->lang->line('press_deletemessage');
				return escapeJsonString($response , FALSE);
			}
			else
			{
				return '{"status":"invalid"}';
			}
		}
		else
		{
			return '{"status":"invalid"}';
		}
	}

	function Edit(){

		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM pressNotes WHERE pressnoteId  = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{

				$this->form_validation->set_rules('pressnote', 'lang:press_pressnote', 'required');

				if ($this->form_validation->run() == FALSE)
				{
					$this->load->view('press/edit', $query);
				}
				else
				{
					$objectEdit = array(
						'pressnote' => $this->input->post('pressnote'),
						);

					$this->db->where('pressnoteId', $query->pressnoteId);
					$this->db->update('pressNotes', $objectEdit);
					$response = new StdClass();
					$response->status 	= 'ok';
					$response->message 	= $this->lang->line('press_editmessage');
					echo escapeJsonString($response, FALSE);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function GetOldPressNotes(){
		if ($this->input->post('last')) {
			$data = new stdClass();
			$sql = "SELECT * FROM pressNotes WHERE pressnoteId < ? ORDER BY pressnoteId DESC LIMIT 0,".$this->maxpressnotes;
			$data->notes = $this->GetNotes($sql, array($this->input->post('last')));
			if (count($data->notes) > 0) {
				$data->status = 'ok';
			}
			else
			{	
				$data->status = 'empty';
			}
			return escapeJsonString($data, FALSE);

		}
		else
		{
			return '{"status":"invalid"}';
		}
	}

	function GetNotes($sql, $object = NULL)
	{
		$notes = $this->db->query($sql, $object)->result();
		return $notes;
	}

	function GetClipping($sql, $object = NULL)
	{
		$clipping = $this->db->query($sql, $object)->result();
		return $clipping;
	}

	function GetOldPressClipping(){
		if ($this->input->post('last')) {
			$data = new stdClass();
			$sql = "SELECT * FROM pressClippings WHERE pressclippingId < ? ORDER BY pressclippingId DESC LIMIT 0,".$this->maxpressnotes;
			$data->clippings = $this->GetClipping($sql, array($this->input->post('last')));
			if (count($data->clippings) > 0) {
				$data->status = 'ok';
			}
			else
			{	
				$data->status = 'empty';
			}
			return escapeJsonString($data, FALSE);

		}
		else
		{
			return '{"status":"invalid"}';
		}
	}

	public function GetUnread()
	{
		$unread = 0;
		if ($this->Identity->Validate('press/note/view')){ 
			$sql = "SELECT count(pn.pressnoteId) c FROM pressNotes AS pn
			WHERE NOT EXISTS (SELECT 1 FROM pressnoteViews AS pnv WHERE pn.pressnoteId = pnv.pressnoteId && pnv.userId = ?)";
			$unread = $this->db->query($sql, $this->session->UserId)->row()->c;
		}
		if ($this->Identity->Validate('press/clipping/view')){ 
			$sql = "SELECT count(pc.pressclippingId) c FROM pressClippings AS pc 
			WHERE NOT EXISTS (SELECT 1 FROM pressclippingViews AS pcv WHERE pc.pressclippingId = pcv.pressclippingId && pcv.userId = ?)";
			$unread += $this->db->query($sql, $this->session->UserId)->row()->c;
		}
		
		return $unread;
	}

}