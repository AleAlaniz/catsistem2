<?php
Class Layout_model extends CI_Model {

	public function GetUser()
	{
		$sql = 'SELECT up.name, up.lastName, uc.background, uc.campaignId FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData WHERE userId = ?) AS up ON u.userId = up.userId INNER JOIN (SELECT campaignId, background, userId FROM userComplementaryData WHERE userId = ?) AS uc ON u.userId = uc.userId WHERE u.userId = ?';
		return $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $this->session->UserId))->row();
	}

	public function GetSections()
	{
		$validRole = '&& sectionId = '.$this->session->sectionId;
		if ($this->Identity->Validate('sections/viewall')) {
			$validRole = '';
		}

		$sectionsSql = 'SELECT icon,name,sectionId FROM sections WHERE true '.$validRole;
		$sectionsQuery = $this->db->query($sectionsSql)->result();

		foreach ($sectionsQuery as $key => $value) {	 
			$value->noViews = FALSE;
			$sql = 'SELECT subsectionId FROM subSections WHERE sectionId = ?';
			$value->subsections = $this->db->query($sql, array($value->sectionId))->result();

			foreach ($value->subsections as $ssvalue) {
				$sql = 'SELECT subsectionId FROM subSections WHERE fereingsubsectionId = ?';
				$sssectionExist = $this->db->query($sql, array($ssvalue->subsectionId))->result();
				$value->subsections = array_merge($value->subsections ,$sssectionExist);
			}

			foreach ($value->subsections as $ssvalue) {

				$sql = 
				'SELECT itemId FROM items WHERE subsectionId = ?  && (SELECT itemforumId FROM itemForums WHERE itemForums.itemId = items.itemId &&
				(SELECT count(*) FROM itemforumTopics 
				WHERE itemforumTopics.itemforumId = itemForums.itemforumId && ( 
				(SELECT count(*) FROM itemforumMessages WHERE userId != ? && itemforumMessages.itemforumtopicId = itemforumTopics.itemforumtopicId 
				&& (SELECT count(*) FROM itemforummessageViews WHERE itemforummessageViews.itemforummessageId = itemforumMessages.itemforummessageId && userId = ?) = 0) > 0 || 
				(SELECT count(*) FROM itemforumtopicViews WHERE itemforumtopicViews.itemforumtopicId = itemforumTopics.itemforumtopicId && userId = ?) = 0) > 0)
				> 0)
				';

				if ($this->db->query($sql, array($ssvalue->subsectionId, $this->session->UserId, $this->session->UserId, $this->session->UserId))->row()) {
					$value->noViews = TRUE;
				}

			}



		}

		return $sectionsQuery;
	}

	public function GetChatNumber()
	{
		$noRead = 0;
		$sql = 'SELECT count(*) c FROM chatuMessages WHERE fromuserId != ? &&
		(SELECT count(*) FROM chatU WHERE chatuId = chatuMessages.chatuId && (fromuserId = ? || touserId = ?)) > 0 &&
		(SELECT count(*) FROM chatumessageViews WHERE chatumessageId = chatuMessages.chatumessageId && userId = ?) = 0 &&
		(SELECT count(*) FROM chatuDeletes WHERE userId = ? && chatuId = chatuMessages.chatuId && currentlyRemoved = "FALSE" && lastmessageIdDelete >= chatuMessages.chatumessageId ORDER BY chatudeleteId DESC LIMIT 1) = 0';
		$noRead += $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $this->session->UserId, $this->session->UserId, $this->session->UserId))->row()->c;

		$sql = 'SELECT count(*) c FROM chatmMessages WHERE userId != ? && (SELECT count(*) FROM chatmUsers WHERE chatmId = chatmMessages.chatmId && userId = ?) > 0 && (SELECT count(*) FROM chatmmessageViews WHERE chatmmessageId = chatmMessages.chatmmessageId && userId = ?) = 0';
		$noRead += $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $this->session->UserId))->row()->c;

		if ($noRead > 0) {
			$noRead = '<span class="messagesnumber">'.$noRead.'</span>';
		}
		else
		{
			$noRead = '';
		}

		return $noRead;
	}

	public function GetPressNotes()
	{
		$noReads = 0;
		if ($this->Identity->Validate('press/index')){
			if ($this->Identity->Validate('press/note/view')){ 
				$sql = "SELECT count(pn.pressnoteId) c FROM pressNotes AS pn
				WHERE NOT EXISTS (SELECT 1 FROM pressnoteViews AS pnv WHERE pn.pressnoteId = pnv.pressnoteId && pnv.userId = ?)";

				$noReads = $this->db->query($sql, $this->session->UserId)->row()->c;
			}
			if ($this->Identity->Validate('press/clipping/view')){ 
				$sql = "SELECT count(pc.pressclippingId) c FROM pressClippings AS pc 
				WHERE NOT EXISTS (SELECT 1 FROM pressclippingViews AS pcv WHERE pc.pressclippingId = pcv.pressclippingId && pcv.userId = ?)";
				$noReads += $this->db->query($sql, $this->session->UserId)->row()->c;
			}


			if ($noReads > 0) {
				$noReads = '<span class="messagesnumber">'.$noReads.'</span>';
			}
			else
			{
				$noReads = '';
			}
		}
		else
		{
			$noReads = '';
		}
		return $noReads;
	}

	public function GetNewsletter()
	{
		$noRead = 0;
		if ($this->Identity->Validate('newsletter/index')){ 
			if ($this->Identity->Validate('newsletter/edit')) {
				$sql = "SELECT * FROM newsletters WHERE (SELECT count(*) FROM newsletterViews WHERE newsletterViews.newsletterId = newsletters.newsletterId && newsletterViews.userId = ?) = 0";
				$noRead = count($this->db->query($sql, $this->session->UserId)->result());
			}
			else
			{
				$sql = "SELECT newsletterId FROM newsletterLinks WHERE (sectionId = ? || roleId = ? || siteId = ? || userlinkId = ?) &&
				(SELECT count(*) FROM newsletterViews WHERE newsletterViews.newsletterId = newsletterLinks.newsletterId && newsletterViews.userId = ?) = 0 
				GROUP BY newsletterId";
				$noRead = count($this->db->query($sql, array($this->session->sectionId,$this->session->roleId, $this->session->siteId, $this->session->UserId, $this->session->UserId))->result());
			}

			if ($noRead > 0) {
				$noRead = '<span class="messagesnumber">'.$noRead.'</span>';
			}
			else
			{
				$noRead = '';
			}
		}
		else
		{
			$noRead = '';
		}
		return $noRead;
	}

	public function GetSuggestBox()
	{
		$noRead = 0;
		if ($this->Identity->Validate('suggestbox/view')){
			/*$roles = ' && sectionId = '.$this->session->sectionId;
			if ($this->Identity->Validate('sections/viewall')) {
				$roles = ' ';
			}
			$sql = 'SELECT sectionId FROM sections WHERE true '.$roles;
			$sections = $this->db->query($sql)->result();
			$sql = 'SELECT Users.sectionId FROM suggestBox INNER JOIN Users ON suggestBox.userId = Users.UserId WHERE (SELECT count(*) FROM suggestboxViews WHERE suggestboxViews.suggestboxId = suggestBox.suggestboxId && userId = ?) = 0';
			$suggests = $this->db->query($sql, $this->session->UserId)->result();
			foreach ($suggests as $suggest) {
				if (!isset($suggest->sectionId)) {
					$suggest->sectionId = 0;
				}

				foreach ($sections as $section) {
					if ($suggest->sectionId == $section->sectionId) {
						$noRead++;
						break;
					}
				}

			}*/
			$unread = 0;
			$roles = ' && sectionId = '.$this->session->sectionId;
			if ($this->Identity->Validate('sections/viewall')) {
				$roles = ' ';
			}
			$sql = 'SELECT count(sb.suggestboxId) AS c 
			FROM suggestBox AS sb
			INNER JOIN (SELECT sectionId, userId FROM users) AS u ON sb.userId = u.userId 
			INNER JOIN (SELECT sectionId FROM sections WHERE true '.$roles.') AS s ON u.sectionId = s.sectionId 
			WHERE NOT EXISTS (SELECT 1 FROM suggestboxViews WHERE suggestboxViews.suggestboxId = sb.suggestboxId && userId = ?)';
			$suggests 	= $this->db->query($sql, $this->session->UserId)->row();
			$noRead 	= $suggests->c;

			if ($noRead > 0) {
				$noRead = '<span class="messagesnumber">'.$noRead.'</span>';
			}
			else
			{
				$noRead = '';
			}
		}
		else
		{
			$noRead = '';
		}
		return $noRead;
	}

	public function GetPast($timestamp)
	{
		$localTime 	= time();
		$rest      	= $localTime-$timestamp;
		$minute 	= 59;
		$hour   	= 3540;
		$day    	= 82800;
		$week   	= 518400;
		$month   	= 2484000;
		$year   	= 30931200;

		if ($rest >= $year)
		{
			$timeStr = 	round($rest/$year);
			$timeStr .= ($timeStr > 1) ? " años" : " año" ;
		}
		else if ($rest >= $month)
		{
			$timeStr = 	round($rest/$month);
			$timeStr .= ($timeStr > 1) ? " meses" : " mes" ;
		}
		else if ($rest >= $week)
		{
			$timeStr = 	round($rest/$week);
			$timeStr .= ($timeStr > 1) ? " semanas" : " semana" ;
		}
		else if ($rest >= $day)
		{
			$timeStr = 	round($rest/$day);
			$timeStr .= ($timeStr > 1) ? " dias" : " dia" ;
		}
		else if ($rest >= $hour)
		{
			$timeStr = 	round($rest/$hour);
			$timeStr .= ($timeStr > 1) ? " horas" : " hora" ;

		}
		else if ($rest >= $minute)
		{
			$timeStr = 	round($rest/$minute);
			$timeStr .= ($timeStr > 1) ? " minutos" : " minuto" ;
		}
		else
		{
			$timeStr = ($rest > 1) ? $rest." segundos" : $rest." segundo" ;
		}
		$timeStr = "Hace ".$timeStr;
		return $timeStr;
	}

	public function customSearch($propToSearch, $array, $prop) {
		foreach ($array as $key => $val) {
			if ($val->{$prop} == $propToSearch) {
				return $key;
			}
		}
		return FALSE;
	}


}