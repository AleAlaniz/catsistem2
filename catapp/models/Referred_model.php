<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Referred_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    function alpha_special($alpha){

        if(! preg_match('/^[a-zA-ZáéíóúÁÉÍÓÚ\s]+$/', $alpha)) {

            $this->form_validation->set_message('alpha_special', 'El campo %s solamente puede contener letras.');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    function cel_number_validation($celnumber){

        if(! preg_match('/^(\+)*[0-9]+$/', $celnumber)) {
            $this->form_validation->set_message('cel_number_validation', 'El campo %s solamente puede contener un símbolo + al inicio, seguido de dígitos numéricos');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }

    public function createReferred($referred)
    {

    	$res = "error";

    	if(isset($referred)){

    		$res = true;

    		$referredCandidate = array(
				'dni' 			=> $referred['dni'],
				'name' 			=> $referred['name'],
				'numberPhone' 	=> $referred['numberPhone'],
				'mail' 			=> (isset($referred['mail'])) ? $referred['mail'] : NULL,
				'userId' 		=> $referred['userId'],
				'date'			=> $referred['date'],
			);
			
			$this->db->insert('referred', $referredCandidate);
			return 'success';
    	}

    	return $res;
    }

    public function allReferredCandidates()
    {
    	$sql = "SELECT 
    	r.referedId,
    	r.dni,
    	r.name,
    	r.numberPhone,
    	r.mail,
    	r.userid,
        r.date,
    	MONTH(FROM_UNIXTIME(r.date)) as month,
    	CONCAT_WS(' ', up.name, up.lastName) as completeName
    	FROM referred r
    	INNER JOIN userPersonalData up ON up.userId = r.userId";
    	$query = $this->db->query($sql)->result();

    	return $query;
    }

    public function editCandidate($candidate)
    {
        
        $res = new StdClass();

        $this->form_validation->set_data($candidate);
        $this->form_validation->set_rules('dni', $this->lang->line('teaming_form_id'), 'required|numeric|min_length[8]|max_length[8]');
        $this->form_validation->set_rules('name', $this->lang->line('teaming_form_name'), 'required|max_length[50]|regex_match[/^[a-zñáéíóúüA-ZÑÁÉÍÓÚÜ ,.+]*$/u]');
        $this->form_validation->set_rules('numberPhone', $this->lang->line('teaming_form_celnumber'), 'required|numeric|max_length[15]');
        $this->form_validation->set_rules('mail', $this->lang->line('administration_users_email'), 'trim|valid_email');
        //sacarle el month!!
        if ($this->form_validation->run() == FALSE){

            $res->success    = false;
            $res->message   = validation_errors();
        }
        else{
            
            $newCandidate = array(
                'referedId'  => $candidate['referedId'],
                'dni' => $candidate['dni'],
                'name' => $candidate['name'],
                'numberPhone' => $candidate['numberPhone'],
                'mail' => $candidate['mail'],
                'userid' => $candidate['userid'],
                'date' => $candidate['date'],
            );
            
            $this->db->where('referedId', $newCandidate["referedId"]);
            $this->db->update('referred', $newCandidate);

             $res->success    = true;
             $res->message    = $newCandidate;
        }

        return $res;
    }
}?>