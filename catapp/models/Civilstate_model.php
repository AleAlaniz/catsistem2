<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Civilstate_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('map_tables');
    }
    
    public function getCivilStates()
    {
        $result = $this->map_tables->get_table('civilStates');
        return $result;
    }

}

/* End of file CivilState.php */

?>