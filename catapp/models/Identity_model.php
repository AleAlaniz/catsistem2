<?php
Class Identity_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('User_model', 'User');
	}

	function Validate($permissionName, $parameter = NULL)
	{
		if ($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId'))
		{
			$validateRole = FALSE;

			$validateRole = in_array($permissionName,$this->session->permissions);

			return $validateRole;
		}
		else
		{
			return FALSE;
		}
	}

	function Autenticate()
	{
		if ($this->input->post('UserName') && $this->input->post('Password')) {
			$sql 	= "SELECT u.userId, u.userName, u.password, u.roleId, u.sectionId, u.siteId FROM users AS u WHERE u.userName = ? && u.active = 1 LIMIT 1";
			$query 	= $this->db->query($sql, $this->input->post('UserName'))->row();

			if (isset($query) && hash_equals($query->password,crypt($this->input->post('Password'), $query->password))) {

				$sql = "SELECT name FROM rolePermissions rp
				JOIN permissons p
				ON (rp.permissionId = p.permisonId)
				WHERE p.active = 'TRUE'
				AND rp.value = 'TRUE'
				AND rp.roleId = ?";

				$data = $this->db->query($sql,$query->roleId)->result();
				$permissions = array();
				foreach ($data as $permis) {
					$permissions [] = $permis->name;
				}
				
				$sessiondata = array(
					'Loged'  		=> TRUE,
					'UserId' 		=> $query->userId,
					'roleId' 		=> $query->roleId,
					'sectionId' 	=> $query->sectionId,
					'siteId' 		=> $query->siteId,
					'permissions' 	=> $permissions
					);

				$this->session->set_userdata($sessiondata);
				
				$access_log = array(
					'userId' 	=> $query->userId,
					'timestamp' => time()
				);
				$this->db->insert('access_logs', $access_log);

				echo 'success';
			}
			else {
				echo $this->lang->line("login_error_message");
			}
		}
		else {
			show_404();
		}
	}

	function divisionAvailable(){

		$sql 	=  "SELECT * FROM teamingsectionavailable WHERE sectionId = ?";
		$query 	=	$this->db->query($sql,$this->session->sectionId)->row();

		if ($this->Identity->validate('teaming/divisions') || ($query!=NULL && $query->isAvailable == 1)){
			return TRUE;
		}

		return FALSE;
	}

	function sawPopup(){

		$sql 	=  "SELECT * FROM usershowpopupteaming WHERE userId = ?";
		$query 	=	$this->db->query($sql,$this->session->UserId)->row();
		
		if($query!=NULL){
			$this->db->delete('usershowpopupteaming', array("userId" => $this->session->UserId));
			return TRUE;
		}

		return FALSE;
	}

	function Logout()
	{
		$log = array(
			'userId' 	=> $this->session->UserId,
			'timestamp' => time()
		);
		$this->db->insert('logout_logs', $log);
		$this->session->sess_destroy();
		header('Location:/'.FOLDERADD);
	}
}
