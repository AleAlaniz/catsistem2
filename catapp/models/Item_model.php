<?php
Class Item_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->lang->load('login_lang');
	}

	var $itemId;
	var $name;
	var $subsectionId;
	var $timestamp;
	var $userId;

	function GetItems(){
		$sql = "SELECT * FROM items ORDER BY name";
		$query = $this->db->query($sql)->result();

		foreach ($query as $key => $item) {	
			$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
			$item->subsection = $this->db->query($sql,$item->subsectionId)->row();
			if(isset($item->subsection))
			{
				
				if ($item->subsection->isssection == 'TRUE') {
					$sqlSection = "SELECT * FROM subSections WHERE subsectionId = ?";
					$querySection = $this->db->query($sqlSection,$item->subsection->fereingsubsectionId)->row();
					$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
					$querysSection = $this->db->query($sqlSection,$querySection->sectionId)->row();
					$item->subsection->name .= ' - '.$querySection->name.' - '.$querysSection->name;
					if (!$this->Identity->Validate('sections/viewall')) {
						if ($querysSection->sectionId != $this->session->sectionId) {
							unset($query[$key]);
						}
					}
				}
				else
				{
					$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
					$querySection = $this->db->query($sqlSection,$item->subsection->sectionId)->row();
					$item->subsection->name .= ' - '.$querySection->name;
					if (!$this->Identity->Validate('sections/viewall')) {
						if ($item->subsection->sectionId != $this->session->sectionId) {
							unset($query[$key]);
						}
					}
				}
			}
		}

		return $query;
	}

	function CreateItem(){
		$sectionsSql = "SELECT * FROM subSections";
		$subsectionsQuery = $this->db->query($sectionsSql)->result();
		$subsectionsIds = '';
		foreach ($subsectionsQuery as $value) {
			$subsectionsIds .= $value->subsectionId.','; 
		}

		$this->form_validation->set_rules('name', 'lang:administration_subsections_name', 'required');
		$this->form_validation->set_rules('subSection', 'lang:administration_subsections_section','required|in_list['.$subsectionsIds.']',
			array('in_list' => $this->lang->line('administration_items_subsectionexist')));

		/*if ($this->input->post('toall') && $this->input->post('toall') == 'FALSE') {
			if (!$this->input->post('usergroupsId[]')) {			
				$this->form_validation->set_rules('usersId[]', 'lang:inbox_selecto', 'required');
			}
		}*/
		$this->form_validation->set_rules('toall', 'lang:inbox_new_to', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$sql = "SELECT * FROM subSections ORDER BY name";
			$query['subSections'] = $this->db->query($sql)->result();


			foreach ($query['subSections'] as $key => $subsection) {
				if ($subsection->isssection == 'TRUE') {
					$sqlSection = "SELECT * FROM subSections WHERE subsectionId = ?";
					$sectionVar = array($subsection->fereingsubsectionId);
					$querySection = $this->db->query($sqlSection,$sectionVar)->row();
					$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
					$querysSection = $this->db->query($sqlSection,$querySection->sectionId)->row();
					$subsection->name .= ' - '.$querySection->name.' - '.$querysSection->name;
					if (!$this->Identity->Validate('sections/viewall')) {
						if ($querysSection->sectionId != $this->session->sectionId) {
							unset($query['subSections'][$key]);
						}
					}
				}
				else
				{
					$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
					$sectionVar = array($subsection->sectionId);
					$querySection = $this->db->query($sqlSection,$sectionVar)->row();
					$subsection->name .= ' - '.$querySection->name;
					if (!$this->Identity->Validate('sections/viewall')) {
						if ($subsection->sectionId != $this->session->sectionId) {
							unset($query['subSections'][$key]);
						}
					}
				}
			}

			$sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
			$query['userGroups'] = $this->db->query($sql, array($this->session->UserId))->result();

			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('items/create', $query);
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			if ($this->input->post('toall') == 'TRUE') {
				$itemInsert = array(
					'name' => $this->input->post('name'),
					'subsectionId' => $this->input->post('subSection'),
					'userId' => $this->session->UserId,
					'description' => $this->input->post('description'),
					'timestamp' => time(),
					'toall' => 'TRUE' 
					);
				$this->db->insert('items', $itemInsert);
				$this->session->set_flashdata('itemMessage', 'create');
				header('Location:/'.FOLDERADD.'/items');
			}
			elseif ($this->input->post('toall') == 'FALSE') {
				$timestamp = time();
				
				$itemInsert = array(
					'name' => $this->input->post('name'),
					'subsectionId' => $this->input->post('subSection'),
					'userId' => $this->session->UserId,
					'description' => $this->input->post('description'),
					'timestamp' => $timestamp,
					'toall' => 'FALSE' 
					);
				$this->db->insert('items', $itemInsert);
				$this->session->set_flashdata('itemMessage', 'create');
				header('Location:/'.FOLDERADD.'/items');


				$sql = "SELECT * FROM items WHERE name = ? && timestamp = ? && subsectionId = ? && userId = ? && toall = 'FALSE' ";
				$query = $this->db->query($sql, array($this->input->post('name'),$timestamp,$this->input->post('subSection'),$this->session->UserId))->row();
				if (isset($query) && $query != NULL) {
					if ($this->input->post('usersId[]')) {
						foreach ($this->input->post('usersId[]') as $key => $user) {
							$validRole = '&& roleId != '.$this->session->Role;

							if ($this->Identity->Validate('home/inbox/new/torole')) {
								$validRole = '';
							}

							$sql = "SELECT userId FROM users WHERE userId = ? && userId != ? ".$validRole;
							$userExist = $this->db->query($sql, array($user, $this->session->UserId))->row();

							if (isset($userExist) && $userExist != NULL) {
								$objectInsert = array(
									'timestamp' => $timestamp,
									'userId' => $user,
									'itemId' => $query->itemId,
									);
								$this->db->insert('itemUsers', $objectInsert);
							}
						}
					}
					if ($this->input->post('usergroupsId[]') && $this->Identity->Validate('usergroups/index')) {
						foreach ($this->input->post('usergroupsId[]') as $usergroupKey => $usergroup) {
							$sql = "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
							$groupExist = $this->db->query($sql, array($usergroup, $this->session->UserId))->row();
							if ($groupExist != NULL) {
								$sql = "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
								$groupUsers = $this->db->query($sql,array($groupExist->usergroupId))->result();
								foreach ($groupUsers as $key => $user) {
									$validRole = '&& roleId != '.$this->session->Role;

									if ($this->Identity->Validate('home/inbox/new/torole')) {
										$validRole = '';
									}
									$sql = "SELECT userId FROM users WHERE userId = ? && userId != ? ".$validRole;
									$userExist = $this->db->query($sql, array($user->userId, $this->session->UserId))->row();

									if (isset($userExist) && $userExist != NULL) {
										$sql = "SELECT userId, itemId FROM itemUsers WHERE itemId = ? && userId = ?";
										$useritemExist = $this->db->query($sql,array($query->itemId, $user->userId))->row();
										if ($useritemExist == NULL) {
											$objectInsert = array(
												'timestamp' => $timestamp,
												'userId' => $user->userId,
												'itemId' => $query->itemId,
												);
											$this->db->insert('itemUsers', $objectInsert);
										}
									}
								}
							}
						}
					}

					$this->session->set_flashdata('itemMessage', 'create');
					header('Location:/'.FOLDERADD.'/items');
				}
				else{
					header('Location:/'.FOLDERADD.'/items');
				}
			}
			else{
				show_404();
			}
		}
	}

	function DeleteItem(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM items WHERE itemId = ?";
			$item = array($this->uri->segment(3));
			$query = $this->db->query($sql,$item)->row();

			if (isset($query)) {

				$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
				$query->subsection = $this->db->query($sql,$query->subsectionId)->row();

				if ($query->subsection->isssection == 'TRUE') {
					$sqlSection = "SELECT * FROM subSections WHERE subsectionId = ?";
					$querySection = $this->db->query($sqlSection,$query->subsection->fereingsubsectionId)->row();
					$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
					$querysSection = $this->db->query($sqlSection,$querySection->sectionId)->row();
					if (!$this->Identity->Validate('sections/viewall')) {
						if ($querysSection->sectionId != $this->session->sectionId) {
							unset($query);
						}
					}
				}
				else
				{
					$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
					$querySection = $this->db->query($sqlSection,$query->subsection->sectionId)->row();
					if (!$this->Identity->Validate('sections/viewall')) {
						if ($query->subsection->sectionId != $this->session->sectionId) {
							unset($query);
						}
					}
				}
			}


			if (isset($query))
			{
				if ($this->input->post('itemId') && $this->input->post('itemId') == $query->itemId ) {

					insert_audit_logs('items','DELETE',$query);

					$itemDelete = array(
						'itemId' => $this->input->post('itemId')
						);
					$this->db->delete('items', $itemDelete);
					$this->session->set_flashdata('itemMessage', 'delete');
					header('Location:/'.FOLDERADD.'/items');
				}
				else{
					$query->navBar = $this->load->view('items/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('items/delete', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function DetailsItem(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM items WHERE itemId = ?";
			$item = array($this->uri->segment(3));
			$query = $this->db->query($sql,$item)->row();
			if (isset($query)) {

				$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
				$query->subsection = $this->db->query($sql,$query->subsectionId)->row();

				if ($query->subsection->isssection == 'TRUE') {
					$sqlSection = "SELECT * FROM subSections WHERE subsectionId = ?";
					$querySection = $this->db->query($sqlSection,$query->subsection->fereingsubsectionId)->row();
					$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
					$querysSection = $this->db->query($sqlSection,$querySection->sectionId)->row();
					if (!$this->Identity->Validate('sections/viewall')) {
						if ($querysSection->sectionId != $this->session->sectionId) {
							unset($query);
						}
					}
				}
				else
				{
					$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
					$querySection = $this->db->query($sqlSection,$query->subsection->sectionId)->row();
					if (!$this->Identity->Validate('sections/viewall')) {
						if ($query->subsection->sectionId != $this->session->sectionId) {
							unset($query);
						}
					}
				}
			}
			if (isset($query))
			{
				$query->navBar = $this->load->view('items/_navbar', $query, TRUE);
				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('items/details', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function EditItem(){

		if ($this->uri->segment(3)) {

			$sql = "SELECT * FROM items WHERE itemId = ?";
			$item = array($this->uri->segment(3));
			$query = $this->db->query($sql,$item)->row();

			if (isset($query)) {

				$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
				$query->subsection = $this->db->query($sql,$query->subsectionId)->row();

				if ($query->subsection->isssection == 'TRUE') {

					$sqlSection = "SELECT * FROM subSections WHERE subsectionId = ?";
					$querySection = $this->db->query($sqlSection,$query->subsection->fereingsubsectionId)->row();

					$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
					$querysSection = $this->db->query($sqlSection,$querySection->sectionId)->row();

					if (!$this->Identity->Validate('sections/viewall')) {

						if ($querysSection->sectionId != $this->session->sectionId) {
							unset($query);
						}
					}
				}
				else
				{
					$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
					$querySection = $this->db->query($sqlSection,$query->subsection->sectionId)->row();

					if (!$this->Identity->Validate('sections/viewall')) {
						if ($query->subsection->sectionId != $this->session->sectionId) {
							unset($query);
						}
					}
				}
			}
			if (isset($query))
			{
				if ($query->toall == 'TRUE') {
					$query->tousers = FALSE;
					$query->toall 	= TRUE;
				}
				else{
					$query->tousers = TRUE;
					$query->toall 	= FALSE;

				}

				$sql = "SELECT u.userId, u.name, u.lastName FROM itemUsers AS iu INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS u ON iu.userId = u.userId WHERE itemId = ?";
				$query->users = $this->db->query($sql, $query->itemId)->result();

				$sql = "SELECT * FROM subSections ORDER BY name";
				$query->subSections = $this->db->query($sql)->result();

				foreach ($query->subSections as $key => $subsection) {
					if ($subsection->isssection == 'TRUE') {
						$sqlSection = "SELECT * FROM subSections WHERE subsectionId = ?";
						$sectionVar = array($subsection->fereingsubsectionId);
						$querySection = $this->db->query($sqlSection,$sectionVar)->row();
						$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
						$querysSection = $this->db->query($sqlSection,$querySection->sectionId)->row();
						$subsection->name .= ' - '.$querySection->name.' - '.$querysSection->name;
						if (!$this->Identity->Validate('sections/viewall')) {
							if ($querysSection->sectionId != $this->session->sectionId) {
								unset($query->subSections[$key]);
							}
						}
					}
					else
					{
						$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
						$sectionVar = array($subsection->sectionId);
						$querySection = $this->db->query($sqlSection,$sectionVar)->row();
						$subsection->name .= ' - '.$querySection->name;
						if (!$this->Identity->Validate('sections/viewall')) {
							if ($subsection->sectionId != $this->session->sectionId) {
								unset($query->subSections[$key]);
							}
						}
					}
				}

				$sql = "SELECT * FROM userGroups WHERE userId = ? ORDER BY name";
				$query->userGroups = $this->db->query($sql, array($this->session->UserId))->result();

				if ($this->input->post('itemId') && $this->input->post('itemId') == $query->itemId ) {

					$subsectionsSql = "SELECT * FROM subSections";
					$subsectionsQuery = $this->db->query($subsectionsSql)->result();
					$subsectionsIds = '';
					foreach ($subsectionsQuery as $value) {
						$subsectionsIds .= $value->subsectionId.','; 
					}
					$this->form_validation->set_rules('name', 'lang:administration_items_name', 'required');
					$this->form_validation->set_rules('subSection', 'lang:administration_items_subsection','required|in_list['.$subsectionsIds.']',
						array('in_list' => $this->lang->line('administration_items_subsectionexist')));


					$this->form_validation->set_rules('toall', 'lang:inbox_new_to', 'required');

					if ($this->form_validation->run() == FALSE)
					{
						$query->navBar = $this->load->view('items/_navbar', $query, TRUE);
						$this->load->view('_shared/_administrationlayoutheader');
						$this->load->view('items/edit',$query);
						$this->load->view(PRELAYOUTFOOTER);
					}
					else
					{
						if ($this->input->post('toall') == 'TRUE') {

							insert_audit_logs('items','UPDATE',$query);
							
							$itemEdit = array(
								'name' => $this->input->post('name'),
								'subsectionId' => $this->input->post('subSection'),
								'description' => $this->input->post('description'),
								'toall' => 'TRUE'
								);

							$this->db->where('itemId', $query->itemId);
							$this->db->update('items', $itemEdit);

							$itemDelete = array(
								'itemId' => $this->input->post('itemId')
								);
							$this->db->delete('itemUsers', $query);
							$this->session->set_flashdata('itemMessage', 'edit');
							header('Location:/'.FOLDERADD.'/items/details/'.$query->itemId);
						}
						elseif ($this->input->post('toall') == 'FALSE') {
							$timestamp = time();

							$itemEdit = array(
								'name' => $this->input->post('name'),
								'subsectionId' => $this->input->post('subSection'),
								'description' => $this->input->post('description'),
								'toall' => 'FALSE'
								);
							$this->db->where('itemId', $query->itemId);
							$this->db->update('items', $itemEdit);

							$itemDelete = array(
								'itemId' => $this->input->post('itemId')
								);
							$this->db->delete('itemUsers', $itemDelete);

							if ($this->input->post('usersId[]')) {
								foreach ($this->input->post('usersId[]') as $key => $user) {
									$validRole = '&& roleId != '.$this->session->Role;

									if ($this->Identity->Validate('home/inbox/new/torole')) {
										$validRole = '';
									}

									$sql = "SELECT userId FROM users WHERE userId = ? && userId != ? ".$validRole;
									$userExist = $this->db->query($sql, array($user, $this->session->UserId))->row();

									if (isset($userExist) && $userExist != NULL) {
										$objectInsert = array(
											'timestamp' => $timestamp,
											'userId' => $user,
											'itemId' => $query->itemId,
											);
										$this->db->insert('itemUsers', $objectInsert);
									}
								}
							}
							if ($this->input->post('usergroupsId[]') && $this->Identity->Validate('usergroups/index')) {
								$validRole = '&& roleId != '.$this->session->Role;

								if ($this->Identity->Validate('home/inbox/new/torole')) {
									$validRole = '';
								}
								foreach ($this->input->post('usergroupsId[]') as $usergroupKey => $usergroup) {
									$sql = "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
									$groupExist = $this->db->query($sql, array($usergroup, $this->session->UserId))->row();
									if ($groupExist != NULL) {
										$sql = "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
										$groupUsers = $this->db->query($sql,array($groupExist->usergroupId))->result();
										foreach ($groupUsers as $key => $user) {
											$sql = "SELECT userId FROM users WHERE userId = ? && userId != ? ".$validRole;
											$userExist = $this->db->query($sql, array($user->userId, $this->session->UserId))->row();

											if (isset($userExist) && $userExist != NULL) {
												$sql = "SELECT userId, itemId FROM itemUsers WHERE itemId = ? && userId = ?";
												$useritemExist = $this->db->query($sql,array($query->itemId, $user->userId))->row();
												if ($useritemExist == NULL) {
													$objectInsert = array(
														'timestamp' => $timestamp,
														'userId' => $user->userId,
														'itemId' => $query->itemId,
														);
													$this->db->insert('itemUsers', $objectInsert);
												}
											}
										}
									}
								}
							}

							$this->session->set_flashdata('itemMessage', 'edit');
							header('Location:/'.FOLDERADD.'/items/details/'.$query->itemId);
						}
						else{
							show_404();
						}
					}
				}
				else{
					$query->navBar = $this->load->view('items/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('items/edit', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function GetItem($view)
	{
		if ($this->input->post('itemId'))
		{
			$sql = "SELECT * FROM items WHERE itemId = ?";
			$item = array($this->input->post('itemId'));
			$query = $this->db->query($sql,$item)->row();
			
			if (isset($query) && !$this->Identity->Validate('items/edit') && $query->toall == 'FALSE')
			{
				$sql = 'SELECT * FROM itemUsers WHERE itemId = ? && userId = ?';
				$userExist = $this->db->query($sql, array($query->itemId, $this->session->UserId))->row();

				if (!isset($userExist)) 
				{
					$query = NULL;
				}
			}

			if (isset($query))
			{
				$query->noViews = FALSE;

				$sql = "SELECT itemfavoriteId FROM itemFavorites WHERE itemId = ? && userId = ?";
				$query->isFavorite =  ($this->db->query($sql, array($query->itemId, $this->session->UserId))->row() != NULL) ? TRUE : FALSE; 

				$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
				$subSection = array($query->subsectionId);
				$query->subSection = $this->db->query($sql,$subSection)->row();

				$sectionIdtouse = $query->subSection->sectionId;

				if (isset($query->subSection->fereingsubsectionId))
				{
					$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
					$sssection = array($query->subSection->fereingsubsectionId);
					$query->sssection = $this->db->query($sql,$sssection)->row();
					$sectionIdtouse = $query->sssection->sectionId;
				}
				
				$sql = "SELECT * FROM sections WHERE sectionId = ?";
				$section = array($sectionIdtouse);
				$query->section = $this->db->query($sql,$section)->row();

				
				$query->partialView = $this->load->view('items/_viewitem', $query,true);
				
				if($view)
					return $query;
				else
					echo json_encode($query);
				
			}
			else
			{
				echo "fail";
			}
		}
		else
		{
			echo "fail";
		}
	}


	public function AddFavorite()
	{
		if($this->input->post('itemId'))
		{
			$sql="SELECT itemfavoriteId
			FROM itemFavorites
			WHERE itemId=? and userId=?";

			$data=$this->db->query($sql,array($this->input->post('itemId'),$this->session->UserId))->row();

			if(isset($data))
			{
				$this->db->where('itemfavoriteId',$data->itemfavoriteId);
				$this->db->delete('itemFavorites');
			}
			else
			{
				$newRow=array(
					'userId'=>$this->session->UserId,
					'itemId'=>$this->input->post('itemId'),
					'timestamp'=>time()
					);

				$this->db->insert('itemFavorites',$newRow);
			}

			echo "success";
		}
		else
			echo "no item";

	}
}
