<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity_model extends CI_Model {

    public function __construct()
    {
		parent::__construct();
		$this->load->library('map_tables');
    }

    public function getActivities()
    {
		$sql = "SELECT turn FROM userComplementaryData WHERE userId = ?";
		$turn = $this->db->query($sql,$this->session->UserId)->row();

        if ($this->Identity->Validate('activities/manage')) {
			$sql = "
			SELECT a.description, a.name, a.endDate, a.startDate, a.activityId,a.capacity,a.image, a.timestamp,a.active, 
			(SELECT count(*) FROM activityCompletes AS ac WHERE ac.activityId = a.activityId && ac.userId = ? LIMIT 1) complete, 
			(SELECT resp.activityanswerId FROM activityCompletes AS ac INNER JOIN activityResponses resp ON ac.activitycompleteId = resp.activitycompleteId WHERE ac.activityId = a.activityId && ac.userId = ? LIMIT 1) willAssist
			FROM activities AS a 
			ORDER BY a.activityId DESC";
			$activities = $this->db->query($sql, array($this->session->UserId, $this->session->UserId))->result();
		}
		else
		{
			$sql = "
			SELECT a.description, a.name, a.endDate, a.startDate, a.activityId,a.capacity,a.image, a.timestamp,a.active,al.turns,
			(SELECT count(*) FROM activityCompletes AS ac WHERE ac.activityId = a.activityId && ac.userId = ? LIMIT 1) complete,
			(SELECT resp.activityanswerId FROM activityCompletes AS ac INNER JOIN activityResponses resp ON ac.activitycompleteId = resp.activitycompleteId WHERE ac.activityId = a.activityId && ac.userId = ? LIMIT 1) willAssist
			FROM activityLinks AS al 
			INNER JOIN activities AS a ON al.activityId = a.activityId
			WHERE (al.userId = ? || al.sectionId = ? || al.siteId = ? || al.roleId = ?) 
			AND (al.turns = 'NO' || FIND_IN_SET(?,al.turns))
			AND a.active='TRUE'
			GROUP BY al.activityId";
			$activities = $this->db->query($sql, array(
				$this->session->UserId, 
				$this->session->UserId, 
				$this->session->UserId, 
				$this->session->sectionId, 
				$this->session->siteId, 
				$this->session->roleId,
				$turn->turn))->result();
		}
		foreach ($activities as  $activity) {
			$timestamp = time();
			$activity->isTime = (($activity->startDate < $timestamp || $activity->startDate == NULL) && ($activity->endDate > $timestamp || $activity->endDate == NULL)) ? TRUE : FALSE;
			if ($activity->endDate) {
				$activity->endDate = date('d/m/Y',$activity->endDate);
			}
			if ($activity->startDate) {
				$activity->startDate = date('d/m/Y',$activity->startDate);
			}
		}
		return $activities;
    }

	public function getActivity($activityId)
	{

		$sql="SELECT activityId,name,description,image,closeMessage,date_format(from_unixtime(startDate),'%m/%d/%Y') as startDate,
		date_format(from_unixtime(endDate),'%m/%d/%Y') as endDate,capacity,required
		FROM activities
		WHERE activityId = ?";

		$data = $this->db->query($sql,$activityId)->row();

		if(isset($data))
		{
			$sql="SELECT *
			FROM activityQuestions";

			$data->questions=$this->db->query($sql)->result();

			foreach ($data->questions as $q)
			{
				$sql="SELECT *
				FROM  activityAnswers
				where activityquestionId=?";

				$q->answers=$this->db->query($sql,$q->activityquestionId)->result();
			}

			$sql = "SELECT sectionId, name FROM sections ORDER BY name";
			$data->sections = $this->db->query($sql)->result();

			$sql = "SELECT roleId, name FROM roles ORDER BY name";
			$data->roles = $this->db->query($sql)->result();

			$sql = "SELECT siteId, name FROM sites ORDER BY name";
			$data->sites = $this->db->query($sql)->result();

			if ($this->Identity->Validate('usergroups/create'))
			{
				$sql = "SELECT userGroupId, name FROM userGroups WHERE userId = ? ORDER BY name";
				$data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
			}

			$sql="SELECT * FROM activityLinks where activityId = ?";
			$links=$this->db->query($sql,$activityId)->result();

			$users=array();

			foreach($links as $link)
			{
				if ($link->sectionId != NULL)
				{
					for($i=0;$i < count($data->sections);$i++)
					{
						if($data->sections[$i]->sectionId==$link->sectionId)
						{
							$data->sections[$i]->active=true;
							break;
						}
					}

				}
				elseif ($link->roleId != NULL)
				{
					for($i=0;$i < count($data->roles);$i++)
					{
						if($data->roles[$i]->roleId==$link->roleId)
						{
							$data->roles[$i]->active=true;
							break;
						}
					}
				}
				elseif ($link->siteId != NULL) 
				{
					for($i=0;$i < count($data->sites);$i++)
					{
						if($data->sites[$i]->siteId==$link->siteId)
						{
							$data->sites[$i]->active=true;
							break;
						}
					}
				}
				elseif ($link->userId != NULL)
				{
					$sql = "SELECT u.userId, up.name, up.lastName, u.userName FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData WHERE userId = ?) AS up ON u.userId = up.userId WHERE u.userId = ?";
					$tempVar = $this->db->query($sql, array($link->userId, $link->userId))->row();
					array_push($users, $tempVar);	
				}
			}

			$data->users=$users;
			$data->turns = $this->map_tables->get_table('turns');
			echo json_encode($data);
		}
		else
		{
			echo "error";
			return;
		}
	}

    public function getUsersInActivity($like,$activity)
	{	
		$usersInActivity = new StdClass();

		if ($activity)
		{
			if ($this->Identity->Validate('activities/manage'))
			{
				
				$query = "SELECT concat_ws(' ', up.name, up.lastName) AS completeName, up.userId, users.userName
				FROM userPersonalData up
				JOIN users
				ON (users.userId = up.userId)
				WHERE (concat_ws(' ', name, lastName) LIKE '%$like%' OR userName LIKE '%$like%')
				AND users.userId NOT IN (SELECT userId FROM activityCompletes WHERE activityId = ?)";
				
				$usersInActivity = $this->db->query($query,$activity)->result();
			}
		}

		return $usersInActivity;
	}
    
    public function CompleteActivityManual($userId)
	{
		$sql = "SELECT userId as UserId,sectionId,siteId,roleId
		FROM users 
		WHERE userId = ?";

		$user = $this->db->query($sql,$userId)->row();

		if (isset($user)) {
			$this->CompleteActivity($user,true);
		}
		else
		{
			show_404();	
		}

	}

	public function CompleteActivityNormal()
	{
		$this->CompleteActivity($this->session);
	}

    public function CompleteActivity($user,$manual = false)
	{
		
		$timestamp = time();
		if(isset($user))
			{
				$sql = "SELECT turn FROM userComplementaryData WHERE userId = $user->UserId";
				$turn = $this->db->query($sql)->row();
			}
		if ($this->uri->segment(3) != NULL) 
		{
			
			if ($this->Identity->Validate('activities/manage'))
			{
				$sql = 
				"SELECT activities.activityId, activities.name, activities.description, activities.closeMessage,activities.image, activities.required, activities.active,activities.capacity
				FROM activities
				WHERE activities.active='TRUE' && activities.activityId = ? ";
				
				$param = array($this->uri->segment(3));
				
				if(!$manual)
				{
					$sql.=" && (activities.startDate <= ? || activities.startDate IS NULL) 
					&& (activities.endDate >= ? || activities.endDate IS NULL)
					&& (SELECT count(*) FROM activityCompletes WHERE activityCompletes.activityId = activities.activityId && activityCompletes.userId = ?) = 0";
					
					array_push($param,$timestamp, $timestamp,$user->UserId);
				}

				$activity = $this->db->query($sql, $param)->row();
				
			}
			else
			{
				$sql = 
				"SELECT activities.activityId, activities.name, activities.description, activities.closeMessage,activities.image, activities.required,activities.active,activities.capacity
				FROM activityLinks INNER JOIN activities ON activityLinks.activityId = activities.activityId
				WHERE (activityLinks.userId = ? || activityLinks.sectionId = ? || activityLinks.siteId = ? || activityLinks.roleId = ?)
				&& (activityLinks.turns = 'NO' || FIND_IN_SET(?,activityLinks.turns))
				&& activities.active='TRUE'
				&& (activities.startDate <= ? || activities.startDate IS NULL) 
				&& (activities.endDate >= ? || activities.endDate IS NULL)
				&& (SELECT count(*) FROM activityCompletes 
				WHERE activityCompletes.activityId = activities.activityId && activityCompletes.userId = ?) = 0 && activities.activityId = ? ";

				$activity = $this->db->query($sql, 
					array($user->UserId, 
						$user->sectionId, 
						$user->siteId, 
						$user->roleId,
						$turn->turn,
						$timestamp, 
						$timestamp,
						$user->UserId,
						$this->uri->segment(3)))->row();
			}
		}
		else
		{
			
			$sql = 
			"SELECT activities.activityId, activities.name, activities.description,activities.required,activities.image,activities.closeMessage,activities.active,activities.capacity
            FROM activityLinks INNER JOIN activities ON activityLinks.activityId = activities.activityId
            WHERE activities.required = 'TRUE' && (capacity > 0 || capacity IS NULL) 
			&& (activityLinks.userId = ? || activityLinks.sectionId = ? || activityLinks.siteId = ? || activityLinks.roleId = ?)
			&& (activityLinks.turns = 'NO' || FIND_IN_SET(?,activityLinks.turns))
            && activities.active = 'TRUE'
            && (activities.startDate <= ? || activities.startDate IS NULL) 
            && (activities.endDate >= ? || activities.endDate IS NULL)
            && (SELECT count(*) FROM activityCompletes WHERE activityCompletes.activityId = activities.activityId && activityCompletes.userId = ?) = 0
            GROUP BY activityLinks.activityId ORDER BY activities.activityId ASC";

			$activity = $this->db->query($sql, 
				array($user->UserId, 
					$user->sectionId, 
					$user->siteId, 
					$user->roleId,
					$turn->turn,
					$timestamp, 
					$timestamp,
					$user->UserId))->row();
		}

		if (isset($activity)) 
		{
			$this->form_validation->set_rules('activity', 'activity', 'required');
			
			if ($this->form_validation->run() === FALSE)
			{
				$sql = "SELECT * FROM activityQuestions";
				$activity->questions = $this->db->query($sql, $activity->activityId)->result();
				foreach ($activity->questions as $question) 
				{
					$sql = "SELECT * FROM activityAnswers WHERE activityquestionId = ?";
					$question->answers = $this->db->query($sql, $question->activityquestionId)->result();
				}
				
				$activity->isManual = $manual;
				
				$this->load->view("activities/_activities_header", array('isManual'=>$activity->isManual));
				$this->load->view("activities/complete", $activity);
				$this->load->view("activities/_activities_footer");
			}
			else
			{
				if(!isset($user)){ 
					$this->session->set_flashdata('flash_message', 'notUser');
					header('Location:/'.FOLDERADD.'/activity/showManualCharge/'.$activity->activityId);
					exit();
				}

				$sql = "SELECT * FROM activityCompletes WHERE userId = ? AND activityId = ?";
				$isCompleted = $this->db->query($sql,array($user->UserId,$activity->activityId))->num_rows();

				if($isCompleted == 0)
				{
					
					if ($this->input->post('activity') == $activity->activityId) 
					{
						if($activity->capacity > 0 || $activity->capacity == NULL)
						{
							$timestamp = time();
							$object = array(
								'timestamp' 	=> $timestamp,
								'userId' 		=> $user->UserId,
								'activityId' 	=> $activity->activityId
							);

							$this->db->insert('activityCompletes', $object);
							
							if($activity->capacity != NULL){
								$capacity = intval($activity->capacity);
								if($this->input->post("questions[1]") == 1){
									$capacity--;
								}
								$capacity = strval($capacity);
	
								$this->db->where('activityId', $activity->activityId);
								$this->db->update('activities', array('capacity' => $capacity));
							}

							$question = new StdClass();
							$sql = "SELECT activityquestionId, required FROM activityQuestions";
							$question = $this->db->query($sql)->row();
							
							$question->answer = $this->input->post("questions[$question->activityquestionId]");

							$sql = "SELECT activitycompleteId FROM activityCompletes WHERE userId = ? && activityId = ?";
							$activityComplete = $this->db->query($sql, array($user->UserId, $activity->activityId))->last_row();

							if (isset($activityComplete)) {
								
								$object = array(
									'activityanswerId'	=> $question->answer,
									'activityquestionId'  => $question->activityquestionId,
									'activitycompleteId' 	=> $activityComplete->activitycompleteId
								);
								$this->db->insert('activityResponses', $object);

								$this->session->set_flashdata('flash_message', 'activityCompleted');
								if($manual)
								{
									header('Location:/'.FOLDERADD.'/activity/showManualCharge/'.$activity->activityId);	
								}
								else
								{
									header('Location:/'.FOLDERADD.'/#/activities');
								}
							}
							else
							{
								show_404();
							}
						}
						else {
							header('Location:/'.FOLDERADD.'/#/activities');
						}
						
					}
					else
					{
						show_404();
					}
				}
				else 
				{
					$this->session->set_flashdata('flash_message', 'userInCompleteactivity');
					header('Location:/'.FOLDERADD.'/activity/showManualCharge/'.$activity->activityId);	
				}

			}

		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function ViewActivityPreview()
	{
		if($this->uri->segment(3))
		{
			if ($this->Identity->Validate('activities/manage'))
			{
				$sql = 
				"SELECT activities.activityId, activities.name, activities.description, activities.closeMessage, activities.required,activities.active
				FROM activities
				WHERE activities.activityId = ?";

				$activity = $this->db->query($sql, $this->uri->segment(3))->row();

				if(isset($activity))
				{
					$sql = "SELECT * FROM activityQuestions WHERE activityId = ?";
					$activity->questions = $this->db->query($sql, $activity->activityId)->result();

					foreach ($activity->questions as $question) 
					{
						if ($question->type == 1 || $question->type == 2) 
						{
							$sql = "SELECT * FROM activityAnswers WHERE activityquestionId = ?";
							$question->answers = $this->db->query($sql, $question->activityquestionId)->result();
						}
					}

					$this->load->view("activities/_activities_header",array('isManual'=>FALSE));
					$this->load->view("activities/complete", $activity);
					$this->load->view("_shared/_footer");
				}
				else
				{
					header('Location:/'.FOLDERADD);		
				}
			}
			else
			{
				header('Location:/'.FOLDERADD);
			}
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function Delete()
	{
		$status = "Error";
		if ($this->uri->segment(3)) {

			$sql = "SELECT * FROM activities WHERE activityId = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();

			if (isset($query))
			{

				$sql = "SELECT * FROM activityLinks WHERE activityId = ?";
				$query->activityLinks = $this->db->query($sql, $query->activityId)->result();

				$sql = "SELECT * FROM activityCompletes WHERE activityId = ?";
				$query->activitycompletes = $this->db->query($sql, $query->activityId)->result();

				$query->activityResponses = array();
				
				foreach ($query->activitycompletes as $complete) {
					$sql = "SELECT * FROM activityResponses WHERE activitycompleteId = ?";
					$response = $this->db->query($sql, $complete->activitycompleteId)->row();
					array_push($query->activityResponses, $response);
				}

				insert_audit_logs('activities','DELETE',$query);

				$object = array(
					'activityId' => $this->uri->segment(3)
				);
				unlink('./catapp/_activities_images/'.$query->image); 
				$this->db->delete('activities', $object);

				$status = $this->lang->line('activity_deleted');;
			}
		}

		return $status;
	}

	public function Discharge()
	{
		$status = 'fail';

		if ($this->uri->segment(3)) {

			$sql = "SELECT * FROM activities WHERE activityId = ?";
			$activity = $this->db->query($sql,$this->uri->segment(3))->row();

			if (isset($activity))
			{
				$sql = "SELECT activitycompleteId FROM activityCompletes WHERE activityId = ? AND userId = ?";
				$completeId = $this->db->query($sql,array("activityId" => $this->uri->segment(3),"userId" => $this->session->UserId))->row();
				
				if (isset($completeId)){
					$status = 'ok';
					$sql = "SELECT activityresponseId,activityanswerId FROM activityResponses WHERE activitycompleteId = $completeId->activitycompleteId";
					$result = $this->db->query($sql)->row();
					
					$answerId = $result->activityanswerId;
					if($answerId == 1){
						if($activity->capacity != NULL){
							$capacity = intval($activity->capacity);
							$capacity ++;
							$capacity = strval($capacity);

							$this->db->where('activityId', $activity->activityId);
							$this->db->update('activities', array('capacity' => $capacity));
						}
					}

					$this->db->where('activityresponseId', $result->activityresponseId);
					$this->db->delete('activityResponses');

					$this->db->where('activitycompleteId', $completeId->activitycompleteId);
					$this->db->delete('activityCompletes');
				}
			}
		}

		return $status;
	}

	public function Create()
	{
		$this->form_validation->set_rules('name', 'lang:activity_name', 'required');
		$this->form_validation->set_rules('startDate', 'lang:survey_start_date', 'required');
		$this->form_validation->set_rules('endDate', 'lang:survey_end_date', 'required');
		// $this->form_validation->set_rules('userfile', 'lang:attached_file', 'required');

		
		if (!$this->input->post('sections[]') && !$this->input->post('roles[]') && !$this->input->post('sites[]') && !$this->input->post('userGroups[]') && !$this->input->post('users[]')) {
			$this->form_validation->set_rules('whose', 'lang:activity_whose', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			$data = new StdClass();
			$sql = "SELECT sectionId, name FROM sections ORDER BY name";
			$data->sections = $this->db->query($sql)->result();

			$sql = "SELECT roleId, name FROM roles ORDER BY name";
			$data->roles = $this->db->query($sql)->result();

			$sql = "SELECT siteId, name FROM sites ORDER BY name";
			$data->sites = $this->db->query($sql)->result();

			if ($this->Identity->Validate('usergroups/create')) {
				$sql = "SELECT userGroupId, name FROM userGroups WHERE userId = ? ORDER BY name";
				$data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
			}

			$data->turns = $this->map_tables->get_table('turns');

			$this->load->view('activities/create',$data);

		}
		else
		{

			$timestamp = time();

			$config['upload_path']          = './catapp/_activities_images/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['max_size']             = 50480;
			if (isset($_FILES['userfile']) && $_FILES['userfile'] != NULL) {
				$name = $this->sanear_string($_FILES['userfile']['name']);
				$config['file_name']			= $timestamp.'-'. $name;
			}
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('userfile'))
			{
				$response = new StdClass();
				$response->status  = $this->upload->display_errors();
				$response->message = $this->lang->line('corporatedoccategory_completeall');
				echo escapeJsonString($response,false);
				return;
			}
			else
			{
				$startDate 	= NULL;
				$endDate 	= NULL;

				$startDate = strtotime($this->input->post('startDate'));
				if ($startDate === FALSE) {
					echo '{"status":"invalid"}';
					return;
				}

				$endDate = strtotime($this->input->post('endDate'));
				if ($endDate === FALSE) {
					echo '{"status":"invalid"}';
					return;
				}

				$question = new StdClass();
				$sql = "SELECT * FROM activityQuestions";
				$question = $this->db->query($sql)->row();

				$sql = "SELECT * FROM activityAnswers";
				$question->answers = $this->db->query($sql)->result();


				$closeMessage = $this->input->post('closeMessage');
				if (!$closeMessage || !(is_string($closeMessage) || is_int($closeMessage))) {
					$closeMessage = NULL;
				}


				$isRequired = $this->input->post('required') ? 'TRUE' : 'FALSE'; 
				$object = array(
					'name'		 	=> $this->input->post('name'),
					'timestamp' 	=> $timestamp,
					'image'			=> $this->upload->data('file_name'),
					'userId' 		=> $this->session->UserId,
					'description' 	=> $this->input->post('descriptionS'),
					'startDate' 	=> $startDate,
					'endDate' 		=> $endDate,
					'capacity'		=> $this->input->post('coupons'),
					'closeMessage' 	=> $closeMessage,
					'required'		=> $isRequired,
					'active'		=>'FALSE'
				);
				$this->db->insert('activities', $object);

				$sql = "SELECT activityId FROM activities WHERE name = ? && timestamp = ? && userId = ?";
				$activity = $this->db->query($sql, array($this->input->post('name'), $timestamp, $this->session->UserId))->last_row();

				if(isset($activity))
				{
					$cTurn = count($this->input->post('turns[]'));
					
					$sections 	= $this->input->post('sections[]');
					$roles 		= $this->input->post('roles[]');
					$sites 		= $this->input->post('sites[]');
					$userGroups = $this->input->post('userGroups[]');
					$users 		= $this->input->post('users[]');
					$turns    	= $cTurn > 0 ? join(',',$this->input->post('turns[]')) : 'NO';

					if($sections)
					{
						foreach ($sections as $section) {
							$sql 	= "SELECT * FROM sections WHERE sectionId = ?";
							$exist 	= $this->db->query($sql, $section)->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM activityLinks WHERE activityId = ? && sectionId = ?";
								$exist 	= $this->db->query($sql, array($activity->activityId, $section))->row();
								if (!isset($exist)) {
									$object = array(
										'sectionId' 	=> $section,
										'activityId' 	=> $activity->activityId,
										'turns'			=> $turns
									);
									$this->db->insert('activityLinks', $object);
								}
							}
						}
					}

					if($roles)
					{
						foreach ($roles as $role) {
							$sql 	= "SELECT * FROM roles WHERE roleId = ?";
							$exist 	= $this->db->query($sql, $role)->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM activityLinks WHERE activityId = ? && roleId = ?";
								$exist 	= $this->db->query($sql, array($activity->activityId, $role))->row();
								if (!isset($exist)) {
									$object = array(
										'roleId' 		=> $role,
										'activityId' 	=> $activity->activityId,
										'turns'			=> $turns
									);
									$this->db->insert('activityLinks', $object);
								}
							}
						}
					}

					if($sites)
					{
						foreach ($sites as $site) {
							$sql 	= "SELECT * FROM sites WHERE siteId = ?";
							$exist 	= $this->db->query($sql, $site)->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM activityLinks WHERE activityId = ? && siteId = ?";
								$exist 	= $this->db->query($sql, array($activity->activityId, $site))->row();
								if (!isset($exist)) {
									$object = array(
										'siteId' 		=> $site,
										'activityId' 	=> $activity->activityId,
										'turns'			=> $turns
									);
									$this->db->insert('activityLinks', $object);
								}
							}
						}
					}

					if($userGroups && $this->Identity->Validate('usergroups/create'))
					{
						foreach ($userGroups as $userGroup) {
							$sql 	= "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
							$exist 	= $this->db->query($sql, array($userGroup, $this->session->UserId))->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
								$group 	= $this->db->query($sql, $userGroup)->result();
								if (isset($group) && count($group) > 0) {
									foreach ($group as $user) {
										$sql 	= "SELECT * FROM activityLinks WHERE activityId = ? && userId = ?";
										$exist 	= $this->db->query($sql, array($activity->activityId, $user->userId))->row();
										if (!isset($exist)) {
											$object = array(
												'userId' 		=> $user->userId,
												'activityId' 	=> $activity->activityId,
												'turns'			=> $turns
											);
											$this->db->insert('activityLinks', $object);
										}
									}
								}
							}
						}
					}

					if ($users) {
						foreach ($users as $user) {
							$sql 	= "SELECT userId FROM users WHERE userId = ?";
							$exist 	= $this->db->query($sql, $user)->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM activityLinks WHERE activityId = ? && userId = ?";
								$exist 	= $this->db->query($sql, array($activity->activityId, $user))->row();
								if (!isset($exist)) {
									$object = array(
										'userId' 		=> $user,
										'activityId' 	=> $activity->activityId,
										'turns'			=> $turns
									);
									$this->db->insert('activityLinks', $object);
								}
							}
						}
					}

					$data 			= new StdClass();
					$data->status 	= 'ok';
					$data->message 	= $this->lang->line('activity_create_message');
					echo escapeJsonString($data, FALSE);
					return;
				}
				else{
					echo '{"status": "invalid"}';
					return;
				}
			}
		}
	}

	public function Edit($activityId)
	{
		$this->form_validation->set_rules('name', 'lang:activity_name', 'required');
		$this->form_validation->set_rules('startDate', 'lang:survey_start_date', 'required');
		$this->form_validation->set_rules('endDate', 'lang:survey_end_date', 'required');

		if (!$this->input->post('sections[]') && !$this->input->post('roles[]') && !$this->input->post('sites[]') && !$this->input->post('userGroups[]') && !$this->input->post('users[]')) {
			$this->form_validation->set_rules('whose', 'lang:activity_whose', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			$data = new StdClass();
			$sql = "SELECT sectionId, name FROM sections ORDER BY name";
			$data->sections = $this->db->query($sql)->result();

			$sql = "SELECT roleId, name FROM roles ORDER BY name";
			$data->roles = $this->db->query($sql)->result();

			$sql = "SELECT siteId, name FROM sites ORDER BY name";
			$data->sites = $this->db->query($sql)->result();

			if ($this->Identity->Validate('usergroups/create')) {
				$sql = "SELECT userGroupId, name FROM userGroups WHERE userId = ? ORDER BY name";
				$data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
			}
			$data->turns = $this->map_tables->get_table('turns');
			
			$this->load->view('activities/edit',$data);

		}
		else
		{
			$sql = "SELECT * FROM  activities WHERE activityId = ?";
			$exist = $this->db->query($sql,$activityId)->row();
			if(isset($exist))
			{
				$timestamp = time();

				if($exist->image != NULL)
				{
					$imageName = $exist->image;
					if(isset($_FILES['userfile']))
					{
						$config['upload_path']          = './catapp/_activities_images/';
						$config['allowed_types']        = 'gif|jpg|png';
						$config['max_size']             = 50480;
						if (isset($_FILES['userfile']) && $_FILES['userfile'] != NULL) {
							$name = $this->sanear_string($_FILES['userfile']['name']);
							$config['file_name']			= $timestamp.'-'. $name;
						}
						$this->load->library('upload', $config);

						if (!$this->upload->do_upload('userfile'))
						{
							$response = new StdClass();
							$response->status  = $this->upload->display_errors();
							$response->message = $this->lang->line('corporatedoccategory_completeall');
							echo escapeJsonString($response,false);
							return;
						}
						else
						{
							unlink('./catapp/_activities_images/'.$exist->image); 
							$imageName = $this->upload->data('file_name');
						}
					}
				}
				
				$startDate 	= NULL;
				$endDate 	= NULL;

				$startDate = strtotime($this->input->post('startDate'));
				if ($startDate === FALSE) {
					echo '{"status":"invalid"}';
					return;
				}

				$endDate = strtotime($this->input->post('endDate'));
				if ($endDate === FALSE) {
					echo '{"status":"invalid"}';
					return;
				}

				$question = new StdClass();
				$sql = "SELECT * FROM activityQuestions";
				$question = $this->db->query($sql)->row();

				$sql = "SELECT * FROM activityAnswers";
				$question->answers = $this->db->query($sql)->result();


				$closeMessage = $this->input->post('closeMessage');
				if (!$closeMessage || !(is_string($closeMessage) || is_int($closeMessage))) {
					$closeMessage = null;
				}
				$coupons = $this->input->post('coupons');
				if($this->input->post('coupons') == ""){
					$coupons = null;
				}
				$isRequired = $this->input->post('required') ? 'TRUE' : 'FALSE'; 
				$object = array(
					'name'		 	=> $this->input->post('name'),
					'timestamp' 	=> $timestamp,
					'image'			=> $imageName,
					'userId' 		=> $this->session->UserId,
					'description' 	=> $this->input->post('descriptionS'),
					'startDate' 	=> $startDate,
					'endDate' 		=> $endDate,
					'capacity'		=> $coupons,
					'closeMessage' 	=> $closeMessage,
					'required'		=> $isRequired,
					'active'		=>'FALSE'
				);
				$this->db->where('activityId', $exist->activityId);
				$this->db->update('activities', $object);

				$this->db->where('activityId', $exist->activityId);
				$this->db->delete('activityLinks');

				$sql = "SELECT activityId FROM activities WHERE name = ? && timestamp = ? && userId = ?";
				$activity = $this->db->query($sql, array($this->input->post('name'), $timestamp, $this->session->UserId))->last_row();

				if(isset($activity))
				{
					$cTurn = count($this->input->post('turns[]'));

					$sections 	= $this->input->post('sections[]');
					$roles 		= $this->input->post('roles[]');
					$sites 		= $this->input->post('sites[]');
					$userGroups = $this->input->post('userGroups[]');
					$users 		= $this->input->post('users[]');
					$turns    	= $cTurn > 0 ? join(',',$this->input->post('turns[]')) : 'NO';

					if($sections)
					{
						foreach ($sections as $section) {
							$sql 	= "SELECT * FROM sections WHERE sectionId = ?";
							$exist 	= $this->db->query($sql, $section)->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM activityLinks WHERE activityId = ? && sectionId = ?";
								$exist 	= $this->db->query($sql, array($activity->activityId, $section))->row();
								if (!isset($exist)) {
									$object = array(
										'sectionId' 	=> $section,
										'activityId' 	=> $activity->activityId,
										'turns'			=> $turns
									);
									$this->db->insert('activityLinks', $object);
								}
							}
						}
					}

					if($roles)
					{
						foreach ($roles as $role) {
							$sql 	= "SELECT * FROM roles WHERE roleId = ?";
							$exist 	= $this->db->query($sql, $role)->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM activityLinks WHERE activityId = ? && roleId = ?";
								$exist 	= $this->db->query($sql, array($activity->activityId, $role))->row();
								if (!isset($exist)) {
									$object = array(
										'roleId' 		=> $role,
										'activityId' 	=> $activity->activityId,
										'turns'			=> $turns
									);
									$this->db->insert('activityLinks', $object);
								}
							}
						}
					}

					if($sites)
					{
						foreach ($sites as $site) {
							$sql 	= "SELECT * FROM sites WHERE siteId = ?";
							$exist 	= $this->db->query($sql, $site)->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM activityLinks WHERE activityId = ? && siteId = ?";
								$exist 	= $this->db->query($sql, array($activity->activityId, $site))->row();
								if (!isset($exist)) {
									$object = array(
										'siteId' 		=> $site,
										'activityId' 	=> $activity->activityId,
										'turns'			=> $turns
									);
									$this->db->insert('activityLinks', $object);
								}
							}
						}
					}

					if($userGroups && $this->Identity->Validate('usergroups/create'))
					{
						foreach ($userGroups as $userGroup) {
							$sql 	= "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
							$exist 	= $this->db->query($sql, array($userGroup, $this->session->UserId))->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
								$group 	= $this->db->query($sql, $userGroup)->result();
								if (isset($group) && count($group) > 0) {
									foreach ($group as $user) {
										$sql 	= "SELECT * FROM activityLinks WHERE activityId = ? && userId = ?";
										$exist 	= $this->db->query($sql, array($activity->activityId, $user->userId))->row();
										if (!isset($exist)) {
											$object = array(
												'userId' 		=> $user->userId,
												'activityId' 	=> $activity->activityId,
												'turns'			=> $turns
											);
											$this->db->insert('activityLinks', $object);
										}
									}
								}
							}
						}
					}

					if ($users) {
						foreach ($users as $user) {
							$sql 	= "SELECT userId FROM users WHERE userId = ?";
							$exist 	= $this->db->query($sql, $user)->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM activityLinks WHERE activityId = ? && userId = ?";
								$exist 	= $this->db->query($sql, array($activity->activityId, $user))->row();
								if (!isset($exist)) {
									$object = array(
										'userId' 		=> $user,
										'activityId' 	=> $activity->activityId,
										'turns'			=> $turns
									);
									$this->db->insert('activityLinks', $object);
								}
							}
						}
					}

					$data 			= new StdClass();
					$data->status 	= 'ok';
					$data->message 	= $this->lang->line('activity_edit_message');
					echo escapeJsonString($data, FALSE);
					return;
				}
				else{
					echo '{"status": "invalid"}';
					return;
				}
				
			}
		}
	}

	

	public function ActivateActivity()
	{	
		if($this->uri->segment(3))
		{
			$sql="SELECT *
			FROM activities
			WHERE activityId=?";

			$query = $this->db->query($sql,$this->uri->segment(3))->row();

			if(isset($query))
			{
				$editRow = array('active'=>'TRUE');

				$this->db->where('activityId', $query->activityId);
				$this->db->update('activities',$editRow);

				return "success";
			}
		}
	}

	public function GetactivityReport()
	{
		if($this->uri->segment(3)){

			$sql = "SELECT * FROM activities WHERE activityId = ?";
			$data = $this->db->query($sql,$this->uri->segment(3))->row();
			if(isset($data))
			{
				$sql = "SELECT up.dni, up.name, up.lastName, up.gender, c.name campaign,sc.name subcampaign, uc.turn, up.birthDate, ac.timestamp date, ac.activityId, ar.activityanswerId as answer, up.userId, s.name site
				FROM 		activityCompletes AS ac 
				INNER JOIN  (SELECT name, dni, lastName, gender, birthDate, userId 	FROM userPersonalData) 		AS up 	ON ac.userId 	= up.userId 
				INNER JOIN  (SELECT turn, campaignId,subCampaignId, userId			FROM userComplementaryData) AS uc 	ON ac.userId 	= uc.userId 
				INNER JOIN  (SELECT siteId, userId 									FROM users) 				AS u 	ON ac.userId 	= u.userId
				INNER JOIN  (SELECT activityanswerId,activitycompleteId				FROM activityResponses)		AS ar	ON ar.activitycompleteId = ac.activitycompleteId 
				LEFT JOIN 	campaigns 																			AS c 	ON c.campaignId = uc.campaignId
				LEFT JOIN 	subCampaigns 																		AS sc 	ON sc.subCampaignId = uc.subCampaignId
				LEFT JOIN 	sites 																				AS s 	ON s.siteId 	= u.siteId
				WHERE ac.activityId = ?";

				$data->answered = $this->db->query($sql, $data->activityId)->result();
				$this->load->library('map_tables');

				foreach ($data->answered as $user) {

					if($user->gender == NULL) 
						$user->gender = $this->lang->line('survey_undefined');
					else
						$user->gender = $this->map_tables->get_gender($user->gender);

					if($user->campaign == NULL) 
						$user->campaign = $this->lang->line('survey_undefined');

					if($user->subcampaign == NULL)
						$user->subcampaign = $this->lang->line('subcampaigns_not_apply');

					if($user->turn == NULL) 
						$user->turn 	= $this->lang->line('survey_undefined');
					else
						$user->turn 	= $this->map_tables->get_turn($user->turn);

					if($user->site == NULL) 
						$user->site 	= $this->lang->line('survey_undefined');

					if ($user->birthDate)
						$user->birthDate = date('d/m/Y', strtotime($user->birthDate));

					if($user->answer)
						$user->answer = ($user->answer == 1)? $this->lang->line('activity_confirmAnswer'): $this->lang->line('activity_denyAnswer');

					$user->date 		= date('d/m/Y H:i', $user->date);
				}


				//$arrayNotAnswered
				$activityId = $this->uri->segment(3);
				$sql = "SELECT up.dni, up.name, up.lastName, up.gender, c.name campaign, sc.name subcampaign, uc.turn, up.birthDate,  up.userId, s.name site
				FROM 		users ud
				INNER JOIN  (SELECT name, lastName, dni, gender, birthDate, userId 	FROM userPersonalData) 		AS up 	ON ud.userId 	= up.userId 
				INNER JOIN  (SELECT turn, campaignId, subCampaignId, userId 		FROM userComplementaryData) AS uc 	ON ud.userId 	= uc.userId 
				INNER JOIN  (SELECT siteId, userId 									FROM users) 				AS u 	ON ud.userId 	= u.userId
				LEFT JOIN 	campaigns 																			AS c 	ON c.campaignId = uc.campaignId
				LEFT JOIN 	subCampaigns 																		AS sc 	ON sc.subCampaignId = uc.subCampaignId
				LEFT JOIN 	sites 																				AS s 	ON s.siteId 	= ud.siteId
				WHERE 
					ud.userId IN (
					select al.userId 
					FROM activityLinks al
					WHERE al.activityId = $activityId
					AND ud.userId NOT IN (SELECT ac.userId FROM activityCompletes ac WHERE ac.activityId = $activityId)
					)
				OR 
					ud.roleId IN (
					select al.roleId 
					FROM activityLinks al
					WHERE al.activityId = $activityId
					AND ud.userId NOT IN (SELECT ac.userId FROM activityCompletes ac WHERE ac.activityId = $activityId)
					)
				OR
					ud.siteId IN (
					select al.siteId 
					FROM activityLinks al
					WHERE al.activityId = $activityId
					AND ud.userId NOT IN (SELECT ac.userId FROM activityCompletes ac WHERE ac.activityId = $activityId)
					)
				OR
					ud.sectionId IN (
					select al.sectionId 
					FROM activityLinks al
					WHERE al.activityId = $activityId
					AND ud.userId NOT IN (SELECT ac.userId FROM activityCompletes ac WHERE ac.activityId = $activityId)
					)
				AND ud.userId NOT IN (SELECT ac.userId FROM activityCompletes ac WHERE ac.activityId = $activityId)
				AND ud.active = 1";

				$data->notAnswered = $this->db->query($sql)->result();

				foreach ($data->notAnswered as $user) {

					if($user->gender == NULL) 
						$user->gender 	= $this->lang->line('survey_undefined');
					else
						$user->gender = $this->map_tables->get_gender($user->gender);
					if($user->campaign == NULL) 
						$user->campaign = $this->lang->line('survey_undefined');

					if($user->subcampaign == NULL)
						$user->subcampaign = $this->lang->line('subcampaigns_not_apply');
						
					if($user->turn == NULL) 
						$user->turn 	= $this->lang->line('survey_undefined');
					else
						$user->turn 	= $this->map_tables->get_turn($user->turn);
					if ($user->birthDate)
						$user->birthDate = date('d/m/Y', strtotime($user->birthDate));
				}
				return $data;
			}
		}
	}

	function sanear_string($string)
	{
	
		$string = trim($string);
	
		$string = str_replace(
			array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$string
		);
	
		$string = str_replace(
			array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$string
		);
	
		$string = str_replace(
			array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$string
		);
	
		$string = str_replace(
			array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$string
		);
	
		$string = str_replace(
			array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$string
		);
	
		$string = str_replace(
			array('ñ', 'Ñ', 'ç', 'Ç'),
			array('n', 'N', 'c', 'C',),
			$string
		);
	
		//Esta parte se encarga de eliminar cualquier caracter extraño
		$string = str_replace(
			array("º","!","·","$","%","&","(",")","=","¿","Ç","@","#","~","€","¬","}","`","+","´","-","_","[","]","{","}"),
			array('','','','','','','','','','','','','','','','','','','','','','','','','',''),
			$string
		);
	
	
		return $string;
	}

	function UpdateDate($activityId,$endDate)
	{
		$endDate = strtotime($endDate);

		$this->db->set('endDate',$endDate);
		$this->db->where('activityId', $activityId);
		$this->db->update('activities');
		return 'ok';
	}
}

/* End of file Activity_model.php */
