<?php
Class Role_model extends CI_Model {

	function GetRoles(){
		$data = new StdClass();
		$sql = "SELECT * FROM roles ORDER BY name";
		$data->model = $this->db->query($sql)->result();
		$this->load->view('_shared/_administrationlayoutheader');
		$this->load->view('roles/roles', $data);
		$this->load->view(PRELAYOUTFOOTER);
	}

	public function GetRolesForComponent()
	{
		$sql = 
		"SELECT roleId as objectId, name
		FROM roles";
		return $this->db->query($sql)->result();
	}

	function Create(){

		$nameRules = 'required|is_unique[roles.name]';
		$nameRulesMessages = array('is_unique' => $this->lang->line('roles_nameexist'));

		$this->form_validation->set_rules('name', 'lang:roles_name', $nameRules ,$nameRulesMessages);

		if ($this->form_validation->run() == FALSE)
		{
			$query = new StdClass();
			$sql = "SELECT * FROM permissons WHERE active != 'FALSE'" ;
			$query->permissions = $this->db->query($sql)->result();

			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('roles/create', $query);
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			$query = new StdClass();
			$name = htmlspecialchars($this->input->post('name'));

			$query->name = $name;
			$query->permissions = $this->input->post('permissions[]');

			insert_audit_logs('roles','INSERT',$query);

			$objectInsert = array(
				'name' => $name,
				'userId' => $this->session->UserId,
				'timestamp' => time(),
				);
			$this->db->insert('roles', $objectInsert);

			$sql = "SELECT * FROM roles WHERE name = ?";
			$query = $this->db->query($sql,array($name))->row();
			if (isset($query))
			{
				$sql = "SELECT * FROM permissons WHERE active != 'FALSE'" ;
				$query->permissions = $this->db->query($sql)->result();
				if ($this->input->post('permissions[]')) 
				{
					$allpermissionsInput = $this->input->post('permissions[]');
				}
				else
				{
					$allpermissionsInput =  array();
				}

				foreach ($query->permissions as $key => $permission) {
					$actualValue = 'FALSE';
					$existPermission = array_search($permission->permisonId, $allpermissionsInput);
					if ($existPermission || $existPermission === 0) {
						$actualValue = 'TRUE';
					}
					$sectionInsert = array(
						'value' => $actualValue,
						'permissionId' => $permission->permisonId,
						'roleId' => $query->roleId
						);
					$this->db->insert('rolePermissions', $sectionInsert);

				}

				$this->session->set_flashdata('flashMessage', 'create');
				header('Location:/'.FOLDERADD.'/roles');
			}
			else
			{
				show_404();
			}
		}
	}

	function Delete(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM roles WHERE roleId = ?";
			$query = $this->db->query($sql,array($this->uri->segment(3)))->row();
			if (isset($query))
			{
				if ($this->input->post('roleId') && $this->input->post('roleId') == $query->roleId ) {

					$query->permissions = $this->input->post('permissions[]');
					insert_audit_logs('roles','DELETE',$query);

					$objectDelete = array(
						'roleId' => $this->input->post('roleId')
						);
					$this->db->delete('roles', $objectDelete);
					$this->session->set_flashdata('flashMessage', 'delete');
					header('Location:/'.FOLDERADD.'/roles');
				}
				else
				{
					$query->navBar = $this->load->view('roles/_navbar', $query, TRUE);
					$sql = "SELECT * FROM permissons WHERE active != 'FALSE'" ;
					$query->permissions = $this->db->query($sql)->result();
					foreach ($query->permissions as $key => $permission) {
						$sql = "SELECT * FROM rolePermissions WHERE permissionId = ? && roleId = ?";
						$permissionExist = $this->db->query($sql, array($permission->permisonId, $query->roleId))->row();
						if (isset($permissionExist) && $permissionExist != NULL) {
							if ($permissionExist->value == 'TRUE') {
								$permission->textValue = $this->lang->line('roles_truepermission');
							}
							else
							{
								$permission->textValue = $this->lang->line('roles_falsepermission');	
							}
						}
						else
						{
							$permission->textValue = $this->lang->line('roles_nosetpermission');
						}
					}	
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('roles/delete', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function DetailsRole(){

		if ($this->uri->segment(3)) {

			$sql = "SELECT * FROM roles WHERE roleId = ?";
			$query = $this->db->query($sql,array($this->uri->segment(3)))->row();

			if (isset($query))
			{
				$query->navBar = $this->load->view('roles/_navbar', $query, TRUE);

				$sql = "SELECT * FROM permissons WHERE active != 'FALSE'" ;
				$query->permissions   = $this->db->query($sql)->result();
				$query->availablePermissions = array();

				foreach ($query->permissions as $key => $permission) {

					$sql = "SELECT * FROM rolePermissions WHERE permissionId = ? && roleId = ?";
					$permissionExist = $this->db->query($sql, array($permission->permisonId, $query->roleId))->row();

					if (isset($permissionExist) && $permissionExist != NULL) {

						if ($permissionExist->value == 'TRUE') {

							//$permission->textValue = $this->lang->line('roles_truepermission');
							array_push($query->availablePermissions, $permission);
						}
						//else
						//{
						//	$permission->textValue = $this->lang->line('roles_falsepermission');	
						//}
					}
					//else
					//{
					//	$permission->textValue = $this->lang->line('roles_nosetpermission');
					//}
				}

				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('roles/details', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function Edit(){

		if ($this->uri->segment(3)) {

			$sql = "SELECT * FROM roles WHERE roleId = ?";
			$query = $this->db->query($sql,array($this->uri->segment(3)))->row();

			if (isset($query))
			{
				$nameRules = 'required|is_unique[roles.name]';
				$nameRulesMessages = array('is_unique' => $this->lang->line('roles_nameexist'));

				if ($this->input->post('name') && $query->name == $this->input->post('name')) {

					$nameRules = 'required';
					$nameRulesMessages = NULL;
				}

				$this->form_validation->set_rules('name', 'lang:roles_name', $nameRules ,$nameRulesMessages);

				if ($this->form_validation->run() == FALSE)
				{
					$query->navBar = $this->load->view('roles/_navbar', $query, TRUE);
					$sql = "SELECT * FROM permissons WHERE active != 'FALSE'" ;
					$query->permissions = $this->db->query($sql)->result();

					foreach ($query->permissions as $key => $permission) {

						$permission->isSelected = FALSE;
						$sql = "SELECT * FROM rolePermissions WHERE permissionId = ? && roleId = ?";
						$permissionExist = $this->db->query($sql, array($permission->permisonId, $query->roleId))->row();

						if (isset($permissionExist) && $permissionExist != NULL) {

							if ($permissionExist->value == 'TRUE') {

								$permission->textValue = $this->lang->line('roles_truepermission');
								$permission->isSelected = TRUE;
							}
							else
							{
								$permission->textValue = $this->lang->line('roles_falsepermission');	
							}
						}
						else
						{
							$permission->textValue = $this->lang->line('roles_nosetpermission');
						}
					}

					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('roles/edit',$query);
					$this->load->view(PRELAYOUTFOOTER);
				}
				else
				{

					$sql = "SELECT * FROM permissons WHERE active != 'FALSE'" ;
					$query->permissions = $this->db->query($sql)->result();

					if ($this->input->post('permissions[]')) 
					{
						$allpermissionsInput = $this->input->post('permissions[]');
					}
					else
					{
						$allpermissionsInput =  array();
					}

					$query2 = $query;
					$query2->permissions = $allpermissionsInput;
					insert_audit_logs('roles','UPDATE',$query);

					foreach ($query->permissions as $key => $permission) {

						$existPermission = array_search($permission->permisonId, $allpermissionsInput);

						if ($existPermission || $existPermission === 0) {

							$sql = "SELECT * FROM rolePermissions WHERE permissionId = ? && roleId = ?";
							$permissionExist = $this->db->query($sql, array($permission->permisonId, $query->roleId))->row();

							//if (isset($permissionExist) && $permissionExist != NULL) {
//
							//	$objectEdit = array(
							//		'value' => 'TRUE'
							//		);
							//	$this->db->where('rolepermissionId', $permissionExist->rolepermissionId);
							//	$this->db->update('rolePermissions', $objectEdit);
							//}
							//else
							//{
							//	$sectionInsert = array(
							//		'value'  	   => 'TRUE',
							//		'permissionId' => $permission->permisonId,
							//		'roleId'  	   => $query->roleId
							//		);
							//	
							//	$this->db->insert('rolePermissions', $sectionInsert);
							//}
							if (!isset($permissionExist) || $permissionExist == NULL){
								$sectionInsert = array(
									'value'  	   => 'TRUE',
									'permissionId' => $permission->permisonId,
									'roleId'  	   => $query->roleId
									);
								$this->db->insert('rolePermissions', $sectionInsert);

							}

						}
						else
						{

							$sql = "SELECT * FROM rolePermissions WHERE permissionId = ? && roleId = ?";
							$permissionExist = $this->db->query($sql, array($permission->permisonId, $query->roleId))->row();

							if (isset($permissionExist) && $permissionExist != NULL) {

								$this->db->where('rolepermissionId', $permissionExist->rolepermissionId);
								$this->db->delete('rolePermissions');
							}
							//else
							//{
							//	$sectionInsert = array(
							//		'value'        => 'FALSE',
							//		'permissionId' => $permission->permisonId,
							//		'roleId'       => $query->roleId
							//		);
							//	print_r($sectionInsert);
							//	$this->db->insert('rolePermissions', $sectionInsert);
							//}
						}

					}

					$objectEdit = array(
						'name' => $this->input->post('name')
						);
					$this->db->where('roleId', $query->roleId);
					$this->db->update('roles', $objectEdit);

					$this->session->set_flashdata('flashMessage', 'edit');
					header('Location:/'.FOLDERADD.'/roles/details/'.$query->roleId);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function GetSection(){

		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM sections WHERE sectionId = ?";
			$section = array($this->uri->segment(3));
			$query = $this->db->query($sql,$section)->row();
			if (isset($query))
			{
				$sql = "SELECT * FROM subSections WHERE sectionId = ? ORDER BY name";
				$section = array($query->sectionId);
				$query->subSections = $this->db->query($sql,$section)->result();

				foreach ($query->subSections as $sskey => $ssvalue) {
					$ssvalue->noViews = FALSE;
					
					$sql = 'SELECT * FROM subSections WHERE fereingsubsectionId = ?';
					$ssvalue->sssections = $this->db->query($sql, array($ssvalue->subsectionId))->result();

					foreach ($ssvalue->sssections as $sssection) {
						$sssection->noViews = FALSE;
						$sql = 'SELECT * FROM items WHERE subsectionId = ?';
						$sssection->items = $this->db->query($sql, array($sssection->subsectionId))->result();

						foreach ($sssection->items as $ikey => $ivalue) {

							$ivalue->noViews = FALSE;
							$sql = 'SELECT itemforumId FROM itemForums WHERE itemId = ?';
							$ivalue->forums = $this->db->query($sql, array($ivalue->itemId))->result();
							foreach ($ivalue->forums as $fkey => $fvalue) {
								$sql = 'SELECT itemforumtopicId, userId FROM itemforumTopics WHERE itemforumId = ?';
								$fvalue->topics = $this->db->query($sql, array($fvalue->itemforumId))->result();
								foreach ($fvalue->topics as $tkey => $tvalue) {
									$sql = 'SELECT itemforummessageId, userId FROM itemforumMessages WHERE itemforumtopicId = ?';
									$tvalue->messages = $this->db->query($sql, array($tvalue->itemforumtopicId))->result();
									foreach ($tvalue->messages as $mkey => $mvalue) {
										$sql = 'SELECT itemforummessageviewsId FROM itemforummessageViews WHERE itemforummessageId = ? && userId = ?';
										$mviewexist = $this->db->query($sql, array($mvalue->itemforummessageId, $this->session->UserId))->result();
										if ($mvalue->userId != $this->session->UserId) {
											if ($mviewexist == NULL) {
												$ssvalue->noViews = TRUE;
												$ivalue->noViews = TRUE;
												$sssection->noViews = TRUE;
											}
										}
									}
									if ($tvalue->userId != $this->session->UserId) {
										$sql = 'SELECT itemforumtopicviewId FROM itemforumtopicViews WHERE itemforumtopicId = ? && userId = ?';
										$tviewexist = $this->db->query($sql, array($tvalue->itemforumtopicId, $this->session->UserId))->result();
										if ($tviewexist == NULL) {
											$ssvalue->noViews = TRUE;
											$ivalue->noViews = TRUE;
											$sssection->noViews = TRUE;
										}
									}
								}
							}
						}
					}

					$sql = 'SELECT * FROM items WHERE subsectionId = ?';
					$ssvalue->items = $this->db->query($sql, array($ssvalue->subsectionId))->result();

					foreach ($ssvalue->items as $ikey => $ivalue) {
						$ivalue->noViews = FALSE;
						$sql = 'SELECT itemforumId FROM itemForums WHERE itemId = ?';
						$ivalue->forums = $this->db->query($sql, array($ivalue->itemId))->result();
						foreach ($ivalue->forums as $fkey => $fvalue) {
							$sql = 'SELECT itemforumtopicId, userId FROM itemforumTopics WHERE itemforumId = ?';
							$fvalue->topics = $this->db->query($sql, array($fvalue->itemforumId))->result();
							foreach ($fvalue->topics as $tkey => $tvalue) {
								$sql = 'SELECT itemforummessageId, userId FROM itemforumMessages WHERE itemforumtopicId = ?';
								$tvalue->messages = $this->db->query($sql, array($tvalue->itemforumtopicId))->result();
								foreach ($tvalue->messages as $mkey => $mvalue) {
									$sql = 'SELECT itemforummessageviewsId FROM itemforummessageViews WHERE itemforummessageId = ? && userId = ?';
									$mviewexist = $this->db->query($sql, array($mvalue->itemforummessageId, $this->session->UserId))->result();
									if ($mvalue->userId != $this->session->UserId) {
										if ($mviewexist == NULL) {
											$ssvalue->noViews = TRUE;
											$ivalue->noViews = TRUE;
										}
									}
								}
								if ($tvalue->userId != $this->session->UserId) {
									$sql = 'SELECT itemforumtopicviewId FROM itemforumtopicViews WHERE itemforumtopicId = ? && userId = ?';
									$tviewexist = $this->db->query($sql, array($tvalue->itemforumtopicId, $this->session->UserId))->result();
									if ($tviewexist == NULL) {
										$ssvalue->noViews = TRUE;
										$ivalue->noViews = TRUE;
									}
								}
							}
						}
					}
				}

				$this->load->view('_shared/_layoutheader');
				$this->load->view('sections/section', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}

	}

}
