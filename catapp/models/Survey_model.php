<?php
Class Survey_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getSurveys()
	{
		if ($this->Identity->Validate('surveys/manage')) {
			$sql = "SELECT s.description, s.name, s.endDate, s.startDate, s.surveyId, s.timestamp,s.active, (SELECT count(*) FROM surveyCompletes AS sc WHERE sc.surveyId = s.surveyId && sc.userId = ? LIMIT 1) complete FROM surveys AS s ORDER BY s.surveyId DESC";
			$surveys = $this->db->query($sql, $this->session->UserId)->result();
		}
		else
		{
			$sql = "SELECT s.description, s.name, s.endDate, s.startDate, s.surveyId, s.timestamp,s.active,  (SELECT count(*) FROM surveyCompletes AS sc WHERE sc.surveyId = s.surveyId && sc.userId = ? LIMIT 1) complete
			FROM surveyLinks AS sl 
			INNER JOIN surveys AS s ON sl.surveyId = s.surveyId
			WHERE (sl.userId = ? || sl.sectionId = ? || sl.siteId = ? || sl.roleId = ?) and s.active='TRUE'
			GROUP BY sl.surveyId";
			$surveys = $this->db->query($sql, array($this->session->UserId, $this->session->UserId, $this->session->sectionId, $this->session->siteId, $this->session->roleId))->result();
		}
		foreach ($surveys as  $survey) {
			$timestamp = time();
			$survey->isTime = (($survey->startDate < $timestamp || $survey->startDate == NULL) && ($survey->endDate > $timestamp || $survey->endDate == NULL)) ? TRUE : FALSE;
			if ($survey->endDate) {
				$survey->endDate = date('d/m/Y',$survey->endDate);
			}
			if ($survey->startDate) {
				$survey->startDate = date('d/m/Y',$survey->startDate);
			}
		}
		return $surveys;
	}

	
	public function getUsersInSurvey($like,$survey)
	{	
		$usersInSurvey = new StdClass();

		if ($survey)
		{
			if ($this->Identity->Validate('surveys/manage'))
			{
				
				$query = "SELECT concat_ws(' ', up.name, up.lastName) AS completeName, up.userId, users.userName
				FROM userPersonalData up
				JOIN users
				ON (users.userId = up.userId)
				WHERE (concat_ws(' ', name, lastName) LIKE '%$like%' OR userName LIKE '%$like%')
				AND users.userId NOT IN (SELECT userId FROM surveyCompletes WHERE surveyId = ?)";
				// "SELECT concat_ws(' ', up.name, up.lastName) AS completeName, up.userId, users.userName
				// 	FROM userPersonalData up
				// 	INNER JOIN users ON up.userId = users.userId
				// 	WHERE up.userId IN (
				// 		SELECT userId
				// 		FROM(SELECT users.userId,surveyId
				// 		FROM users
				// 		JOIN surveyLinks
				// 		ON (users.siteId = surveyLinks.siteId)  
				// 		UNION (
				// 		SELECT users.userId,surveyId
				// 		FROM users
				// 		JOIN surveyLinks
				// 		ON (users.roleId = surveyLinks.roleId))
				// 		UNION(
				// 		SELECT users.userId,surveyId
				// 		FROM users
				// 		JOIN surveyLinks
				// 		ON (users.sectionId = surveyLinks.sectionId))
				// 		UNION(
				// 		SELECT users.userId,surveyId
				// 		FROM users
				// 		JOIN surveyLinks
				// 		ON (users.userId = surveyLinks.userId))) AS usersOnSurvey
				// 		WHERE usersOnSurvey.surveyId = ?
				// 	    AND userId NOT IN (SELECT userId FROM surveyCompletes WHERE surveyId = ?)
				// 	    AND (concat_ws(' ', name, lastName) LIKE '%$like%' OR userName LIKE '%$like%')
				// 	)";
				$usersInSurvey = $this->db->query($query,$survey)->result();
			}
		}

		return $usersInSurvey;
	}

	public function CompleteSurveyManual($userId)
	{
		$sql = "SELECT userId as UserId,sectionId,siteId,roleId
		FROM users 
		WHERE userId = ?";

		$user = $this->db->query($sql,$userId)->row();

		if (isset($user)) {
			$this->CompleteSurvey($user,true);
		}
		else
		{
			show_404();	
		}

	}

	public function CompleteSurveyNormal()
	{
		$this->CompleteSurvey($this->session);
	}

	public function CompleteSurvey($user,$manual = false)
	{
		$timestamp = time();
		if ($this->uri->segment(3) != NULL) 
		{
			if ($this->Identity->Validate('surveys/manage'))
			{
				$sql = 
				"SELECT surveys.surveyId, surveys.name, surveys.description, surveys.closeMessage, surveys.required, surveys.active
				FROM surveys
				WHERE surveys.active='TRUE' && surveys.surveyId = ? ";
				
				$param = array($this->uri->segment(3));

				if(!$manual)
				{
					$sql.=" && (surveys.startDate <= ? || surveys.startDate IS NULL) 
					&& (surveys.endDate >= ? || surveys.endDate IS NULL)
					&& (SELECT count(*) FROM surveyCompletes WHERE surveyCompletes.surveyId = surveys.surveyId && surveyCompletes.userId = ?) = 0";
					
					array_push($param,$timestamp, $timestamp,$user->UserId);
				}

				$survey = $this->db->query($sql, $param)->row();

			}
			else
			{
				$sql = 
				"SELECT surveys.surveyId, surveys.name, surveys.description, surveys.closeMessage, surveys.required,surveys.active
				FROM surveyLinks INNER JOIN surveys ON surveyLinks.surveyId = surveys.surveyId
				WHERE (surveyLinks.userId = ? || surveyLinks.sectionId = ? || surveyLinks.siteId = ? || surveyLinks.roleId = ?)
				&& surveys.active='TRUE'
				&& (surveys.startDate <= ? || surveys.startDate IS NULL) 
				&& (surveys.endDate >= ? || surveys.endDate IS NULL)
				&& (SELECT count(*) FROM surveyCompletes WHERE surveyCompletes.surveyId = surveys.surveyId && surveyCompletes.userId = ?) = 0 && surveys.surveyId = ? ";

				$survey = $this->db->query($sql, 
					array($user->UserId, 
						$user->sectionId, 
						$user->siteId, 
						$user->roleId,
						$timestamp, 
						$timestamp,
						$user->UserId,
						$this->uri->segment(3)))->row();
			}
		}
		else
		{
			$sql = 
			"SELECT surveys.surveyId, surveys.name, surveys.description, surveys.closeMessage, surveys.required,surveys.active
			FROM surveyLinks INNER JOIN surveys ON surveyLinks.surveyId = surveys.surveyId
			WHERE (surveyLinks.userId = ? || surveyLinks.sectionId = ? || surveyLinks.siteId = ? || surveyLinks.roleId = ?)
			&&  surveys.active='TRUE'
			&& (surveys.startDate <= ? || surveys.startDate IS NULL) 
			&& (surveys.endDate >= ? || surveys.endDate IS NULL)
			&& (SELECT count(*) FROM surveyCompletes WHERE surveyCompletes.surveyId = surveys.surveyId && surveyCompletes.userId = ?) = 0 
			GROUP BY surveyLinks.surveyId ORDER BY surveys.surveyId ASC";

			$survey = $this->db->query($sql, 
				array($user->UserId, 
					$user->sectionId, 
					$user->siteId, 
					$user->roleId,
					$timestamp, 
					$timestamp,
					$user->UserId))->row();
		}

		if (isset($survey)) 
		{
			$this->form_validation->set_rules('survey', 'survey', 'required');

			if ($this->form_validation->run() === FALSE)
			{
				$sql = "SELECT * FROM surveyQuestions WHERE surveyId = ?";
				$survey->questions = $this->db->query($sql, $survey->surveyId)->result();
				foreach ($survey->questions as $question) 
				{
					if ($question->type == 1 || $question->type == 2) 
					{
						$sql = "SELECT * FROM surveyAnswers WHERE surveyquestionId = ?";
						$question->answers = $this->db->query($sql, $question->surveyquestionId)->result();
					}
				}

				$survey->isManual = $manual;

				if (!$manual) 
				{
					$timestamp = time();
					$object = array(
						'timestamp' 	=> $timestamp,
						'userId' 		=> $user->UserId,
						'surveyId' 		=> $survey->surveyId);
					$this->db->insert('surveyViews', $object);
				}


				$this->load->view("surveys/_surveys_header", array('isManual'=>$survey->isManual));
				$this->load->view("surveys/complete", $survey);
				$this->load->view("surveys/_surveys_footer");
			}
			else
			{

				if(!isset($user)){ 
					$this->session->set_flashdata('flash_message', 'notUser');
					header('Location:/'.FOLDERADD.'/surveys/showManualCharge/'.$survey->surveyId);
					exit();
				}

				$sql = "SELECT * FROM surveyCompletes WHERE userId = ? AND surveyId = ?";
				$isCompleted = $this->db->query($sql,array($user->UserId,$survey->surveyId))->num_rows();

				if($isCompleted == 0)
				{
					if ($this->input->post('survey') == $survey->surveyId) 
					{
						$sql = "SELECT surveyquestionId, type, required FROM surveyQuestions WHERE surveyId = ?";
						$questions = $this->db->query($sql, $survey->surveyId)->result();


						foreach ($questions as $question) 
						{
							if ($question->type == 4) {
								$question->answer = $this->input->post("questions[$question->surveyquestionId]");
								$incorrectValues = ($question->answer != "TRUE" && $question->answer != "FALSE");

								if($question->required == 'TRUE' && $incorrectValues)
								{
									show_404();
									exit();
								}

								if($question->required == 'FALSE' && $incorrectValues && $question->answer !== '')
								{
									$question->answer = '';
								}

							}
							elseif ($question->type == 3) {
								$question->answer = $this->input->post("questions[$question->surveyquestionId]");
								if ($question->required == 'TRUE' && !is_string($question->answer) && !is_int($question->answer)) {
									show_404();
									exit();
								}
							}
							elseif ($question->type == 2) {
								$question->answers = $this->input->post("questions[$question->surveyquestionId]");
								if (is_array($question->answers)) {
									foreach ($question->answers as $answer) {
										if (is_int($answer) || is_string($answer)) {
											$sql = "SELECT surveyanswerId FROM surveyAnswers WHERE surveyquestionId = ? && surveyanswerId = ?";
											if(!$this->db->query($sql, array($question->surveyquestionId, $answer))->row())
											{
												show_404();
												exit();
											}
										}
										else
										{
											show_404();
											exit();
										}
									}
								}
								else if ($question->required == 'TRUE')	{
									show_404();
									exit();
								}
							}
							elseif ($question->type == 1) {
								$question->answer = $this->input->post("questions[$question->surveyquestionId]");
								if (is_string($question->answer) || is_int($question->answer)) {
									$sql = "SELECT surveyanswerId FROM surveyAnswers WHERE surveyquestionId = ? && surveyanswerId = ?";
									if(!$this->db->query($sql, array($question->surveyquestionId, $question->answer))->row())
									{
										show_404();
										exit();
									}
								}
								else if ($question->required == 'TRUE') {
									show_404();
									exit();
								}
							}
							else
							{
								show_404();
								exit();
							}
						}

						$timestamp = time();
						$object = array(
							'timestamp' 	=> $timestamp,
							'userId' 		=> $user->UserId,
							'surveyId' 		=> $survey->surveyId
						);

						$this->db->insert('surveyCompletes', $object);

						$sql = "SELECT surveycompleteId FROM surveyCompletes WHERE userId = ? && surveyId = ?";
						$surveyComplete = $this->db->query($sql, array($user->UserId, $survey->surveyId))->last_row();
						if (isset($surveyComplete)) {
							foreach ($questions as $question) 
							{
								if ($question->type == 4 || $question->type == 3) {
									$object = array(
										'answer'		 	=> $question->answer,
										'surveyquestionId'  => $question->surveyquestionId,
										'surveycompleteId' 	=> $surveyComplete->surveycompleteId
									);
									$this->db->insert('surveyResponses', $object);
								}
								elseif ($question->type == 2) {
									foreach ($question->answers as $answer) {
										$object = array(
											'surveyanswerId'	=> $answer,
											'surveyquestionId'  => $question->surveyquestionId,
											'surveycompleteId' 	=> $surveyComplete->surveycompleteId
										);
										$this->db->insert('surveyResponses', $object);
									}
								}
								elseif ($question->type == 1) {
									$object = array(
										'surveyanswerId'	=> $question->answer,
										'surveyquestionId'  => $question->surveyquestionId,
										'surveycompleteId' 	=> $surveyComplete->surveycompleteId
									);
									$this->db->insert('surveyResponses', $object);
								}
							}
							$this->session->set_flashdata('flash_message', 'surveyCompleted');
							if($manual)
							{
								header('Location:/'.FOLDERADD.'/surveys/showManualCharge/'.$survey->surveyId);	
							}
							else
							{
								header('Location:/'.FOLDERADD.'/#/surveys/myanswers/'.$survey->surveyId);
							}
						}
						else
						{
							show_404();
						}
					}
					else
					{
						show_404();
					}
				}
				else 
				{
					$this->session->set_flashdata('flash_message', 'userInCompleteSurvey');
					header('Location:/'.FOLDERADD.'/surveys/showManualCharge/'.$survey->surveyId);	
				}

			}

		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function ViewSurveyPreview()
	{
		if($this->uri->segment(3))
		{
			if ($this->Identity->Validate('surveys/manage'))
			{
				$sql = 
				"SELECT surveys.surveyId, surveys.name, surveys.description, surveys.closeMessage, surveys.required,surveys.active
				FROM surveys
				WHERE surveys.surveyId = ?";

				$survey = $this->db->query($sql, $this->uri->segment(3))->row();

				if(isset($survey))
				{
					$sql = "SELECT * FROM surveyQuestions WHERE surveyId = ?";
					$survey->questions = $this->db->query($sql, $survey->surveyId)->result();

					foreach ($survey->questions as $question) 
					{
						if ($question->type == 1 || $question->type == 2) 
						{
							$sql = "SELECT * FROM surveyAnswers WHERE surveyquestionId = ?";
							$question->answers = $this->db->query($sql, $question->surveyquestionId)->result();
						}
					}

					$this->load->view("surveys/_surveys_header",array('isManual'=>FALSE));
					$this->load->view("surveys/complete", $survey);
					$this->load->view("_shared/_footer");
				}
				else
				{
					header('Location:/'.FOLDERADD);		
				}
			}
			else
			{
				header('Location:/'.FOLDERADD);
			}
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	public function GetSurveyDetails()
	{
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM surveys WHERE surveyId = ?";
			$data = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($data))
			{
				$sql = "SELECT * FROM surveyQuestions WHERE surveyId = ?";
				$data->questions = $this->db->query($sql, $data->surveyId)->result();
				foreach ($data->questions as $question) {
					if ($question->type == 1 || $question->type == 2) {
						$sql = "SELECT * FROM surveyAnswers WHERE surveyquestionId = ?";
						$question->answers = $this->db->query($sql, $question->surveyquestionId)->result();
					}
				}
				if ($data->endDate) {
					$data->endDate = date('d/m/Y',$data->endDate);
				}
				if ($data->startDate) {
					$data->startDate = date('d/m/Y',$data->startDate);
				}

				$sql = "SELECT sections.name FROM surveyLinks, sections WHERE surveyLinks.surveyId = ?  && surveyLinks.sectionId = sections.sectionId";
				$data->sections = $this->db->query($sql, $data->surveyId)->result();

				$sql = "SELECT sites.name FROM surveyLinks, sites WHERE surveyLinks.surveyId = ?  && surveyLinks.siteId = sites.siteId";
				$data->sites = $this->db->query($sql, $data->surveyId)->result();

				$sql = "SELECT roles.name FROM surveyLinks, roles WHERE surveyLinks.surveyId = ?  && surveyLinks.roleId = roles.roleId";
				$data->roles = $this->db->query($sql, $data->surveyId)->result();

				$sql = "SELECT concat(up.name, ' ', up.lastName) completeName, up.userId FROM surveyLinks INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON surveyLinks.userId = up.userId WHERE surveyLinks.surveyId = ?";
				$data->users = $this->db->query($sql, $data->surveyId)->result();

				return $data;
			}
		}
	}

	public function GetSurveyReport()
	{
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM surveys WHERE surveyId = ?";
			$data = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($data))
			{
				$sql = "SELECT up.name, up.lastName, up.gender , c.name campaign, uc.turn, up.birthDate, sc.timestamp date, sc.surveyId, up.userId, s.name site
				FROM 		surveyCompletes AS sc 
				INNER JOIN  (SELECT name, lastName, gender, birthDate, userId 	FROM userPersonalData) 		AS up 	ON sc.userId 	= up.userId 
				INNER JOIN  (SELECT turn, campaignId, userId 					FROM userComplementaryData) AS uc 	ON sc.userId 	= uc.userId 
				INNER JOIN  (SELECT siteId, userId 								FROM users) 				AS u 	ON sc.userId 	= u.userId 
				LEFT JOIN 	campaigns 																		AS c 	ON c.campaignId = uc.campaignId
				LEFT JOIN 	sites 																			AS s 	ON s.siteId 	= u.siteId
				WHERE sc.surveyId = ?";

				$data->completeTimes = $this->db->query($sql, $data->surveyId)->result();

				$this->load->library('map_tables');

				foreach ($data->completeTimes as $user) {
					if($user->gender == NULL) 
						$user->gender = $this->lang->line('survey_undefined');
					else
						$user->gender = $this->map_tables->get_gender($user->gender);
					if($user->campaign == NULL) 
						$user->campaign = $this->lang->line('survey_undefined');
					if($user->turn == NULL) 
						$user->turn 	= $this->lang->line('survey_undefined');
					else
						$user->turn 	= $this->map_tables->get_turn($user->turn);
					if($user->site == NULL) 
						$user->site 	= $this->lang->line('survey_undefined');
					if ($user->birthDate)
						$user->birthDate = date('d/m/Y', strtotime($user->birthDate));
					$user->date 		= date('d/m/Y H:i', $user->date);

					$sql = 'SELECT count(sr.surveyresponseId) as count FROM surveyResponses AS sr 
					LEFT JOIN surveyAnswers AS sa ON sa.surveyanswerId = sr.surveyanswerId
					INNER JOIN surveyCompletes AS sc ON sr.surveycompleteId = sc.surveycompleteId
					INNER JOIN surveyQuestions AS sq ON sr.surveyquestionId = sq.surveyquestionId
					WHERE sc.surveyId = ? && sc.userId = ? && IF(sq.type = 1 || sq.type = 2, sa.isTrue = "TRUE", IF(sq.type = 4, sr.answer = sq.isTrue ,false))';
					$user->correctresponses = $this->db->query($sql, array($user->surveyId, $user->userId))->row();

				}
				$sql = "SELECT up.name, up.lastName, up.gender , c.name campaign, uc.turn, up.birthDate, s.name site
				FROM 		surveyViews 	AS sv 
				INNER JOIN  (SELECT name, lastName, gender, birthDate, userId 	FROM userPersonalData) 		AS up 	ON sv.userId 	= up.userId 
				INNER JOIN  (SELECT turn, campaignId, userId 					FROM userComplementaryData) AS uc 	ON sv.userId 	= uc.userId 
				INNER JOIN  (SELECT siteId, userId 								FROM users) 				AS u 	ON sv.userId 	= u.userId 
				LEFT JOIN 	campaigns 		AS c ON c.campaignId 	= uc.campaignId
				LEFT JOIN 	sites 			AS s ON s.siteId 		= u.siteId
				WHERE surveyId = ? && NOT EXISTS (SELECT 1 FROM surveyCompletes	WHERE surveyId = sv.surveyId && userId = sv.UserId) GROUP BY sv.userId";
				$data->incompleteTimes = $this->db->query($sql, $data->surveyId)->result();
				foreach ($data->incompleteTimes as $user) {
					if($user->gender == NULL) 
						$user->gender 	= $this->lang->line('survey_undefined');
					else
						$user->gender = $this->map_tables->get_gender($user->gender);
					if($user->campaign == NULL) 
						$user->campaign = $this->lang->line('survey_undefined');
					if($user->turn == NULL) 
						$user->turn 	= $this->lang->line('survey_undefined');
					else
						$user->turn 	= $this->map_tables->get_turn($user->turn);
					if ($user->birthDate)
						$user->birthDate = date('d/m/Y', strtotime($user->birthDate));
				}
				$sql = "SELECT * FROM surveyQuestions WHERE surveyId = ?";
				$data->questions = $this->db->query($sql, $data->surveyId)->result();
				foreach ($data->questions as $question) {
					if ($question->type == 1 || $question->type == 2)
					{
						$question->totalResponses = 0;
						$sql = "SELECT surveyanswerId, answer, isTrue FROM surveyAnswers WHERE surveyquestionId = ?";
						$question->answers = $this->db->query($sql, $question->surveyquestionId)->result();

						foreach ($question->answers as $answer) {
							$sql = "SELECT surveycompleteId FROM surveyResponses WHERE surveyanswerId = ?";
							$answer->responses = $this->db->query($sql, $answer->surveyanswerId)->result();

							$sql="SELECT concat(up.name,' ',up.lastName) AS completeName
							FROM surveyResponses sr join surveyCompletes sc
							ON sr.surveycompleteId=sc.surveycompleteId
							INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) up
							ON (sc.userId=up.userId)
							WHERE sr.surveyanswerId=?
							ORDER BY completeName";

							$answer->users=$this->db->query($sql,$answer->surveyanswerId)->result();

							$question->totalResponses += count($answer->responses);
						}
					}
					elseif ($question->type == 3) {
						$sql = "SELECT answer FROM surveyResponses WHERE surveyquestionId = ? && answer != ''";
						$question->answers = $this->db->query($sql, $question->surveyquestionId)->result();

					}
					elseif ($question->type == 4) {

						$question->answers = new StdClass();
						$sql = "SELECT answer FROM surveyResponses WHERE surveyquestionId = ? && answer = 'TRUE' ";
						$question->answers->true = $this->db->query($sql, $question->surveyquestionId)->result();
						$sql = "SELECT answer FROM surveyResponses WHERE surveyquestionId = ? && answer = 'FALSE' ";
						$question->answers->false = $this->db->query($sql, $question->surveyquestionId)->result();

					}
				}

				return $data;
			}
		}
	}

	public function Delete()
	{



		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM surveys WHERE surveyId = ?";
			$query = $this->db->query($sql,$this->uri->segment(3))->row();
			if (isset($query))
			{
				$sql = "SELECT * FROM surveyQuestions WHERE surveyId = ?";
				$query->questions = $this->db->query($sql, $query->surveyId)->result();
				foreach ($query->questions as $question) {
					if ($question->type == 1 || $question->type == 2) {
						$sql = "SELECT * FROM surveyAnswers WHERE surveyquestionId = ?";
						$question->answers = $this->db->query($sql, $question->surveyquestionId)->result();
					}
				}
				$sql = "SELECT * FROM surveyLinks WHERE surveyId = ?";
				$query->surveyLinks = $this->db->query($sql, $query->surveyId)->result();

				insert_audit_logs('surveys','DELETE',$query);

				$object = array(
					'surveyId' => $this->uri->segment(3)
				);
				$this->db->delete('surveys', $object);
				return $this->lang->line('survey_delete_message');
			}
		}
	}

	public function Create()
	{
		$this->form_validation->set_rules('name', 'lang:survey_name', 'required');

		if (!$this->input->post('sections[]') && !$this->input->post('roles[]') && !$this->input->post('sites[]') && !$this->input->post('userGroups[]') && !$this->input->post('users[]')) {
			$this->form_validation->set_rules('whose', 'lang:survey_whose', 'required');
		}

		if ($this->form_validation->run() == FALSE)
		{
			$data = new StdClass();
			$sql = "SELECT sectionId, name FROM sections ORDER BY name";
			$data->sections = $this->db->query($sql)->result();

			$sql = "SELECT roleId, name FROM roles ORDER BY name";
			$data->roles = $this->db->query($sql)->result();

			$sql = "SELECT siteId, name FROM sites ORDER BY name";
			$data->sites = $this->db->query($sql)->result();

			if ($this->Identity->Validate('usergroups/create')) {
				$sql = "SELECT userGroupId, name FROM userGroups WHERE userId = ? ORDER BY name";
				$data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
			}

			$this->load->view('surveys/create',$data);

		}
		else
		{

			$startDate 	= NULL;
			$endDate 	= NULL;

			if($this->input->post('startDate'))
			{
				$startDate = strtotime($this->input->post('startDate'));
				if ($startDate === FALSE) {
					echo '{"status":"invalid"}';
					return;
				}
			}

			if($this->input->post('endDate'))
			{
				$endDate = strtotime($this->input->post('endDate'));
				if ($endDate === FALSE) {
					echo '{"status":"invalid"}';
					return;
				}
			}

			$questionsTemp = $this->input->post('question[]');
			$questions = array();

			if ($questionsTemp != NULL && count($questionsTemp) > 0) {
				foreach ($questionsTemp as $key => $questionTemp) {
					$question 				= new StdClass();
					$question->question 	= $questionTemp;
					$question->type 		= $this->input->post("type[".$key."]");
					$question->description 	= $this->input->post("description[".$key."]");
					$question->required 	= $this->input->post("requiredQ[".$key."]") == 'TRUE' ? 'TRUE' : 'FALSE';

					$question->isTrue 	= $this->input->post("isTrue[".$key."]");
					if ($question->isTrue != "TRUE") {
						$question->isTrue = "FALSE";
					}

					if ($question->type == "1" || $question->type == "2" || $question->type == "3" || $question->type == "4") {
						if ($question->type == "1" || $question->type == "2") {
							$tempAnswers = $this->input->post("answer[".$key."]");
							if ($tempAnswers != NULL && count($tempAnswers) > 0) {
								$question->answers 		= array();
								foreach ($tempAnswers as $keyA => $answerTemp) {
									$answer = new StdClass();
									$answer->answer = $answerTemp;
									$answer->isTrue = $this->input->post("isTrueA[".$key."][".$keyA."]");
									if ($answer->isTrue != "TRUE") {
										$answer->isTrue = "FALSE";
									}
									$question->answers[] = $answer;
								}
							}
							else
							{
								echo '{"status":"invalid"}';
								return;
							}
						}
					}
					else
					{
						$question->type = "3";
					}
					$questions[] = $question;
				}
			}
			else
			{
				echo '{"status":"invalid"}';
				return;
			}

			$timestamp = time();
			$closeMessage = $this->input->post('closeMessage');
			if (!$closeMessage || !(is_string($closeMessage) || is_int($closeMessage))) {
				$closeMessage = NULL;
			}

			$isRequired = $this->input->post('required') ? 'TRUE' : 'FALSE'; 
			$object = array(
				'name'		 	=> $this->input->post('name'),
				'timestamp' 	=> $timestamp,
				'userId' 		=> $this->session->UserId,
				'description' 	=> $this->input->post('descriptionS'),
				'startDate' 	=> $startDate,
				'endDate' 		=> $endDate,
				'closeMessage' 	=> $closeMessage,
				'required'		=> $isRequired,
				'active'		=>'FALSE'
			);
			$this->db->insert('surveys', $object);

			$sql = "SELECT surveyId FROM surveys WHERE name = ? && timestamp = ? && userId = ?";
			$survey = $this->db->query($sql, array($this->input->post('name'), $timestamp, $this->session->UserId))->last_row();
			if (isset($survey)) {

				$sections 	= $this->input->post('sections[]');
				$roles 		= $this->input->post('roles[]');
				$sites 		= $this->input->post('sites[]');
				$userGroups = $this->input->post('userGroups[]');
				$users 		= $this->input->post('users[]');

				if($sections)
				{
					foreach ($sections as $section) {
						$sql 	= "SELECT * FROM sections WHERE sectionId = ?";
						$exist 	= $this->db->query($sql, $section)->row();
						if (isset($exist)) {
							$sql 	= "SELECT * FROM surveyLinks WHERE surveyId = ? && sectionId = ?";
							$exist 	= $this->db->query($sql, array($survey->surveyId, $section))->row();
							if (!isset($exist)) {
								$object = array(
									'sectionId' 	=> $section,
									'surveyId' 		=> $survey->surveyId
								);
								$this->db->insert('surveyLinks', $object);
							}
						}
					}
				}

				if($roles)
				{
					foreach ($roles as $role) {
						$sql 	= "SELECT * FROM roles WHERE roleId = ?";
						$exist 	= $this->db->query($sql, $role)->row();
						if (isset($exist)) {
							$sql 	= "SELECT * FROM surveyLinks WHERE surveyId = ? && roleId = ?";
							$exist 	= $this->db->query($sql, array($survey->surveyId, $role))->row();
							if (!isset($exist)) {
								$object = array(
									'roleId' 		=> $role,
									'surveyId' 		=> $survey->surveyId
								);
								$this->db->insert('surveyLinks', $object);
							}
						}
					}
				}

				if($sites)
				{
					foreach ($sites as $site) {
						$sql 	= "SELECT * FROM sites WHERE siteId = ?";
						$exist 	= $this->db->query($sql, $site)->row();
						if (isset($exist)) {
							$sql 	= "SELECT * FROM surveyLinks WHERE surveyId = ? && siteId = ?";
							$exist 	= $this->db->query($sql, array($survey->surveyId, $site))->row();
							if (!isset($exist)) {
								$object = array(
									'siteId' 		=> $site,
									'surveyId' 		=> $survey->surveyId
								);
								$this->db->insert('surveyLinks', $object);
							}
						}
					}
				}

				if($userGroups && $this->Identity->Validate('usergroups/create'))
				{
					foreach ($userGroups as $userGroup) {
						$sql 	= "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
						$exist 	= $this->db->query($sql, array($userGroup, $this->session->UserId))->row();
						if (isset($exist)) {
							$sql 	= "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
							$group 	= $this->db->query($sql, $userGroup)->result();
							if (isset($group) && count($group) > 0) {
								foreach ($group as $user) {
									$sql 	= "SELECT * FROM surveyLinks WHERE surveyId = ? && userId = ?";
									$exist 	= $this->db->query($sql, array($survey->surveyId, $user->userId))->row();
									if (!isset($exist)) {
										$object = array(
											'userId' 		=> $user->userId,
											'surveyId' 		=> $survey->surveyId
										);
										$this->db->insert('surveyLinks', $object);
									}
								}
							}
						}
					}
				}

				if ($users) {
					foreach ($users as $user) {
						$sql 	= "SELECT userId FROM users WHERE userId = ?";
						$exist 	= $this->db->query($sql, $user)->row();
						if (isset($exist)) {
							$sql 	= "SELECT * FROM surveyLinks WHERE surveyId = ? && userId = ?";
							$exist 	= $this->db->query($sql, array($survey->surveyId, $user))->row();
							if (!isset($exist)) {
								$object = array(
									'userId' 		=> $user,
									'surveyId' 		=> $survey->surveyId
								);
								$this->db->insert('surveyLinks', $object);
							}
						}
					}
				}

				foreach ($questions as $key => $question) {
					$object = array(
						'question'		=> $question->question,
						'type' 			=> $question->type,
						'description' 	=> $question->description,
						'required' 		=> $question->required,
						'surveyId' 		=> $survey->surveyId,
						'userId' 		=> $this->session->UserId,
						'isTrue' 		=> $question->isTrue
					);
					$this->db->insert('surveyQuestions', $object);

					if ($question->type == "1" || $question->type == "2") {
						$sql = "SELECT surveyquestionId FROM surveyQuestions WHERE question = ? && type = ? && userId = ? && surveyId = ?";
						$dbQuestion = $this->db->query($sql, array($question->question, $question->type, $this->session->UserId, $survey->surveyId))->last_row();

						if ($dbQuestion) {
							foreach ($question->answers as $keyA => $answer) {
								$object = array(
									'surveyquestionId'	=> $dbQuestion->surveyquestionId,
									'userId' 			=> $this->session->UserId,
									'answer' 			=> $answer->answer,
									'isTrue' 			=> $answer->isTrue
								);
								$this->db->insert('surveyAnswers', $object);
							}
						}
					}
				}

				$data 			= new StdClass();
				$data->status 	= 'ok';
				$data->message 	= $this->lang->line('survey_create_message');
				echo escapeJsonString($data, FALSE);
				return;

			}
			else
			{
				echo '{"status":"invalid"}';
				return;
			}
		}
	}

	public function GetDataById()
	{

		if($this->input->post('surveyId'))
		{
			$sql="SELECT surveyId,name,description,closeMessage,date_format(from_unixtime(startDate),'%m/%d/%Y') as startDate,
			date_format(from_unixtime(endDate),'%m/%d/%Y') as endDate,
			required
			FROM surveys
			WHERE surveyId=?";

			$data=$this->db->query($sql,$this->input->post('surveyId'))->row();

			if(isset($data))
			{
				$sql="SELECT *
				FROM surveyQuestions
				WHERE surveyId=?";

				$data->questions=$this->db->query($sql,$data->surveyId)->result();

				foreach ($data->questions as $q)
				{
					$sql="SELECT *
					FROM  surveyAnswers
					where surveyquestionId=?";

					$q->answers=$this->db->query($sql,$q->surveyquestionId)->result();
				}

				$sql = "SELECT sectionId, name FROM sections ORDER BY name";
				$data->sections = $this->db->query($sql)->result();

				$sql = "SELECT roleId, name FROM roles ORDER BY name";
				$data->roles = $this->db->query($sql)->result();

				$sql = "SELECT siteId, name FROM sites ORDER BY name";
				$data->sites = $this->db->query($sql)->result();

				if ($this->Identity->Validate('usergroups/create'))
				{
					$sql = "SELECT userGroupId, name FROM userGroups WHERE userId = ? ORDER BY name";
					$data->userGroups = $this->db->query($sql, $this->session->UserId)->result();
				}

				$sql="SELECT * FROM surveyLinks where surveyId=?";
				$links=$this->db->query($sql,$this->input->post('surveyId'))->result();

				$users=array();

				foreach($links as $link)
				{
					if ($link->sectionId != NULL)
					{
						for($i=0;$i < count($data->sections);$i++)
						{
							if($data->sections[$i]->sectionId==$link->sectionId)
							{
								$data->sections[$i]->active=true;
								break;
							}
						}

					}
					elseif ($link->roleId != NULL)
					{
						for($i=0;$i < count($data->roles);$i++)
						{
							if($data->roles[$i]->roleId==$link->roleId)
							{
								$data->roles[$i]->active=true;
								break;
							}
						}
					}
					elseif ($link->siteId != NULL) 
					{
						for($i=0;$i < count($data->sites);$i++)
						{
							if($data->sites[$i]->siteId==$link->siteId)
							{
								$data->sites[$i]->active=true;
								break;
							}
						}
					}
					elseif ($link->userId != NULL)
					{
						$sql = "SELECT u.userId, up.name, up.lastName, u.userName FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData WHERE userId = ?) AS up ON u.userId = up.userId WHERE u.userId = ?";
						$tempVar = $this->db->query($sql, array($link->userId, $link->userId))->row();
						array_push($users, $tempVar);	
					}
				}

				$data->users=$users;

				echo json_encode($data);
			}
			else
			{
				echo "error";
				return;
			}

		}
		else
		{
			echo "error";
			return;
		}
	}

	public function Edit()
	{
		if($this->input->post('surveyId'))
		{
			$sql="SELECT surveyId
			FROM surveys
			WHERE surveyId=?";

			$query=$this->db->query($sql,$this->input->post('surveyId'))->row();

			if(isset($query))
			{
				$this->form_validation->set_rules('name', 'lang:survey_name', 'required');

				if (!$this->input->post('sections[]') && !$this->input->post('roles[]') && !$this->input->post('sites[]') && !$this->input->post('userGroups[]') && !$this->input->post('users[]')) {
					$this->form_validation->set_rules('whose', 'lang:survey_whose', 'required');
				}

				if ($this->form_validation->run() == FALSE)
				{
					echo '{"status":"invalid"}';
					return;
				}
				else
				{
					$startDate 	= NULL;
					$endDate 	= NULL;

					if($this->input->post('startDate'))
					{
						$startDate = strtotime($this->input->post('startDate'));
						if ($startDate === FALSE) 
						{
							echo '{"status":"invalid"}';
							return;
						}
					}

					if($this->input->post('endDate'))
					{
						$endDate = strtotime($this->input->post('endDate'));
						if ($endDate === FALSE) {
							echo '{"status":"invalid"}';
							return;
						}
					}

					$questionsTemp = $this->input->post('question[]');
					$questions = array();

					if ($questionsTemp != NULL && count($questionsTemp) > 0)
					{
						foreach ($questionsTemp as $key => $questionTemp)
						{
							$question 				= new StdClass();
							$question->question 	= $questionTemp;
							$question->type 		= $this->input->post("type[".$key."]");
							$question->description 	= $this->input->post("description[".$key."]");
							$question->required 	= $this->input->post("requiredQ[".$key."]") == 'TRUE' ? 'TRUE' : 'FALSE';

							$question->isTrue 	= $this->input->post("isTrue[".$key."]");
							if ($question->isTrue != "TRUE")
							{
								$question->isTrue = "FALSE";
							}

							if ($question->type == "1" || $question->type == "2" || $question->type == "3" || $question->type == "4")
							{
								if ($question->type == "1" || $question->type == "2") 
								{
									$tempAnswers = $this->input->post("answer[".$key."]");

									if ($tempAnswers != NULL && count($tempAnswers) > 0) 
									{
										$question->answers 		= array();

										foreach ($tempAnswers as $keyA => $answerTemp)
										{
											$answer = new StdClass();
											$answer->answer = $answerTemp;
											$answer->isTrue = $this->input->post("isTrueA[".$key."][".$keyA."]");
											if ($answer->isTrue != "TRUE")
											{
												$answer->isTrue = "FALSE";
											}

											$question->answers[] = $answer;
										}
									}
									else
									{
										echo '{"status":"invalid"}';
										return;
									}
								}
							}
							else
							{
								$question->type = "3";
							}

							$questions[] = $question;
						}
					}
					else
					{
						echo '{"status":"invalid"}';
						return;
					}

					$timestamp = time();
					$closeMessage = $this->input->post('closeMessage');
					if (!$closeMessage || !(is_string($closeMessage) || is_int($closeMessage))) {
						$closeMessage = NULL;
					}

					$isRequired = $this->input->post('required') ? 'TRUE' : 'FALSE'; 

					$object = array(
						'name'		 	=> $this->input->post('name'),
						'description' 	=> $this->input->post('descriptionS'),
						'startDate' 	=> $startDate,
						'endDate' 		=> $endDate,
						'closeMessage' 	=> $closeMessage,
						'required'		=> $isRequired
					);

					$this->db->where('surveyId', $query->surveyId);
					$this->db->update('surveys', $object);


					$this->db->where('surveyId', $query->surveyId);
					$this->db->delete('surveyLinks');

					$this->db->where('surveyId', $query->surveyId);
					$this->db->delete('surveyQuestions');


					$sections 	= $this->input->post('sections[]');
					$roles 		= $this->input->post('roles[]');
					$sites 		= $this->input->post('sites[]');
					$userGroups = $this->input->post('userGroups[]');
					$users 		= $this->input->post('users[]');

					if($sections)
					{
						foreach ($sections as $section) {
							$sql 	= "SELECT * FROM sections WHERE sectionId = ?";
							$exist 	= $this->db->query($sql, $section)->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM surveyLinks WHERE surveyId = ? && sectionId = ?";
								$exist 	= $this->db->query($sql, array($query->surveyId, $section))->row();
								if (!isset($exist)) {
									$object = array(
										'sectionId' 	=> $section,
										'surveyId' 		=> $query->surveyId
									);
									$this->db->insert('surveyLinks', $object);
								}
							}
						}
					}

					if($roles)
					{
						foreach ($roles as $role) {
							$sql 	= "SELECT * FROM roles WHERE roleId = ?";
							$exist 	= $this->db->query($sql, $role)->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM surveyLinks WHERE surveyId = ? && roleId = ?";
								$exist 	= $this->db->query($sql, array($query->surveyId, $role))->row();
								if (!isset($exist)) {
									$object = array(
										'roleId' 		=> $role,
										'surveyId' 		=> $query->surveyId
									);
									$this->db->insert('surveyLinks', $object);
								}
							}
						}
					}

					if($sites)
					{
						foreach ($sites as $site) {
							$sql 	= "SELECT * FROM sites WHERE siteId = ?";
							$exist 	= $this->db->query($sql, $site)->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM surveyLinks WHERE surveyId = ? && siteId = ?";
								$exist 	= $this->db->query($sql, array($query->surveyId, $site))->row();
								if (!isset($exist)) {
									$object = array(
										'siteId' 		=> $site,
										'surveyId' 		=> $query->surveyId
									);
									$this->db->insert('surveyLinks', $object);
								}
							}
						}
					}

					if($userGroups && $this->Identity->Validate('usergroups/create'))
					{
						foreach ($userGroups as $userGroup) {
							$sql 	= "SELECT * FROM userGroups WHERE usergroupId = ? && userId = ?";
							$exist 	= $this->db->query($sql, array($userGroup, $this->session->UserId))->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM usergroupUsers WHERE usergroupId = ?";
								$group 	= $this->db->query($sql, $userGroup)->result();
								if (isset($group) && count($group) > 0) {
									foreach ($group as $user) {
										$sql 	= "SELECT * FROM surveyLinks WHERE surveyId = ? && userId = ?";
										$exist 	= $this->db->query($sql, array($query->surveyId, $user->userId))->row();
										if (!isset($exist)) {
											$object = array(
												'userId' 		=> $user->userId,
												'surveyId' 		=> $query->surveyId
											);
											$this->db->insert('surveyLinks', $object);
										}
									}
								}
							}
						}
					}

					if ($users) {
						foreach ($users as $user) {
							$sql 	= "SELECT userId FROM users WHERE userId = ?";
							$exist 	= $this->db->query($sql, $user)->row();
							if (isset($exist)) {
								$sql 	= "SELECT * FROM surveyLinks WHERE surveyId = ? && userId = ?";
								$exist 	= $this->db->query($sql, array($query->surveyId, $user))->row();
								if (!isset($exist)) {
									$object = array(
										'userId' 		=> $user,
										'surveyId' 		=> $query->surveyId
									);
									$this->db->insert('surveyLinks', $object);
								}
							}
						}
					}

					foreach ($questions as $key => $question) {
						$object = array(
							'question'		=> $question->question,
							'type' 			=> $question->type,
							'description' 	=> $question->description,
							'required' 		=> $question->required,
							'surveyId' 		=> $query->surveyId,
							'userId' 		=> $this->session->UserId,
							'isTrue' 		=> $question->isTrue
						);
						$this->db->insert('surveyQuestions', $object);

						if ($question->type == "1" || $question->type == "2") {
							$sql = "SELECT surveyquestionId FROM surveyQuestions WHERE question = ? && type = ? && userId = ? && surveyId = ?";
							$dbQuestion = $this->db->query($sql, array($question->question, $question->type, $this->session->UserId, $query->surveyId))->last_row();

							if ($dbQuestion) {
								foreach ($question->answers as $keyA => $answer) {
									$object = array(
										'surveyquestionId'	=> $dbQuestion->surveyquestionId,
										'userId' 			=> $this->session->UserId,
										'answer' 			=> $answer->answer,
										'isTrue' 			=> $answer->isTrue
									);
									$this->db->insert('surveyAnswers', $object);
								}
							}
						}
					}

					$data 			= new StdClass();
					$data->status 	= 'ok';
					$data->message 	= $this->lang->line('survey_edit_message');
					echo escapeJsonString($data, FALSE);
					return;


				}
			}
			else
			{
				echo '{"status":"invalid"}';
				return;

			}
		}
		else
		{
			echo '{"status":"invalid"}';
			return;

		}
	}

	public function GetSurveyAnswers()
	{
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM surveys WHERE surveyId = ? && EXISTS (SELECT 1 FROM surveyCompletes WHERE surveyCompletes.surveyId = surveys.surveyId && surveyCompletes.userId = ?)";
			$data = $this->db->query($sql,array($this->uri->segment(3), $this->session->UserId))->row();
			if (isset($data))
			{
				$sql = "SELECT surveycompleteId FROM surveyCompletes	WHERE surveyCompletes.surveyId = ? && surveyCompletes.userId = ?";
				$complete = $this->db->query($sql, array($data->surveyId, $this->session->UserId))->row();


				$sql = 'SELECT count(sr.surveyresponseId) as count FROM surveyResponses AS sr 
				LEFT JOIN surveyAnswers AS sa ON sa.surveyanswerId = sr.surveyanswerId
				INNER JOIN surveyCompletes AS sc ON sr.surveycompleteId = sc.surveycompleteId
				INNER JOIN surveyQuestions AS sq ON sr.surveyquestionId = sq.surveyquestionId
				WHERE sc.surveyId = ? && sc.userId = ? && IF(sq.type = 1 || sq.type = 2, sa.isTrue = "TRUE", IF(sq.type = 4, sr.answer = sq.isTrue ,false))';
				$data->correctresponses =  $this->db->query($sql, array($data->surveyId, $this->session->UserId))->row();

				$sql = "SELECT * FROM surveyQuestions WHERE surveyId = ?";
				$data->questions = $this->db->query($sql, $data->surveyId)->result();
				foreach ($data->questions as $question) {
					if ($question->type == 1 || $question->type == 2) {
						$question->totalResponses = 0;
						$sql = "SELECT surveyanswerId, answer, isTrue FROM surveyAnswers WHERE surveyquestionId = ?";
						$question->answers = $this->db->query($sql, $question->surveyquestionId)->result();
						foreach ($question->answers as $answer) {
							$sql = "SELECT surveycompleteId FROM surveyResponses WHERE surveyanswerId = ? && surveycompleteId = ?";
							$answer->response = $this->db->query($sql, array($answer->surveyanswerId, $complete->surveycompleteId))->row();
						}
					}
					else {
						$sql = "SELECT answer FROM surveyResponses WHERE surveyquestionId = ? && surveycompleteId = ?";
						$question->answer = $this->db->query($sql, array($question->surveyquestionId, $complete->surveycompleteId))->row();
					}
				}

				return $data;
			}
		}	
	}

	public function SearchUsers()
	{
		$likeuser = $this->db->escape_like_str($this->input->post('like'));

		if ($likeuser != '') {

			$sql = "SELECT concat_ws(' ', up.name, up.lastName) as completeName, u.userId, u.userName FROM users AS u INNER JOIN (SELECT name, lastName, userId FROM userPersonalData) AS up ON u.userId = up.userId WHERE concat_ws(' ', up.name, up.lastName) LIKE '%$likeuser%' || u.userName LIKE '%$likeuser%' ORDER BY completeName LIMIT 20";
			$query = $this->db->query($sql)->result();
			if (count($query) > 0) {
				echo json_encode($query);

			}
			else
			{
				echo "empty";
			}
		}
		else
		{
			echo "empty";
		}
	}

	public function ActivateSurvey()
	{
		if($this->uri->segment(3))
		{
			$sql="SELECT *
			FROM surveys
			WHERE surveyId=?";

			$query=$this->db->query($sql,$this->uri->segment(3))->row();

			if(isset($query))
			{
				$editRow=array('active'=>'TRUE');

				$this->db->where('surveyId', $query->surveyId);
				$this->db->update('surveys',$editRow);

				return sprintf($this->lang->line('survey_activate_message'),$query->name);
			}
		}
	}

	function UpdateDate($surveyId,$endDate)
	{
		$endDate = strtotime($endDate);

		$this->db->set('endDate',$endDate);
		$this->db->where('surveyId', $surveyId);
		$this->db->update('surveys');
		return 'ok';
	}

	function CollectData($surveyId)
	{
		$data = new StdClass();

		$sql = "SELECT surveyquestionId,question,type FROM surveyQuestions WHERE surveyId = ?";
		$data->questions = $this->db->query($sql, $surveyId)->result();
		foreach ($data->questions as $question) {

			if ($question->type == 1 || $question->type == 2){
				$sql = "SELECT sc.userId, sa.answer
				FROM surveyAnswers sa
				JOIN surveyResponses sr
				ON (sa.surveyAnswerId = sr.surveyanswerId)
				JOIN surveyCompletes sc
				ON (sc.surveycompleteId = sr.surveycompleteId)
				WHERE sa.surveyquestionId = ?";
				$question->answers = $this->db->query($sql, $question->surveyquestionId)->result();
			}

			if($question->type == 3 || $question->type == 4){
				$sql = "SELECT sc.userId, sr.answer 
				FROM surveyResponses sr
				JOIN surveyCompletes sc
				ON (sc.surveycompleteId = sr.surveycompleteId)
				WHERE surveyquestionId = ? && answer != ''";
				$question->answers = $this->db->query($sql, $question->surveyquestionId)->result();
			}

		}
			$sql = "SELECT sc.userId, concat_ws(' ', up.name, up.lastName) as completeName
			FROM surveyCompletes sc
			JOIN userPersonalData up
			ON (sc.userId = up.userId)
			WHERE sc.surveyId = ?";
			$data->users = $this->db->query($sql, $surveyId)->result();
		return $data;
	}

	function CreateReportTable($data)
	{
		$numQuestions = count($data->questions);
		$numUsers = count($data->users);
		$table = "<table><thead>";

		//creacion de header
		$table.="<tr style='border: 1px solid;background-color:#ABCECE;'>";
		$table.= "<th>".$this->lang->line('administration_users_username');"</th>";
		foreach ($data->questions as $question) {
			$table.= "<th>".$question->question."</th>";
		}
		$table.="</tr>";

		$table.="</thead><tbody>";
		//creacion del body
		for ($i=0; $i < $numUsers; $i++) { 
			$user = $data->users[$i];
			$table.= "<tr style='border: 1px solid'>";
				//lenado de la fila
				for ($j=0; $j < $numQuestions+1; $j++) {
					if($j == 0){
						$table.="<td>".$user->completeName."</td>";
					}
					else {
						$question = $data->questions[$j-1];
						if(count($question->answers)>0){
							//para casos de respuestas multiples
							if($question->type == 2){
								$table.="<td>";
								for ($h=0; $h < count($question->answers); $h++) { 
									$answer = $question->answers[$h];
									if($user->userId == $answer->userId){
										$table.= $answer->answer.",";
									}
								}
								$table.="</td>";
							}
							else{
								$questionFound = false;
								for ($h=0; $h < count($question->answers); $h++) {
									if($user->userId == $question->answers[$h]->userId){
										$questionFound = true;
										if($question->type == 4){
											$answer = ($question->answers[$h]->answer == "TRUE") ? $this->lang->line('general_true') : $this->lang->line('general_false');
											$table.="<td>".$answer."</td>";
										}
										else{
											$table.="<td>".$question->answers[$h]->answer."</td>";
										}
									}
								}
								if(!$questionFound){
									$table.="<td> </td>";
								}
								
							}
						}
						else {
							$table.="<td></td>";
						}
							
					}
				}
			$table.="</tr>";
		}
		$table.="</tbody></table>";
		return $table;
	}

	function exportSurveyResults($surveyId)
	{
		$data = $this->CollectData($surveyId);
		$date = date('d-m-y');
		
		$fileName = "Resultados_de_la_encuesta_$date.xls";
		$fileName = (filter_var($fileName,FILTER_SANITIZE_EMAIL));
		
		header("Content-type: application/vnd.ms-excel; name='excel'");
		header("Content-Disposition: attachment; filename = $fileName");
		header("Pragma: no-cache");
		header("Expires: 0"); 
		print_r("\xEF\xBB\xBF"); // UTF-8 BOM

		echo $this->CreateReportTable($data);
	}
}
