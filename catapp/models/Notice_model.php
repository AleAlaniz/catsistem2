<?php
Class Notice_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->lang->load('login_lang');
	}

	var $itemnoticeId;
	var $title;
	var $userId;
	var $timestamp;
	var $itemId;
	var $details;


	function GetNotices($query,$page)
	{
		if(!isset($page)) {
			$page = 1;
		}
		$offset = ($page -1) * 10;
		$sql = "SELECT * FROM itemNotices WHERE itemId = ? ORDER BY timestamp DESC LIMIT $offset, 10";
		$notices = array($query->itemId);
		$query->notices = $this->db->query($sql,$notices)->result();

		$query->available = "false";
		if($this->AvailableToCreate($query->itemId)== true){
			$query->available = "true";
		}
		$query->pageData = $this->FillPageData($query->itemId);
		
		$query->ActionView = $this->load->view('items/notices/_viewnotices_get', $query, TRUE);
		$query->partialView = $this->load->view('items/notices/_viewnotices', $query, TRUE);

		echo json_encode($query);
	}
		

	function GetCreateNoticeView($query)
	{

		//$query->ActionView = $this->load->view('items/notices/_viewnotices_create', $query, TRUE);
		$query->partialView = $this->load->view('items/notices/_viewnotices_create', $query, TRUE);

		echo json_encode($query);
	}

	function GetEditNoticeView($query)
	{

		$sql="SELECT *
		FROM itemNotices
		where itemnoticeId=? and itemId=?";

		$params=array($this->input->post('itemnoticeId'),$query->itemId);

		$query->itemToEdit=$this->db->query($sql,$params)->row();

		$query->partialView = $this->load->view('items/notices/_viewnotices_edit', $query, TRUE);

		echo json_encode($query);
	}

	function CreateNotice()
	{
		if($this->input->post('itemId'))
		{
			$this->form_validation->set_rules('title', 'lang:administration_items_notice_title', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				echo "fail";
			}
			else
			{
				$objecttoInsert = array(
					'title' => $this->input->post('title'),
					'timestamp' => time(),
					'userId' => $this->session->UserId,
					'itemId' => $this->input->post('itemId'),
					'details' => $this->input->post('details'),
					);
				$this->db->insert('itemNotices', $objecttoInsert);

				echo "success";
			}
		}
		echo "fail";

	}


	function EditNotice($query)
	{

		if($this->input->post('itemnoticeId'))
		{
			$this->form_validation->set_rules('title', 'lang:administration_items_notice_title', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				echo "fail";
			}
			else
			{
				$objecttoEdit = array(
					'title' => $this->input->post('title'),
					'details' => $this->input->post('details'),
					);

				$this->db->where('itemnoticeId', $this->input->post('itemnoticeId'));
				$this->db->update('itemNotices', $objecttoEdit);

				echo "success";

			}
		}
		else
			echo "fail";
	}


	function DeleteNotice()
	{
		if ($this->input->post('itemnoticeId') && $this->input->post('itemId')) 
		{
			$sqltoDelete = "SELECT *
			FROM itemNotices
			WHERE itemnoticeId = ? and itemId = ?";
			
			$objecttoDetele =array($this->input->post('itemnoticeId') , $this->input->post('itemId'));
			$queryevent = $this->db->query($sqltoDelete,$objecttoDetele)->row();

			if (isset($queryevent))
			{
				$objecttoDetele = array(
					'itemnoticeId' => $queryevent->itemnoticeId
					);

				$this->db->delete('itemNotices', $objecttoDetele);

				echo "success";
			}
			else
			{
				echo "fail";
			}
		}
		else 
		{
			echo "fail";
		}
	}

	function DeleteAllNotices()
	{
		if ($this->input->post('itemId'))
		{						
			$sqltoDelete = "SELECT *
			FROM itemNotices
			WHERE itemId = ?";

			$objecttoDetele = array($this->input->post('itemId'));
			
			$queryevent = $this->db->query($sqltoDelete,$objecttoDetele)->row();
			
			if (isset($queryevent))
			{
				$eventDelete = array(
					'itemId' => $this->input->post('itemId')
					);

				$this->db->delete('itemNotices', $eventDelete);
				
				echo "success";

			}
			else{
				echo "fail";
			}
		}
		else
			echo "fail";
	}

	function AvailableToCreate($itemId)
	{
		if($itemId){
			$sql = "SELECT COUNT(*) as count FROM `itemNotices` where itemId = $itemId group by itemId";
			$result = $this->db->query($sql)->row();
			if(isset($result)){
				if($result->count < 100){
					return true;
				}
				else{
					return false;
				}
			}
			else{
				return true;
			}
		}
	}

	public function FillPageData($itemId)
	{
		$pageData = new StdClass();
		$query = "SELECT COUNT(*) as count FROM `itemNotices` where itemId = $itemId group by itemId";
		$numrows = $this->db->query($query)->row();
		if(isset($numrows))
		{
			$pageData->numrows = $numrows->count;
			$pageData->numPages = ceil($pageData->numrows/10);
		}
		else {
			$pageData->numrows = 0;
			$pageData->numPages = 0;
		}
		return $pageData;
	}

	public function GetUnread($sectionId)
	{
		$unread = 0;
		$sql = "SELECT ss.subsectionId,i.itemId,count(itemnoticeId) AS c 
		FROM itemNotices itn
		LEFT JOIN items i
		ON i.itemId = itn.itemId
		LEFT JOIN subSections ss
		ON ss.subsectionId = i.subsectionId
		WHERE NOT EXISTS 
			(SELECT 1 FROM itemNoticesView WHERE itemNoticesView.itemnoticeId = itn.itemnoticeId && itemNoticesView.userId = ?)
		AND ss.sectionId = ?
		GROUP BY i.itemId";
		$unread = $this->db->query($sql,array($this->session->UserId,$sectionId))->result();
		
		return $unread;
	}

	public function AddToView($itemId)
	{
		$sql = "SELECT  itn.itemnoticeId,itn.userId
		FROM itemNotices itn
		JOIN items i
		ON i.itemId = itn.itemId
		JOIN subSections ss
		ON ss.subsectionId = i.subsectionId
		WHERE i.itemId = ?";
		$notices = $this->db->query($sql,$itemId)->result();

		foreach ($notices as $notice) {
			$sql = "SELECT * FROM itemNoticesView WHERE itemNoticeId = ? && userId = ?";
			$isView = $this->db->query($sql,array($notice->itemnoticeId,$this->session->UserId))->row();
			if($isView == NULL){
				$objectInsert = array(
					'dateView' 		=> date('Y-m-d'), 
					'userId' 		=> $this->session->UserId,
					'itemnoticeId'	=> $notice->itemnoticeId
				);
				$this->db->insert('itemNoticesView',$objectInsert);
			}
		}
	}
}
