<?php
Class Subsection_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->lang->load('login_lang');
	}

	var $subsectionId;
	var $name;
	var $sectionId;
	var $timestamp;
	var $icon;
	var $userId;

	function GetSubsections(){
		$sql = "SELECT * FROM subSections ORDER BY name";
		$query = $this->db->query($sql)->result();
		foreach ($query as $key => $subsection) {
			if ($subsection->isssection == 'TRUE') {
				$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
				$section = array($subsection->fereingsubsectionId);
				$subsection->section = $this->db->query($sql,$section)->row();

				$sql = "SELECT name, sectionId FROM sections WHERE sectionId = ?";
				$subsection->fereingsubsection = $this->db->query($sql, $subsection->section->sectionId)->row();
				$subsection->section->name .= ' - '.$subsection->fereingsubsection->name;

			} 
			else
			{
				$sql = "SELECT * FROM sections WHERE sectionId = ?";
				$section = array($subsection->sectionId);
				$subsection->section = $this->db->query($sql,$section)->row();
			}
		}
		return $query;
	}

	function CreateSubsection(){

		if ($this->input->post('appendto') && $this->input->post('appendto') == 'subsection') {
			$sectionsSql = "SELECT * FROM subSections WHERE sectionId != 'NULL' ";
			$sectionsQuery = $this->db->query($sectionsSql)->result();
			$sectionsIds = '';
			foreach ($sectionsQuery as $value) {
				$sectionsIds .= $value->subsectionId.','; 
			}
			$this->form_validation->set_rules('subsection', 'lang:administration_subsections_subsection','required|in_list['.$sectionsIds.']',
				array('in_list' => $this->lang->line('administration_subsections_subsectionexist')));	
		}
		else
		{
			$sectionsSql = "SELECT * FROM sections";
			$sectionsQuery = $this->db->query($sectionsSql)->result();
			$sectionsIds = '';
			foreach ($sectionsQuery as $value) {
				$sectionsIds .= $value->sectionId.','; 
			}
			$this->form_validation->set_rules('section', 'lang:administration_subsections_section','required|in_list['.$sectionsIds.']',
				array('in_list' => $this->lang->line('administration_subsections_sectionexist')));	
		}
		$this->form_validation->set_rules('name', 'lang:administration_subsections_name', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$sql = "SELECT * FROM sections ORDER BY name";
			$query['sections'] = $this->db->query($sql)->result();

			$sql = "SELECT * FROM subSections WHERE sectionId != 'NULL' ORDER BY name";
			$query['subSections'] = $this->db->query($sql)->result();


			for($i=0; count($query['subSections']) > $i; $i++){ 
				$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
				$sectionVar = array($query['subSections'][$i]->sectionId);
				$querySection = $this->db->query($sqlSection,$sectionVar)->row();
				$query['subSections'][$i]->name .= ' - '.$querySection->name;
			}

			$this->load->view('_shared/_administrationlayoutheader');
			$this->load->view('subsections/create', $query);
			$this->load->view(PRELAYOUTFOOTER);
		}
		else
		{
			if (!$this->input->post('icon') || $this->input->post('icon') == '') {
				$iconSet = 'fa-sitemap';
			}
			else{
				$iconSet = $this->input->post('icon');
			}

			if ($this->input->post('appendto') && $this->input->post('appendto') == 'subsection') {
				$sectionidvar = NULL;
				$subsectionidvar = $this->input->post('subsection');
				$itsssection = 'TRUE';
			}
			else
			{
				$sectionidvar = $this->input->post('section');;
				$subsectionidvar = NULL;
				$itsssection = 'FALSE';
			}
			$subSectionInsert = array(
				'name' => $this->input->post('name'),
				'sectionId' => $sectionidvar,
				'fereingsubsectionId' => $subsectionidvar,
				'icon' => $iconSet,
				'userId' => $this->session->UserId,
				'isssection' => $itsssection,
				'timestamp' => time()
				);

			insert_audit_logs('subSections','INSERT',$subSectionInsert);

			$this->db->insert('subSections', $subSectionInsert);
			$this->session->set_flashdata('subsectionMessage', 'create');
			header('Location:/'.FOLDERADD.'/subsections');
		}
	}

	function DeleteSubsection(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
			$subSection = array($this->uri->segment(3));
			$query = $this->db->query($sql,$subSection)->row();
			if (isset($query))
			{
				if ($this->input->post('subsectionId') && $this->input->post('subsectionId') == $query->subsectionId ) {
					$sectionDelete = array(
						'subsectionId' => $this->input->post('subsectionId')
						);

					insert_audit_logs('subSections','DELETE',$query);

					$this->db->delete('subSections', $sectionDelete);
					$this->session->set_flashdata('subsectionMessage', 'delete');
					header('Location:/'.FOLDERADD.'/subsections');
				}
				else{
					if (isset($query->sectionId)) {
						$sql = "SELECT * FROM sections WHERE sectionId = ?";
						$section = array($query->sectionId);
						$query->section = $this->db->query($sql,$section)->row();
					}
					elseif (isset($query->fereingsubsectionId)) {
						$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
						$section = array($query->fereingsubsectionId);
						$query->subsection = $this->db->query($sql,$section)->row();

						$sql = "SELECT name, sectionId FROM sections WHERE sectionId = ?";
						$query->fereingsubsection = $this->db->query($sql, $query->subsection->sectionId)->row();
						$query->subsection->name .= ' - '.$query->fereingsubsection->name;
					}
					$query->navBar = $this->load->view('subsections/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('subsections/delete', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function DetailsSubsection(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
			$subsection = array($this->uri->segment(3));
			$query = $this->db->query($sql,$subsection)->row();
			if (isset($query))
			{
				if (isset($query->sectionId)) {
					$sql = "SELECT * FROM sections WHERE sectionId = ?";
					$section = array($query->sectionId);
					$query->section = $this->db->query($sql,$section)->row();
				}
				elseif (isset($query->fereingsubsectionId)) {
					$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
					$section = array($query->fereingsubsectionId);
					$query->subsection = $this->db->query($sql,$section)->row();
					$sql = "SELECT name, sectionId FROM sections WHERE sectionId = ?";
					$query->fereingsubsection = $this->db->query($sql, $query->subsection->sectionId)->row();
					$query->subsection->name .= ' - '.$query->fereingsubsection->name;
				}
				$query->navBar = $this->load->view('subsections/_navbar', $query, TRUE);
				$this->load->view('_shared/_administrationlayoutheader');
				$this->load->view('subsections/details', $query);
				$this->load->view(PRELAYOUTFOOTER);
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

	function EditSubsection(){
		if ($this->uri->segment(3)) {
			$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
			$subsection = array($this->uri->segment(3));
			$query = $this->db->query($sql,$subsection)->row();
			if (isset($query))
			{
				$sql = "SELECT * FROM sections ORDER BY name";
				$query->sections = $this->db->query($sql)->result();
				if ($this->input->post('subsectionId') && $this->input->post('subsectionId') == $query->subsectionId ) {

					if ($this->input->post('appendto') && $this->input->post('appendto') == 'subsection') {
						$sectionsSql = "SELECT * FROM subSections  WHERE sectionId != 'NULL' && subsectionId != ?  ";
						$sectionsQuery = $this->db->query($sectionsSql, array($query->subsectionId))->result();
						$sectionsIds = '';
						foreach ($sectionsQuery as $value) {
							if ($value->subsectionId != $query->subsectionId) {
								$sectionsIds .= $value->subsectionId.','; 
							}
						}
						$this->form_validation->set_rules('subsection', 'lang:administration_subsections_subsection','required|in_list['.$sectionsIds.']',
							array('in_list' => $this->lang->line('administration_subsections_subsectionexist')));	
					}
					else
					{
						$sectionsSql = "SELECT * FROM sections";
						$sectionsQuery = $this->db->query($sectionsSql)->result();
						$sectionsIds = '';
						foreach ($sectionsQuery as $value) {
							$sectionsIds .= $value->sectionId.','; 
						}
						$this->form_validation->set_rules('section', 'lang:administration_subsections_section','required|in_list['.$sectionsIds.']',
							array('in_list' => $this->lang->line('administration_subsections_sectionexist')));	
					}

					$this->form_validation->set_rules('name', 'lang:administration_subsections_name', 'required');

					if ($this->form_validation->run() == FALSE)
					{
						$sql = "SELECT * FROM sections ORDER BY name";
						$query->sections = $this->db->query($sql)->result();

						$sql = "SELECT * FROM subSections   WHERE sectionId != 'NULL' && subsectionId != ? ORDER BY name";
						$query->subSections = $this->db->query($sql, array($query->subsectionId))->result();


						for($i=0; count($query->subSections) > $i; $i++){ 
							$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
							$sectionVar = array($query->subSections[$i]->sectionId);
							$querySection = $this->db->query($sqlSection,$sectionVar)->row();
							$query->subSections[$i]->name .= ' - '.$querySection->name;
							
						}
						$query->navBar = $this->load->view('subsections/_navbar', $query, TRUE);
						$this->load->view('_shared/_administrationlayoutheader');
						$this->load->view('subsections/edit',$query);
						$this->load->view(PRELAYOUTFOOTER);
					}
					else
					{
						if (!$this->input->post('icon') || $this->input->post('icon') == '') {
							$iconSet = 'fa-sitemap';
						}
						else{
							$iconSet = $this->input->post('icon');
						}

						if ($this->input->post('appendto') && $this->input->post('appendto') == 'subsection') {
							$sectionidvar = NULL;
							$subsectionidvar = $this->input->post('subsection');
							$itsssection = 'TRUE';
						}
						else
						{
							$sectionidvar = $this->input->post('section');;
							$subsectionidvar = NULL;
							$itsssection = 'FALSE';
						}


						$subsectionEdit = array(
							'name' => $this->input->post('name'),
							'sectionId' => $sectionidvar,
							'fereingsubsectionId' => $subsectionidvar,
							'isssection' => $itsssection,
							'icon' => $iconSet
							);

						insert_audit_logs('subSections','UPDATE',$query);

						$this->db->where('subsectionId', $query->subsectionId);
						$this->db->update('subSections', $subsectionEdit);
						$this->session->set_flashdata('subsectionMessage', 'edit');
						header('Location:/'.FOLDERADD.'/subsections/details/'.$query->subsectionId);
					}
				}
				else{
					$sql = "SELECT * FROM sections ORDER BY name";
					$query->sections = $this->db->query($sql)->result();

					$sql = "SELECT * FROM subSections   WHERE sectionId != 'NULL' && subsectionId != ? ORDER BY name";
					$query->subSections = $this->db->query($sql, array($query->subsectionId))->result();


					for($i=0; count($query->subSections) > $i; $i++){  
						$sqlSection = "SELECT * FROM sections WHERE sectionId = ?";
						$sectionVar = array($query->subSections[$i]->sectionId);
						$querySection = $this->db->query($sqlSection,$sectionVar)->row();
						$query->subSections[$i]->name .= ' - '.$querySection->name;
					}

					$query->navBar = $this->load->view('subsections/_navbar', $query, TRUE);
					$this->load->view('_shared/_administrationlayoutheader');
					$this->load->view('subsections/edit', $query);
					$this->load->view(PRELAYOUTFOOTER);
				}
			}
			else
			{
				show_404();
			}
		}
		else{
			show_404();
		}
	}

}
