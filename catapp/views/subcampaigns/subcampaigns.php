<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('subcampaigns_index');?></li>
</ol>

<?php if($this->Identity->Validate('subcampaigns/create')) { ?>
<p>
	<a href="/<?=FOLDERADD?>/subcampaigns/create" class="btn btn-sm btn-success "><i class="fa fa-plus"></i><strong> <?=$this->lang->line('administration_create');?></strong></a>
</p>
<?php }
if (isset($_SESSION['flashMessage'])){ ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['flashMessage'] == 'create'){
		echo $this->lang->line('subcampaign_successmessage');
	}
	elseif ($_SESSION['flashMessage'] == 'delete'){
		echo $this->lang->line('subcampaign_deletemessage');
	}
	elseif($_SESSION['flashMessage']=='edit')
	{
		echo $this->lang->line('subcampaign_editmessage');
	}
	?>
</div>
<?php } ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('general_subCampaign');?></strong>
		<span class="badge pull-right bg-success"><?=count($model)?></span>
	</div>
	<table class="table table-hover">
		<?php if (count($model) > 0) {?>
		<thead>
			<tr class="active">
				<th><?=$this->lang->line('campaign_name');?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>

			<?php foreach ($model as $subcampaign){ ?>
			<tr class="optionsUser">
				<td><?=encodeQuery($subcampaign->name)?></td>
				<td class="text-right">
					<?php if($this->Identity->Validate('subcampaigns/edit')) { ?>
					<a href="/<?=FOLDERADD?>/subcampaigns/edit/<?=$subcampaign->subCampaignId?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp; 
					<?php } ?>
					<?php if($this->Identity->Validate('subcampaigns/delete')) { ?>
					<a href="/<?=FOLDERADD?>/subcampaigns/delete/<?=$subcampaign->subCampaignId?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
					<?php } ?>
				</td>
			</tr>

			<?php } ?>
		<?php } 
			else { 
				?>
				<tr class="text-center">
					<td colspan="3"><i class="fa fa-bullhorn"></i> <?=$this->lang->line('campaign_empty');?></td>
				</tr>
				<?php
			}?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
$('#nav_subCampaigns').addClass('active');

</script>