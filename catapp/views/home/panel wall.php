<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/timeline.css">
<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/custombox.min.css">
<script>
function likeToggle(argument) {

	argument.find($('.like > .fa')).click(function() {
		if ($(this).hasClass('like-o')) {
			$(this).removeClass('fa-heart like-o');
			$(this).addClass('fa-heart-o');
			var likeValue = $(this).parent().next().text();
			likeValue--;
			$(this).parent().next().text(likeValue);
		}
		else
		{
			$(this).removeClass('fa-heart-o ');
			$(this).addClass('fa-heart like-o');
			var likeValue = $(this).parent().next().text();
			likeValue++;
			$(this).parent().next().text(likeValue);
		}

		var dataAjax = {
			wallpostId: $(this).parent().parent().attr('wallpostId'),
		};
		$.ajax({
			method: "POST",
			url: 'home/togglelike',
			data: dataAjax
		}).done(function (data) {
			if(data == 'invalid'){
				$(location).attr('href', '/<?php echo FOLDERADD;?>');
			}
		});


	});

}

function commentsToggle(argument) {

	argument.find($('.btn-comments > .fa')).click(function() {
		$(this).parent().parent().next().fadeToggle();
	});
	argument.find($('.StatusContain > .btn.btn-xs.pull-right.text-info')).click(function(e) {
		var goToF = confirm('<?=$this->lang->line("home_deletepostareyousure")?>');
		if(!goToF){
			e.preventDefault();
		}
	});
}

function deleteComment(argument) {
	argument.find($('.fa-trash')).click(function() {
		var goToF = confirm('<?=$this->lang->line("home_deletecommentareyousure")?>');
		if(!goToF){
			return;
		}
		
		$(this).removeClass('fa-trash ');
		$(this).addClass('fa-spinner fa-pulse');
		var parentComment = $(this).parent().parent().parent();
		var dataAjax = {
			wallcommentId: parentComment.attr('wallcommentId'),
		};
		$.ajax({
			method: "POST",
			url: 'home/deletecomment',
			data: dataAjax
		}).done(function (data) {
			if(data == 'invalid'){
				$(location).attr('href', '/<?php echo FOLDERADD;?>');
			}
			else{
				parentComment.fadeOut();
				var commentValue = parentComment.parent().parent().parent().find($('[ariaLabel="commentNumber"]')).text();
				commentValue--;
				parentComment.parent().parent().parent().find($('[ariaLabel="commentNumber"]')).text(commentValue);
				parentComment.remove();
			}
		});
	});
}

function postComment(wallPost) {

	var wallPostForm = wallPost.find($("form"));

	wallPostForm.submit(function (e) {

		e.preventDefault();

		var datosForm = {
			Comment: $(this).find("textarea").val(),
			wallpostId : wallPost.find($('[wallpostId]')).attr('wallpostId')
		};

		$(this).find($('button')).attr('disabled', 'TRUE');
		$(this).find($('textarea')).attr('disabled', 'TRUE');
		$(this).find($('button')).html('<i class="fa fa-spinner fa-pulse fa-lg"></i>');
		$(this).find($('p.text-danger')).slideUp();

		$.ajax({
			method: "POST",
			url: 'home/createcomment',
			data: datosForm,
		}).done(function (data) {

			if(data == 'invalid'){
				$(location).attr('href', '/<?php echo FOLDERADD;?>');
			}
			else
			{
				wallPostForm.find($('button')).removeAttr('disabled');
				wallPostForm.find($('textarea')).removeAttr('disabled');
				wallPostForm.find($('button')).html('<?=$this->lang->line("home_submitcomment")?> &nbsp;<i class="fa fa-angle-up"></i>');
				wallPostForm.find($('p.text-danger')).slideUp();

				if (data == 'commentEmpty') {
					wallPostForm.find($('p.text-danger')).html('<?=$this->lang->line("home_commentempty")?>');
					wallPostForm.find($('p.text-danger')).slideDown();
				}
				else{
					wallPost.find($('[ariaLabel="allcoments"]')).append(data);
					wallPostForm.find("textarea").val('');

					var commentValue = wallPost.find($('[ariaLabel="commentNumber"]')).text();
					commentValue++;
					wallPost.find($('[ariaLabel="commentNumber"]')).text(commentValue);

				}

			}

		});
	});



}
</script>
<div class="row" id="content">
	<div class="col-md-7">
		<div class="panel panel-default wall">
			<div class="panel-heading">
				<strong><?=$this->lang->line('home_thewall')?></strong>
			</div>

			<div class="well wall-post"> 
				<h4><?=$this->lang->line('home_whatsnew')?></h4>
				<form id="postForm" class="form-horizontal" role="form">
					<div class="form-group" style="padding:14px;">
						<textarea id="postText" class="form-control" placeholder="<?=$this->lang->line('home_statusplaceholder')?>"></textarea>
					</div>
					<p class="text-danger" style="display:none" id="posterrormMessage"></p>
					
					<button id="formSubmit" class="btn btn-info pull-right" type="submit"><?=$this->lang->line('home_submitpost')?> &nbsp;<i class="fa fa-angle-down"></i></button>
					<div class="dropup">
						<span class="pull-right btn btn-primary dropdown-toggle" style="margin-right: 5px;" type="button" id="smiles-table" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-smile-o"></i>
						</span>
						<table class="dropdown-menu pull-right clearfix smiles-table" aria-labelledby="smiles-table" style="padding: 5px;">
							<?php 
							if (file_exists(APPPATH.'config/smileys.php'))
							{
								include(APPPATH.'config/smileys.php');
							}

							if (file_exists(APPPATH.'config/'.ENVIRONMENT.'/smileys.php'))
							{
								include(APPPATH.'config/'.ENVIRONMENT.'/smileys.php');
							}

							if (empty($smileys) OR ! is_array($smileys))
							{
								$_smileys = array();
								return FALSE;
							}
							$totalSmileys = count($smileys);
							$totalCells = 10;
							$restCells = $totalSmileys % $totalCells;
							$totalRows = ($totalSmileys - $restCells) / $totalCells;
							for ($row=1; $row <= $totalRows; $row++) { 
								echo "<tr>";
								for ($cell=1; $cell <= $totalCells; $cell++) { 
									$actualSmiley = ($row * $totalCells) - $totalCells + 1 + $cell - 1;
									$actualKey = 1;
									$cellKey =  NULL;
									foreach ($smileys as $key => $val)
									{
										if ($actualSmiley == $actualKey) {
											$cellKey = $key;
										}
										$actualKey++;
									}
									echo '<td><div class="smiley smiley-btn" style=" background-position:'.$smileys[$cellKey][1].'px '.$smileys[$cellKey][2].'px;" title="'.$smileys[$cellKey][3].'"></div></td>';
								}
								echo "</tr>";
							}
							echo "<tr>";
							for ($restCell=1; $restCell <= $restCells; $restCell++) { 
								$actualSmiley = ($totalCells * $totalRows) + $restCell;
								$actualKey = 1;
								$cellKey =  NULL;
								foreach ($smileys as $key => $val)
								{
									if ($actualSmiley == $actualKey) {
										$cellKey = $key;
									}
									$actualKey++;
								}

								echo '<td><div class="smiley smiley-btn" style=" background-position:'.$smileys[$cellKey][1].'px '.$smileys[$cellKey][2].'px;" title="'.$smileys[$cellKey][3].'"></div></td>';

							}
							echo "</tr>";
							?>
						</table>
					</div>
				</form>
			</div>

			<div class="alert alert-success" style="display:none" id="postSuccess" role="alert">
				<strong><i class="fa fa-check"></i></strong> 
				<?php 
				echo $this->lang->line('home_updatestatusok');
				?>
			</div>

			<div class="newpostsBar"  id="newpostsBar">
				<span id="newpostNumber"></span> 
				<?=$this->lang->line('home_newposts')?>
			</div>

			<div id="allposts">
			</div>
			<div class="alert alert-info" style="margin-top: 10px;display:none" id="postEmpty" role="alert">
				<strong><i class="fa fa-info-circle fa-lg fa-fw"></i></strong> 
				<?php 
				echo $this->lang->line('home_postsempty');
				?>
			</div>
			<div class="progress progress-striped active">
				<div class="progress-bar progress-bar-info" style="width: 100%"></div>
			</div>
		</div>
	</div>
	<div class="col-md-5" id="fixFixedtop">
		<?php
		if ($this->Identity->Validate('home/alert') && isset($alert->message) && $alert->message != NULL) 
		{
			?>
			<div class="panel panel-warning">
				<div class="panel-heading">
					<strong><?=$this->lang->line('home_alerttitle')?></strong>
					<div class="pull-right badge"><i class="fa fa-info"></i></div>
				</div>
				<div class="panel-body">
					<?=$alert->message?>
				</div>
			</div>
			<?php
		}
		?>
		<div class="panel panel-success">
			<div class="panel-heading">
				<div class="pull-right btn  btn-xs" id="message-toggle"><i class="fa fa-minus"></i> / <i class="fa fa-plus"></i></div>
				<strong><?=$this->lang->line('general_welcomemessage');?></strong>
			</div>
			<div class="panel-body" id="message-body" style="overflow: hidden;">
				<?=$message?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$('#nav_home').addClass('active');
$('#message-toggle').click(function () {
	$('#message-body').slideToggle();
} );

$('.alert-alert').hover(function () {
	$(this).removeClass('alert-alert');
},
function () {
	$(this).addClass('alert-alert');
} );

$(document).ready(function(){

	var datos = {
		request: 'TRUE',
	};

	$.ajax({
		method: "POST",
		url: 'home/getposts',
		data: datos,
	}).done(function (data) {
		if(data == 'invalid'){
			$(location).attr('href', '/<?php echo FOLDERADD;?>');
		}
		else
		{

			if (data == '') {
				$('#postEmpty').slideDown();
			}

			$("#allposts").prepend(data);
			$('.progress').slideUp();
			$('.status').fadeIn();


			var elementPosition = $('#fixFixedtop').offset();
			var elementWidth = $('#fixFixedtop').css('width');
			elementWidth = elementWidth.split('px');
			elementWidth = elementWidth[0];
			elementWidth;
			var windowsize = $(window).width();
			$(window).resize(function(){
				var rest = windowsize - $(window).width();
				elementPosition = windowsize - rest;
				elementWidth = windowsize - rest;
				windowsize = $(window).width();
			});

			function fixtotop(){
				if($(window).scrollTop() + 65 > elementPosition.top && $(window).width() >= 992 && $(window).height() > $('#fixFixedtop').height() + 45){
					$('#fixFixedtop').css('position','fixed').css({
						'top': elementPosition.top,
						'left' : elementPosition.left,
						'width' : elementWidth+'px',
					});
				} 
				else {
					$('#fixFixedtop').css({
						'position' : 'relative',
						'width' : '',
						'top': '',
						'left' : '',
					});
				} 
			}

			fixtotop();  

			$(window).scroll(function(){
				fixtotop();  
			});
		}
	});


if ($(window).width() < 992) {
	$('#content').prepend($('#fixFixedtop'));
}

$(window).resize(function(){
	if ($(window).width() < 992) {
		$('#content').prepend($('#fixFixedtop'));
	}
	else
	{
		$('#content').append($('#fixFixedtop'));
	}
});

$("#postForm").submit(function (e) {
	e.preventDefault();
	var datosForm = {
		Post: $(this).find("#postText").val(),
	};
	$('#postText').attr('disabled', 'TRUE');
	$('#formSubmit').attr('disabled', 'TRUE');
	$('#formSubmit').html('<i class="fa fa-spinner fa-pulse fa-lg"></i>')
	$('#posterrormMessage').slideUp();

	$.ajax({
		method: "POST",
		url: 'home/createpost',
		data: datosForm,
	}).done(function (data) {

		if(data == 'invalid'){
			$(location).attr('href', '/<?php echo FOLDERADD;?>');
		}
		else
		{
			$('#postText').removeAttr('disabled');
			$('#formSubmit').removeAttr('disabled');
			$('#formSubmit').html('<?=$this->lang->line("home_submitpost")?> &nbsp;<i class="fa fa-angle-down"></i>');
			if (data == 'postEmpty') {
				$('#posterrormMessage').html('<?=$this->lang->line("home_postempty")?>');
				$('#posterrormMessage').slideDown();
			}
			else if(data == 'statusOk'){
				$('#postText').val('');
				$('#postSuccess').slideDown();
				datos = {
					request: 'TRUE',
				};
				$.ajax({
					method: "POST",
					url: 'home/getlastpost',
					data: datos,
				}).done(function (data) {
					$("#allposts").prepend(data);
					$('.status').fadeIn();
				});
			}

		}

	});
});

$("[aria-labelledby='smiles-table']").css('margin-right', ($('#formSubmit').width()+ 30)+'px');


var $win = $(window);
var $getPosts = true;

function GetOldPosts() {
	$getPosts = false;
	$('.progress').show();

	var dataAjax = {
		request: 'TRUE',
	};

	$.ajax({
		method: "POST",
		url: 'home/getoldposts',
		data: dataAjax
	}).done(function (data) {
		if(data == 'invalid'){
			$(location).attr('href', '/<?php echo FOLDERADD;?>');
		}
		else
		{
			$('.progress').hide();
			if (data != '') {
				$("#allposts").append(data);
				$('.status').fadeIn();
				$getPosts = true;
			}
			else{
				$('#postEmpty').slideDown();
			}
		}

	});
};

$win.scroll(function () {
	if ($win.height() + $win.scrollTop() >= ($(document).height() - 200) && $getPosts) {
		GetOldPosts();
	}
});

});


setInterval(function() {
	var dataAjax = {
		request: 'TRUE',
	};
	$.ajax({
		method: "POST",
		url: 'home/getnewpostsnumber',
		data : dataAjax
	}).done(function (data) {

		if(data == 'invalid'){
			$(location).attr('href', '/<?php echo FOLDERADD;?>');
		}
		else
		{
			if (data != '') {
				$('#newpostNumber').text(data);
				$('#newpostsBar').slideDown();
			}
			else
			{
				$('#newpostsBar').slideUp();
			}
		}

	});
}, 2000);

$('#newpostsBar').click(function() {

	var dataAjax = {
		request: 'TRUE',
	};
	$('#newpostsBar').hide();
	$.ajax({
		method: "POST",
		url: 'home/getnewposts',
		data : dataAjax
	}).done(function (data) {

		if(data == 'invalid'){
			$(location).attr('href', '/<?php echo FOLDERADD;?>');
		}
		else
		{
			if (data != '') {
				$("#allposts").prepend(data);
				$('.status').fadeIn();
			}
		}

	});
});

<?php 
foreach ($smileys as $key => $val)
{
	if (isset($remplaceSpecial[$key])) {
		echo "$('.smiley-btn[title=\"".$remplaceSpecial[$key][0]."\"]').click(function() {
			$('#postText').val($('#postText').val() + ' ".$remplaceSpecial[$key][1]." ');
		});";
}
else{
	echo "$('.smiley-btn[title=\"".$smileys[$key][3]."\"]').click(function() {
		$('#postText').val($('#postText').val() + ' ".$smileys[$key][3]." ');
	});";
}
}
?>



</script>
<?php
if ($this->session->welcomepopup){ 
	?>

	<div class="panel panel-info" style="display:none" id="message-popup">
		<div class="panel-heading">
			<div class="pull-right btn btn-xs" id="message-popup-toggle"><i class="fa fa-times"></i></div>
			<strong>Bienvenido <?=$this->session->welcomepopup?></strong>
		</div>
		<div class="panel-body" id="message-body">
			Estamos trabajando en la costruccion del sitio.
		</div>
	</div>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/custombox.min.js"></script>
	<script>
	$(document).ready(function() {
		Custombox.open({
			target: '#message-popup',
			effect: 'flash'
		});
		$('#message-popup-toggle').on('click', function(e) {
			Custombox.close();
		});
	});
	</script>
	<?php 
	$this->session->unset_userdata('welcomepopup');
} 
?>