<div class="col-xs-12 flat-style">
	<div class="alert bg-green" role="alert" ng-show="message != ''">
		<button type="button" class="close"  ng-click="message = ''"><span>&times;</span></button>
		<strong><i class="fa fa-check"></i> <span ng-bind="message"></span></strong> 
	</div>
	<div class="row" id="content">
		<?php if (isset($_SESSION['flashMessage']))
		{
			?>
			<div class="alert bg-lightgreen alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<?php 
				if ($_SESSION['flashMessage'] == 'surveycomplete')
				{
					echo $this->lang->line('home_surveycomplete');
				}
				?>
			</div>
			<?php 
		}
		?>
		<div class="col-md-12">
			<div class="panel panel-default" style="background: transparent;border: unset;box-shadow: unset;">
				<div class="panel-body text-center" id="panel_container">
					<?php echo $panelMessage->message; ?>
				</div>

			</div>
		</div>
		<div class="col-md-6">
			<div class="panel">
				<div class="panel-custom-heading bg-lightgreen">
					<h5>
						<?=$this->lang->line('general_welcomemessage');?>
					</h5>
				</div>
				<div class="text-center panel-body" id="welcome_loading">
					<i class='fa fa-refresh fa-spin fa-5x fa-fw dark' ></i>
				</div>
				<div class="panel-body" id="welcome_container" style="overflow: hidden;">

				</div>
				<div class="panel-body" id="welcome_empty" style="overflow: hidden;">
					<?php 
						echo '<div class="text-center">';
						echo $this->lang->line('home_welcomemessageempty');
						echo '</div>';
					?>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<?php
			if ($this->Identity->Validate('home/alert')) 
			{
				?>

				<div class="panel ">
					<div class="panel-custom-heading bg-yelow">
						<h5><?=$this->lang->line('home_alerttitle')?></h5>
					</div>
					<div class="text-center panel-body" id="panel_alert_loading">
						<i class='fa fa-refresh fa-spin fa-5x fa-fw dark' ></i>
					</div>
					<div id="panel_alert_container" style="display:none">
						<div class="text-center panel-body" ng-hide="alerts.length > 0">
							<?php echo $this->lang->line('panel_alertempty');?>
						</div>
						<ul class="list-group" style="max-height: 250px;overflow: auto; margin-bottom:0" >
							<li class="list-group-item" style="word-break: normal;" ng-repeat="alert in alerts">
								<?php
								if ($this->Identity->Validate('administration/alert')) {
									?>
									<span class="pull-right btn btn-xs text-danger" role="deletebutton" data-target="#confirm-delete" data-toggle="modal" data-id="{{alert.alertId}}" ><i class="fa fa-times  fa-lg"></i></span>
									<?php
								}
								?>
								<span ng-bind-html="alert.message | HtmlSanitize" ></span>
								<p class="alert-date">
									<small class="text-muted"><i class="fa fa-clock-o" style="width: auto !important"></i> <span ng-bind="alert.timestamp"></span></small>
								</p>
							</li>
						</ul>
					</div>
				</div>
				<input type="hidden" id="birthDate" value= <?php echo $birthDate; ?> >
				<input type="hidden" id="userId" value= <?php echo $userId; ?> >
				<?php
			}
			?>
		</div>
		<div class="modal fade" tabindex="-1" role="dialog" id="birthDayDialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<button type="button" class="close " style="width: 20px; display: inline-block; position: absolute; top: 0; right: 0;" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div class="birthday-img">
						<img style="width: 100%" src="/<?php echo APPFOLDERADD; ?>/libraries/images/happy_birthday.jpg">
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal animated shake" id="confirm-delete" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line('administration_deletealertareyousure'); ?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<span class="btn btn-lightgreen" role="deleteInfo"><?php echo $this->lang->line('general_yes'); ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

