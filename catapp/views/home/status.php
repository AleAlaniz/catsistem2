<script>
function likeToggle(argument) {

	argument.find($('.like > .fa')).click(function() {
		if ($(this).hasClass('like-o')) {
			$(this).removeClass('fa-heart like-o');
			$(this).addClass('fa-heart-o');
			var likeValue = $(this).parent().next().text();
			likeValue--;
			$(this).parent().next().text(likeValue);
		}
		else
		{
			$(this).removeClass('fa-heart-o ');
			$(this).addClass('fa-heart like-o');
			var likeValue = $(this).parent().next().text();
			likeValue++;
			$(this).parent().next().text(likeValue);
		}

		var dataAjax = {
			wallpostId: $(this).parent().parent().attr('wallpostId'),
		};
		$.ajax({
			method: "POST",
			url: '../../home/togglelike',
			data: dataAjax
		}).done(function (data) {
			if(data == 'invalid'){
				$(location).attr('href', '/<?php echo FOLDERADD;?>');
			}
		});


	});

}

function commentsToggle(argument) {

	argument.find($('.btn-comments > .fa')).click(function() {
		$(this).parent().parent().next().fadeToggle();
	});
	argument.find($('.StatusContain > .btn.btn-xs.pull-right.text-info')).click(function(e) {
		var goToF = confirm('<?=$this->lang->line("home_deletepostareyousure")?>');
		if(!goToF){
			e.preventDefault();
		}
	});
}

function deleteComment(argument) {
	argument.find($('.fa-trash')).click(function() {
		var goToF = confirm('<?=$this->lang->line("home_deletecommentareyousure")?>');
		if(!goToF){
			return;
		}
		$(this).removeClass('fa-trash ');
		$(this).addClass('fa-spinner fa-pulse');
		var parentComment = $(this).parent().parent().parent();
		var dataAjax = {
			wallcommentId: parentComment.attr('wallcommentId'),
		};
		$.ajax({
			method: "POST",
			url: '../../home/deletecomment',
			data: dataAjax
		}).done(function (data) {
			if(data == 'invalid'){
				$(location).attr('href', '/<?php echo FOLDERADD;?>');
			}
			else{
				parentComment.fadeOut();
				var commentValue = parentComment.parent().parent().parent().find($('[ariaLabel="commentNumber"]')).text();
				commentValue--;
				parentComment.parent().parent().parent().find($('[ariaLabel="commentNumber"]')).text(commentValue);
				parentComment.remove();
			}
		});
	});
}

function postComment(wallPost) {

	var wallPostForm = wallPost.find($("form"));

	wallPostForm.submit(function (e) {

		e.preventDefault();

		var datosForm = {
			Comment: $(this).find("textarea").val(),
			wallpostId : wallPost.find($('[wallpostId]')).attr('wallpostId')
		};

		$(this).find($('button')).attr('disabled', 'TRUE');
		$(this).find($('textarea')).attr('disabled', 'TRUE');
		$(this).find($('button')).html('<i class="fa fa-spinner fa-pulse fa-lg"></i>');
		$(this).find($('p.text-danger')).slideUp();

		$.ajax({
			method: "POST",
			url: '../../home/createcomment',
			data: datosForm,
		}).done(function (data) {

			if(data == 'invalid'){
				$(location).attr('href', '/<?php echo FOLDERADD;?>');
			}
			else
			{
				wallPostForm.find($('button')).removeAttr('disabled');
				wallPostForm.find($('textarea')).removeAttr('disabled');
				wallPostForm.find($('button')).html('<?=$this->lang->line("home_submitcomment")?> &nbsp;<i class="fa fa-angle-up"></i>');
				wallPostForm.find($('p.text-danger')).slideUp();

				if (data == 'commentEmpty') {
					wallPostForm.find($('p.text-danger')).html('<?=$this->lang->line("home_commentempty")?>');
					wallPostForm.find($('p.text-danger')).slideDown();
				}
				else{
					wallPost.find($('[ariaLabel="allcoments"]')).append(data);
					wallPostForm.find("textarea").val('');

					var commentValue = wallPost.find($('[ariaLabel="commentNumber"]')).text();
					commentValue++;
					wallPost.find($('[ariaLabel="commentNumber"]')).text(commentValue);

				}

			}

		});
	});



}
</script>
<div class="row">
	<div class="col-xs-12">
		<?php
		$this->load->model('Wall_model', 'Wall');
		$this->Wall->GetPost($value);		
		?>	
	</div>
</div>

<script type='text/javascript'>
$('#nav_home').addClass('active');
$('.status').show();
</script>
