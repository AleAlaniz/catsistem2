<div class="page-title">
	<h3 class="title"><?php echo $this->lang->line('general_notifications'); ?></h3>
	<?php if($this->Identity->Validate('notifications/manage')) { ?>
		<div class="btn-group">
			<a aria-expanded="false" class="btn btn-white dropdown-toggle" href="#/notifications/createNotification"><i class="fa fa-plus text-muted"></i> <?php echo $this->lang->line('notifications_create')?></a>
		</div> 
	<?php } ?>
</div>
<div class="col-xs-12 flat-style">
	<!-- aviso de la creacion de la notificacion -->
	<div class="alert bg-green" role="alert" ng-show="message.message != '' && message.message != null">
		<button type="button" class="close"  ng-click="message.setMessage('')"><span>&times;</span></button>
		<strong>
			<i class="fa fa-check"></i> <span ng-bind="message.message"></span>
		</strong> 
	</div>
	<!-- fin de aviso -->
	
	<!-- modal de borrado -->
	<div class="modal animated shake open" tabindex="-1" id="confirm-delete">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p ng-show="deleteType == 'notification'"><?php echo $this->lang->line('notifications_deleteareyousure'); ?></p>
					<p ng-show="deleteType == 'event'"><?php echo $this->lang->line('event_deleteareyousure'); ?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<span class="btn btn-lightgreen" ng-click="toDelete()" ng-show="!deleting"><?php echo $this->lang->line('general_yes'); ?></span>
						<span class="btn btn-lightgreen disabled" ng-show="deleting"><i class="fa fa-refresh fa-spin"></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fin de modal -->
	<div class="panel">
		<div class="panel-body">
			<div role="containt ng-hide" ng-show="!loading">
				<div ng-repeat="notification in notifications" class="panel">
					<div class="panel-heading pointer bg-lightgreen" ng-click="notification.open = !notification.open">
						<span class="pull-right btn btn-xs white m-r_10 btn-noshadow" ng-click="setDelete('notification', notification.notificationId)" data-target="#confirm-delete" data-toggle="modal"title="<?php echo $this->lang->line('notifications_delete');?>"><i class="fa fa-times  fa-lg"></i></span>
						<a href="#/notifications/editNotification/{{notification.notificationId}}" class="pull-right btn btn-xs  m-r_10 white" title ="<?php echo $this->lang->line('notifications_edit');?>"><i class="fa fa-pencil fa-lg"></i></a>
						<a href="#/notifications/createEvent/{{notification.notificationId}}" class="pull-right btn btn-xs  m-r_10 white" title="<?php echo $this->lang->line('events_create');?>"><i class="fa fa-plus fa-lg"></i></a>
						<strong ng-bind="notification.title"></strong>
					</div>
					<!-- los eventos dentro de las notificaciones -->
					<div class="" ng-show="notification.open">
						<div ng-repeat="event in notification.events track by $index" >
							<div ng-click="event.open = !event.open" class="container bg-yellow">
								<h3 ng-bind="event.title" class="pointer text-center text-primary"  ng-click="setSelected(event)" data-target="#event-details" data-toggle="modal" title="<?php echo $this->lang->line('event_show'); ?>"></h3>
								<div class=" well well-sm col-lg-10 col-lg-offset-1" ng-show="event.open">
									<div class="col-md-3"> 
										<p><?php echo $this->lang->line('general_start_date');?>: <b ng-bind="event.startDate"></b></p> 
										<p><?php echo $this->lang->line('general_end_date');?>: <b ng-bind="event.endDate"></b></p>
										<p><b><?php echo $this->lang->line('reminders');?>:</b></p>
										<p ng-repeat="eventReminder in event.eventReminders track by $index" ng-bind="eventReminder"></p> 
									</div>
									<div class="col-md-3 pull-right">
										<span class="btn btn-green m-r_10 btn-noshadow" ng-click="setModal(event)" data-toggle="modal" data-target="#event-report" title="<?php echo $this->lang->line('event_report');?>"><i class="fa fa-eye"></i></span>
										<span class="btn btn-red m-r_10 btn-noshadow" ng-click="setDelete('event', event.eventId)" data-target="#confirm-delete" data-toggle="modal" title="<?php echo $this->lang->line('event_delete');?>">
											<i class="fa fa-trash"></i>
										</span>
										<a href="#/notifications/editEvent/{{event.eventId}}" class="btn btn-ms btn-lightgreen m-r_10 btn-noshadow" title="<?php echo $this->lang->line('event_edit');?>"><i class="fa fa-pencil"></i></a>
									</div> 
									<div class="jumbotron">
										<img class="img-responsive" src="/<?php echo FOLDERADD;?>/catapp/_notifications_images/{{event.image}}" alt="">
									</div>
								</div>
							</div>
						</div>
						<div class="list-group-item text-center" ng-hide="notification.events.length > 0">
							<?php echo $this->lang->line('events_empty'); ?>
						</div>
					</div>
					<!-- fin de los eventos -->
				</div>
				<div class="modal animated" tabindex="-1" id="event-report">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<dl class="dl-horizontal">
									<dt><?php echo $this->lang->line('event_name'); ?></dt>
									<dd ng-bind="event.title"></dd>
								</dl>
									<p class="text-center" ng-show="loadingReport && !event.report" style="margin-top: 10px;"><i class="fa fa-refresh fa-spin fa-3x"></i></p>
									
									<span class="btn btn-block btn-dark"  style="margin-top: 10px;" ng-show="!event.report && !loadingReport" ng-click="getReport(event,'viewed')">
										<?php echo $this->lang->line('events_report'); ?>
									</span>

								<div ng-if="event.report != null" style="margin-top: 15px">
								
									<ul class="nav nav-tabs">
										<li role="presentation" ng-class="{active : reportOption == 'viewed'}"><a href="" ng-click="getReport(event,'viewed')">Leyeron el documento</a></li>
										<li role="presentation" ng-class="{active : reportOption == 'no_viewed'}"><a href="" ng-click="getReport(event,'no_viewed')">No leyeron el documento</a></li>
									</ul>

									<p class="text-center" ng-show="loadingReport" style="margin-top: 10px;"><i class="fa fa-refresh fa-spin fa-3x"></i></p>
									<div ng-show="!loadingReport" style="margin-top:15px">
										<strong>
											<?php echo $this->lang->line('events_total'); ?> 
											<span ng-bind="event.report.length"></span>
										</strong>
										<table class="table">
											<thead>
												<tr>
													<th><?php echo $this->lang->line('general_user'); ?> </th>
													<th><?php echo $this->lang->line('general_section'); ?> </th>
													<th><?php echo $this->lang->line('users_turn'); ?></th>
												</tr>
											</thead>
											<tbody>

												<tr ng-repeat="user in event.report track by $index">
													<td ng-bind="user.name + ' ' + user.lastName"></td>
													<td ng-bind="user.section"></td>
													<td ng-bind="user.turn"></td>
												</tr>
											</tbody>
										</table>
										<div class="alert bg-dark" ng-if="event.report.length == 0" role="alert" style="margin: 0 15px">
											<i class="fa fa-info-circle fa-lg"></i>
											<?php echo $this->lang->line('events_users_empty');?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="alert bg-dark ng-hide" role="alert" ng-hide="notifications.length > 0" style="margin: 0 15px">
					<i class="fa fa-info-circle fa-lg"></i>
					<?php echo $this->lang->line('notifications_empty');?>
				</div>
			</div>
			<div class="text-center" ng-if="loading">
				<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
			</div>
		</div>
	</div>
</div>