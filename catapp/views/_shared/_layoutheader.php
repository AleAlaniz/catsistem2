<!DOCTYPE HTML>
<html>
<head>
	<script src="/<?=APPFOLDERADD?>/libraries/script/excanvas.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="/<?php echo APPFOLDERADD;?>/libraries/images/favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/style.min.css"">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/jquery-ui.min.css">
	<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD;?>/libraries/css/animate.min.css">
	<title>CAT - Technologies</title>
</head>
<body>
	<?php

	$query = $this->Layout->GetUser();
	$sectionsQuery = $this->Layout->GetSections();
	$noReads = array(
		'chat'			=> $this->Layout->GetChatNumber(),
		'press'			=> $this->Layout->GetPressNotes(),
		'newsletter'	=> $this->Layout->GetNewsletter(),
		'suggestbox'	=> $this->Layout->GetSuggestBox()
	);
	$background 	= ($query->background == NULL) ? '' : 'style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'/'.APPFOLDERADD.'/_users_backgrounds/'.$query->background.'\',sizingMethod=\'scale\');background-image:url(/'.APPFOLDERADD.'/_users_backgrounds/'.$query->background.')"';
	?>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/jquery-1.11.3.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/bootstrap.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/jquery-ui.min.js"></script>
	<script src="/<?php echo APPFOLDERADD;?>/libraries/script/angular.min.js"></script>


	<nav class="navbar navbar-default contain-fixed navbar-fixed-top" style="z-index:1000" id="siteNavBar">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/<?=FOLDERADD;?>">
					<img alt="cat" src="/<?=APPFOLDERADD;?>/libraries/images/logo-mini.png">
				</a>

			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<div class="profileImage profileImage25" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId=<?php echo $this->session->UserId;?>&wah=200',sizingMethod='scale');background-image:url(/<?=FOLDERADD?>/users/profilephoto?userId=<?=$this->session->UserId?>&wah=200)"></div>
							<?=encodeQuery($query->name)?> <?=encodeQuery($query->lastName)?> 
							<span class="caret"></span></span>
						</a>
						<ul class="dropdown-menu">
							<?php
							if ($this->Identity->Validate('users/config')){
								?>
								<li>
									<a href="/<?=FOLDERADD?>/users/config">
										<i class="fa fa-user"></i> &nbsp;&nbsp;<?=$this->lang->line('general_miprofile');?>
									</a>
								</li>
								<?php 
							}
							?>
							<li role="separator" class="divider"></li>
							<li><a href="/<?=FOLDERADD?>/home/logout"><i class="fa fa-sign-out"></i> &nbsp;&nbsp;Salir</a></li>
						</ul>
					</li>
					<?php
					if ($this->Identity->Validate('_layoutheader/diary')){
						?>
						<li><a href="#"><i class="fa fa-calendar"></i></a></li>
						<?php 
					}
					?>

					<?php if ($this->Identity->Validate('administration/index')){ ?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-cog"></i> <span class="visible-xs-inline"><?=$this->lang->line('general_administration');?></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="/<?=FOLDERADD?>/administration"><i class="fa fa-lock"></i> &nbsp;&nbsp;&nbsp;<?=$this->lang->line('general_administration');?></a></li>
							<?php if ($this->Identity->Validate('theme/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/themes"><i class="fa fa-image"></i> &nbsp;&nbsp;<?=$this->lang->line('general_themes');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('users/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/users"><i class="fa fa-users"></i> &nbsp;&nbsp;<?=$this->lang->line('general_users');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('usergroups/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/usergroups"><i class="fa fa-slideshare"></i> &nbsp;&nbsp;<?=$this->lang->line('administration_usergroups');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('roles/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/roles"><i class="fa fa-user-plus"></i> &nbsp;&nbsp;<?=$this->lang->line('general_roles');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('sections/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/sections"><i class="fa fa-list-alt"></i> &nbsp;&nbsp;<?=$this->lang->line('general_sections');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('sites/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/sites"><i class="fa fa-building"></i> &nbsp;&nbsp;<?=$this->lang->line('general_sites');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('subsections/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/subsections"><i class="fa fa-sitemap"></i> &nbsp;&nbsp;<?=$this->lang->line('general_subsections');?></a></li>
							<?php } ?>
							<?php if ($this->Identity->Validate('items/index')){ ?>
							<li><a href="/<?=FOLDERADD?>/items"><i class="fa fa-circle"></i> &nbsp;&nbsp;<?=$this->lang->line('general_items');?></a></li>
							<?php } ?>
						</ul>
					</li>
					<?php } ?>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2 col-sm-2 fixed">
				<ul class="nav nav-pills nav-stacked" ng-controller="Cnav">
					<?php
					if (($this->uri->segment(1) == 'users' && $this->uri->segment(2) == 'config') || (isset($security_questions) && $security_questions)) {
						if ($this->Identity->Validate('home/index')){
							?>
							<li role="presentation">
								<a href="/<?=FOLDERADD?>">
									<i class="fa fa-home"></i><span class="hidden-xs"><?=$this->lang->line('administration_comeback');?></span>
								</a>
							</li>
							<?php 
						}
						if ($this->Identity->Validate('users/config')){
							?>
							<li role="presentation" id="nav_config">
								<a href="/<?=FOLDERADD?>/users/config">
									<i class="fa fa-cog"></i>
									<span class="hidden-xs"><?=$this->lang->line('general_config');?></span>
								</a>
							</li>
							<?php 
						}
						if ($this->Identity->Validate('users/config/data')){
							?>
							<li role="presentation" id="nav_configData">
								<a href="/<?=FOLDERADD?>/users/config?cmd=data">
									<i class="fa fa-pencil "></i>
									<span class="hidden-xs"><?=$this->lang->line('administration_users_config');?></span>
								</a>
							</li>
							<?php 
						}
						if ($this->Identity->Validate('users/config/photo')){
							?>
							<li role="presentation" id="nav_configPhoto">
								<a href="/<?=FOLDERADD?>/users/config?cmd=photo">
									<i class="fa fa-picture-o"></i>
									<span class="hidden-xs"><?=$this->lang->line('administration_users_configphoto');?></span>
								</a>
							</li>
							<?php 
						}
						if ($this->Identity->Validate('users/config/password')){
							?>
							<li role="presentation" id="nav_configPassword">
								<a href="/<?=FOLDERADD?>/users/config?cmd=password">
									<i class="fa fa-lock"></i>
									<span class="hidden-xs"><?=$this->lang->line('administration_users_configpassword');?></span>
								</a>
							</li>
							<?php 
						}
						?>
						<?php

						if ($this->Identity->Validate('users/config/select_photo')){
							?>
							<li role="presentation" id="nav_selectPhoto">
								<a href="/<?=FOLDERADD?>/users/config?cmd=selectphoto">
									<i class="fa fa-picture-o"></i>
									<span class="hidden-xs"><?=$this->lang->line('administration_select_photo');?></span>
								</a>
							</li>
							<?php 
						}
						if ($this->Identity->Validate('users/config/background')){
							?>
							<li role="presentation" id="nav_selectbackground">
								<a href="/<?=FOLDERADD?>/users/config?cmd=background">
									<i class="fa fa-desktop"></i>
									<span class="hidden-xs"><?=$this->lang->line('administration_users_selectbackground');?></span>
								</a>
							</li>
							<?php 
						}
						if ($this->Identity->Validate('users/security_questions')){
							?>
							<li role="presentation" id="nav_security_questions">
								<a href="/<?=FOLDERADD?>/securityquestions/my_security_questions">
									<i class="fa fa-shield"></i>
									<span class="hidden-xs"><?=$this->lang->line('question_security_questions');?></span>
								</a>
							</li>
							<?php
						}
					}
					else
					{
						?>
						<?php if ($this->Identity->Validate('home/index')){ ?>
						<li role="presentation" id="nav_home"><a href="/<?=FOLDERADD?>/"><i class="fa fa-home"></i><span class="hidden-xs"><?=$this->lang->line('general_home');?></span></a></li>
						<hr>
						<?php } ?>
						<?php if ($this->Identity->Validate('home/wecat')){ ?>
						<li role="presentation"><a class="specialhover" href="/<?=FOLDERADD?>#/home/wecat" id="nav_wecat"><i class="fa" style="text-align:left"><img class="img-wecat" src="/<?php echo APPFOLDERADD; ?>/libraries/images/we-cat.png"></i><span class="hidden-xs">&nbsp;<?=$this->lang->line('general_wecat');?></span></a></li>
						<?php } ?>
						<?php if ($this->Identity->Validate('home/wellnes')){ ?>
						<li role="presentation"><a class="specialhover" href="/<?=FOLDERADD?>/home/wellnes" id="nav_wellnes"><i class="fa fa-child"></i><span class="hidden-xs"><?=$this->lang->line('general_wellnes');?></span></a></li>
						<?php } ?>
						<?php if ($this->Identity->Validate('home/inspired')){ ?>
						<li role="presentation"><a class="specialhover" href="/<?=FOLDERADD?>/home/inspired" id="nav_inspired"><i class="fa fa-lightbulb-o"></i><span class="hidden-xs"><?=$this->lang->line('general_inspired');?></span></a></li>
						<?php } ?>
						<?php if ($this->Identity->Validate('home/catinaction')){ ?>
						<li role="presentation"><a class="specialhover" href="/<?=FOLDERADD?>/home/catinaction" id="nav_catinaction"><i class="fa fa-hand-o-right"></i><span class="hidden-xs"><?=$this->lang->line('general_catinaction');?></span></a></li>
						<?php } ?>
						<?php if ($this->Identity->Validate('home/tryout')){ ?>
						<li role="presentation"><a class="specialhover" href="/<?=FOLDERADD?>/home/tryout" id="nav_tryout"><i class="fa fa-bullhorn"></i><span class="hidden-xs"><?=$this->lang->line('general_tryout');?></span></a></li>
						<?php } ?>
						<hr>
						<?php if ($this->Identity->Validate('newsletter/index')){ ?>
						<li role="presentation" id="nav_newsletter"><a href="/<?=FOLDERADD?>#/newsletter"><i class="fa fa-newspaper-o"><?=$noReads['newsletter']; ?></i><span class="hidden-xs"><?=$this->lang->line('newsletter_title');?></span></a></li>
						<?php } ?>

						<?php if ($this->Identity->Validate('suggestbox/index')){ ?>
						<li role="presentation" id="nav_suggestbox"><a href="/<?=FOLDERADD?>#/suggestbox"><i class="fa fa-archive"><?=$noReads['suggestbox']; ?></i><span class="hidden-xs"><?=$this->lang->line('suggestbox_title');?></span></a></li>
						<?php } ?>

						<?php if ($this->Identity->Validate('press/index')){ ?>
						<li role="presentation" id="nav_press"><a href="/<?=FOLDERADD?>#/press"><i class="fa fa-camera"><?=$noReads['press']; ?></i><span class="hidden-xs"><?=$this->lang->line('press_title');?></span></a></li>
						<?php } ?>


						<?php if ($this->Identity->Validate('chat/index')){ ?>
						<li role="presentation" id="nav_chat"><a href="/<?=FOLDERADD?>#/chat/unique"><i class="fa fa-comment-o"><?=$noReads['chat']?></i><span class="hidden-xs"><?=$this->lang->line('chat_title');?></span></a></li>
						<?php } ?>

						<?php if ($this->Identity->Validate('sections/section')){ ?>
						<?php 
						foreach ($sectionsQuery as $section){ 
							?>
							<li role="presentation" id="sectionId-<?=$section->sectionId?>">
								<a href="/<?=FOLDERADD?>/sections/section/<?=$section->sectionId?>">
									<?php
									if ($section->noViews) {
										?>
										<i class="fa fa-circle noreadDot"></i>
										<?php
									}
									?>
									<i class="fa <?=encodeQuery($section->icon)?>"></i>
									<span class="hidden-xs"><?=encodeQuery($section->name)?></span>
								</a>
							</li>
							<?php 
						}
						?>
						<?php if ($this->Identity->Validate('tools/index'))
						{ 
							?>
							<li role="presentation" id="nav_tools"><a href="/<?=FOLDERADD?>#/tools"><i class="fa fa-wrench"></i><span class="hidden-xs"><?=$this->lang->line('tools_title');?></span></a></li>
							<?php 
						} 
						if ($this->Identity->Validate('surveys/manage'))
						{ 
							?>
							<li role="presentation" id="nav_surveys"><a href="/<?=FOLDERADD?>/surveys/manage"><i class="fa fa-pie-chart "></i><span class="hidden-xs"><?=$this->lang->line('survey_surveys');?></span></a></li>
							<?php 
						} 
						?>
						<?php 
					} 
					?>
					<?php
				}
				?>
			</ul>
		</div>
		<span id="memusages"><?php /*echo round(memory_get_usage()/1024, 2);*/ ?></span>
		<div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-1 col-xs-11 col-md-10 col-sm-10 scrollit" <?php echo $background; ?> >
			<div ng-view>

