<div class="page-title">
	<h3 class="title"><?php echo $this->lang->line('survey_details'); ?></h3>
	<a href="#/surveys" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">
	<div class="panel" role="loading" ng-show="loading">
		<div class="panel-body text-center">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark' ></i>
		</div>
	</div>
	<div class="panel ng-hide" ng-show="survey && !loading">
		<div class="panel-custom-heading" >
			<h4  class="f-w-500" ng-bind="survey.name"></h4>
		</div>
		<div class="panel-body">
			<div class="details-list">
				<div class="details-item" ng-show="survey.description">
					<div class="details-label">
						<?php echo $this->lang->line('survey_description'); ?>
					</div>	
					<div class="details-detail" ng-bind="survey.description"></div>
				</div> 
			</div>
			<h5 ng-show="survey.correctresponses.count > 0" class="text-warning"><?php echo $this->lang->line('survey_youcorrectresponses'); ?> <span ng-bind="survey.correctresponses.count"></span></h5>
			<h4><?php echo $this->lang->line('survey_questions'); ?></h4>
			<div class="panel panel-default border-dark" ng-repeat="question in survey.questions">
				<div class="panel-custom-heading ">
					<span class="badge pull-right bg-black" ng-bind="$index+1"></span>
					<span ng-bind-html="question.question | HtmlSanitize"></span>
				</div>
				<div class="panel-body">
					<p ng-show="question.description" ng-bind="question.description"></p>
					<p style="margin-bottom:0px">
						<?php echo $this->lang->line('survey_question_type'); ?>:
						<i class="strong" ng-if="question.type == 1"> <?php echo $this->lang->line('survey_question_optionunique'); ?></i>
						<i class="strong" ng-if="question.type == 2"> <?php echo $this->lang->line('survey_question_severaloptions'); ?></i>
						<i class="strong" ng-if="question.type == 3"> <?php echo $this->lang->line('survey_question_input'); ?></i>
						<i class="strong" ng-if="question.type == 4"> <?php echo $this->lang->line('survey_question_trueorfalse'); ?></i>
					</p>
					<div ng-if="question.type == 1 || question.type == 2">
						<h4><?php echo $this->lang->line('survey_options');?></h4>
						<div>
							<table class="table">
								<tbody>
									<tr ng-repeat="answer in question.answers">
										<td class="text-center" style="width: 50%" ng-bind="answer.answer"></td>
										<td class="text-center strong" style="width: 50%">
											<i class="fa fa-check" ng-show="answer.isTrue == 'TRUE'"></i>
											<i class="fa fa-times" ng-hide="answer.isTrue == 'TRUE'"></i>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<p style="margin-bottom:0px" ng-if="question.type == 4">
						<?php echo $this->lang->line('survey_answer_correct'); ?>:
						<i class="strong" ng-if="question.isTrue == 'TRUE'"> <?php echo $this->lang->line('general_true'); ?></i>
						<i class="strong" ng-if="question.isTrue != 'TRUE'"> <?php echo $this->lang->line('general_false'); ?></i>
					</p>
					<h4><?php echo $this->lang->line('survey_myanswer');?></h4>

					<p style="margin-bottom:0px" class="text-center" ng-if="question.type == 4">
						<i class="strong" ng-if="question.answer.answer == 'TRUE'"> <?php echo $this->lang->line('general_true'); ?></i>
						<i class="strong" ng-if="question.answer.answer == 'FALSE'"> <?php echo $this->lang->line('general_false'); ?></i>
					</p>

					<p style="margin-bottom:0px" class="strong text-center" ng-if="question.type == 3" ng-bind="question.answer.answer"></p>

					<div ng-if="question.type == 1 || question.type == 2">
						<div>
							<table class="table">
								<tbody>
									<tr ng-repeat="answer in question.answers" ng-if="answer.response">
										<td class="text-center strong" style="width: 50%" ng-bind="answer.answer"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>


				</div>
			</div>
			
		</div>
	</div>
</div>
