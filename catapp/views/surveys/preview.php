<div class="page-title">
	<h3 class="title"><?php echo $this->lang->line('survey_details'); ?></h3>
	<a href="#/surveys" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">
	<div class="panel" role="loading" ng-show="loading">
		<div class="panel-body text-center">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark' ></i>
		</div>
	</div>
	<div class="panel ng-hide" ng-show="survey && !loading" ng-init="trueLang = '<?php echo $this->lang->line('general_true'); ?>'; falseLang = '<?php echo $this->lang->line('general_false'); ?>'">
		<div class="panel-custom-heading" >
			<h4  class="f-w-500">
				{{survey.name}}
			</h4>
		</div>
		<div class="panel-body">
			<div class="details-list">

				<div ng-show="survey.description" class="details-item">
					<div class="details-label">
						<?php echo $this->lang->line('survey_description'); ?>
					</div>	
					<div class="details-detail">
						{{survey.description}}
					</div>
				</div> 

				<div ng-show="survey.startDate" class="details-item">
					<div class="details-label">
						<?php echo $this->lang->line('survey_start_date'); ?>
					</div>	
					<div class="details-detail">
						{{survey.startDate}}
					</div>
				</div> 
				<div ng-show="survey.endDate"  class="details-item">

					<div class="details-label">
						<?php echo $this->lang->line('survey_end_date'); ?>
					</div>	
					<div class="details-detail">
						{{survey.endDate}}
					</div>
				</div> 
				<div ng-show="survey.closeMessage" class="details-item">

					<div class="details-label">
						<?php echo $this->lang->line('survey_closemessage'); ?>
					</div>	
					<div class="details-detail">
						{{survey.closeMessage}}
					</div>
				</div> 

				<div class="details-item">

					<div class="details-label">
						<?php echo $this->lang->line('survey_whose'); ?>
					</div>	
					<div class="details-detail">

						<div class="details-list row">
							<div  ng-show="survey.sections.length > 0" class="details-item col-sm-6" style="border-top: 0;">
								<div class="details-label strong">
									<?php echo $this->lang->line('general_sections'); ?>
								</div>	
								<div ng-repeat="s in survey.sections" class="details-detail">
									{{s.name}}
								</div>
							</div>

							<div  ng-show="survey.sites.length > 0" class="details-item col-sm-6" style="border-top: 0;">
								<div class="details-label strong">
									<?php echo $this->lang->line('general_sites'); ?>
								</div>	
								
								<div ng-repeat="s in survey.sites" class="details-detail">
									{{s.name}}
								</div>

							</div>

							<div ng-show="survey.roles.length > 0" class="details-item col-sm-6" style="border-top: 0;">
								<div class="details-label strong">
									<?php echo $this->lang->line('general_roles'); ?>
								</div>	

								<div ng-repeat="r in survey.roles" class="details-detail">
									{{r.name}}
								</div>

							</div>

							<div ng-show="survey.users.length > 0" class="details-item col-sm-6" style="border-top: 0;">
								<div class="details-label strong">
									<?php echo $this->lang->line('general_users'); ?>
								</div>	

								<div  ng-repeat="u in survey.users"  class="details-detail">
									{{u.completeName}}
								</div>
							</div>
						</div>
					</div>
				</div> 

				<h4><?php echo $this->lang->line('survey_questions'); ?></h4>
			</div>
			<div ng-repeat="q in survey.questions" class="panel panel-default">
				<div class="panel-custom-heading ">
					<h4>
						<div class="" ng-bind-html="q.question | HtmlSanitize"></div>
						<span class="badge pull-right bg-black">{{$index+1}}</span>
					</h4>
				</div>
				<div class="panel-body">
					<p ng-show="q.description">{{q.description}}</p>
					<p style='margin-bottom:0px'>
						<?=$this->lang->line('survey_question_type').':' ?>
						
						<span ng-switch="q.type">
							<i class="strong" ng-switch-when="1"><?=$this->lang->line('survey_question_optionunique')?></i>
							<i class="strong" ng-switch-when="2"><?=$this->lang->line('survey_question_severaloptions')?></i>
							<i class="strong" ng-switch-when="3"><?=$this->lang->line('survey_question_input')?></i>
							<i class="strong" ng-switch-when="4"><?=$this->lang->line('survey_question_trueorfalse')?></i>
						</span>
						<div>
							<?=$this->lang->line('survey_required_question').':' ?>

							<span ng-switch="q.required">
								<i class="strong" ng-switch-when="TRUE"><?=$this->lang->line('general_yes')?></i>
								<i class="strong" ng-switch-when="FALSE"><?=$this->lang->line('general_no')?></i>
							</span>
						</div>
					</p>
					<div ng-if="q.type == 1 ||  q.type == 2">
						<h4><?=$this->lang->line('survey_options')?></h4>
						<table class="table">
							<tbody>
								<tr ng-repeat="a in q.answers">
									<td class="text-center" style="width: 50%">{{a.answer}}</td>
									<td class="text-center strong" style="width: 50%"> 
										<span ng-if="a.isTrue =='TRUE'"><i  class='fa fa-check'></i> <?=$this->lang->line('survey_answer_correct')?></span>
										<span ng-if="a.isTrue =='FALSE'"><i  class='fa fa-times'></i> <?=$this->lang->line('survey_answer_incorrect')?></span>
									</td>
								</tr>
								<?php
										/*echo "<p style='margin-bottom:0px'>".encodeQuery($answer->answer).": <i class='strong'>";
										if ($answer->isTrue == 'TRUE') {
											echo $this->lang->line('survey_answer_correct');
										}
										else
										{
											echo $this->lang->line('survey_answer_incorrect');
										}
										echo "</i></p>";
										*/
										?>
									</tbody>
								</table>
							</div>

							<div ng-if="q.type==4">
								<p style='margin-bottom:0px'><?=$this->lang->line('survey_answer_correct').':' ?>

									<span ng-switch="q.isTrue">
										<i class="strong" ng-switch-when="TRUE"></i><?=$this->lang->line('general_true')?>
										<i class="strong" ng-switch-default></i><?=$this->lang->line('general_false')?>
									</span>
								</p>
							</div>
						</div>

					</div>

				</div>
			</div>

			<script type="text/javascript">
				$(document).ready(function() {
					$('#nav_surveys').addClass('active');
				});
			</script>