<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/animate.min.css" rel="stylesheet">

<div style="margin-top: 20px;" ng-app="surveys" ng-controller="form">
	<?php
	if ($required == 'TRUE' && !$isManual) {
		?>
		<div class="modal fade" id="message" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm ">
				<div class="modal-content">
					<div class="modal-body">
						<p><?php echo $this->lang->line('survey_complete_message')?></p>
						<div class="text-right">
							<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	?>
	<?php
		$flash_message = $this->session->flashdata('flash_message');
		if(in_array($flash_message,array('notUser','userInCompleteSurvey','surveyCompleted')))
		{
			$color = 'red';
			$icon = 'fa-exclamation-triangle';
			switch ($flash_message) {
				case 'notUser':
					$text = $this->lang->line('notUser');
					break;
				
				case 'userInCompleteSurvey':
					$text = $this->lang->line('userInCompleteSurvey');
					break;

				case 'surveyCompleted':
					$color = 'green';
					$icon = 'fa-check';
					$text = $this->lang->line('surveyCompleted');
					break;
			}
			echo('<div class="alert bg-'.$color.' alert-dismissible" role="alert" ng-show="!message_notUser">
					<button type="button" class="close" ng-click="message_notUser = true"><span aria-hidden="true">&times;</span></button>
					<strong><i class="fa '.$icon.'"></i></strong> <span>'.$text.'</span>
				 </div>');
			}?>

	<form method="POST" class="form-horizontal" action="#" id="form-survey">
		<input type="hidden" name="survey" value="<?php echo $surveyId; ?>" />

		<?php if(isset($isManual) && $isManual)
		{?>
		<input type="hidden" id="selectedUser" name="user_id" ng-value="users[0].userId" />

		<?php
		}
		else
		{ ?>

		<input type="hidden" id="selectedUser" name="user_id" value="<?php echo $this->session->UserId ?>" />
		<?php 
		} ?>
		
		<div class="panel-body">
			<div class="jumbotron bg-white">
				<h2><?php echo encodeQuery($name); ?></h2>
				<hr/>
				<p><?php echo encodeQuery($description); ?></p>

			</div>

			<?php
			if (isset($isManual) && $isManual) {
				?>
				<div class="jumbotron bg-white">
					<div>
					<div class="container-fluid"><?php echo $this->lang->line('survey_insert_user');?>
					</div>
					<br>
					<div class="container-fluid">
						<div class="select-container container" ng-click="selectClick($event);">
							<i class="fa fa-spin fa-refresh pull-right " style="position:absolute; top:13px;right:25px" ng-show="finding"></i>
							<span class="select-selected">
								<span class="select-selected-item" ng-repeat="user in users track by $index" ng-init="i=$index">
									<span cn-square-image cls="selected-item-image" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200"></span>
									<span class="selected-item-label" ng-bind="user.completeName+' <'+user.userName+'>'" ></span>
									<span class="selected-item-delete no-selectable" ng-click="removeUser(i)">&times;</span>
								</span>
							</span>
							<input type="text" autocomplete="off" class="input-sm form-control select-input" ng-trim="true" ng-model="findLike" ng-change="searchUsers('<?php echo $surveyId; ?>')" id="findInput" 	placeholder="<?php echo $this->lang->line('general_find');?>"/>
						</div>
						<div class="select-options-container-super" ng-show="userOptions.length > 0">
							<div class="select-options-container">
								<div class="select-option no-selectable" ng-repeat="user in userOptions track by $index" ng-init="i=$index" ng-click="addUser(i)">
									<span cn-square-image cls="selected-option-image" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200"></span>
									<span class="selected-option-label" ng-bind="user.completeName+' <'+user.userName+'>'"></span>
								</div>
							</div>
						</div>
					</div>
					</div>
					<br>
					<div class="container-fluid alert alert-danger"><?php echo $this->lang->line('survey_insert_alert');?>
					</div>
				</div>
				
				<?php
			}
			?>

			<?php
			foreach ($questions as $key => $question) 
			{
				?>
				<div class="panel panel-default" <?php if ($question->type != 3 ){ echo 'role="question"';} if ($question->required == 'TRUE' ){ echo 'required-q="TRUE"';} ?> >
					<div class="panel-custom-heading bg-white" style="padding-top: 20px;padding-bottom: 20px;" >

						<?php echo $question->question; ?>

					</div>
					<div class="panel-body" >
						<?php
						if ($question->description) {
							echo "<p>".encodeQuery($question->description)."</p>";
						}

						if ($question->type == 1) {
							foreach ($question->answers as $keyA => $answer) {

								?>
								<div class="radio radio-angular">
									<label ng-class="{check: questions[<?php echo $question->surveyquestionId;?>] == <?php echo $answer->surveyanswerId; ?>}">
										<input type="radio" name="questions[<?php echo $question->surveyquestionId;?>]" value="<?php echo $answer->surveyanswerId; ?>" ng-model="questions[<?php echo $question->surveyquestionId;?>]">
										<?=encodeQuery($answer->answer)?>
									</label>
								</div>
								<?php
							}
						}
						elseif ($question->type == 2) {
							foreach ($question->answers as $keyA => $answer) {
								?>
								<div class="checkbox checkbox-angular">
									<label ng-class="{check: answers[<?php echo $key;?>][<?php echo $keyA;?>]}">
										<input type="checkbox" name="questions[<?php echo $question->surveyquestionId; ?>][]" value="<?=$answer->surveyanswerId?>"  ng-model="answers[<?php echo $key;?>][<?php echo $keyA;?>]" >
										<?=encodeQuery($answer->answer)?>
									</label>
								</div>
								<?php
							}
						}
						elseif ($question->type == 3) {
							?>
							<input type="text" class="form-control input-sm" name="questions[<?php echo $question->surveyquestionId?>]" placeholder="<?php echo $this->lang->line('survey_answer'); ?>">
							<?php
						}
						elseif ($question->type == 4) {
							?>
							<div class="radio radio-angular">
								<label ng-class="{check: questions[<?php echo $key;?>] == 'TRUE'}">
									<input type="radio" name="questions[<?php echo $question->surveyquestionId?>]" ng-model="questions[<?php echo $key;?>]" value="TRUE">
									<?=$this->lang->line('general_true')?>
								</label>
							</div>
							<div class="radio radio-angular">
								<label ng-class="{check: questions[<?php echo $key;?>] == 'FALSE'}">
									<input type="radio" name="questions[<?php echo $question->surveyquestionId?>]" ng-model="questions[<?php echo $key;?>]" value="FALSE">
									<?=$this->lang->line('general_false')?>
								</label>
							</div>
							<?php
						}
						?>
					</div>
				</div>
				<?php
			}
			?>

			<div class="panel text-center">
				<?php
				if ($closeMessage != NULL) {
					?> 
					<div class="panel-custom-heading bg-black f-w-500" style="padding-top: 20px;padding-bottom: 20px;">

						<?php echo encodeQuery($closeMessage); ?>
					</div>
					<?php
				}
				?>
				<div class="panel-body ">

					<p>
						<?php
						if($active == 'TRUE')
							{?>	
						<button onclick= "if(document.getElementById('selectedUser').value.length == 0) event.preventDefault();" type="submit" class="btn btn-red btn-lg" ><?=$this->lang->line('survey_ok');?></button>
						<?php }else {?>
						<a href="<?=APPFOLDERADD?>/#/surveys" class="btn btn-green btn-lg"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
						<?php }?>
					</p>
				</div>
			</div>
		</div>
	</form>
	<div class="modal animated shake" id="no-complete" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line('survey_answer_all')?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/angular.min.js"></script>
<script type="text/javascript">

	if(typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, ''); 
		}
	}

	angular
	.module("surveys", [])
	.controller("form", ["$scope","$http",function($scope, $http) {

		$scope.userOptions = [];
		$scope.findLike = '';
		$scope.searchNumber = 0;
		$scope.searchActual = 0;
		$scope.users = [];

		var url="/<?php echo FOLDERADD ?>/";

		$scope.checkUser=function(event)
		{
			console.log($scope.users[0].length);
			if($scope.users[0].length == 0)
				event.preventDefault();
		}

		$("#form-survey").submit(function (e) {

			var /*questions = $('input[name*="question"]'),*/
			incomplete = false;

			var questionsType3 = $('[required-q="TRUE"] input[type=text][name*="questions"]');
			jQuery.each(questionsType3, function(i, question) {
				question = $(question);
				if (question.val().trim() == "")
				{
					incomplete=true;
				}
			});

			var questionsContainers = $('[role=question][required-q="TRUE"]');
			jQuery.each(questionsContainers, function(i, questionsContainer) {
				questionsContainer = $(questionsContainer);
				if ($('input:checked', questionsContainer).length <= 0)
				{
					incomplete=true;
				}
			});


			if(incomplete)
			{
				e.preventDefault();
				$('#no-complete').modal('show');
			}
		});

		$scope.selectClick = function (e) {
			$('.select-input', e.currentTarget).focus();
		};

		$scope.addUser = function(i) {
				$scope.users = [];
				$scope.users.push($scope.userOptions[i]);
				$scope.userOptions = [];
				$scope.findLike = '';
				
			};

			$scope.removeUser = function(i) {
				$scope.users.splice(i, 1);
			};

			$scope.searchUsers = function(surveyid) {
				if($scope.findLike != ''){

					$scope.finding = true;
					var thisSearch = $scope.searchNumber;
					$scope.searchNumber = $scope.searchNumber+1;
					$.ajax({
						method 	: "POST",
						url		: url+'surveys/getUsers',
						data 	: {'like': $scope.findLike,'surveyId': surveyid},
						dataType : 'json'
					}).done(function(data) {
						if(data.status == 'ok' && thisSearch >= $scope.searchActual && $scope.findLike != ''){

							$scope.searchActual = thisSearch;
							$scope.$apply(function() {
								$scope.userOptions = data.message;
								$scope.finding = false;
							});

						}
						else if (thisSearch >= $scope.searchActual)
						{
							$scope.searchActual = thisSearch;
							$scope.$apply(function() {
								$scope.finding = false;
								$scope.userOptions = [];
							});
						}
					});
				}
				else
				{
					$scope.userOptions = [];
					$scope.finding = false;
				}
			};


	}]);

	$(document).ready(function() {
		$('#message').modal('show');
	});
</script>
