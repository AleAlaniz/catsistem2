<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('general_sites');?></li>
</ol>

<?php if($this->Identity->Validate('sites/create')) { ?>
<p>
	<a href="/<?=FOLDERADD?>/sites/create" class="btn btn-sm btn-success "><i class="fa fa-plus"></i><strong> <?=$this->lang->line('administration_create');?></strong></a>
</p>
<?php }
if (isset($_SESSION['flashMessage'])){ ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['flashMessage'] == 'create'){
		echo $this->lang->line('sites_successmessage');
	}
	elseif ($_SESSION['flashMessage'] == 'delete'){
		echo $this->lang->line('sites_deletemessage');
	}
	?>
</div>
<?php } ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('general_sites');?></strong>
		<span class="badge pull-right bg-success"><?=count($model)?></span>
	</div>
	<table class="table table-hover">
		<thead>
			<tr class="active">
				<th><?=$this->lang->line('sites_name');?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($model) > 0) {?>

			<?php foreach ($model as $site){ ?>
			<tr class="optionsUser">
				<td><?=encodeQuery($site->name)?></td>
				<td>
					<?php if($this->Identity->Validate('sites/details')) { ?>
					<a href="/<?=FOLDERADD?>/sites/details/<?=$site->siteId?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>&nbsp; 
					<?php } ?>
					<?php if($this->Identity->Validate('sites/edit')) { ?>
					<a href="/<?=FOLDERADD?>/sites/edit/<?=$site->siteId?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp; 
					<?php } ?>
					<?php if($this->Identity->Validate('sites/delete')) { ?>
					<a href="/<?=FOLDERADD?>/sites/delete/<?=$site->siteId?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
					<?php } ?>
				</td>
			</tr>

			<?php } ?>
			<?php } 
			else { 
				?>
				<tr class="text-center">
					<td colspan="3"><i class="fa fa-building"></i> <?=$this->lang->line('sites_empty');?></td>
				</tr>
				<?php
			}?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
$('#nav_sites').addClass('active');
$('.optionsUser').hover(function(){
	$(this).find('.glyphicon').css('visibility','visible');

},function(){
	$(this).find('.glyphicon').css('visibility','hidden');

});
</script>