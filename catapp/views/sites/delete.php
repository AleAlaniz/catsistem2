<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/sites"><?=$this->lang->line('general_sites');?></a></li>
	<li><a href="/<?=FOLDERADD?>/sites/details/<?=$siteId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_delete');?></li>
</ol>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('sites_delete');?></strong>
	</div>
	<div class="panel-body">
		<h4><?=$this->lang->line('sites_deleteareyousure');?></h4>
		<hr>
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('sites_name');?></dt>
			<dd><?=encodeQuery($name)?></dd>
		</dl>
		<form method="POST"  >
			<input type="hidden" name="siteId" value="<?=$siteId?>">
			<div class="form-group text-center col-xs-12">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_delete');?></button>
				<a href="/<?=FOLDERADD?>/sites" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
</div>
<script type="text/javascript">
$('#nav_sites').addClass('active');
$('#NavDelete').addClass('active');
</script>