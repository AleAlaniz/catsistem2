<div class="page-title">
    <h3 class="title"><?php echo $this->lang->line('tools_faq'); ?></h3>
    <?php if($this->Identity->Validate('tools/faq/manage')){?>
        <span ng-click="showCreate = true" class="btn"><i class="fa fa-plus" title="<?php echo $this->lang->line('tools_faq_create');?>"></i></span>
    <?php }?>
</div>
<div class="row">
    <div class="alert bg-green" role="alert" ng-show="notifType != '' && notifType != null">
        <button type="button" class="close"  ng-click="notifType = ''"><span>&times;</span></button>
        <strong>
            <i class="fa fa-check"></i>
            <span ng-show="notifType == 'created'"><?php echo $this->lang->line('tools_faq_created');?></span>
            <span ng-show="notifType == 'edited'"><?php echo $this->lang->line('tools_faq_edited');?></span>
            <span ng-show="notifType == 'deleted'"><?php echo $this->lang->line('tools_faq_deleted');?></span>
        </strong> 
    </div>
</div>

<!-- contenedor de creacion de preguntas -->
<?php if($this->Identity->Validate('tools/faq/manage')){?>
<div ng-if="showCreate" class="row">
    <div class="col-md-8 well" style="margin-left:1em;">
        <span class="btn btn-red btn-xs" ng-click="closePanel()"><i class="fa fa-times pull left"></i></span>
        
        <h3><?php echo $this->lang->line('tools_create_title');?></h3>
        <label for="title"><?php echo $this->lang->line('tools_title');?></label>
        <input ng-model="question.title" class="form-control" type="text" name="title" id="" required>
        
        <label for="message"><?php echo $this->lang->line('tools_message');?></label>
        <textarea ng-model="question.message" class="form-control" name="message" id="" required rows="5" cols=""></textarea>

        <h2></h2>
        <a ng-click="createQuestion()" ng-hide="loading" class="btn btn-green"><?php echo $this->lang->line('administration_create');?></a>
        <span ng-show="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></span>
        <!-- <span ng-show="!loading && success" title="<?php echo $this->lang->line('add_success');?>"<i class="fa fa-check fa-2x text-success"></i></span> -->
    </div>
</div>
<?php }?>
<!-- contenedor de las preguntas frecuentes -->
<div class="row">
    <div class="jumbotron" ng-repeat="q in questions">
    <?php if($this->Identity->Validate('tools/faq/manage')){?>
        <span class="pull-right btn btn-red m-r_10" ng-click="setDelete(q.faqId)"><i class="fa fa-trash "></i></span>
        <span class="pull-right btn btn-lightgreen m-r_10" ng-click="showEdit = true;q.editTitle = q.title;q.editMessage = q.message"><i class="fa fa-pencil "></i></span>

        <div ng-show="showEdit">
            <span class="btn bg-red btn-xs" ng-click="showEdit = false"><i class="fa fa-times pull left"></i> <?php echo $this->lang->line('general_cancel');?></span>
            <h2></h2>

            <label for="title"><?php echo $this->lang->line('tools_title');?></label>
            <input ng-model="q.editTitle" type="text" name="title_edit" class="form-control">

            <label for="message"><?php echo $this->lang->line('tools_message');?></label>
            <textarea ng-model="q.editMessage" class="form-control" name="message" id="" required rows="5" cols=""></textarea>

            <h2></h2>
            <a ng-click="editQuestion(q.faqId,q.editTitle,q.editMessage)" ng-hide="editing" class="btn btn-lightgreen pull-right"><?php echo $this->lang->line('general_edit');?></a>
            <span ng-show="editing"><i class="fa fa-spinner fa-spin fa-2x pull-right"></i></span>
        </div>
    <?php }?>
        <div ng-show="!showEdit">
            <h2><b ng-bind="q.title"></b></h2>
            <hr/>
            <p ng-bind="q.message"></p>
        </div>
    </div>
    <hr/>
</div>
<!-- cuando no hay preguntas -->
<div class="row">
    <div class="alert bg-dark ng-hide" role="alert" ng-hide="questions.length > 0" style="margin: 0 15px; clear:both;">
        <i class="fa fa-info-circle fa-lg"></i>
        <?php echo $this->lang->line('tools_faq_empty');?>
    </div>
    <div class="text-center" ng-if="loading">
        <i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
    </div>
</div>

<!-- modal de no completado -->
<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo $this->lang->line('general_completeall')?> <strong class="text-danger">*</strong></p>
                <div class="text-right">
                    <span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal de borrado -->
<div class="modal animated shake open" tabindex="-1" id="confirm-delete">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line('tools_faq_areyousure'); ?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<span class="btn btn-lightgreen" ng-click="toDelete()" ng-show="!deleting"><?php echo $this->lang->line('general_yes'); ?></span>
						<span class="btn btn-lightgreen disabled" ng-show="deleting"><i class="fa fa-refresh fa-spin"></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>