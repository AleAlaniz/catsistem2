<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/bootstrap-datepicker.css" rel="stylesheet">
<?php echo $navBar; ?>
<div class="col-xs-12 flat-style">
	<div class="panel panel-default">
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate ng-submit="getData()">
				<div class="form-group">
					<label for="date" class="col-sm-2 control-label"><?php echo $this->lang->line('tools_analitycs_date');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<div class=" input-group input-daterange">
							<input type="text" data-date-format="mm/dd/yyyy" class="form-control" ng-init="dateStart = '<?php echo date('m/d/Y');?>'" name="dateStart" ng-model="dateStart" placeholder="<?=$this->lang->line('tools_analitycs_datestart');?>">
							<span class="input-group-addon"><?=$this->lang->line('tools_analitycs_dateto');?></span>
							<input type="text" data-date-format="mm/dd/yyyy" class="form-control" ng-init="dateFinish = '<?php echo date('m/d/Y');?>'" name="dateFinish" ng-model="dateFinish" placeholder="<?=$this->lang->line('tools_analitycs_datefinish');?>">
						</div>
					</div>
				</div>
				<div class="row" ng-hide="someChecked">
					<div class="col-md-12">
						<searchUsers></searchUsers>
					</div>
				</div>
				<div class="form-group" ng-show="users.length == 0">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_sites');?> <span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<?php
						foreach ($sites as $key => $site) {
							?>
							<div class="checkbox checkbox-angular" ng-init="sites[<?php echo $key;?>].siteId = <?php echo $site->siteId; ?>">
								<label ng-class="{check: sites[<?php echo $key;?>].active}" >
									<input type="checkbox" name="sites[]" ng-model="sites[<?php echo $key;?>].active"  value="<?php echo $site->siteId; ?>" ng-change="change(sites[<?php echo $key;?>])">
									<?php echo encodeQuery($site->name);?>
								</label>
							</div>
							<?php
						}
						?>
					</div>
				</div>
				<div class="form-group text-center">
					<button class="btn btn-green ng-hide" disabled="true" type="submit" ng-show="sending"><i class='fa fa-refresh fa-spin fa-lg fa-fw'></i></button>
					<button type="submit" class="btn btn-green" ng-hide="sending"><?php echo $this->lang->line('tools_analitycs_show');?></button>
				</div>
				<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
					<div class="modal-dialog modal-sm">
						<div class="modal-content">
							<div class="modal-body">
								<p><?php echo $this->lang->line('general_completeall')?> <strong class="text-danger">*</strong></p>
								<div class="text-right">
									<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!-- form para exportar tabla a excel -->
			<form action="/<?=FOLDERADD?>/tools/to_excel_login_data" method="post" target="_blank" id="exportForm">
				<button class="btn bg-lightblue" ng-if="data.users.length > 0" ng-click="exportData()"><i class="fa fa-file-excel-o"></i> <?php echo $this->lang->line('tools_analitycs_export');?></button>			
				<input type="hidden" id="date" name="date"/>
				<input type="hidden" id="sites" name="sites"/>
				<input type="hidden" id="sendData" name="sendData"/>
			</form>
			<!-- fin de form de exportar -->
			<div ng-show="data.users != null" class="ng-hide">
				<hr>
				<p ng-if="data.users.length == 0" class="text-center"><?php echo $this->lang->line('tools_analitycs_usersempty'); ?></p>
				<p ng-if="data.users.length != 0"><strong><?php echo $this->lang->line('general_users'); ?></strong>: <span ng-bind="data.users.length"></span></p>
				<table class="table table-hover" id="reportTable" ng-if="data.users.length != 0">
					<thead>
						<tr class="bg-lightblue">
							<th style="border:1px #888;color: #FFF;background-color:rgb(25, 187, 216);"><?php echo $this->lang->line('tools_analitycs_name'); ?></th>
							<th style="border:1px #888;color: #FFF;background-color:rgb(25, 187, 216);"><?php echo $this->lang->line('tools_analitycs_lastName'); ?></th>
							<th style="border:1px #888;color: #FFF;background-color:rgb(25, 187, 216);"><?php echo $this->lang->line('tools_analitycs_userName'); ?></th>
							<th style="border:1px #888;color: #FFF;background-color:rgb(25, 187, 216);"><?php echo $this->lang->line('tools_analitycs_dni'); ?></th>
							<th style="border:1px #888;color: #FFF;background-color:rgb(25, 187, 216);"><?php echo $this->lang->line('tools_analitycs_date'); ?></th>
							<th style="border:1px #888;color: #FFF;background-color:rgb(25, 187, 216);"><?php echo $this->lang->line('tools_analitycs_logintime'); ?></th>
							<th style="border:1px #888;color: #FFF;background-color:rgb(25, 187, 216);"><?php echo $this->lang->line('tools_analitycs_logouttime'); ?></th>
							<th style="border:1px #888;color: #FFF;background-color:rgb(25, 187, 216);"><?php echo $this->lang->line('tools_analitycs_site'); ?></th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="user in data.users">
							<td style='border:1px #888 solid;color:#555;' ng-bind="user.name"></td>
							<td style='border:1px #888 solid;color:#555;' ng-bind="user.lastName"></td>
							<td style='border:1px #888 solid;color:#555;' ng-bind="user.userName"></td>
							<td style='border:1px #888 solid;color:#555;' ng-bind="user.dni"></td>
							<td style='border:1px #888 solid;color:#555;' ng-bind="(user.timestamp * 1000) | date: 'dd-MM-yyyy'"></td>
							<td style='border:1px #888 solid;color:#555;' ng-bind="(user.timestamp * 1000) | date : 'HH:mm'"></td>
							<td style='border:1px #888 solid;color:#555;' ng-bind="(user.logoutTimestamp != null) ? ((user.logoutTimestamp * 1000) | date : 'HH:mm') : '-'"></td>
							<td style='border:1px #888 solid;color:#555;' ng-bind="user.site"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
