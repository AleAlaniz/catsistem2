<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/animate.min.css" rel="stylesheet">
   <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.7/angular.min.js"></script> 
   <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.3/angular-route.js"></script>

<style>

</style>
<div class="col-xs-12">
	<!-- Claudio 14/06 -->
	<!-- <ol class="breadcrumb">
		<li><a href="/<?=FOLDERADD?>/tools"><?=$this->lang->line('tools_title');?></a></li>
		<li class="active"><?=$this->lang->line('tools_input_gamification');?></li>
	</ol> -->
	<div class="page-title">
		<h3 class="title"><?php echo $this->lang->line('tools_input_gamification'); ?></h3>
		<a href="/<?=FOLDERADD?>#/tools" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i><span class="hidden-xs"> <?php echo $this->lang->line('general_goback'); ?></span></a>
	</div>
	<!-- Claudio 14/06 -->

	<div class="panel panel-default" ng-controller="Ctools">
		<div class="panel-heading">
			<strong><?=$this->lang->line('tools_input_gamification');?></strong>
			<span id="loading" style="margin-left:10px">
						<i class="fa fa-refresh fa-spin fa-lg"></i>
			</span>
		</div>
		<div class="panel-body" id="container" style="display:none;">
			
		<div  ng-app="users" ng-controller="form">

			<div class="col-sm-5">
			<!--  
				<input type="text" id="code" ng-model="code" ng-focus="codefocus=true;" ng-blur="codefocus=false;" ng-trim="true" />
					<label for="code" class="no-selectable"><?php echo $this->lang->line('tools_prize_code');?></label>
				</div>

			-->			
				<input type="hidden"  ng-repeat="user in users track by $index" name="users[]" value="{{user.userId}}" />
				<label class="col-sm-1 control-label"><?=$this->lang->line('general_user');?></label>
			
				<div class="col-sm-10 col-sm-offset-1">
					<span class="selected-item-delete selectable" ng-click="removeUser()" id="removeUser" style="color:red;display:none">&times;</span>
					<div class="select-container">
						<span class="select-selected" id="selectedUserContainer">
							<span class="select-selected-item" ng-model="selectedUser" id="selectedUser">
								<span class="selected-item-image" style="background-image: url('/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{selectedUser.userId}}&amp;amp;wah=200');filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{selectedUser.userId}}&wah=200',sizingMethod='scale');"></span>
								<span class="selected-item-label">{{selectedUser.completeName}} &lt;{{selectedUser.userName}}&gt; </span>
							</span>
						</span>
						<input type="text" autocomplete="off" class="form-control" style="width:100%" ng-trim="true" ng-model="findLike" ng-change="searchUsers()" id="findInput" value="{{user.userId}}" />
					</div>
					<div class="select-options-container-super" ng-show="userOptions.length > 0">
						<div class="select-options-container">
							<div class="select-option no-selectable" ng-repeat="user in userOptions track by $index" ng-init="i=$index" ng-click="addUser(i)">
								<span class="selected-option-image" style="background-image: url('/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&amp;amp;wah=200');filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200',sizingMethod='scale');"></span>
								<span class="selected-option-label">{{user.completeName}} &lt;{{user.userName}}&gt; </span>
							</div>
						</div>
					</div>
					
				</div>

			</div>
		
			<div class="col-sm-7">
				
				<label class="col-sm-1 control-label"><?=$this->lang->line('gamificationadmin_achievements');?></label>
				<div class="col-sm-10 col-sm-offset-1 ">	
					<div class="select-option no-selectable" ng-repeat="achievement in achievements track by $index" ng-init="i=$index" ng-click="showAchievement(i)" data-toggle="modal" data-target="#visibility_details" >
						<span class="selected-option-image" style="background-image: url('/<?php echo FOLDERADD; ?>/gamification/getachievementimage/{{achievement.achievementId}}');"></span>
						<span class="selected-option-label">{{achievement.name}} </span>
					</div>
					<h4 ng-show="achievements.length == 0">No hay objetivos disponibles.</h4>
				</div>	
			</div>

				<div class="modal fade" id="visibility_details" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header bg-lightgreen">
								<h5 class="modal-title"><strong>Control gamification</strong></h5>
							</div>
								<div class="alert alert-success alert-dismissible" role="alert" ng-show="ok">
											<strong><i class="fa fa-check"></i></strong>
											<span>Valor guardado correctamente</span>
								</div>
							<div ng-model="achievementInput" div class="col-sm-offset-1 col-sm-10" style="margin-top:12px"><!--Cuerpo del modal-->
								<label class="control-label strong">Nombre</label>
								<div style="padding-left:10px">
									<label>{{achievementInput.name}}</label>        
								</div>
								<label class="control-label strong">Descripción</label>
								<div style="padding-left:10px">
									<label>{{achievementInput.description}}</label>        
								</div>
								<label class="control-label strong">Cantidad Objetivo<span>{{}}</span></label>
								<div style="padding-left:10px">
									<label>{{achievementInput.count}}</label>        
								</div>

								<hr>
								<h4>Últimas inserciones</h4>
								<div style="overflow-y:scroll;height:200px">
									<table class="table">
										<thead>
											<tr>
												<th>Usuario</th>
												<th>Cantidad</th>
												<th>fecha</th>
											</tr>
										</thead>
										<tbody>
											<tr id="loadingLogs" style="display:none">
												<td></td>
												<td><span><i class="fa fa-refresh fa-spin fa-lg"></i></span></td>
												<td></td>
											</tr>
											<tr ng-repeat="log in logs">
												<td>{{log.completeName}}</td>
												<td>{{log.count}}</td>
												<td>{{log.date}}</td>
											</tr>
										</tbody>
									</table>
								</div>
								<br>




								<div class="form-group">
									<label for="name" class="col-sm-2 control-label">Nueva cantidad</label>
									<div class="col-sm-10">
										<input type="number" min="1" class="form-control input-sm" ng-model="achievementValue">
										<p id="error" style="color:red"></p>
									</div>
								</div>

							</div>
							<div class="modal-footer">
								<p style="text-align:center" id="waiting"></p>
								<div class="form-group text-center">
									<button name="action" type="submit" class="btn btn-success btn-sm" data-toggle="modal" ng-click="showConfirm()">Incrementar</button>
									<a name="action" href="" class="btn btn-danger btn-sm" data-dismiss="modal"><?=$this->lang->line('general_back');?></a>
								</div>
							</div>
						</div>
					</div>
				</div>

<!--claudio -->



<!--claudio -->
				<div class="modal animated bounceInDown" id="confirm-increment" role="confirm-dialog" style="margin-top:10%">
					<div class="col-sm-2 col-sm-offset-5">
						<div class="modal-content">
							<div><!--Cuerpo del modal-->
								<p style="text-align:center;margin-top:10px" ><strong>¿Estás seguro de incrementar este objetivo?</strong></p>
							</div>
							<div>
								<div class="form-group text-center">
									<button name="action" type="submit" class="btn btn-success btn-sm" ng-click="saveIncrement()" data-dismiss="modal">Si</button>
									<a name="action" href="" class="btn btn-danger btn-sm" data-dismiss="modal">No</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!--iframe src="http://ecrm5.cat/ecrm/" style="width:100%;height:800px; margin-top:20px;">
				</iframe-->
				

			</div>

		</div>
	</div>

</div>


<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/angular.min.js"></script>

<script type="text/javascript">

$('#nav_tools').addClass('active');



angular
.module("users", [])
.controller("form", ["$scope",function($scope)
{
	$('#selectedUser').hide();
	$scope.users = [];
	$scope.achievements =[];
	$scope.ok=false;
	$scope.logs=[];

	$scope.showConfirm=function()
	{

		if($scope.achievementValue > 0)
			{
				$('#confirm-increment').modal('show');
				$('#error').text("");
			}
			
		else
			{
				$('#error').text("Ingrese un numero válido.");
			}
	}

	$scope.saveIncrement=function()
	{

		$.ajax({
			method 	: "POST",
			url		: '/<?php echo FOLDERADD;?>/gamificationadmin/incrementachievementvalue',
			data 	: {'userId':$scope.selectedUser.userId,'achievementId': $scope.achievementInput['achievementId'],'achievementValue':$scope.achievementValue},
			beforeSend: function()
			{ 
				$('[name=action]').hide();
				$scope.ok=false;
				$('#waiting').text("Guardando datos...");
			}
		}).done(function (answer)
		{
			if(answer!="ok")
			{
				$('#error').text(answer);
				$scope.ok=false;
			}
			else
			{
				$('#error').text("");
				$scope.ok=true;
				
			}
			$('[name=action]').show();
			$('#waiting').text('');
			$scope.$apply($scope.achievementValue=null);

		});	
	}

	$scope.showAchievement=function(i)
	{
		$scope.achievementInput=$scope.achievements[i];
		$scope.achievementValue=null;
		$('#error').text("");
		$scope.ok=false;
		$scope.logs=[];

		$.ajax({
			method 	: "POST",
			url		: '/<?php echo FOLDERADD;?>/gamificationadmin/getachievementlogs',
			data 	: {'userId': $scope.selectedUser.userId,'achievementId':$scope.achievementInput.achievementId},
			beforeSend: function()
			{
			 $("#loadingLogs").show();
			
			}
		}).done(function (logs)
		{
			$("#loadingLogs").hide();
			var achievementsLogs;
			try
			{
				achievementsLogs = $.parseJSON(logs);
			
				jQuery.each(achievementsLogs, function(i, log)
				{
					$scope.$apply($scope.logs.push(log));
				});


			}catch(e)
			{	
				return;
			}

		});
	}


	$scope.removeUser = function() {
		
		$scope.selectedUser=null;
		$('#selectedUser').hide();
		$('#removeUser').hide();
		$("#findInput").show();
		$scope.achievements =[];


	};

	$scope.addUser = function(i) {
		$scope.selectedUser=$scope.userOptions[i];
		$scope.achievements=[];
		$('#selectedUser').show();
		$('#findInput').hide();
		

		$.ajax({
			method 	: "POST",
			url		: '/<?php echo FOLDERADD;?>/gamificationadmin/getAchievementsByUser',
			data 	: {'userId': $scope.selectedUser.userId},
			beforeSend: function()
			{
			 $("#removeUser").hide();
			 $("#loading").show();
			}
		}).done(function (data)
		{
			$("#removeUser").show();
			$("#loading").hide();
	

			var achievementsData;
			try
			{

				achievementsData = $.parseJSON(data);
			
				jQuery.each(achievementsData, function(i, achievement)
				{
					$scope.$apply($scope.achievements.push(achievement));
				});


			}catch(e)
			{	
				return;
			}

		});

		$scope.userOptions = [];
		$scope.findLike = '';

	};


	$scope.searchNumber = 0;
	$scope.searchActual = 0;

	$scope.searchUsers = function() {
		if($scope.findLike != '') 
		{
			var thisSearch = $scope.searchNumber;
			$scope.searchNumber = $scope.searchNumber+1;
			$.ajax({
				method 	: "POST",
				url		: '/<?php echo FOLDERADD;?>/surveys/searchusers',
				data 	: {'like': $scope.findLike}
			}).done(function (data) {
				 if(data != 'empty' && thisSearch >= $scope.searchActual)
				{
					$scope.searchActual = thisSearch;
					$scope.$apply($scope.addUserOptions(data));
				}
				else if (thisSearch >= $scope.searchActual)
				{
					$scope.searchActual = thisSearch;
					$scope.$apply($scope.userOptions = []);


				}
			});
		}
		else
		{
			$scope.userOptions = [];
			$scope.achievements =[];
		}
	};

	$scope.addUserOptions = function (users) {
		try{

			users = $.parseJSON(users);

		}catch(e){
			return;
		}

		$scope.userOptions = [];
		jQuery.each(users, function(i, user) {
			var exist = false;
			jQuery.each($scope.users, function(i, userExist) {
				if (userExist.userId == user.userId) {exist = true;}
			});
			if (!exist) {
				$scope.userOptions.push(user);}
			});


	};


$('#loading').hide();
$('#container').show();

}]);	


</script>


