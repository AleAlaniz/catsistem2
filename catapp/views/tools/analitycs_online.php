<?php echo $navBar; ?>
<div class="col-md-12 flat-style">
	<div class="panel panel-default">
		<div class="panel-body row" style="padding-top: 0;">
			<div class="col-md-12">
				<h2 class="text-center"><?php echo $this->lang->line('tools_analitycs_onlineusers'); ?></h2>
				<h2 class="text-center"><span ng-bind="onlineUsers"></span></h2>
			</div>
			<div class="col-md-12 row" style="margin-left: 0.5em;">
				<h3><?php echo $this->lang->line('tools_analitycs_online_sites'); ?></h3>
				<table class="table">
					<tr>
						<th ng-repeat="c in count">
							<p class="text-center">{{c.name}}</p>
						</th>
					</tr>
					<tr>
						<td ng-repeat="c in count">
							<p class="text-center">{{c.number}}</p>
						</td>
					</tr>
				</table>	
			</div>
		</div>
		
	</div>
</div>