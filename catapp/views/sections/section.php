<style>
#content
{
	display:none;
}

.wait
{
	margin-left: 10px;
	color:#006687;
	display: none;
}
</style>

<div id="content">
	<div class="page-title">	
		<h3 class="title">{{siteTitle}}</h3>
	</div>

	<div class="col-xs-12 flat-style">
		<div class="panel bg-white">
			<ul class="list-group">
			
				<li ng-repeat="s in itemsData.subSections" class="list-group-item" >
					<i ng-show="s.noViews" class="fa fa-circle noreadDot"></i>
					<strong ng-click="showSubItems('subsectionId-'+s.subsectionId)" id="subsectionId-{{s.subsectionId}}" class="pointer"><i class="text-center fa {{s.icon}}"></i>{{s.name}} 
						{{sumSpans(0,s.subsectionId,false)}}
					</strong> 
					<ul style="display:none;" id="itemsforsubsectionId-{{s.subsectionId}}">

						<li ng-repeat="ss in s.sssections" class="list-group-item">
							<i ng-show="ss.noViews" class="fa fa-circle noreadDot"></i>
							<strong ng-click="showSubItems('subsectionId-'+ss.subsectionId)" id="subsectionId-{{ss.subsectionId}}" class="pointer"><i class="text-center fa {{ss.icon}}"></i>{{ss.name}}
								{{sumSpans(0,ss.subsectionId,true)}}
							</strong>
							
							<ul style="display:none;" id="itemsforsubsectionId-{{ss.subsectionId}}">		
								<li ng-repeat="i in ss.items" class="list-group-item">

									<i ng-show="i.noViews" class="fa fa-circle noreadDot"></i>
									<a ng-href="#/items/item/{{i.itemId}}" ng-click="addToView(i.itemId)">
										<i class="text-center fa fa-circle-o"></i> {{i.name}}

										<span ng-repeat="u in unread">
											<span  ng-repeat="su in u.subsectionUnread">
												<span ng-if="su.itemId == i.itemId" class="messagesnumber count-{{ss.subsectionId}}" ng-bind="su.c"></span>
											</span>
										</span>
									</a>
								</li>
								<li ng-show="ss.items.length == 0" class="text-center text-default list-group-item">
									<i class="fa fa-exclamation-circle"></i> <?=$this->lang->line('administration_items_empty');?>
								</li>
							</ul>
						</li>
						<li ng-repeat="si in s.items" class="list-group-item">
							<i ng-show="si.noViews" class="fa fa-circle noreadDot"></i>

							<a ng-href="#/items/item/{{si.itemId}}" ng-click="addToView(si.itemId)">
								<i class="text-center fa fa-circle-o"></i> {{si.name}}

								<span ng-repeat="u in unread">
									<span  ng-repeat="su in u.subsectionUnread">
										<span ng-if="su.itemId == si.itemId" class="messagesnumber count-{{s.subsectionId}}" ng-bind="su.c"></span>
									</span>
								</span>
							</a>
						</li>
						<li ng-show="s.sssections.length == 0 && s.items.length == 0" class="text-center text-default list-group-item">
							<i class="fa fa-exclamation-circle"></i> <?=$this->lang->line('administration_items_empty');?>
						</li>
					</ul>
				</li>

				<li ng-show="sections.length == 0" class="text-center text-default list-group-item">
					<i class="fa fa-exclamation-circle"></i> <?=$this->lang->line('administration_subsections_empty');?>
				</li>

			</ul>
			<li ng-show="itemsData.subSections.length == 0" class="text-center text-default list-group-item">
				<i class="fa fa-exclamation-circle"></i> <?=$this->lang->line('administration_subsections_empty');?>
			</li>

		</div>
		<div class="panel">
			<div class="panel-custom-heading">
				<h4 class="f-w-500">
					<?php echo $this->lang->line('section_favorites'); ?>
				</h4>
			</div>
			<ul ng-repeat="f in itemsData.favorites" class="list-group">
				<li class="list-group-item">
					<span class="strong"><i class="fa {{f.icon}}"></i>{{f.subsectionName}}</span>&nbsp; <i class="fa fa-chevron-right"></i>
					<a ng-href="#/items/item/{{f.itemId}}">
						<i class="text-center fa fa-circle-o"></i>{{f.name}}
					</a>
				</li>
			</ul>

			<div ng-show="itemsData.favorites.length == 0" class="panel-body text-center"><i class="fa fa-star-o"></i> <?php echo $this->lang->line('section_favoritesempty'); ?></div>
		</div>
	</div>
</div>
<div id="loading" class="panel-body text-center">
	<h2><i class="fa fa-refresh	 fa-spin fa-2x"></i></h2>
	<h3><?php echo $this->lang->line('general_wait') ?></h3>
</div>