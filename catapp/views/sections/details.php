<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/sections"><?=$this->lang->line('general_sections');?></a></li>
	<li><a href="/<?=FOLDERADD?>/sections/details/<?=$sectionId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_details');?></li>
</ol>
	<?php if (isset($_SESSION['sectionMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['sectionMessage'] == 'edit'){
			echo $this->lang->line('administration_sections_editmessage');
		}
		?>
	</div>
<?php endif; ?>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_sections_details');?></strong>
	</div>
	<div class="panel-body">
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('administration_sections_name');?></dt>
			<dd><?=encodeQuery($name)?></dd>
		</dl>
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('administration_sections_icon');?></dt>
			<dd><i class="fa <?=encodeQuery($icon)?>"></i></dd>
		</dl>
	</div>
</div>
</div>
<script type="text/javascript">
$('#nav_sections').addClass('active');
$('#sectionNavDetails').addClass('active');
</script>