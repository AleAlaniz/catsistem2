<ul class="nav nav-tabs">
	<?php if($this->Identity->Validate('sections/details')) { ?>
	<li role="presentation" id="sectionNavDetails"><a href="/<?=FOLDERADD?>/sections/details/<?=$sectionId;?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_details');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('sections/edit')) { ?>
	<li role="presentation" id="sectionNavEdit"><a href="/<?=FOLDERADD?>/sections/edit/<?=$sectionId;?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_edit');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('sections/delete')) { ?>
	<li role="presentation" id="sectionNavDelete"><a href="/<?=FOLDERADD?>/sections/delete/<?=$sectionId;?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_delete');?></span></a></li>
	<?php } ?>
</ul>