<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/sections"><?=$this->lang->line('general_sections');?></a></li>
	<li><a href="/<?=FOLDERADD?>/sections/details/<?=$sectionId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_delete');?></li>
</ol>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_sections_delete');?></strong>
	</div>
	<div class="panel-body">
		<h4><?=$this->lang->line('administration_sections_delete_areyousure');?></h4>
		<hr>
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('administration_sections_name');?></dt>
			<dd><?=encodeQuery($name)?></dd>
		</dl>
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('administration_sections_icon');?></dt>
			<dd><i class="fa <?=encodeQuery($icon)?>"></i></dd>
		</dl>
		<div class="alert alert-danger col-sm-12" role="alert">
			<strong><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> <?=$this->lang->line('general_atention');?></strong>
			 <?=$this->lang->line('administration_sections_deletemessage_subsections');?>
		</div>
		<form method="POST"  >
			<input type="hidden" name="sectionId" value="<?=$sectionId?>">
			<div class="form-group text-center col-xs-12">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_delete');?></button>
				<a href="/<?=FOLDERADD?>/sections" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
</div>
<script type="text/javascript">
$('#nav_sections').addClass('active');
$('#sectionNavDelete').addClass('active');
</script>