<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">
<div class="back-teaming">
	<?php $header ?>
	<div class="container-fluid" id="discharge-panel">
		<?php echo $navbar;?>
		<div class="panel panel-default" style="overflow: auto;">
			<div class="panel-heading" style="background-color: rgb(66,166,42); font-family: Segoe UI Symbol;">
				<?=$this->lang->line('teaming_reports_reports');?>
			</div>
			<div class="panel-body">
				<!-- datepicker -->
				<div class="span5 col-md-4 well well-sm" id="sandbox-container">
					<p><b><?=$this->lang->line('general_comm_report_date_range_search');?></b></p>
					<div class="input-daterange input-group" id="datepicker" style ="margin-bottom: 2em;">
						<p style="width: 50%;display: inline-block;"><b><?=$this->lang->line('general_comm_report_date_from');?></b></p>
						<p style="width: 50%; display: inline;"><b><?=$this->lang->line('general_comm_report_date_until');?></b></p>
						<input type="text" class="input-sm form-control" id="start" style="width: 45%;margin-right: 1.5em;">
						<input type="text" class="input-sm form-control" id="end" style="width: 45%;">
					</div>
				</div>

				<div class="col-md-1">
				</div>
				<div class="col-md-9" id="date-reports" ng-show="!loading" class="ng-hide pull-right">
					<table class="table table-bordered table-responsive">
						<thead>
							<tr class="col-6-sm" style="display: none;">
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr class="row-md-1">
								<td>
									<?=$this->lang->line('general_comm_report_joined');?>
								</td>
								<td>
									{{registedusers}}
								</td>
							</tr>
							<tr class="row-md-1">
								<td>
									<?=$this->lang->line('general_comm_report_joined_today');?>
								</td>
								<td>
									<ul>
										<li>
											<?=$this->lang->line('teaming_form_load_type_manual').":  "?>
											{{joinedInDateRange["manual"]}}
										</li>
										<li>
											<?=$this->lang->line('teaming_form_load_type_online').":  ";?>
											{{joinedInDateRange["online"]}}
										</li>
									</ul>
								</td>
							</tr>
							<tr class="row-md-1">
								<td>
									<?=$this->lang->line('general_comm_report_discharged_in_date_range');?>
								</td>
								<td>
									<ul>
										<li>
											<?=str_replace("esp", "", $this->lang->line('general_discharge_teaming')).":  ";?>
											{{discharges["d"]}}
										</li>
										<li>
											<?=str_replace("esp", "", $this->lang->line('admin_discharge_teaming')).":  ";?>
											{{discharges["r"]}}
										</li>
										<li>
											<?=str_replace("esp", "", $this->lang->line('admin_discharge_fired')).":  ";?>
											{{discharges["f"]}}
										</li>
									</ul>
								</td>
							</tr>
							<tr class="row-md-1">
								<td>
									<?=$this->lang->line('general_comm_report_max_updates_in_date_range');?>
								</td>
								<td>
									{{updatesInDateRange}}
								</td>
							</tr>
							<tr class="row-md-1">
								<td>
									<?=$this->lang->line('general_comm_report_max_amount');?>
								</td>
								<td>
									{{maxAmount}}
								</td>
							</tr>
							<tr class="row-md-1">
								<td>
									<?=$this->lang->line('general_comm_report_amount_total');?>
								</td>
								<td>
									{{totalAmount}}
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="text-center col-md-12" ng-if="loading">
					<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
				</div>
			</div>
		</div>
	</div>
	<div class="sep">
		&nbsp;
	</div>
	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
	<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
	<script>
		$('#communicationReports').replaceWith("<li><?php echo $this->lang->line('general_comm_report');?></li>");
	</script>

</div>
