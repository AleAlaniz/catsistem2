<div class="back-teaming" ng-controller='Cteaming'>
	<div class="add-header">
		Políticas de Privacidad
	</div>
	<div class="container-fluid" style="margin-right: auto;margin-left: auto;">
		<div class="panel panel-default" id="panel-agreement-terms">
			<div class="panel-body" id="panel-agreement-terms-body">
				<img src="/<?php echo APPFOLDERADD; ?>/libraries/images/logo_teaming.png" class="teaming-small-terms pull-left"><img src="/<?php echo APPFOLDERADD; ?>/libraries/images/logo.png" class="cat-small-terms pull-right"><br><br><br>
				<div class="agreement-terms-paragraph">
					<span class="bold">Políticas de Privacidad</span><br><br>
					LEÉ ESTO CON DETENIMIENTO. ESTE DOCUMENTO INDICA CÓMO INTRANET (<a href="http://intranet2.cat/catnet" id="teaming-link">http://intranet2.cat/CATnet</a>) en adelante “CAT”, UTILIZARÁ Y PROTEGERÁ TUS DATOS PERSONALES.<br><br>
					<span class="bold">Seguridad y Protección de tus Datos Personales</span><br>
					La seguridad de los datos personales es una prioridad para “CAT”. Nos esforzamos por ofrecer el más alto nivel de seguridad. Nos encontramos alineados con la Ley Nacional de Protección de Datos Personales, N° 25.326 y sus normas complementarias.<br><br>
					<span class="bold">Privacidad</span><br>
					“CAT” respeta tu privacidad. Toda la información que nos proporciones se tratará con sumo cuidado y diligencia y solo se utilizará de acuerdo con los límites establecidos en el presente documento.<br><br>
					<span class="bold">¿Cómo se utiliza tu información?</span><br>
					“CAT” utiliza la información que nos brindás para: administrar, gestionar y concretar la donación a las instituciones. “CAT” también utiliza la información que nos brindás para personalizar, mejorar nuestros servicios y para fines estadísticos de la compañía.<br><br>
					<span class="bold">Acceso a la información</span><br>
					“CAT” siempre está comprometido a presentar nuevas soluciones que mejoren el valor de sus productos y servicios. La información no identificable y estadística también podrá ser compartida con socios comerciales. <br>
					A excepción de los casos anteriores, no compartirá información que podría identificar personalmente a los participantes de Teaming.<br><br>
					Al ingresar a la intranet, estás prestando el consentimiento para utilizarlos de acuerdo con esta Política de Privacidad.<br><br>
					<span class="bold">Errores/Omisiones</span><br>
					La información y material contenido en esta intranet han sido verificados para que sean exactos. Sin embargo, dicha información y material son suministrados sin ninguna garantía expresa o implícita. “CAT” no asume ninguna responsabilidad por cualquier error u omisión en dicha información y material.<br><br>
					<span class="bold">Modificaciones de nuestras Políticas de Privacidad</span><br>
					“CAT” se reserva el derecho, a su exclusiva discreción, de modificar, alterar, agregar o eliminar parte de estas Políticas de Privacidad / Notas legales en cualquier momento. Recomendamos que examines esta política cada vez que visites la intranet.<br>
					Esta Política de Privacidad se modificó por última vez y se publicó en nuestro Sitio Web el 21 de Agosto de 2017.<br><br>
				</div>
			</div>
		</div>
		<div class="buttons col-sm-12">
			<a type="submit" name="submit"  class="btn btn-primary" id="first-try" href="#/teaming/sendSuggestion">
				<?=$this->lang->line('teaming_suggest_institution');?>
			</a>
		</div>
		<div class="sep" style="margin-bottom:  120px">
			&nbsp;
		</div>
		<img class="img-responsive pull-right" id="big-heart-agreement-terms" src="/<?php echo APPFOLDERADD; ?>/libraries/images/corazon_grande.png">
	</div>
