<ul class="nav nav-tabs nav-teaming">
	<li role="presentation" id="teaming_reports">
		<a href="/<?=FOLDERADD?>/#/teaming/communicationReports">
			<?=$this->lang->line('teaming_reports');?>
		</a>
	</li>
	<li role="presentation" id="teaming_newsletter">
		<a href="/<?=FOLDERADD?>/#/teaming/newsletterReports">
			<?=$this->lang->line('teaming_newsletter');?>
		</a>
	</li>
	<li role="presentation" id="teaming_survey">
		<a href="/<?=FOLDERADD?>/#/teaming/surveyReports">
			<?=$this->lang->line('teaming_survey');?>
		</a>
	</li>
</ul>


