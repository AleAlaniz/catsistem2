<div class="back-teaming">
	<div class="container-fluid" style="margin-right: auto;margin-left: auto;">
		<div class="panel panel-default" id="panel-agreement">
			<div class="panel-body" id="panel-agreement-body">
				<img src="/<?php echo APPFOLDERADD; ?>/libraries/images/logo_teaming.png" class="teaming-small pull-left"><img src="/<?php echo APPFOLDERADD; ?>/libraries/images/logo.png" class="cat-small pull-right"><br><br><br>
				<?php echo validation_errors(); ?>
				<div class="agreement-paragraph">
					<span class="bold" style="font-family: Segoe UI Cursiva;"><?=$this->lang->line('teaming_form_company_id');?> <input type="text" class="teaming-fields" name="id" ng-model="formData.id" only-numbers="onlyNumbers()" maxlength="6"></span><br><br>
					<span class="bold" style="font-family: Segoe UI Cursiva;"><?=$this->lang->line('teaming_form_update');?></span><br><br>
					<?=$this->lang->line('teaming_form_first_paragraph');?>
					<?=$this->lang->line('teaming_form_update_exclamation');?>
					<?=$this->lang->line('teaming_form_final_exclamation');?>
				</div>
				<form>
					<div class="second-paragraph">
						Yo, <?=$query->name?> <?=$query->lastName?>,<br>
						Titular del (DNI/LC) nº<input type="text" class="teaming-fields" name="dni" ng-model="formData.dni" only-numbers="onlyNumbers()" maxlength="8">, autorizo a <span class="bold"><?php echo $this->lang->line('teaming_sign_title_section_'.$this->session->siteId)?></span> (desde la fecha que figura al pie de este formulario) a actualizar el monto de mi donación y a deducir de mi salario la suma de $ <input only-numbers="onlyNumbers()" type="text" ng-change="toBlack('#amount-qty')" id="amount-qty" class="teaming-fields redify" name="amount" maxlength="4" ng-model="formData.amount" style=" max-width: 40px;">.- <span class="bold">EL VALOR MÍNIMO ACEPTADO PARA DONAR ES DE 25 PESOS</span> (escribir en letra la cifra: <input type="text" ng-change="toBlack('#amount-ltr')" id="amount-ltr" class="teaming-fields redify" name="amountcharacters" ng-model="formData.amountCharacters" maxlength="46" style="max-width: 399px;" alpha-input="alphaInput()" >) para destinarlo como donación a las campañas de ayuda solidaria de Teaming, mientras dure mi relación laboral con la empresa, o hasta que proceda a darme de baja voluntariamente.<br><br>
						Dispongo de la capacidad de modificar el monto asignado o bien definir el período de mi permanencia en tal actividad de RSE (Responsabilidad Social Empresaria).<br><br>
						<div class="cat-sign"> <span class="bold"><?php echo $this->lang->line('teaming_sign_title_section_'.$this->session->siteId)?></span><br>
							<?php echo $this->lang->line('teaming_sign_body_site_'.$this->session->siteId);  ?>
						</div>

						Fecha:<span class="underline"></span>/<span class="underline"></span>/201<span class="underline" style="padding-left: 10px;"></span><br><br><br><br>

						Firma:<span class="underline" style="padding-left: 150px">&nbsp;</span><input type="checkbox" class="field" name="acceptterms" value="yes" ng-model="formData.acceptterms" required>* Acepto <a href="#/teaming/agreementTerms" target="_blank" style="color: rgb(66,166,42); text-decoration: underline;">las Políticas de Privacidad.</a><br><br><br>
						Aclaración:<span class="underline" style="padding-left: 150px">&nbsp;</span><br><br><br>
					</div>
					<div class="foot">
						<div class="panel panel-default">
							<div class="panel-body padding-top-zero"><?=$this->lang->line('teaming_form_area_campaign');?>: <input type="text" class="teaming-fields" name="areacampaign" ng-model="formData.areaCampaign" style="max-width: 100%;" maxlength="70" alpha-input="alphaInput()" ></div>
							<div class="panel-body padding-top-zero"><?=$this->lang->line('teaming_form_mobile_number');?>: <input type="text" class="teaming-fields" name="celnumber" ng-model="formData.mobileNumber" style="max-width: 260px;" maxlength="20" cel-number="celNumberInput()" ></div>
							<div class="panel-body padding-top-zero"><?=$this->lang->line('teaming_form_institution_suggestion');?>: <input type="text" class="teaming-fields redify" name="institutionname" id="institution" ng-change="toBlack('#institution')" ng-model="formData.institutionName" maxlength="70" style="max-width: 100%;"></div>
						</div>
						*Este documento tendrá validez una vez que se encuentre firmado.<br>
						Para realizar consultas y sugerir nuevas instituciones, escribinos a: <a href="mailto:teaming@cat-technologies.com" style="color: rgb(66,166,42); text-decoration: underline;">teaming@cat-technologies.com</a><br><br><br>
					</div>
					<div ng-bind-html="errors | HtmlSanitize" style="text-align: left;"></div>
				</div>
			</div>
			<div class="buttons">
				<button type="submit" name="update" ng-click="update()" class="btn btn-primary" id="first-try"><?=$this->lang->line('teaming_send');?></button>
				<a type="submit" name="submit" href="#/teaming/sendSuggestion" target="_blank" class="btn btn-primary" id="first-try"><?=$this->lang->line('teaming_suggest_institution');?></a>
			</div>
			<div class="sep" style="margin-bottom:  120px">
				&nbsp;
			</div>
		</form>
	</div>
	<img class="img-responsive pull-right" id="big-heart-form" src="/<?php echo APPFOLDERADD; ?>/libraries/images/corazon_grande.png">
</div>
