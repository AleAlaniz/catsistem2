<div class="back-teaming">
	<?php $header ?>
	<div class="container-fluid" id="discharge-panel">
		<?php echo $navbar;?>
		<div class="panel panel-default" style=" overflow: auto;">
			<div class="panel-heading" style="background-color: rgb(66,166,42); font-family: Segoe UI Symbol;">
				<?=$this->lang->line('teaming_newsletter_reports');?>
			</div>
			<div class="panel-body">
				<div class="col-md-3 span-2 input-group pull-left">
					<div class="input-group-addon">
						<i class="fa fa-search" aria-hidden="true"></i>
					</div>
					<input type="text" name="name" value="" ng-model="f.title" class="form-control span-2" aria-describedby="sizing-addon3" placeholder = "<?=$this->lang->line('general_comm_news_report_search_by_name');?>">
				</div>
				<div class="col-md-1"></div>
				<!-- datepicker -->
				<div class="col-md-4 well well-sm" id="sandbox-container">
					<p><b><?=$this->lang->line('general_comm_report_date_range_search');?></b></p>
					<div class="input-daterange input-group" id="datepicker" style ="margin-bottom: 2em;">
						<p style="width: 50%;display: inline-block;"><b><?=$this->lang->line('general_comm_report_date_from');?></b></p>
						<p style="width: 50%; display: inline;"><b><?=$this->lang->line('general_comm_report_date_until');?></b></p>
						<input type="text" class="input-sm form-control" id="start" style="width: 45%;margin-right: 1.5em;">
						<input type="text" class="input-sm form-control" id="end" style="width: 45%;">
					</div>
				</div>
				<!-- end of datepicker -->
				<table class="table table-responsive ng-hide" ng-show="!loading" style="font-family: Segoe UI Symbol;">
					<thead>
						<tr class="col-6-sm">
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr class="col-6-sm">
							<td>
								<?=$this->lang->line('general_comm_news_report_total_news');?>
							</td>
							<td>
								{{totalNewsletters}}
							</td>
						</tr>
						<tr class="col-6-sm">
							<td>
								<?=$this->lang->line('general_comm_news_report_news');?>
							</td>
							<td>
								<?=$this->lang->line('general_comm_msg_report_recipients');?>
							</td>
						</tr>
						<tr ng-repeat="newsletter in teamingNews | filter:f" class="col-6-sm">
							<td>
								<div>
									<span>
										<?=$this->lang->line('general_comm_msg_report_subject');?> {{newsletter.title}}<br>
										<?=$this->lang->line('general_comm_news_report_creation_date');?>{{newsletter.creation_date}}
									</span>
									<br>
								</span>
							</div>
						</td>
						<td id="recipients"><button class="btn" data-target="#demo{{$parent.$index+($index+1)}}" data-toggle="collapse">Mostrar datos de destinatarios</button>
							<div id="demo{{$parent.$index+($index+1)}}" class="collapse well">
								<table class="table table-responsive well">
									<thead>
										<th>
											<?=$this->lang->line('general_comm_msg_recipient_name');?>
										</th>
										<th>
											<?=$this->lang->line('general_comm_msg_recipient_saw');?>
										</th>
										<th>
											<?=$this->lang->line('general_comm_msg_recipient_isJoinedNow');?>
										</th>
									</thead>
									<tbody ng-repeat="user in newsletter.recipients">
										<td>
											<span >{{user.name}}</span>
										</td>
										<td>
											<span >{{(user.saw)? "Si" : "No"}}</span>
										</td>
										<td>
											<span >{{(user.isApproved)? "Si" : "No"}}</span>
										</td>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="text-center col-md-12" ng-if="loading">
				<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
			</div>
		</div>
	</div>
</div>
<div class="sep">
	&nbsp;
</div>
<script>
	$('#communicationReports').replaceWith("<li><?php echo $this->lang->line('general_comm_report');?></li>");
</script>
</div>
