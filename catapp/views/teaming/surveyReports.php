<div class="back-teaming">
	<?php $header ?>
	<div class="container-fluid" id="discharge-panel">
		<?php echo $navbar;?>
		<div class="panel panel-default" style=" overflow: auto;">
			<div class="panel-heading" style="background-color: rgb(66,166,42); font-family: Segoe UI Symbol;">
				<?=$this->lang->line('teaming_survey_reports');?>
			</div>
			<div class="panel-body" ng-show="!loading" class="ng-hide">
				<table class="table table-responsive">
					<thead>
						<th>
							<?=$this->lang->line('general_comm_srv_name');?>
						</th>
						<th>
							<?=$this->lang->line('general_comm_srv_data');?>
						</th>
					</thead>
					<tbody ng-repeat="survey in surveys">
						<td>
							{{survey.name}}
						</td>
						<td>
							<a class="btn btn-green" href="#/surveys/report/{{survey.surveyId}}">
								<i class="fa fa-bar-chart"></i> <?php echo $this->lang->line('report'); ?>
							</a>
							<a class="btn btn-lightgreen" href="#/surveys/details/{{survey.surveyId}}">
								<i class="fa fa-search"></i> <?php echo $this->lang->line('details'); ?>
							</a>
						</td>
					</tbody>
				</table>
			</div>
			<div class="text-center col-md-12" ng-if="loading">
				<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
			</div>
		</div>
	</div>
	<div class="sep">
		&nbsp;
	</div>
</div>
<script>
	$('#communicationReports').replaceWith("<li><?php echo $this->lang->line('general_comm_report');?></li>");
</script>


