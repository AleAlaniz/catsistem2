<?php 
if (isset($_SESSION['flashMessage'])){ 
	?>
	<div class="alert alert-success" role="alert">
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['flashMessage'] == 'editpassword'){
			echo $this->lang->line('administration_users_editpasswordmessage');
		}
		?>
	</div>
	<?php 
}
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_users_resetpassword');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST" novalidate >
			<div class="form-group">
				<label for="userName" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_username');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" id="userName" name="userName" value="<?php echo set_value('userName');?>" placeholder="<?=$this->lang->line('administration_users_username');?>" required>
					<?php 
					if (isset($_SESSION['nouserMessage'])) {
						echo "<p class='text-danger'>".$this->lang->line('administration_users_resetpasswordnouser')."</p>";
					}
					echo form_error('userName'); 
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_newpassword');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="password" class="form-control input-sm" id="password" name="password" value="<?php echo set_value('password');?>" placeholder="<?=$this->lang->line('administration_users_newpassword');?>" required>
					<?php echo form_error('password'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="passwordConfirm" class="col-sm-2 control-label"><?=$this->lang->line('administration_users_passwordconfirm');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="password" class="form-control input-sm" id="passwordConfirm" name="passwordConfirm" value="<?php echo set_value('passwordConfirm');?>" placeholder="<?=$this->lang->line('administration_users_passwordconfirm');?>" required>
					<?php echo form_error('passwordConfirm'); ?>
				</div>
			</div>
		
			<hr>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$('#nav_administration').addClass('active');
</script>