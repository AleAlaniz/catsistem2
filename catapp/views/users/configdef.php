<?php
if ($this->input->get('cmd') == 'alert') {
	?>
	<div class="alert alert-danger" role="alert">
		<strong><i class="fa fa-info-circle fa-lg"></i></strong> &nbsp; 
		<?php echo $this->lang->line('administration_users_alert');	?>
	</div>
	<?php
}
?>
<div class="panel panel-default">
	<div class="panel-heading"><strong><?=$this->lang->line('general_config');?></strong></div>
	<ul class="list-group">
		<?php 
		if($this->Identity->Validate('users/config/data')) {
			?>
			<li class="list-group-item">
				<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/users/config?cmd=data"><i class="fa fa-cog "></i> <?=$this->lang->line('administration_config');?></a>
				<strong class="text-primary"><?=$this->lang->line('administration_users_data');?></strong> - <?=$this->lang->line('administration_users_datamessage');?>
			</li>
			<?php 
		} 
		if($this->Identity->Validate('users/config/photo')) { ?>
			<li class="list-group-item">
				<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/users/config?cmd=photo"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
				<strong class="text-primary"><?=$this->lang->line('administration_users_photo');?></strong> - <?=$this->lang->line('administration_users_photomessage');?>
			</li>
			<?php 
		}
		if($this->Identity->Validate('users/config/password')) { ?>
			<li class="list-group-item">
				<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/users/config?cmd=password"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
				<strong class="text-primary"><?=$this->lang->line('administration_users_password');?></strong> - <?=$this->lang->line('administration_users_passwordmessage');?>
			</li>
			<?php 
		} 
		if($this->Identity->Validate('users/config/select_photo')) { ?>
			<li class="list-group-item">
				<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/users/config?cmd=selectphoto"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
				<strong class="text-primary"><?=$this->lang->line('administration_select_photo');?></strong> - <?=$this->lang->line('administration_select_photo_message');?>
			</li>
			<?php 
		} 
		if($this->Identity->Validate('users/config/background')) { ?>
			<li class="list-group-item">
				<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/users/config?cmd=background"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
				<strong class="text-primary"><?=$this->lang->line('administration_users_selectbackground');?></strong> - <?=$this->lang->line('administration_users_selectbackground_message');?>
			</li>
			<?php 
		} 
		if($this->Identity->Validate('users/security_questions')) { ?>
			<li class="list-group-item">
				<a class="pull-right btn btn-info btn-xs btn-linear-info" href="/<?=FOLDERADD?>/securityquestions/my_security_questions"><i class="fa fa-cog"></i> <?=$this->lang->line('administration_config');?></a>
				<strong class="text-primary"><?=$this->lang->line('question_security_questions');?></strong> - <?=$this->lang->line('question_config_details');?>
			</li>
			<?php }?>
	</ul>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#nav_config').addClass('active');
	});
</script>