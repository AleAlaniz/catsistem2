<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/users"><?=$this->lang->line('general_users');?></a></li>
	<li><a href="/<?=FOLDERADD?>/users/details/<?=$userId;?>"><?=encodeQuery($lastName);?> <?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_delete');?></li>
</ol>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('administration_delete_user');?></strong>
		</div>
		<div class="panel-body">
			<h4><?=$this->lang->line('administration_users_delete_areyousure');?></h4>
			<hr>
			<dl class="dl-horizontal col-sm-6">
				<dt><?=$this->lang->line('administration_users_name');?></dt>
				<dd><?=encodeQuery($name)?></dd>
				<dt><?=$this->lang->line('administration_users_lastname');?></dt>
				<dd><?=encodeQuery($lastName)?></dd>
				<dt><?=$this->lang->line('administration_users_role');?></dt>
				<dd><?=$role;?></dd>
				<dt><?=$this->lang->line('general_site');?></dt>
				<dd><?=encodeQuery($site)?></dd>
			</dl>
			<dl class="dl-horizontal col-sm-6">
				<dt><?=$this->lang->line('general_section');?></dt>
				<dd><?=encodeQuery($section)?></dd>
				<dt><?=$this->lang->line('administration_users_username');?></dt>
				<dd><?=encodeQuery($userName)?></dd>
				<dt><?=$this->lang->line('administration_users_active');?></dt>
				<dd><?php echo ($active == 1) ? "Si" : "No" ; ?></dd>
			</dl>
			<form method="POST"  >
				<input type="hidden" name="userId" value="<?=$userId?>">
				<div class="form-group text-center col-xs-12">
					<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_delete');?></button>
					<a href="/<?=FOLDERADD?>/users" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#nav_users').addClass('active');
	$('#userNavDelete').addClass('active');
</script>