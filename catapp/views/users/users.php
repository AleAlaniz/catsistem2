<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">
<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('general_users');?></li>
</ol>
<?php if($this->Identity->Validate('users/create')) { ?>
<p>
	<a href="/<?=FOLDERADD?>/users/create" class="btn btn-sm btn-success "><i class="fa fa-plus"></i><strong> <?=$this->lang->line('administration_create');?></strong></a>
</p>
<?php }
if (isset($_SESSION['userMessage'])): ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['userMessage'] == 'create'){
		echo $this->lang->line('administration_users_successmessage');
	}
	elseif ($_SESSION['userMessage'] == 'delete'){
		echo $this->lang->line('administration_users_deletemessage');
	}
	
	?>
</div>
<?php endif; ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('general_users');?></strong> 
		<span class="badge pull-right"></span>
	</div>
	<div class="panel-body">
		<table class="table table-hover">
			<thead>
				<tr class="active  form-inline">
					<th></th>
					<th><?=$this->lang->line('administration_users_name');?></th>
					<th><?=$this->lang->line('administration_users_lastname');?></th>
					<th><?=$this->lang->line('administration_users_username');?></th>
					<th><?=$this->lang->line('administration_users_role');?></th>
					<th><?=$this->lang->line('administration_users_active');?></th>
					<th></th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('#nav_users').addClass('active');

		$('.table').DataTable( {
			"processing": true,
			"serverSide": true,
			"ajax":{
        			url :"users/getUsers", // json datasource
					type: "post",  // method  , by default get	
				},
				language: {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ usuarios",
					"sZeroRecords":    "<i class='fa fa-users'></i> No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando usuarios del _START_ al _END_ de un total de _TOTAL_ usuarios",
					"sInfoEmpty":      "Mostrando usuarios del 0 al 0 de un total de 0 usuarios",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				},

				"columns":[
				null,
				null,
				null,
				null,
				null,
				null,
				{
                 "sortable": false,
                 "render": function ( data, type, full, meta ) {

                     var user_id = full[0];
                     return '<?php if($this->Identity->Validate('users/details')) { ?><a href="/<?=FOLDERADD?>/users/details/'+user_id+'"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>&nbsp; <?php } ?><?php if($this->Identity->Validate('users/edit')) { ?><a href="/<?=FOLDERADD?>/users/edit/'+user_id+'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp; <?php } ?><?php if($this->Identity->Validate('users/delete')) { ?><a href="/<?=FOLDERADD?>/users/delete/'+user_id+'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a><?php } ?><?php if($this->Identity->Validate('users/config/config_photo')) { ?> &nbsp; <a href="/<?=FOLDERADD?>/users/userimages/'+user_id+'"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></a><?php } ?>';
                 }
             	},
				],
				"columnDefs": [
            	{
            	    "targets": [ 0 ],
            	    "visible": false,
            	    "searchable": false
            	}],
			});
	});
</script>

























