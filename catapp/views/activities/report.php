<div class="page-title">
	<h5 class="title">
		<?php echo $this->lang->line('survey_report'); ?>	
	</h5>
	<?php if($this->Identity->Validate('activities/manage')) 
	{ 
		?>
		<a href="#/activities" class="btn  btn-white pull-right">
			<i class="fa fa-chevron-left"></i> 
			<?=$this->lang->line('general_goback');?>
		</a>
		<?php 
	}
	?>
</div>

<div class="col-xs-12 flat-style">

	<div class="panel" role="loading" ng-show="loading">
		<div class="panel-body text-center">
			<i class='fa fa-refresh fa-spin fa-5x fa-fw dark' ></i>
		</div>
	</div>


	<div class="panel" ng-show="activity && !loading">
		<div class="panel-custom-heading">
			<h4 class="f-w-500" ng-bind="activity.name"></h4>
		</div>
		<div class="panel-body">
			<button class="btn bg-lightgreen" ng-if="activity.answered.length > 0" ng-click="exportData()"><i class="fa fa-file-excel-o"></i> <?php echo $this->lang->line('survey_export_report');?></button>
			<div class="details-list">
				<div class="details-item">
					<div class="details-label">
						<?php echo $this->lang->line('activity_report_total_assist'); ?>
					</div>	
					<div class="details-detail" ng-bind="activity.answered.length"></div>
						<div ng-if="activity.answered.length > 0">
							<button class="btn" ng-click="displayTables('cTable')" title="<?=$this->lang->line('general_showhide_content');?>"><i ng-bind="openCtable ? 'Ocultar detalles' : 'Mostrar detalles'">	</i></button>	
							<div id="cTable">
								<table id="completesTable" class="table table-hover" role="dataTable">
									<thead>
										<tr class="active  form-inline" style="border: 1px solid;background-color:#ABCECE;">
											<th><?=$this->lang->line('teaming_form_id');?></th>
											<th><?=$this->lang->line('survey_user_name');?></th>
											<th><?=$this->lang->line('survey_user_lastname');?></th>
											<th><?=$this->lang->line('administration_users_campaign');?></th>
											<th><?=$this->lang->line('subcampaign');?></th>
											<th><?=$this->lang->line('general_site');?></th>
											<th><?=$this->lang->line('survey_user_sex');?></th>
											<th><?=$this->lang->line('survey_user_birthday');?></th>
											<th><?=$this->lang->line('survey_user_date');?></th>
											<th><?=$this->lang->line('administration_users_turn');?></th>
											<th><?=$this->lang->line('activity_answer');?></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="user in activity.answered" style="border: 1px solid">
											<td ng-bind="user.dni"></td>
											<td ng-bind="user.name"></td>
											<td ng-bind="user.lastName"></td>
											<td ng-bind="user.campaign"></td>
											<td ng-bind="user.subcampaign"></td>
											<td ng-bind="user.site"></td>
											<td ng-bind="user.gender"></td>
											<td ng-bind="user.birthDate"></td>
											<td ng-bind="user.date"></td>
											<td ng-bind="user.turn"></td>
											<td ng-bind="user.answer"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
				</div>

				<div class="details-item" style="margin:40px 0">
					<div class="details-label">
						<?php echo $this->lang->line('activity_report_total_not_assist'); ?>
					</div>	
					<div class="details-detail" ng-bind="activity.notAnswered.length"></div>
					<div ng-if="activity.notAnswered.length > 0">
						<button class="btn" ng-click="displayTables('iTable')" title="<?=$this->lang->line('general_showhide_content');?>"><i ng-bind="openItable ? 'Ocultar detalles' : 'Mostrar detalles'"></i></button>
						<div id="iTable">
							<table id="incompletesTable" class="table table-hover">
								<thead>
									<tr class="active  form-inline" style="border: 1px solid;background-color:#ABCECE;">
										<th><?=$this->lang->line('teaming_form_id');?></th>
										<th><?=$this->lang->line('survey_user_name');?></th>
										<th><?=$this->lang->line('survey_user_lastname');?></th>
										<th><?=$this->lang->line('administration_users_campaign');?></th>
										<th><?=$this->lang->line('subcampaign');?></th>
										<th><?=$this->lang->line('general_site');?></th>
										<th><?=$this->lang->line('survey_user_sex');?></th>
										<th><?=$this->lang->line('survey_user_birthday');?></th>
										<th><?=$this->lang->line('administration_users_turn');?></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="user in activity.notAnswered" style="border: 1px solid">
										<td ng-bind="user.dni"></td>
										<td ng-bind="user.name"></td>
										<td ng-bind="user.lastName"></td>
										<td ng-bind="user.campaign"></td>
										<td ng-bind="user.subcampaign"></td>
										<td ng-bind="user.site"></td>
										<td ng-bind="user.gender"></td>
										<td ng-bind="user.birthDate"></td>
										<td ng-bind="user.turn"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div> 
			</div>
		</div>
