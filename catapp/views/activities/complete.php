<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/animate.min.css" rel="stylesheet">

<div style="margin-top: 20px;" ng-app="activities" ng-controller="form">
	<?php
	if ($required == 'TRUE' && !$isManual) {
		?>
		<div class="modal fade" id="message" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-sm ">
				<div class="modal-content">
					<div class="modal-body">
						<p><?php echo $this->lang->line('activity_complete_message')?></p>
						<div class="text-right">
							<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	?>
	<?php
		$flash_message = $this->session->flashdata('flash_message');
		if(in_array($flash_message,array('notUser','userInCompleteactivity','activityCompleted')))
		{
			$color = 'red';
			$icon = 'fa-exclamation-triangle';
			switch ($flash_message) {
				case 'notUser':
					$text = $this->lang->line('notUser');
					break;
				
				case 'userInCompleteactivity':
					$text = $this->lang->line('userInCompleteactivity');
					break;

				case 'activityCompleted':
					$color = 'green';
					$icon = 'fa-check';
					$text = $this->lang->line('surveyCompleted');
					break;
			}
			echo('<div class="alert bg-'.$color.' alert-dismissible" role="alert" ng-show="!message_notUser">
					<button type="button" class="close" ng-click="message_notUser = true"><span aria-hidden="true">&times;</span></button>
					<strong><i class="fa '.$icon.'"></i></strong> <span>'.$text.'</span>
				 </div>');
			}?>

	<form method="POST" class="form-horizontal" action="#" id="form-activity">
		<input type="hidden" name="activity" value="<?php echo $activityId; ?>" />

		<?php if(isset($isManual) && $isManual)
		{?>
		<input type="hidden" id="selectedUser" name="user_id" ng-value="users[0].userId" />

		<?php
		}
		else
		{ ?>

		<input type="hidden" id="selectedUser" name="user_id" value="<?php echo $this->session->UserId ?>" />
		<?php 
		} ?>
		
		<div class="panel-body">
            <div class="jumbotron bg-white">
                <div class="container">
                    <div class="card">
                        <img class="card-img-top center-block" src="/<?php echo FOLDERADD;?>/catapp/_activities_images/<?php echo encodeQuery($image); ?>" alt="" class="img-fluid">
                        <div class="card-body">
                            <h2 class="card-title"><?php echo encodeQuery($name); ?></h2>
                            <hr/>
                            <p class="card-text"><?php echo encodeQuery($description); ?></p>
                        </div>
                    </div>
                </div>
                
			</div>

			<?php
			if (isset($isManual) && $isManual) {
				?>
				<div class="jumbotron bg-white">
					<div>
					<div class="container-fluid"><?php echo $this->lang->line('survey_insert_user');?>
					</div>
					<br>
					<div class="container-fluid">
						<div class="select-container container" ng-click="selectClick($event);">
							<i class="fa fa-spin fa-refresh pull-right " style="position:absolute; top:13px;right:25px" ng-show="finding"></i>
							<span class="select-selected">
								<span class="select-selected-item" ng-repeat="user in users track by $index" ng-init="i=$index">
									<span cn-square-image cls="selected-item-image" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200"></span>
									<span class="selected-item-label" ng-bind="user.completeName+' <'+user.userName+'>'" ></span>
									<span class="selected-item-delete no-selectable" ng-click="removeUser(i)">&times;</span>
								</span>
							</span>
							<input type="text" autocomplete="off" class="input-sm form-control select-input" ng-trim="true" ng-model="findLike" ng-change="searchUsers('<?php echo $activityId; ?>')" id="findInput" 	placeholder="<?php echo $this->lang->line('general_find');?>"/>
						</div>
						<div class="select-options-container-super" ng-show="userOptions.length > 0">
							<div class="select-options-container">
								<div class="select-option no-selectable" ng-repeat="user in userOptions track by $index" ng-init="i=$index" ng-click="addUser(i)">
									<span cn-square-image cls="selected-option-image" url="/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200"></span>
									<span class="selected-option-label" ng-bind="user.completeName+' <'+user.userName+'>'"></span>
								</div>
							</div>
						</div>
					</div>
					</div>
					<br>
					<div class="container-fluid alert alert-danger"><?php echo $this->lang->line('survey_insert_alert');?>
					</div>
				</div>
				
				<?php
			}
			?>

			<?php
			foreach ($questions as $key => $question) 
			{
				?>
				<div class="panel panel-default" <?php if ($question->required == 'TRUE' ){ echo 'required-q="TRUE"';} ?> >
					<div class="panel-custom-heading bg-white" style="padding-top: 20px;padding-bottom: 20px;" >

						<?php echo"<h3>". $question->question."</h3>";?>

					</div>
					<div class="panel-body" >
						<?php
						if ($question->description) {
							echo "<p>".encodeQuery($question->description)."</p>";
						}

                        foreach ($question->answers as $keyA => $answer) {

                            ?>
                            <div class="radio radio-angular">
                                <label ng-class="{check: questions[<?php echo $question->activityquestionId;?>] == <?php echo $answer->activityanswerId; ?>}">
                                    <input type="radio" name="questions[<?php echo $question->activityquestionId;?>]" value="<?php echo $answer->activityanswerId; ?>" ng-model="questions[<?php echo $question->activityquestionId;?>]">
                                    <?=encodeQuery($answer->answer)?>
                                </label>
                            </div>
                            <?php
                        }
						?>
					</div>
				</div>
				<?php
			}
			?>

			<div class="panel text-center">
				<?php
				if ($closeMessage != NULL) {
					?> 
					<div class="panel-custom-heading bg-black f-w-500" style="padding-top: 20px;padding-bottom: 20px;">

						<?php echo encodeQuery($closeMessage); ?>
					</div>
					<?php
				}
				?>
				<div class="panel-body ">

					<p>
						<?php
						
						if($active == 'TRUE' && ($capacity > 0 || $capacity == NULL))
							{?>	
						<button onclick= "if(document.getElementById('selectedUser').value.length == 0) event.preventDefault();" type="submit" class="btn btn-red btn-lg" ><?=$this->lang->line('survey_ok');?></button>
						<?php }else {?>
						<a href="<?=APPFOLDERADD?>/#/activities" class="btn btn-green btn-lg"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
						<?php }?>
					</p>
				</div>
			</div>
		</div>
	</form>
	<div class="modal animated shake" id="no-complete" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line('survey_answer_all')?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/angular.min.js"></script>
<script type="text/javascript">

	if(typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, ''); 
		}
	}

	angular
	.module("activities", [])
	.controller("form", ["$scope","$http",function($scope, $http) {

		$scope.userOptions = [];
		$scope.findLike = '';
		$scope.searchNumber = 0;
		$scope.searchActual = 0;
		$scope.users = [];

		var url="/<?php echo FOLDERADD ?>/";

		$scope.checkUser=function(event)
		{
			console.log($scope.users[0].length);
			if($scope.users[0].length == 0)
				event.preventDefault();
		}

		$("#form-activity").submit(function (e) {

			var /*questions = $('input[name*="question"]'),*/
			incomplete = false;

			var questions = $('input[name*="question"]');
			jQuery.each(questions, function(i, question) {
                // question = $(question);
                
				if ($('input:checked').length <= 0)
				{
					incomplete=true;
				}
			});


			if(incomplete)
			{
				e.preventDefault();
				$('#no-complete').modal('show');
			}
		});

		$scope.selectClick = function (e) {
			$('.select-input', e.currentTarget).focus();
		};

		$scope.addUser = function(i) {
				$scope.users = [];
				$scope.users.push($scope.userOptions[i]);
				$scope.userOptions = [];
				$scope.findLike = '';
				
			};

			$scope.removeUser = function(i) {
				$scope.users.splice(i, 1);
			};

			$scope.searchUsers = function(activityid) {
				if($scope.findLike != ''){

					$scope.finding = true;
					var thisSearch = $scope.searchNumber;
					$scope.searchNumber = $scope.searchNumber+1;
					$.ajax({
						method 	: "POST",
						url		: url+'activity/getUsers',
						data 	: {'like': $scope.findLike,'activityId': activityid},
						dataType : 'json'
					}).done(function(data) {
						if(data.status == 'ok' && thisSearch >= $scope.searchActual && $scope.findLike != ''){

							$scope.searchActual = thisSearch;
							$scope.$apply(function() {
								$scope.userOptions = data.message;
								$scope.finding = false;
							});

						}
						else if (thisSearch >= $scope.searchActual)
						{
							$scope.searchActual = thisSearch;
							$scope.$apply(function() {
								$scope.finding = false;
								$scope.userOptions = [];
							});
						}
					});
				}
				else
				{
					$scope.userOptions = [];
					$scope.finding = false;
				}
			};


	}]);

	$(document).ready(function() {
		// $('#message').modal('show');
	});
</script>
