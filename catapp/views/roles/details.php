<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">

<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/roles"><?=$this->lang->line('general_roles');?></a></li>
	<li><a href="/<?=FOLDERADD?>/roles/details/<?=$roleId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_details');?></li>
</ol>
<?php if (isset($_SESSION['flashMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['flashMessage'] == 'edit'){
			echo $this->lang->line('roles_editmessage');
		}
		?>
	</div>
<?php endif; ?>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('roles_details');?></strong>
		</div>
		<div class="panel-body">
			<h4><?=$this->lang->line('roles_name');?>: <?=encodeQuery($name)?></h4>
			<table class="table table-hover">
				<thead>
					<tr class="active">
						<th><?=$this->lang->line('roles_permission');?></th>
					</tr>
				</thead>
				<tbody>
					<?php if (count($availablePermissions) > 0) 
					{
						foreach ($availablePermissions as $permission)
						{
							?>
							<tr>
								<td><?=encodeQuery($permission->description)?></td>
							</tr>
							<?php
						} 
					}
					else 
					{
						?>
						<tr class="text-center">
							<td colspan="3"><i class="fa fa-user-plus"></i> <?=$this->lang->line('roles_emptypermissions');?></td>
						</tr>

						<?php
					}?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">

$('#nav_roles').addClass('active');
$('#NavDetails').addClass('active');

$(document).ready(function() {

	$('.table').DataTable({

		language: {

			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ permisos",
			"sZeroRecords":    "<i class='fa fa-users'></i> No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando permisos del _START_ al _END_ de un total de _TOTAL_ permisos",
			"sInfoEmpty":      "Mostrando permisos del 0 al 0 de un total de 0 permisos",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ permisos)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {

				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
	});
});
</script>