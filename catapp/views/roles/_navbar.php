<ul class="nav nav-tabs">
	<?php if($this->Identity->Validate('roles/details')) { ?>
	<li role="presentation" id="NavDetails"><a href="/<?=FOLDERADD?>/roles/details/<?=$roleId;?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_details');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('roles/edit')) { ?>
	<li role="presentation" id="NavEdit"><a href="/<?=FOLDERADD?>/roles/edit/<?=$roleId;?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_edit');?></span></a></li>
	<?php } ?>
	<?php if($this->Identity->Validate('roles/delete')) { ?>
	<li role="presentation" id="NavDelete"><a href="/<?=FOLDERADD?>/roles/delete/<?=$roleId;?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <span class="hidden-xs"><?=$this->lang->line('administration_delete');?></span></a></li>
	<?php } ?>
</ul>