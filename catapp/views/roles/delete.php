<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/roles"><?=$this->lang->line('general_roles');?></a></li>
	<li><a href="/<?=FOLDERADD?>/roles/details/<?=$roleId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_delete');?></li>
</ol>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong><?=$this->lang->line('roles_delete');?></strong>
		</div>
		<div class="panel-body">
			<h4><?=$this->lang->line('roles_deleteareyousure');?></h4>
			<hr>
			<h4><?=$this->lang->line('roles_name');?>: <?=encodeQuery($name)?></h4>
			<table class="table table-hover">
				<thead>
					<tr class="active">
						<th><?=$this->lang->line('roles_permission');?></th>
						<th><?=$this->lang->line('roles_value');?></th>
					</tr>
				</thead>
				<tbody>
					<?php if (count($permissions) > 0) 
					{
						$permissions_post = [];
						
						foreach ($permissions as $permission)
						{
							if($permission->textValue == $this->lang->line('roles_truepermission')){
								$permissions_post[] = $permission->permisonId;
							}
							?>
							<tr>
								<td><?=encodeQuery($permission->description)?></td>
								<td><?=$permission->textValue?></td>
							</tr>
							<?php
						}
					}
					else 
					{
						?>
						<tr class="text-center">
							<td colspan="3"><i class="fa fa-user-plus"></i> <?=$this->lang->line('roles_emptypermissions');?></td>
						</tr>

						<?php
					}?>
				</tbody>
			</table>
			<form method="POST"  >
				<input type="hidden" name="roleId" value="<?=$roleId?>">
				<input type="hidden" name="permissions[]" value='<?=json_encode($permissions_post)?>'>
				<div class="form-group text-center col-xs-12">
					<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_delete');?></button>
					<a href="/<?=FOLDERADD?>/roles" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#nav_roles').addClass('active');
$('#NavDelete').addClass('active');
</script>