<div class="page-title">
	<h3 class="title"><?php echo $this->lang->line('general_corporate_docs'); ?></h3>
	<?php if($this->Identity->Validate('corporatedocs/create') || $this->Identity->Validate('corporatedocs/createcategory')) 
	{ 
		?>
		<div class="btn-group">
			<span aria-expanded="false" class="btn btn-white dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus text-muted"></i> <?php echo $this->lang->line('general_create')?></span>
			<ul class="dropdown-menu dropdown-white dropdown-menu-scale">
				<?php if($this->Identity->Validate('corporatedocs/create')) 
				{ 
					?>
					<li><a href="#/corporatedocs/create"><i class="fa fa-file-text-o"></i>&nbsp; <?php echo $this->lang->line('corporatedocs_create')?></a></li>
					<?php 
				}
				?>
				<?php if($this->Identity->Validate('corporatedocs/createcategory')) 
				{ 
					?>
					<li><a href="#/corporatedocs/createcategory"><i class="fa fa-sitemap"></i>&nbsp; <?php echo $this->lang->line('corporatedocs_createcategory')?></a></li>
					<?php 
				}
				?>
			</ul>
		</div>
		<?php 
	}
	?>
</div>
<div class="col-xs-12 flat-style">
	<div class="alert bg-green" role="alert" ng-show="message.message != '' && message.message != null">
		<button type="button" class="close"  ng-click="message.setMessage('', true)"><span>&times;</span></button>
		<strong>
			<i class="fa fa-check"></i> <span ng-bind="message.message"></span>
		</strong> 
	</div>
	<div class="modal animated shake open" tabindex="-1" id="confirm-delete">
		<div class="modal-dialog modal-sm ">
			<div class="modal-content">
				<div class="modal-body">
					<p ng-show="deleteType == 'category'"><?php echo $this->lang->line('corporatedocs_categorydeleteareyousure'); ?></p>
					<p ng-show="deleteType == 'doc'"><?php echo $this->lang->line('corporatedocs_newsdeleteareyousure'); ?></p>
					<div class="text-right">
						<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
						<span class="btn btn-lightgreen" ng-click="toDelete()" ng-show="!deleting"><?php echo $this->lang->line('general_yes'); ?></span>
						<span class="btn btn-lightgreen disabled" ng-show="deleting"><i class="fa fa-refresh fa-spin"></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal animated" tabindex="-1" id="doc-details">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<dl class="dl-horizontal">
						<dt><?php echo $this->lang->line('corporatedocs_name'); ?></dt>
						<dd ng-bind="doc.name"></dd>
						<dt><?php echo $this->lang->line('corporatedocs_description'); ?></dt>
						<dd ng-bind="doc.description" style="white-space: pre-line;"></dd>
					</dl>
					<div class="checkbox checkbox-angular text-center" ng-show="!loadingView && !doc.isView">
						<label ng-class="{check: doc.isview}" >
							<input type="checkbox" ng-click="checkView(doc)">
							<?php echo $this->lang->line('corporatedocs_checkmessage'); ?>
						</label>
					</div>

					

					<a class="btn btn-block btn-green" ng-class="{disabled : !doc.isView}" href="/<?php echo FOLDERADD; ?>/corporatedocs/open/{{doc.corporatedocId}}" target="_blank">
						<?php echo $this->lang->line('corporatedocs_downloadfile'); ?>
					</a>


					<?php
					if($this->Identity->Validate('corporatedocs/report')) 
					{ 
						?>
						<p class="text-center" ng-show="loadingReport && !doc.report" style="margin-top: 10px;"><i class="fa fa-refresh fa-spin fa-3x"></i></p>
						
						<span class="btn btn-block btn-dark"  style="margin-top: 10px;" ng-show="!doc.report && !loadingReport" ng-click="getReport(doc,'read')">
							<?php echo $this->lang->line('corporatedocs_report'); ?>
						</span>
						<?php
					}
					?>

					<div ng-if="doc.report != null" style="margin-top: 15px">
						
						<ul class="nav nav-tabs">
							<li role="presentation" ng-class="{active : reportOption == 'read'}"><a href="" ng-click="getReport(doc,'read')">Leyeron el documento</a></li>
							<li role="presentation" ng-class="{active : reportOption == 'no_read'}"><a href="" ng-click="getReport(doc,'no_read')">No leyeron el documento</a></li>
						</ul>

						<p class="text-center" ng-show="loadingReport" style="margin-top: 10px;"><i class="fa fa-refresh fa-spin fa-3x"></i></p>
						<div ng-show="!loadingReport" style="margin-top:15px">
							<strong>
								<?php echo $this->lang->line('corporatedocs_total'); ?> 
								<span ng-bind="doc.report.length"></span>
							</strong>
							<table class="table">
								<thead>
									<tr>
										<th><?php echo $this->lang->line('general_user'); ?> </th>
										<th><?php echo $this->lang->line('general_section'); ?> </th>
									</tr>
								</thead>
								<tbody>

									<tr ng-repeat="user in doc.report track by $index">
										<td ng-bind="user.name + ' ' + user.lastName"></td>
										<td ng-bind="user.section"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel">
		<div class="panel-body">
			<div role="containt ng-hide" ng-show="!loading">
				<div ng-repeat="category in categories" class="panel">
					<div class="panel-heading pointer bg-lightgreen" ng-click="category.open = !category.open">
						<?php
						if($this->Identity->Validate('corporatedocs/deletecategory')) 
						{ 
							?>
							<span class="pull-right btn btn-xs white m-r_10 btn-noshadow" ng-click="setDelete('category', category.corporatedoccategoryId)" data-target="#confirm-delete" data-toggle="modal"><i class="fa fa-times  fa-lg"></i></span>
							<?php
						}
						if($this->Identity->Validate('corporatedocs/editcategory')) 
						{ 
							?>
							<a href="#/corporatedocs/editcategory/{{category.corporatedoccategoryId}}" class="pull-right btn btn-xs  m-r_10 white"><i class="fa fa-pencil fa-lg"></i></a>
							<?php
						}
						?>
						<strong ng-bind="category.name"></strong>
					</div>
					<div class="list-group" ng-show="category.open">
						<div class="list-group-item" ng-repeat="doc in category.docs track by $index" ng-class="{ 'list-group-item-success' : doc.isView, 'list-group-item-danger' : !doc.isView }">
							<span class="badge bg-green" ng-show="doc.isView"><i class="fa fa-check"></i></span>
							<?php
							if($this->Identity->Validate('corporatedocs/delete')) 
							{ 
								?>
								<span class="pull-right btn btn-xs btn-red m-r_10 btn-noshadow" ng-click="setDelete('doc', doc.corporatedocId)" data-target="#confirm-delete" data-toggle="modal">
									<i class="fa fa-trash"></i>
								</span>
								<?php
							}
							if($this->Identity->Validate('corporatedocs/edit')) 
							{ 
								?>
								<a href="#/corporatedocs/edit/{{doc.corporatedocId}}" class="pull-right btn btn-xs btn-lightgreen m-r_10 btn-noshadow"><i class="fa fa-pencil"></i></a>
								<?php
							}
							?>
							<span ng-bind="doc.name" class="pointer"  ng-click="setSelected(doc)" data-target="#doc-details" data-toggle="modal"></span>
						</div>
						<div class="list-group-item text-center" ng-hide="category.docs.length > 0">
							<?php echo $this->lang->line('corporatedocs_empty'); ?>
						</div>
					</div>
				</div>
				<div class="list-group" ng-show="docs.length > 0">
					<div class="list-group-item" ng-repeat="doc in docs track by $index" ng-class="{ 'list-group-item-success' : !(!doc.isView), 'list-group-item-danger' : !doc.isView }">
						<span class="badge bg-green" ng-show="doc.isView"><i class="fa fa-check"></i></span>
						<?php
						if($this->Identity->Validate('corporatedocs/delete')) 
						{ 
							?>
							<span class="pull-right btn btn-xs btn-red m-r_10 btn-noshadow" ng-click="setDelete('doc', doc.corporatedocId);" data-target="#confirm-delete" data-toggle="modal">
								<i class="fa fa-trash"></i>
							</span>
							<?php
						}
						if($this->Identity->Validate('corporatedocs/edit')) 
						{ 
							?>
							<a href="#/corporatedocs/edit/{{doc.corporatedocId}}" class="pull-right btn btn-xs btn-lightgreen m-r_10 btn-noshadow"><i class="fa fa-pencil"></i></a>
							<?php
						}
						?>
						<span ng-bind="doc.name" class="pointer"  ng-click="setSelected(doc)" data-target="#doc-details" data-toggle="modal"></span>
					</div>
				</div>

				<div class="alert bg-dark ng-hide" role="alert" ng-hide="categories.length > 0 || docs.length > 0" style="margin: 0 15px">
					<i class="fa fa-info-circle fa-lg"></i>
					<?php echo $this->lang->line('corporatedocs_empty');?>
				</div>
			</div>
			<div class="text-center" ng-if="loading">
				<i class='fa fa-refresh fa-spin fa-5x fa-fw dark'></i>
			</div>
		</div>
	</div>
</div>