<div class="page-title">
	<h3 class="title"><?=$this->lang->line('corporatedocs_editcategory');?></h3>
	<a href="#/corporatedocs" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">
	<div class="panel panel-default">
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate ng-submit="editCategory()" ng-show="categorydata != null">
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('corporatedocs_info'); ?></h3>
				<hr style="margin-top: 10px; "/>
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('corporatedoccategory_name');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-sm" id="name" name="name" ng-model="categorydata.name" placeholder="<?=$this->lang->line('corporatedoccategory_name');?>" required>
					</div>
				</div>
				<hr>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-green" id="corporatedocs_editcategory"><?=$this->lang->line('general_save');?></button>
				</div>
			</form>
			<div class="text-center" ng-show="categorydata == null">
				<i class='fa fa-refresh fa-spin fa-5x fa-fw' ></i>
			</div>
			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('corporatedoccategory_completeall	')?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
