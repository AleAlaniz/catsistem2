<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">

<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('question_security_questions');?></li>
</ol>

<?php if($this->Identity->Validate('users/security_questions/adm_create')) { ?>
<p>
	<a href="/<?=FOLDERADD?>/securityquestions/create" class="btn btn-sm btn-success "><i class="fa fa-plus"></i><strong> <?=$this->lang->line('administration_create');?></strong></a>
</p>

<div class="modal animated shake" id="delete_modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm ">
		<div class="modal-content">
			<div class="modal-body">
				<p role="message"><?= $this->lang->line('question_are_you_sure_delete') ?></p>
				<form action="/<?=FOLDERADD?>/securityquestions/delete" class="text-right" method="POST">
					<input name="securityquestionId" id="question_to_delete" type="hidden" value="-1"></input>
					<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_no'); ?></span>
					<button type="submit" class="btn btn-lightgreen" name="delete" role="delete"><?php echo $this->lang->line('general_yes'); ?></button>
				</form>
			</div>
		</div>
	</div>
</div>

<?php }
if (isset($_SESSION['save_success']))
{
	$success = ($_SESSION['save_success']['state'] == 'success');

	?>
	<div class="alert alert-<?= ($success) ? 'success' : 'danger' ?> alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-<?= ($success) ? 'check' : 'times' ?>"></i></strong>
		<?= $_SESSION['save_success']['message'] ?>
	</div>
	<?php 
}?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('question_security_questions');?></strong>
		<span class="badge pull-right bg-success"><?=count($questions)?></span>
	</div>
	<div class="panel-body">

		<?php if(count($questions) > 0)
		{?>
			<table class="table table-hover">
				<thead>
					<tr class="active">
						<th><?=$this->lang->line('question_question');?></th>
						<th><?=$this->lang->line('question_created_by');?></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($questions as $question){ ?>
					<tr class="optionsUser">
						<td><?=encodeQuery($question->question)?></td>
						<td><?=encodeQuery($question->name.' '.$question->lastName)?></td>
						<td>
							<?php if($this->Identity->Validate('users/security_questions/adm_edit') && !$question->inUse) { ?>
							<a href="/<?=FOLDERADD?>/securityquestions/edit/<?=$question->securityquestionId?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp; 
							<?php } ?>
							<?php if($this->Identity->Validate('users/security_questions/adm_delete') && !$question->inUse) { ?>
							<span class="btn btn-xs white" role="deletebutton" data-target="#confirm-delete" data-toggle="modal" data-id="<?= $question->securityquestionId ?>" data-type="category"><i class="glyphicon glyphicon-trash"></i></span>
							<?php } ?>
						</td>
					</tr>

					<?php } ?>

				</tbody>
			</table>
			<?php
		}
		else
		{
			?>
			<div class="text-center" style="margin-top:10px">
				<span class="fa fa-frown-o fa-2x" style="color:#006687"></span>
				<p><?= $this->lang->line('question_questions_empty') ?></p>
			</div>

			<?php
		}
		?>
	</div>
</div>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.table').DataTable( {
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ preguntas",
				"sZeroRecords":    "<i class='fa fa-circle'></i>  No se encontraron resultados",
				"sEmptyTable":     "<?= $this->lang->line('question_questions_empty')?>",
				"sInfo":           "",
				"sInfoEmpty":      "",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		} );



		$("span[role='deletebutton']").click(showModal);

		function showModal()
		{
			$("#question_to_delete").val($(this).attr("data-id"));
			$("#delete_modal").modal({show:true});
		}
	});


	$('#nav_security_questions').addClass('active');
</script>