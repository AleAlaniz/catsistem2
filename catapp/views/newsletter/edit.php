<div class="page-title">
	<h3 class="title"><?=$this->lang->line('newsletter_editnewsletter');?></h3>
	<a href="#/newsletter" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">
	
	<div class="panel panel-default">
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate enctype="multipart/form-data" ng-show="newsletterdata != null" style="display:none" id="editNewsletter" ng-submit="editNewsletter()">
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('newsletter_info'); ?></h3>
				<hr style="margin-top: 10px; "/>

				<div class="form-group">
					<label for="title" class="col-sm-2 control-label"><?=$this->lang->line('newsletter_ntitle');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-sm" id="title" name="title" ng-model="newsletterdata.title" placeholder="<?=$this->lang->line('newsletter_ntitle');?>">
					</div>
				</div>
				<div class="form-group">
					<label for="section" class="col-sm-2 control-label"><?=$this->lang->line('newsletter_category');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<select class="form-control input-sm" id="category" name="category" ng-model="newsletterdata.category">
							<?php
							foreach ($categories as $category) {
								?>
								<option value="<?php echo $category->newslettercategoryId; ?>"><?php echo $category->name; ?></option>
								<?php
							} 
							?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="color" class="col-sm-2 control-label"><?=$this->lang->line('newsletter_color');?></label>
					<div class="col-sm-10">
						<div class="input-group">
							<div class="input-group-addon" ng-class="'bkgr-'+newsletterdata.color"></div>
							<select class="form-control" name="color" ng-model="newsletterdata.color">
								<option value="info">Celeste</option>
								<option value="danger">Rojo</option>
								<option value="success">Verde</option>
								<option value="primary">Azul</option>
								<option value="warning">Amarillo</option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group" >
					<label for="section" class="col-sm-2 control-label"><?=$this->lang->line('newsletter_newsletter');?><span class="text-danger strong"> *</span></label>
					<div class="col-sm-10">
						<textarea class="form-control" id="newsletter" name="newsletter" rows="10" ng-model="newsletterdata.newsletter"></textarea>
					</div>
				</div>

				<div class="form-group">
					<label for="userfile" class="col-sm-2 control-label"><?=$this->lang->line('newsletter_file');?></label>
					<div class="col-sm-10">
						<input type="file" id="userfile" name="userfile"><small><?=$this->lang->line('newsletter_file_allowtypes')?></small>
					</div>
				</div>
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('newsletter_link'); ?> <strong class="text-danger">*</strong></h3>
				<hr style="margin-top: 10px; "/>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_sections');?></label>
					<div class="col-sm-10">
						<?php
						foreach ($sections as $section) {
							?>
							<div class="checkbox checkbox-angular">
								<label ng-class="{check: sections['id-<?php echo $section->sectionId;?>']}">
									<input type="checkbox" name="sections[]" ng-model="sections['id-<?php echo $section->sectionId;?>']"  value="<?=$section->sectionId?>">
									<?php echo encodeQuery($section->name);?>
								</label>
							</div>
							<?php
						}
						?>

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_sites');?></label>
					<div class="col-sm-10">
						<?php
						foreach ($sites as $site) {
							?>
							<div class="checkbox checkbox-angular">
								<label ng-class="{check: sites['id-<?php echo $site->siteId;?>']}">
									<input type="checkbox" name="sites[]" ng-model="sites['id-<?php echo $site->siteId;?>']"  value="<?=$site->siteId?>">
									<?php echo encodeQuery($site->name);?>
								</label>
							</div>
							<?php
						}
						?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_roles');?></label>
					<div class="col-sm-10">
						<?php
						foreach ($roles as $role) {
							?>
							<div class="checkbox checkbox-angular">
								<label ng-class="{check: roles['id-<?php echo $role->roleId;?>']}">
									<input type="checkbox" name="roles[]" ng-model="roles['id-<?php echo $role->roleId;?>']"  value="<?=$role->roleId?>">
									<?php echo encodeQuery($role->name);?>
								</label>
							</div>
							<?php
						}
						?>

					</div>
				</div>
				<?php
				if ($this->Identity->Validate('usergroups/create')) {
					?>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?=$this->lang->line('general_usergroups');?></label>
						<div class="col-sm-10">
							<?php
							foreach ($userGroups as $userGroup) {
								?>
								<div class="checkbox checkbox-angular">
									<label ng-class="{check: userGroups['id-<?php echo $userGroup->usergroupId;?>']}">
										<input type="checkbox" name="userGroups[]" ng-model="userGroups['id-<?php echo $userGroup->usergroupId;?>']"  value="<?=$userGroup->usergroupId?>">
										<?php echo encodeQuery($userGroup->name);?>
									</label>
								</div>
								<?php
							}
							?>

						</div>
					</div>
					<?php
				}
				?>

				<div class="form-group">
					<input type="hidden" ng-repeat="user in users track by $index" name="users[]" value="{{user.userId}}" />
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_users');?></label>
					<div class="col-sm-10 form-group">
						<div class="select-container" ng-click="selectClick($event);">
							<span class="select-selected">
								<span class="select-selected-item" ng-repeat="user in users track by $index" ng-init="i=$index">
									<span class="selected-item-image" style="background-image: url('/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&amp;amp;wah=200');filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200',sizingMethod='scale');"></span>
									<span class="selected-item-label" ng-bind="user.completeName+' <'+user.userName+'>'" ></span>
									<span class="selected-item-delete no-selectable" ng-click="removeUser(i)">&times;</span>
								</span>
							</span>
							<input type="text" autocomplete="off" class="input-sm form-control select-input" ng-trim="true" ng-model="findLike" ng-change="searchUsers()" id="findInput" placeholder="<?=$this->lang->line('survey_writename');?>" />
						</div>
						<div class="select-options-container-super" ng-show="userOptions.length > 0">
							<div class="select-options-container">
								<div class="select-option no-selectable" ng-repeat="user in userOptions track by $index" ng-init="i=$index" ng-click="addUser(i)">
									<span class="selected-option-image" style="background-image: url('/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&amp;amp;wah=200');filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/<?php echo FOLDERADD; ?>/users/profilephoto?userId={{user.userId}}&wah=200',sizingMethod='scale');"></span>
									<span class="selected-option-label" ng-bind="user.completeName+' <'+user.userName+'>'"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-green" id="newsletter_edit"><?=$this->lang->line('general_save');?></button>
				</div>
			</form>
			<div class="text-center" ng-show="newsletterdata == null">
				<i class='fa fa-refresh fa-spin fa-5x fa-fw' ></i>
			</div>
			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('newsletter_completeall')?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript" src="/<?=APPFOLDERADD?>/libraries/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.form.js"></script>
