<div class="page-title">
	<h3 class="title"><?=$this->lang->line('newsletter_ccategory');?></h3>
	<a href="#/newsletter" class="btn btn-white pull-right"><i class="fa fa-chevron-left"></i> <?=$this->lang->line('general_goback');?></a>
</div>
<div class="col-xs-12 flat-style">
	<div class="panel panel-default">
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate ng-submit="createCategory()">
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('newsletter_info'); ?></h3>
				<hr style="margin-top: 10px; "/>
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('newsletter_name');?><span class="text-danger"><strong> *</strong></span></label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-sm" id="name" name="name" ng-model="categorydata.name" placeholder="<?=$this->lang->line('newsletter_name');?>" >
					</div>
				</div>
				<div class="form-group">
					<label for="color" class="col-sm-2 control-label"><?=$this->lang->line('newsletter_color');?></label>
					<div class="col-sm-10">
						<div class="input-group">
							<div class="input-group-addon" ng-class="'bkgr-'+categorydata.color"></div>
							<select class="form-control" name="color" ng-model="categorydata.color">
								<option value="info">Celeste</option>
								<option value="danger">Rojo</option>
								<option value="success">Verde</option>
								<option value="primary">Azul</option>
								<option value="warning">Amarillo</option>
							</select>
						</div>
					</div>
				</div>
				<hr>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-green" id="newsletter_createcategory"><?=$this->lang->line('general_create');?></button>
				</div>
			</form>
			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('newsletter_completeall')?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>