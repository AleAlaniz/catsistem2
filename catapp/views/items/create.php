<script>
function userselectToggle(argument) {
	argument.click(function() {
		if (argument.parent().attr('id') == 'userFinds') {
			var selectExist = $('[user="'+argument.attr("user")+'"]', $('#userSelects'));
			var answerUser = $('[role="answerUser"]');
			if (selectExist.length > 0 || answerUser.attr('user') == argument.attr("user")) {
				alert('<?=$this->lang->line("inbox_new_usernoselectable");?>');
			}
			else
			{
				var newUser = $('<div/>', {
					'class' : 'btn userFinds-user label label-info',
					'user'    : argument.attr('user'),
				});
				newUser.html(argument.text() + ' <i class="fa fa-times text-danger"></i>');
				$('#userSelects').append(newUser);
				userselectToggle(newUser); 

				var newUserinput = $('<input/>', {
					'type' : 'hidden',
					'name' : 'usersId[]',
					'value'    : argument.attr('user'),
				});
				$('#outForm').prepend(newUserinput);
			}
		}
		else if(argument.parent().attr('id') == 'userSelects'){
			argument.remove();
			$('#outForm [value="'+argument.attr("user")+'"]').remove()
		}
	});
}
</script>
<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/items"><?=$this->lang->line('general_items');?></a></li>
	<li class="active"><?=$this->lang->line('administration_create');?></li>
</ol>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_items_create');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST" novalidate id="outForm">
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_name');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo set_value('name');?>" placeholder="<?=$this->lang->line('administration_items_name');?>" required>
					<?php echo form_error('name'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="subSection" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_subsection');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="subSection" name="subSection">
						<?php foreach($subSections as $subSection): ?>
						<option value="<?=$subSection->subsectionId?>" <?php echo  set_select('subSection', $subSection->subsectionId); ?> ><?=$subSection->name?></option>
					<?php endforeach; ?>
				</select>
				<?php echo form_error('subSection'); ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?=$this->lang->line('administration_items_selectto');?><strong class="text-danger"> *</strong></label>
			<div class="col-sm-10">
				<div class="radio">
					<label >
						<input type="radio" name="toall" value="TRUE" <?php echo set_checkbox('toall', 'TRUE' ,FALSE); ?> >
						<?=$this->lang->line('inbox_new_toall')?>
					</label>
				</div>
				<div class="radio">
					<label >
						<input type="radio" name="toall" value="FALSE" <?php echo set_checkbox('toall', 'FALSE', TRUE); ?> >
						<?=$this->lang->line('inbox_selecto')?>
					</label>
				</div>
				<div id="findUser">
					<div class="form-inline">
						<div class="form-group">
							<input autocomplete="off" type="text" class="form-control input-sm" id="userLike" placeholder="<?=$this->lang->line('inbox_finduser');?>">
						</div>
					</div>
					<p id="userFinds"></p>
					<p id="userSelects"><span><?=$this->lang->line('inbox_new_userselects');?>: </span> </p>
					<?php
					if ($this->Identity->Validate('usergroups/index')) 
					{
						?>
						<p><strong><?=$this->lang->line('administration_usergroups')?></strong></p>
						<?php
						foreach ($userGroups as $usergroupKey => $usergroup) {

							?>
							<div class="checkbox" style="padding-left:21px">
								<label >
									<input type="checkbox" name="usergroupsId[]" value="<?=$usergroup->usergroupId?>" <?php echo set_checkbox('usergroupsId[]', '<?=$usergroup->usergroupId?>'); ?> >
									<?=encodeQuery($usergroup->name)?>
								</label>
							</div>
							<?php
						}
					}
					
					echo form_error('usersId[]'); 
					?>
				</div>


				<?php echo form_error('toall'); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="description" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_description');?></label>
			<div class="col-sm-10">
				<textarea class="form-control" id="description" name="description" rows="10"><?php echo set_value('description');?></textarea>
				<?php echo form_error('description'); ?>
			</div>
		</div>
		<hr>
		<div class="form-group text-center">
			<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
			<a href="/<?=FOLDERADD?>/items" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
		</div>
	</form>
</div>
</div>
<script type="text/javascript" src="/<?=APPFOLDERADD?>/libraries/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
$('#nav_items').addClass('active');
tinymce.init({
	toolbar: "cut copy paste | alignleft aligncenter alignright alignjustify | bullist numlist  | outdent indent | forecolor backcolor fontsizeselect  | bold italic underline | link unlink image media | table",
	plugins: "image link media textcolor table",
	selector: "#description",
	language: 'es'
});
if ($("[value='FALSE']").prop('checked'))
{
	$('#findUser').show();
}
else
{
	$('#findUser').hide();
}

$( "[name='toall']" ).on('change',function() {
	if ($(this).val() == 'FALSE')
	{
		$('#findUser').slideDown();
	}
	else if ($(this).val() == 'TRUE')
	{
		$('#findUser').slideUp();
	}
});
$('#userLike').keydown(function(e){
	var code = e.keyCode || e.which;
	if (code == 13) {
		e.preventDefault();
		return false;
	}
});

$('#userLike').on('keyup paste', function() {

	var datosForm = {
		userLike: $(this).val(),
	};

	$.ajax({
		method: "POST",
		url: '/<?php echo FOLDERADD;?>/home/findusers',
		data: datosForm,
	}).done(function (data) {

		if(data == 'invalid'){
			$(location).attr('href', '/<?php echo FOLDERADD;?>');
		}
		else if (data == 'nolike')
		{
			$('#userFinds').html('');
		}
		else if (data == 'empty') {
			$('#userFinds').html('<?=$this->lang->line("inbox_noresults")?>');
		}
		else
		{
			$('#userFinds').html(data);
		}

	});
});
</script>