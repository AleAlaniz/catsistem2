<form class="form-horizontal" method="POST" novalidate>
	<div class="form-group">
		<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_trivia_name');?> <strong class="text-danger">*</strong></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="name" name="name" value="<?=set_value('name')?>" placeholder="<?=$this->lang->line('administration_items_doc_name');?>" required>
			<?php echo form_error('name'); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="description" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_trivia_description');?></label>
		<div class="col-sm-10">
			<textarea class="form-control" id="description" name="description" rows="10"><?php echo set_value('description');?></textarea>
			<?php echo form_error('description'); ?>
		</div>
	</div>
	<div class="form-group">
		<label for="starttime" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_trivia_starttime');?></label>
		<div class="col-xs-10">
			<select  name="fday" id="fday">
				<?php 
				$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
					
				for ($i=1; $i <= 31; $i++) { 
					$selected = FALSE;
					if ($i == date('d')) {
						$selected = TRUE;
					}
					$zero = NULL;
					if ($i < 10) {
						$zero = 0;
					}
					echo '<option value="'.$zero.$i.'"'.set_select('fday',$zero.$i, $selected).'>'.$zero.$i.'</option>';
				}
				?>
			</select> /
			<select name="fmonth" id="fmonth">
				<?php 
				for ($i=1; $i <= 12; $i++) { 
					$selected = FALSE;
					if ($i == date('m')) {
						$selected = TRUE;
					}
					$zero = NULL;
					if ($i < 10) {
						$zero = 0;
					}
					echo '<option value="'.$zero.$i.'"'.set_select('fmonth',$zero.$i, $selected).'>'.$meses[$i-1].'</option>';
				}
				?>
			</select> /
			<select   name="fyear" id="fyear">
				<?php 
				for ($i=2000; $i <= 2020; $i++) { 
					$selected = FALSE;
					if ($i == date('Y')) {
						$selected = TRUE;
					}
					echo '<option value="'.$i.'"'.set_select('fyear',$i, $selected).'>'.$i.'</option>';
				}
				?>
			</select>
			&nbsp;-&nbsp;
			<select  name="fhour" id="fhour">
				<?php 
				for ($i=0; $i <= 23; $i++) { 
					$selected = FALSE;
					if ($i == date('G')) {
						$selected = TRUE;
					}
					$zero = NULL;
					if ($i < 10) {
						$zero = 0;
					}
					echo '<option value="'.$i.'"'.set_select('fhour',$i, $selected).'>'.$zero.$i.'</option>';
				}
				?>
			</select> : 
			<select name="fminute" id="fminute">
				<?php 
				for ($i=0; $i <= 59; $i++) { 
					$selected = FALSE;
					if ($i == date('i')) {
						$selected = TRUE;
					}
					$zero = NULL;
					if ($i < 10) {
						$zero = 0;
					}
					echo '<option value="'.$zero.$i.'"'.set_select('fminute',$zero.$i, $selected).'>'.$zero.$i.'</option>';
				}
				?>
			</select> (d/m/a hh:mm)
		</div>
		<?php echo form_error('starttime'); ?>
	</div>
	<div class="form-group">
		<label for="endtime" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_trivia_endtime');?></label>
		<div class="col-xs-10">
			<select  name="eday" id="eday">
				<?php 
				for ($i=1; $i <= 31; $i++) { 
					$selected = FALSE;
					if ($i == date('d')) {
						$selected = TRUE;
					}
					$zero = NULL;
					if ($i < 10) {
						$zero = 0;
					}
					echo '<option value="'.$zero.$i.'"'.set_select('eday',$zero.$i, $selected).'>'.$zero.$i.'</option>';
				}
				?>
			</select> /
			<select name="emonth" id="emonth">
				<?php 
				for ($i=1; $i <= 12; $i++) { 
					$selected = FALSE;
					if ($i == date('m')) {
						$selected = TRUE;
					}
					$zero = NULL;
					if ($i < 10) {
						$zero = 0;
					}
					echo '<option value="'.$zero.$i.'"'.set_select('emonth',$zero.$i, $selected).'>'.$meses[$i-1].'</option>';
				}
				?>
			</select> /
			<select   name="eyear" id="eyear">
				<?php 
				for ($i=2000; $i <= 2020; $i++) { 
					$selected = FALSE;
					if ($i == date('Y')) {
						$selected = TRUE;
					}
					echo '<option value="'.$i.'"'.set_select('eyear',$i, $selected).'>'.$i.'</option>';
				}
				?>
			</select>
			&nbsp;-&nbsp;
			<select  name="ehour" id="ehour">
				<?php 
				for ($i=0; $i <= 23; $i++) { 
					$selected = FALSE;
					if ($i == date('G')) {
						$selected = TRUE;
					}
					$zero = NULL;
					if ($i < 10) {
						$zero = 0;
					}
					echo '<option value="'.$i.'"'.set_select('ehour',$i, $selected).'>'.$zero.$i.'</option>';
				}
				?>
			</select> : 
			<select name="eminute" id="eminute">
				<?php 
				for ($i=0; $i <= 59; $i++) { 
					$selected = FALSE;
					if ($i == date('i')) {
						$selected = TRUE;
					}
					$zero = NULL;
					if ($i < 10) {
						$zero = 0;
					}
					echo '<option value="'.$zero.$i.'"'.set_select('eminute',$zero.$i, $selected).'>'.$zero.$i.'</option>';
				}
				?>
			</select> (d/m/a hh:mm)
		</div>
		<?php echo form_error('endtime'); ?>
	</div>
	<div class="form-group text-center">
		<input type="submit" class="btn btn-success btn-sm" value="<?=$this->lang->line('general_save');?>"></input>
		<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
	</div>
</form>
<script type="text/javascript" src="/<?=APPFOLDERADD?>/libraries/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
	toolbar: "cut copy paste | alignleft aligncenter alignright alignjustify | bullist numlist  | outdent indent | forecolor backcolor fontsizeselect  | bold italic underline | link unlink image media | table",
	plugins: "image link media textcolor table",
	language: 'es',
	selector: "#description"
});
</script>