<form class="form-horizontal" method="POST" novalidate  id="editDoc">
	<input type="hidden" name="itemdocId" value="<?= $itemToEdit->itemdocId ?>" />
	<div class="form-group">
		<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_doc_name');?> <strong class="text-danger">*</strong></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo set_value('name', $itemToEdit->name);?>" placeholder="<?=$this->lang->line('administration_items_doc_name');?>" required></input>
		</div>
	</div>
	<div class="form-group">
		<label for="folder" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_doc_folder');?></label>
		<div class="col-sm-10">
			<select class="form-control input-sm" id="folder" name="folder">
				<option value=""><?=$this->lang->line('administration_items_doc_nofolder')?></option>
				<?php foreach($folders as $folder): 
				$folderselected = FALSE;
				if ($folder ->itemdocfolderId == $itemToEdit->itemdocfolderId) {
					$folderselected = TRUE;
				}
				?>
				<option value="<?=$folder->itemdocfolderId?>" <?php echo  set_select('folder', $folder->itemdocfolderId, $folderselected); ?> ><?=$folder->name?></option>
			<?php endforeach; ?>
		</select>
	</div>
</div>
<div class="form-group">
	<label for="comment" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_doc_comment');?></label>
	<div class="col-sm-10">
		<textarea class="form-control" id="comment" name="comment" rows="3"><?php echo set_value('comment', $itemToEdit->comment);?></textarea>
	</div>
</div>
<div class="form-group text-center" id="actionsButtons">
	<button type="submit" id="upload" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
	<a href="#/items/item/<?=$itemId?>/docs" class="btn btn-danger btn-sm"><?=$this->lang->line('general_back');?></a>
</div>
</form>

<div class="form-group text-center" style="display:none" id="loadingUpload">
	<p><i class="fa fa-refresh fa-2x fa-spin"></i></p>
</div>

<div class="modal flat-style animated bounce" id="incomplete" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p><?php echo $this->lang->line('general_completeall_error') ?></p>
				<div class="text-right">
					<button  class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_ok') ?></button>
				</div>
			</div>

		</div>

	</div>

</div>

<div class="modal flat-style animated bounceIn" id="edit" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<p><?php echo $this->lang->line('administration_items_doc_editmessage') ?></p>
				<div class="text-right">
					<button  class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_ok') ?></button>
				</div>
			</div>

		</div>

	</div>

</div>


<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.form.js"></script>


<script type="text/javascript">


$('#upload').on('click',function(event)
{
	event.preventDefault();

	if($('#name').val().trim() === '')
	{
		$('#incomplete').modal('show');
		$('#actionsButtons').show();
		$('#loadingUpload').hide();
	}
	else
	{		
		$('#actionsButtons').hide();
		$('#loadingUpload').show();

		$('#editDoc').ajaxSubmit({
			url : '/'+FOLDERADD+'/items/editdoc',
			success: function(data)
			{
				if (data != 'fail')
				{
					$('#actionsButtons').show();
					$('#loadingUpload').hide();

					$('#edit').modal('show');
				}
				else
				{
					$('#incomplete').modal('show');
				}
			}
		});
	}

});
</script>