<?php if (isset($_SESSION['itemdocsMessage']) && $this->Identity->Validate('items/item/docs/actions')){ ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['itemdocsMessage'] == 'create'){
		echo $this->lang->line('administration_items_doc_createmessage');
	} elseif ($_SESSION['itemdocsMessage'] == 'delete'){
		echo $this->lang->line('administration_items_doc_deletemessage');
	}elseif ($_SESSION['itemdocsMessage'] == 'edit'){
		echo $this->lang->line('administration_items_doc_editmessage');
	}elseif ($_SESSION['itemdocsMessage'] == 'createFolder'){
		echo $this->lang->line('administration_items_doc_createfoldermessage');
	}elseif ($_SESSION['itemdocsMessage'] == 'deleteFolder'){
		echo $this->lang->line('administration_items_doc_deletefoldermessage');
	}elseif ($_SESSION['itemdocsMessage'] == 'editFolder'){
		echo $this->lang->line('administration_items_doc_editfoldermessage');
	}
	?>
</div>
<?php 
}

if (count($docs) <= 0 && count($folders) <= 0) {
	echo $this->lang->line('administration_items_doc_empty');
}
else{ 

	foreach ($folders as $folder) { ?>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title-nomargin">
				<button class="pull-right btn btn-xs openFolder" ><i class="fa fa-minus"></i> / <i class="fa fa-plus"></i></button>

				<?php if($this->Identity->Validate('items/item/docs/actions')) {?>
				<?php if($this->Identity->Validate('items/item/docs/actions/deletefolder')) {?>
				<button id="deletefolderId-<?=$folder->itemdocfolderId?>" class="btn btn-xs pull-right deleteButtonFolder"><i class="text-danger fa fa-times fa-lg"></i></button>	
				<?php }?>	
				<?php if($this->Identity->Validate('items/item/docs/actions/editfolder')) {?>
				<a href="#/items/item/<?= $itemId ?>/docs/folder/<?=$folder->itemdocfolderId?>/edit" class="btn btn-xs pull-right"><i class="text-warning fa fa-pencil fa-lg"></i></a>
				<?php }?>	
				<?php }?>
				<span style="display:none;"><i class="text-danger fa fa-refresh fa-spin fa-lg pull-right"></i></span>

				<i class="fa fa-folder-open-o fa-fw fa-lg"></i>
				<strong>
					<?=encodeQuery($folder->name)?>
				</strong>
			</h6>
		</div>
		<div class="panel-body" id="folderBody-<?=$folder->itemdocfolderId?>" style="display:none">
			<?php if($this->Identity->Validate('items/item/docs/actions/deletefolder')){?>
			<div class="alert alert-danger text-center" role="alert" style="display:none;">
				<div><strong><?=$this->lang->line('administration_items_doc_deletefolderareyousure')?></strong></div>
				<button class="btn btn-default btn-sm deleteOkFolder" id="<?=$folder->itemdocfolderId?>"><?=$this->lang->line('general_delete')?></button>
				<button  class="btn btn-default btn-sm deleteCancelFolder"><?=$this->lang->line('general_cancel')?></button>
			</div>
			<?php }?>
			<?php foreach ($folder->docs as $doc) { ?>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h6 class="panel-title-nomargin ">
						<?php if($this->Identity->Validate('items/item/docs/actions')) {?>
						<?php if($this->Identity->Validate('items/item/docs/actions/delete')) {?>
						<button class="btn btn-xs pull-right deleteButtonDoc"><i class="text-danger fa fa-times fa-lg"></i></button>	
						<?php }?>	
						<?php if($this->Identity->Validate('items/item/docs/actions/edit')) {?>
						<a href="#/items/item/<?= $itemId?>/docs/<?=$doc->itemdocId?>/edit" class="btn btn-xs pull-right"><i class="text-warning fa fa-pencil fa-lg"></i></a>
						<?php }?>	
						<?php }?>
						<span style="display:none;"><i class="text-danger fa fa-refresh fa-spin fa-lg pull-right"></i></span>

						<i class="fa fa-file-o fa-fw fa-lg"></i>
						<strong>
							<?=encodeQuery($doc->name)?>
						</strong>
					</h6>
				</div>
				<div class="panel-body">
					<?php if($this->Identity->Validate('items/item/docs/actions/delete')){?>
					<div class="alert alert-danger text-center" role="alert"  style="display:none;">
						<div><strong><?=$this->lang->line('administration_items_doc_deleteareyousure')?></strong></div>
						<button  class="btn btn-default btn-sm deleteOkDoc" id="<?=$doc->itemdocId?>"><?=$this->lang->line('general_delete')?></button>
						<button  class="btn btn-default btn-sm deleteCancelDoc"><?=$this->lang->line('general_cancel')?></button>
					</div>
					<?php }?>
					<p>
						<?=encodeQuery($doc->comment)?>
					</p>
					<p>
						<strong>
							<?=$this->lang->line('administration_items_doc_size')?>: <?=$doc->size?> KB |
							<?=$this->lang->line('administration_items_doc_lastmodified')?>: <?=date('d-m-Y H:i',$doc->timestamp)?>	
						</strong>
					</p>
					<?php 
					$docType = explode('.', $doc->filename);
					$docType = end($docType);
					if (($docType == 'png' || $docType == 'gif' || $docType == 'jpg' || $docType == 'jpeg' || $docType == 'PNG' || $docType == 'GIF' || $docType == 'JPG' || $docType == 'JPEG') && $this->Identity->Validate('items/item/docs/actions/viewimage')) {
						?>
						<div class="thumbnail">
							<img src="/<?=FOLDERADD?>/items/getimage?itemid=<?=$itemId?>&itemdocid=<?=$doc->itemdocId?>" alt="<?=encodeQuery($doc->name)?>" style="height:200px">
						</div>
						<?php
					}
					elseif($this->Identity->Validate('items/item/docs/actions/download')){ ?>
					<a href="/<?=FOLDERADD?>/items/getdoc?itemid=<?=$itemId?>&itemdocid=<?=$doc->itemdocId?>" target="_blank"  class="btn btn-primary btn-sm">
						<i class="fa fa-download fa-lg"></i> <?=$this->lang->line('general_download')?>
					</a>
					<?php } ?>
				</div>
			</div>

			<?php	} ?>
		</div>
	</div>

	<?php	} ?>
	<?php
	foreach ($docs as $doc) {
		?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h6 class="panel-title-nomargin">
					<?php if($this->Identity->Validate('items/item/docs/actions')) {?>
					<?php if($this->Identity->Validate('items/item/docs/actions/delete')) {?>
					<button class="btn btn-xs pull-right deleteButtonDoc"><i class="text-danger fa fa-times fa-lg"></i></button>	
					<?php }?>	
					<?php if($this->Identity->Validate('items/item/docs/actions/edit')) {?>
					<a href="#/items/item/<?= $itemId ?>/docs/<?=$doc->itemdocId?>/edit" class="btn btn-xs pull-right"><i class="text-warning fa fa-pencil fa-lg"></i></a>
					<?php }?>	
					<?php }?>
					<span style="display:none;"><i class="text-danger fa fa-refresh fa-spin fa-lg pull-right"></i></span>

					<i class="fa fa-file-o fa-fw fa-lg"></i>
					<strong>
						<?=encodeQuery($doc->name)?>
					</strong>
				</h6>
			</div>
			<div class="panel-body">
				<?php if($this->Identity->Validate('items/item/docs/actions/delete')){?>
				<div class="alert alert-danger text-center" role="alert" style="display:none;" 	>
					<div><strong><?=$this->lang->line('administration_items_doc_deleteareyousure')?></strong></div>
					<button  class="btn btn-default btn-sm deleteOkDoc" id="<?=$doc->itemdocId?>"><?=$this->lang->line('general_delete')?></button>
					<button  class="btn btn-default btn-sm deleteCancelDoc"><?=$this->lang->line('general_cancel')?></button>
				</div>
				<?php }?>
				<p>
					<?=encodeQuery($doc->comment)?>
				</p>
				<p>
					<strong>
						<?=$this->lang->line('administration_items_doc_size')?>: <?=$doc->size?> KB |
						<?=$this->lang->line('administration_items_doc_lastmodified')?>: <?=date('d-m-Y H:i',$doc->timestamp)?>	
					</strong>
				</p>
				<?php 
				$docType = explode('.', $doc->filename);
				$docType = end($docType);
				if (($docType == 'png' || $docType == 'gif' || $docType == 'jpg' || $docType == 'jpeg' || $docType == 'PNG' || $docType == 'GIF' || $docType == 'JPG' || $docType == 'JPEG') && $this->Identity->Validate('items/item/docs/actions/viewimage')) {
					?>

					<div class="thumbnail">
						<img src="/<?=FOLDERADD?>/items/getimage?itemid=<?=$itemId?>&itemdocid=<?=$doc->itemdocId?>" alt="<?=encodeQuery($doc->name)?>" style="height:200px">

					</div>
					<?php
				}
				elseif($this->Identity->Validate('items/item/docs/actions/download')){ ?>
				<a href="/<?=FOLDERADD?>/items/getdoc?itemid=<?=$itemId?>&itemdocid=<?=$doc->itemdocId?>" target="_blank"  class="btn btn-primary btn-sm">
					<i class="fa fa-download fa-lg"></i> <?=$this->lang->line('general_download')?>
				</a>
				<?php } ?>
			</div>
		</div>
		<?php		
	}
	?>
	<?php	}
	?>

	<script>

	$('.openFolder').on('click',function()
	{
		$(this).closest('div').siblings('div').slideToggle();
	});

	$('.deleteButtonDoc').on('click',function()
	{
		$(this).closest('div').siblings('div').children('div').slideDown();
	});

	$('.deleteButtonFolder').on('click',function()
	{
		$(this).closest('div').siblings('div').children('div').slideDown();
		$(this).closest('div').siblings('div').slideDown();
		
	});

	$('.deleteCancelDoc').on('click',function()
	{
		$(this).closest('div').slideUp();
	});

	$('.deleteCancelFolder').on('click',function()
	{
		$(this).closest('div').slideUp();
	});

	$('.deleteOkDoc').on('click',function()
	{
		$.ajax(
		{
			method:'POST',
			url:'/'+FOLDERADD+'/items/deletedoc',
			data:{'itemdocId':$(this).attr('id'),'itemId':<?=$itemId?>},
			beforeSend:function()
			{

				//ver esto
				$(this).parent('div').slideUp();
				$(this).parent('div').parent('div').prev().find('button').hide();
				$(this).parent('div').parent('div').prev().find('a').hide();
				$(this).parent('div').parent('div').prev().find('span').show();
			}
		})
		.done(function(res)
		{
			if(res != "fail")
			{	
				angular.element('#viewContainer').scope().getDocs();
				angular.element('#viewContainer').scope().$apply();
			}
			else
			{
				//ver esto
				$(this).parent('div').parent('div').prev().find('button').show();
				$(this).parent('div').parent('div').prev().find('a').show();
				$(this).parent('div').parent('div').prev().find('span').hide();
				alert('Ocurrió un error al eliminar el documento. Inténtelo más tarde.');
			}
		});

	});
	


$('.deleteOkFolder').on('click',function()
{
	$.ajax(
	{
		method:'POST',
		url:'/'+FOLDERADD+'/items/deletefolder',
		data:{'itemdocfolderId':$(this).attr('id'),'itemId':<?=$itemId?>},
		beforeSend:function()
		{
			/*$(this).parent('div').slideUp();
			$(this).parent('div').parent('div').prev().find('button').hide();
			$(this).parent('div').parent('div').prev().find('a').hide();
			$(this).parent('div').parent('div').prev().find('span').show();*/
		}
	})
	.done(function(res)
	{
		if(res != "fail")
		{	
			angular.element('#viewContainer').scope().getDocs();
			angular.element('#viewContainer').scope().$apply();
		}
		else
		{
			/*$(this).parent('div').parent('div').prev().find('button').show();
			$(this).parent('div').parent('div').prev().find('a').show();
			$(this).parent('div').parent('div').prev().find('span').hide();*/
			alert('Ocurrió un error al eliminar la carpeta. Inténtelo más tarde.');
		}
	});

});

</script>