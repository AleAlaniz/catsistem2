<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/items"><?=$this->lang->line('general_items');?></a></li>
	<li><a href="/<?=FOLDERADD?>/items/details/<?=$itemId;?>"><?=encodeQuery($name);?></a></li>
	<li class="active"><?=$this->lang->line('administration_details');?></li>
</ol>
	<?php if (isset($_SESSION['itemMessage'])): ?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php if ($_SESSION['itemMessage'] == 'edit'){
			echo $this->lang->line('administration_items_editmessage');
		}
		?>
	</div>
<?php endif; ?>
<div class="col-xs-12" style="margin-bottom:15px">
	<?=$navBar?>
</div>
<div class="col-xs-12">
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('administration_items_information');?></strong>
	</div>
	<div class="panel-body">
		<dl class="dl-horizontal col-sm-6">
			<dt><?=$this->lang->line('administration_items_name');?></dt>
			<dd><?=encodeQuery($name)?></dd>
		</dl>
		<dl class="dl-horizontal col-sm-6">
			<?php 
			$sql = "SELECT * FROM subSections WHERE subsectionId = ?";
			$subsection = array($subsectionId);
			$query = $this->db->query($sql,$subsection)->row();
			?>
			<dt><?=$this->lang->line('administration_items_subsection');?></dt>
			<dd><?=encodeQuery($query->name)?></dd>
		</dl>
		<dl class="dl-horizontal col-sm-12">
			<dt><?=$this->lang->line('administration_items_description');?></dt>
			<dd><?=$description?></dd>
		</dl>
	</div>
</div>
</div>
<script type="text/javascript">
$('#nav_items').addClass('active');
$('#itemNavDetails').addClass('active');
</script>