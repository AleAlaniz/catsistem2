<div class="item-header" ng-controller="CviewNotices">
	<h3>
		<span><?=$this->lang->line('administration_items_notices')?><?php echo "(";?><?=$pageData->numrows;?><?php echo ")";?></span> 
		<?php if($this->Identity->Validate('items/item/notices/actions')) 
		{ 
			if($this->Identity->Validate('items/item/notices/actions/create')) {?>
			<a class="btn btn-success btn-xs" id="createNew" ><i class="fa fa-plus"></i> <?=$this->lang->line('administration_items_notice_create');?></a>
			<input type="hidden" id="available" value="<?=$available?>">
			<?php } 
			if (count($notices) > 0 && $this->Identity->Validate('items/item/notices/actions/deleteall')) {  ?>
			<button  class="btn btn-danger btn-xs" id="deleteallbutton"><i class="fa fa-times"></i> <?=$this->lang->line('administration_items_notice_deleteall');?></button>
			<span id="removing" ng-click="saludar()" style=" display:none" class="btn danger btn-xs" ><i style="color: red;" class="fa fa-trash fa-2x fa-spin"></i> <?= $this->lang->line('general_removing')?></span>
			<span id="deleteSuccess" style=" display:none" ><i style="color: green;" class="fa fa-check"></i></span>
			
			
			<?php } ?>
		<?php }?>
		<div class="btn-group pull-right" id="buttonsPage" role="group" aria-label="First group">
				<?php 
					$index  = 0;
					while ($index<$pageData->numPages) { 
				$index++;	?>
					<button type="button" class="btn btn-default" id="Page<?= $index?>"><?= $index?></button>
				<?php
				} ?>
			</div>
		</h3>
		<?php if($this->Identity->Validate('items/item/notices/actions/deleteall')){?>
		<div class="alert alert-danger text-center" role="alert" id="deleteallalert" style="display:none;" 	>
			<div><strong><?=$this->lang->line('administration_items_notice_deleteallareyousure')?></strong></div>
			<a href="/<?=FOLDERADD?>/items/item/#" class="btn btn-default btn-sm"><?=$this->lang->line('general_delete')?></a>
			<button id="deleteallcancel" class="btn btn-default btn-sm"><?=$this->lang->line('general_cancel')?></button>
		</div>
		<?php }?>
		<hr>
	</div>

	<!-- Modal de no se permite -->
	<div class="modal flat-style animated bounceIn" id="NotAvailableMessage" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body">
				<p><?php echo $this->lang->line('items_notice_notavailable') ?></p>
					<div class="text-right">
						<button id="accept" data-dismiss="modal" class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_accept') ?></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fin del modal -->
	<!-- actionview -->
	<div id="actionView">

	</div>
	<div class="modal flat-style animated lightSpeedIn" id="deleteAllNotices" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body">
					<p><?php echo $this->lang->line('administration_items_notice_deleteallareyousure') ?></p>
					<div class="text-right">
						<button  data-dismiss="modal" class="btn btn-white" data-dismiss="modal"><?= $this->lang->line('general_no') ?></button>
						<button id="deleteAllNoticesButton"  data-dismiss="modal" class="btn btn-lightgreen" ><?= $this->lang->line('general_yes') ?></button>
					</div>
				</div>
			</div>
		</div>
	</div>

<script>
	$(document).ready(function () {

		$('#createNew').on('click',function () {
			if($('#available').val() == "true"){
				window.location.href = "#/items/item/<?= $itemId ?>/notice/create";
			}
			else{
				$('#NotAvailableMessage').modal('show');
			}
		});

		$('#accept').on('click',function () {
			$('#NotAvailableMessage').hide();
		})

		$('#deleteallbutton').on('click',function()
		{
			$('#deleteAllNotices').modal('show');
		});

		$('#deleteAllNoticesButton').on('click',function()
		{
			$.ajax(
			{
				method:'POST',
				url:'/'+FOLDERADD+'/items/deleteallnotices',
				data:{'itemId':<?= $itemId ?>	},
				beforeSend:function()
				{
					$('#removing').show();
					$('#deleteallbutton').hide();
				}
			})
			.done(function(res)
			{
				if(res != "fail")
				{	
					
					$('#removing').hide();
					$('#deleteSuccess').show();

						angular.element('#viewContainer').scope().getNotices();
						angular.element('#viewContainer').scope().$apply();
				}
				else
				{
					alert("Ocurrió un error al eliminar las noticias. Inténtelo más tarde.");
				}
			});
		});

		$('body #buttonsPage').on('click','button',function () {
			$('#buttonsPage button').removeClass("active");
			$(this).addClass('active');
			var pageNumber = $(this).attr('id');
			pageNumber = pageNumber.substring(4);
			var itemId = <?= $itemId ?>;
			
			$.ajax({
			method 	: 'POST',
			url		:'/'+FOLDERADD+'/items/getnotices',
			data 	: {
				'itemId'	: itemId,
				'pageNumber': pageNumber
			},
			})
			.done(function(data) {
				data = jQuery.parseJSON(data);
				document.getElementById('actionView').innerHTML = "";
				document.getElementById('actionView').innerHTML = data.ActionView;
				var r = setInterval(()=>{
					if(document.getElementById('noticesContainer')){
						clearInterval(r);
						functionsOfViewNoticesGet();
						}
					},100)
			});
		})
		if($('#Page1').length == 1){
			document.getElementById("Page1").click();
		}

		function functionsOfViewNoticesGet() {
			$('.cancelDeleteButton').on('click',function()
			{
				$(this).closest('div').slideUp();
			});

			$('.deleteButton').on('click',function()
			{
				$(this).closest('div').siblings('div').children('div').slideDown();
			});

			$('.confirmDeleteNotice').on('click',function()
			{
				$.ajax(
				{
					method:'POST',
					url:'/'+FOLDERADD+'/items/deletenotice',
					data:{'itemnoticeId':$(this).attr('id'),'itemId':<?=$itemId?>},
					beforeSend:function()
					{
						$(this).parent('div').slideUp();
						$(this).parent('div').parent('div').prev().find('button').hide();
						$(this).parent('div').parent('div').prev().find('a').hide();
						$(this).parent('div').parent('div').prev().find('span').show();
					}
				})
				.done(function(res)
				{
					if(res != "fail")
					{	
						angular.element('#viewContainer').scope().getNotices();
						angular.element('#viewContainer').scope().$apply();
					}
					else
					{
						$(this).parent('div').parent('div').prev().find('button').show();
						$(this).parent('div').parent('div').prev().find('a').show();
						$(this).parent('div').parent('div').prev().find('span').hide();
						alert('Ocurrió un error al eliminar la noticia. Inténtelo más tarde.');
					}
				});

			});
		}	
	});
</script>