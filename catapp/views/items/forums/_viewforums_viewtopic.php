<?php if (isset($_SESSION['itemforumMessage']) && $this->Identity->Validate('items/item/forums/actions')){ ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['itemforumMessage'] == 'answertopic'){
		echo $this->lang->line('administration_items_forum_answertopicmessage');
	} elseif ($_SESSION['itemforumMessage'] == 'edittopic'){
		echo $this->lang->line('administration_items_forum_edittopicmessage');
	}elseif ($_SESSION['itemforumMessage'] == 'editmessage'){
		echo $this->lang->line('administration_items_forum_editmessagemessage');
	}elseif ($_SESSION['itemforumMessage'] == 'deletemessage'){
		echo $this->lang->line('administration_items_forum_deletemessagemessage');
	}
	?>
</div>
<?php 
}
?>
<div class="panel panel-default panel-forum">
	<div class="panel-heading">
		<h6 class="panel-title-nomargin">
			<i class="fa fa-comments fa-fw fa-lg"></i>
			<strong>
				<?=encodeQuery($viewtopic->topic)?>
			</strong>
		</h6>
	</div>
	<div class="panel-body">
		<div class="forum-message">
			<div class="forum-info">
				<img class=""  src="/<?=FOLDERADD;?>/users/profilephoto?userId=<?=$viewtopic->userId?>&wah=200">
				<div class="caption">
					<strong style="display:block">
						<?=encodeQuery($viewtopic->initiator->lastName)?> <?=encodeQuery($viewtopic->initiator->name)?>
					</strong>
					<small>
						<?=date('Y-m-d G:i', $viewtopic->timestamp)?>
					</small>
				</div>
			</div>
			<div class="forum-body">
				<?=$viewtopic->description?>
				<p>
					<a class="quote-link" href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=answerTopic&itemforumtopicId=<?=$viewtopic->itemforumtopicId?>&quote=topic"><i class="fa fa-comment-o"></i> <?=$this->lang->line('administration_items_forum_quote');?></a>
					<?php if ($this->Identity->Validate('items/item/forums/actions/edittopic', $viewtopic->userId)) { ?>
					| <a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=editTopic&itemforumtopicId=<?=$viewtopic->itemforumtopicId?>"><i class="text-warning fa fa-pencil fa-fw"></i></a>
					<?php } ?>
				</p>
			</div>
		</div>
		<?php
		foreach ($viewtopic->messages as $message) { 
			?>
			<div class="forum-message">
				<div class="forum-info">
					<img class=""  src="/<?=FOLDERADD;?>/users/profilephoto?userId=<?=$message->userId?>&wah=200">
					<div class="caption">
						<strong style="display:block">
							<?=encodeQuery($message->initiator->lastName)?> <?=encodeQuery($message->initiator->name)?>
						</strong>
						<small>
							<?=date('Y-m-d G:i', $message->timestamp)?>
						</small>
					</div>
				</div>
				<div class="forum-body">
					<?=$message->message?>
					<p>
						<a class="quote-link" href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=answerTopic&itemforumtopicId=<?=$viewtopic->itemforumtopicId?>&quote=<?=$message->itemforummessageId?>"><i class="fa fa-comment-o"></i> <?=$this->lang->line('administration_items_forum_quote');?></a>
						<?php if ($this->Identity->Validate('items/item/forums/actions/editmessage', $message->userId)) { ?>
						| <a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=editMessage&itemforummessageId=<?=$message->itemforummessageId?>"><i class="text-warning fa fa-pencil fa-fw"></i></a>
						<?php } 
						if ($this->Identity->Validate('items/item/forums/actions/deletemessage', $message->userId)) {
							echo '| <span id="deleteId-'.$message->itemforummessageId.'" class="btn btn-xs"><i class="text-danger fa fa-times fa-lg"></i></span>';
						?>
							<div class="alert alert-danger text-center" role="alert" id="deletealertId-<?=$message->itemforummessageId?>" style="display:none;" colspan="7" 	>
								<div><strong><?=$this->lang->line('administration_items_forum_deletemessageareyousure')?></strong></div>
								<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=deleteMessage&itemforummessageId=<?=$message->itemforummessageId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_delete')?></a>
								<button id="deletecancelId-<?=$message->itemforummessageId?>" class="btn btn-default btn-sm"><?=$this->lang->line('general_cancel')?></button>
							</div>
						<?php }
						?>
					</p>
				</div>
			</div>
			<?php
		}
		?>
	</div>
</div>

<script type="text/javascript">
<?php
	foreach ($viewtopic->messages as $message) { 
if($this->Identity->Validate('items/item/forums/actions/deletemessage', $message->userId)){
		?>
		$('#deletecancelId-<?=$message->itemforummessageId?>').click(function(){
			$('#deletealertId-<?=$message->itemforummessageId?>').fadeOut();
		});
		$('#deleteId-<?=$message->itemforummessageId?>').click(function(){
			$('#deletealertId-<?=$message->itemforummessageId?>').fadeIn();
		});
		<?php
	}
}
?>
</script>