<div class="item-header">
	<h3>
		<span><?=$this->lang->line('administration_items_forums')?></span> 
		<?php if($this->Identity->Validate('items/item/forums/actions/create') && !$this->input->get('action')){?>
		<a class="btn btn-success btn-xs" href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=create"><i class="fa fa-plus-square-o"></i> <?=$this->lang->line('administration_items_forum_create');?></a>
		<?php 
	}
	if($this->Identity->Validate('items/item/forums/actions/createcategory') && !$this->input->get('action')){
		?>
		<a class="btn btn-success btn-xs" href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=createCategory"><i class="fa fa-plus-square-o"></i> <?=$this->lang->line('administration_items_forum_createcategory');?></a>	
		<?php
	}
	if($this->Identity->Validate('items/item/forums/actions/createtopic') && $this->input->get('action') && $this->input->get('action') == 'viewforum' && $this->input->get('itemforumId')){
		?>
		<a class="btn btn-success btn-xs" href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=createTopic&itemforumId=<?=$viewforum->itemforumId?>"><i class="fa fa-plus-square-o"></i> <?=$this->lang->line('administration_items_forum_createtopic');?></a>	
		<?php
	}
	if($this->Identity->Validate('items/item/forums/actions/viewtopic') && $this->input->get('action') && $this->input->get('action') == 'viewTopic' && $this->input->get('itemforumtopicId')){
		?>
		<a class="btn btn-success btn-xs" href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums&action=answerTopic&itemforumtopicId=<?=$viewtopic->itemforumtopicId?>"><i class="fa fa-plus-square-o"></i> <?=$this->lang->line('administration_items_forum_answer');?></a>	
		<?php
	}
	?>
</h3>
<hr>
</div>
<?=$ActionView?>
<script type="text/javascript">
$('#forumsrightBar').addClass('active');
</script>