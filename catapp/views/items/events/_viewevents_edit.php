<form class="form-horizontal" method="POST" novalidate >
	<input type="hidden" name="itemeventId" id="itemeventId" value="<?=$eventEdit->itemeventsId?>">
	<div class="form-group">
		<label for="title" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_event_title');?> <strong class="text-danger">*</strong></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="title" name="title" value="<?php echo set_value('title', $eventEdit->title);?>" placeholder="<?=$this->lang->line('administration_items_event_title');?>" required>
			<?php echo form_error('title'); ?>
		</div>
	</div>

	<div class="form-group">
		<label for="date" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_event_date');?></label>

		<div class="col-xs-10">
			<input type="text" class="form-control input-sm" id="date" name="date" value="<?php echo set_value('date', date('Y-m-d H:i',$eventEdit->timestamp));?>" placeholder="<?=$this->lang->line('administration_items_event_date');?>" disabled>
			
		</div>

		<?php echo form_error('date'); ?>

	</div>

	<div class="form-group">
		<label for="duration" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_event_duration');?></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="duration" name="duration" value="<?php echo set_value('duration', $eventEdit->duration);?>" placeholder="<?=$this->lang->line('administration_items_event_duration');?>" required>
			<?php echo form_error('duration'); ?>
		</div>
	</div>

	<div class="form-group">
		<label for="location" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_event_location');?></label>
		<div class="col-sm-10">
			<input type="text" class="form-control input-sm" id="location" name="location" value="<?php echo set_value('location', $eventEdit->location);?>" placeholder="<?=$this->lang->line('administration_items_event_location');?>" required>
			<?php echo form_error('location'); ?>
		</div>
	</div>


	<div class="form-group">
		<label for="details" class="col-sm-2 control-label"><?=$this->lang->line('administration_items_event_details');?></label>
		<div class="col-sm-10">
			<textarea class="form-control" id="details" name="details" rows="10"><?php echo set_value('details', $eventEdit->details);?></textarea>
			<?php echo form_error('details'); ?>
		</div>
	</div>
	<div class="form-group text-center">
		<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
		<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=diary" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
	</div>
</form>
	<script type="text/javascript" src="/<?=APPFOLDERADD?>/libraries/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
	tinymce.init({
		toolbar: "cut copy paste | alignleft aligncenter alignright alignjustify | bullist numlist  | outdent indent | forecolor backcolor fontsizeselect  | bold italic underline | link unlink image media | table",
		plugins: "image link media textcolor table",
		language: 'es',
		selector: "#details"
	});
	</script>