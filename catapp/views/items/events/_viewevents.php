<div class="item-header">
	<h3>
		<span><?=$this->lang->line('administration_items_diary')?></span> 
		<?php if($this->Identity->Validate('items/item/event/actions')) { ?>
		<?php if($this->Identity->Validate('items/item/event/actions/create')) { ?>
		<a class="btn btn-success btn-xs" href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=diary&action=create"><i class="fa fa-calendar-plus-o"></i> <?=$this->lang->line('administration_items_addevent');?></a>
		<?php } 
		if (count($diary) > 0 && $this->Identity->Validate('items/item/event/actions/deleteall')) {  ?>
		<button class="btn btn-danger btn-xs" id="deleteallbutton"><i class="fa fa-calendar-times-o"></i> <?=$this->lang->line('administration_items_deleteallevent');?></button>
		<?php } ?>
		<?php }?>
	</h3>
	<?php if($this->Identity->Validate('items/item/event/actions/deleteall')){?>
	<div class="alert alert-danger text-center" role="alert" id="deleteallalert" style="display:none;" 	>
		<div><strong><?=$this->lang->line('administration_items_event_deleteallareyousure')?></strong></div>
		<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=diary&action=deleteallevent" class="btn btn-default btn-sm"><?=$this->lang->line('general_delete')?></a>
		<button id="deletealleventcancel" class="btn btn-default btn-sm"><?=$this->lang->line('general_cancel')?></button>
	</div>
	<?php }?>
	<hr>
</div>
<?=$ActionView?>
<script type="text/javascript">
$('#diaryrightBar').addClass('active');
</script>
<?php if($this->Identity->Validate('items/item/event/actions/deleteall')){ ?>
	<script type="text/javascript">
	$('#deletealleventcancel').click(function(){
		$('#deleteallalert').slideUp();
	});
	$('#deleteallbutton').click(function(){
		$('#deleteallalert').slideDown();
	});
	</script>
<?php } ?>