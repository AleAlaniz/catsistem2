<style>
#wait
{
	display: none;
	margin-right: 10px;

}
</style>

<div>
	<div id="content" style="display:none">
		<ol class="breadcrumb">
			<li><a ng-href="#/sections/section/{{itemData.section.sectionId}}">{{itemData.section.name}}</a></li>
			<li ng-show="itemData.sssection"><a ng-click="goBack('sssection',itemData.section.sectionId)" href="#/sections/section/{{itemData.section.sectionId}}">{{itemData.sssection.name}}</a></li>
			<li ng-show="itemData.subSection"><a ng-click="goBack('subSection',itemData.section.sectionId)" href="#/sections/section/{{itemData.section.sectionId}}">{{itemData.subSection.name}}</a></li>
			<li class="active">{{itemData.name}}</li>
		</ol>
		<div class="panel panel-default item-panel">
			<div class="panel-heading">
				<strong>{{itemData.name}}</strong>

				<a id="addFavorite" class="btn btn-xs pull-right badge" style="margin-right:5px" href ng-click="addFavorite()">
					&nbsp;

					<span ng-show="itemData.isFavorite == false"> <?=$this->lang->line('item_addtofavorites');?> <i class="fa fa-star-o fa-lg text-warning"></i></span>
					<span ng-show="itemData.isFavorite == true" > <?=$this->lang->line('item_removefromfavorites');?> <i class="fa fa-star fa-lg text-warning"></i></span>
					&nbsp;
				</a>
				<i id="wait" class="pull-right fa fa-star fa-2x fa-spin text-warning"></i>


			</div>
			<div class="panel-body row">
				<ul class="nav nav-pills nav-stacked col-sm-3 col-md-2 nav-items" id="fixFixedtop">
					<li role="presentation"  id="informationrightBar"><a ng-href="#/items/item/{{itemData.itemId}}" class="list-group-item"><i class="fa fa-info"></i> <span><?=$this->lang->line('administration_items_information')?></span></a></li>

					<?php //if($this->Identity->Validate('items/item/event/index')) {?>
					<!--li role="presentation" id="diaryrightBar"><a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=diary" class="list-group-item"><i class="fa fa-calendar"></i> <span><?=$this->lang->line('administration_items_diary')?></span></a></li-->
					<?php //} ?>
					<?php if($this->Identity->Validate('items/item/notices/index')) { ?>
					<li role="presentation" id="noticesrightBar"><a ng-href="#/items/item/{{itemData.itemId}}/notices" class="list-group-item"><i class="fa fa-newspaper-o"></i> <span><?=$this->lang->line('administration_items_notices')?></span></a></li>
					<?php } ?>
					<?php if($this->Identity->Validate('items/item/docs/index')){  ?>
					<li role="presentation" id="docsrightBar"><a ng-href="#/items/item/{{itemData.itemId}}/docs" class="list-group-item"><i class="fa fa-book"></i> <span><?=$this->lang->line('administration_items_doc')?></span></a></li>
					<?php } ?>
					<?php //if($this->Identity->Validate('items/item/trivia/index')) { ?>
					<!--li role="presentation" id="triviarightBar"><a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=trivia" class="list-group-item"><i class="fa fa-check-square-o"></i> <span><?=$this->lang->line('administration_items_trivia')?></span></a></li-->
					<?php //} ?>
			<?php //if($this->Identity->Validate('items/item/forums/index')) { 
				?>
				<!--li role="presentation" id="forumsrightBar">
					<a href="/<?=FOLDERADD?>/items/item/<?=$itemId?>?cmd=forums" class="list-group-item">
						<?php
						if ($noViews) {
							?>
							<i class="fa fa-circle noreadDot"></i>
							<?php
						}
						?>	
						<i class="fa fa-comments"></i> 
						<span><?=$this->lang->line('administration_items_forums')?></span>
					</a>
				</li-->
				<?php 
			//} 
				?>
			</ul>
			<div id="partialView" ng-bind-html="partialView | HtmlSanitize"  class="col-sm-9 col-md-10 item-panel-child">

			</div>
		</div>
	</div>
</div>

<div id="loading" class="panel-body text-center">
	<h2><i class="fa fa-refresh fa-spin fa-2x"></i></h2>
	<h3><?php echo $this->lang->line('general_wait') ?></h3>
</div>
</div>
