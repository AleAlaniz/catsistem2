
<?php echo $navbar; ?>
<div class="col-xs-12 flat-style">
	<div class="panel panel-default">
		<div class="panel-body">
			<form class="form-horizontal" method="POST" novalidate ng-submit="submitChat()" id="chatForm">
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('chat_info'); ?></h3>
				<hr style="margin-top: 10px; "/>
				<div class="form-group">
					<label for="subject" class="col-sm-2 control-label"><?=$this->lang->line('chat_subject');?> <span class="text-danger"> *</span></label>
					<div class="col-sm-10">
						<input autocomplete="off" type="text" class="form-control input-sm" id="subject" name="subject" ng-model="subject" placeholder="<?=$this->lang->line('chat_subject');?>" required>
						<p role="subjectError" class="text-danger" style="display:none"><?=$this->lang->line('chat_subjectrequired');?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="message" class="col-sm-2 control-label"><?=$this->lang->line('chat_message');?> <span class="text-danger"> *</span></label>
					<div class="col-sm-10">
						<input autocomplete="off" type="text" class="form-control input-sm" id="message" name="message" ng-model="message" placeholder="<?=$this->lang->line('chat_message');?>" required>
					</div>
				</div>
				<h3 style="margin-top: 0; margin-bottom: 0; padding-left: 10px"><?php echo $this->lang->line('chat_newto'); ?> <strong class="text-danger">*</strong></h3>
				<hr style="margin-top: 10px; "/>

				<div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('general_users');?></label>
					<div class="col-sm-10" cn-search-users placeholder="<?php echo $this->lang->line('general_find');?>" url="/<?php echo FOLDERADD; ?>/chat/searchusers" users="selectedUsers"></div>
				</div>
				<?php 
				if ($this->Identity->Validate('home/inbox/new/tosites')) 
				{
					?>

					<div class="form-group">
						<label class="col-sm-2 control-label"><?=$this->lang->line('general_sites');?></label>
						<div class="col-sm-10">
							<?php
							foreach ($sites as $key => $site) {
								?>
								<div class="checkbox checkbox-angular">
									<label ng-class="{check: sites[<?php echo $key;?>]}">
										<input type="checkbox" name="sites[]" ng-model="sites[<?php echo $key;?>]"  value="<?=$site->siteId?>">
										<?php echo encodeQuery($site->name);?>
									</label>
								</div>
								<?php
							}
							?>

						</div>
					</div>
					<?php
				} 
				?>
				<?php
				if ($this->Identity->Validate('usergroups/index')) {
					?>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?=$this->lang->line('general_usergroups');?></label>
						<div class="col-sm-10">
							<?php
							foreach ($userGroups as $key => $userGroup) {
								?>
								<div class="checkbox checkbox-angular">
									<label ng-class="{check: userGroups[<?php echo $key;?>]}">
										<input type="checkbox" name="userGroups[]" ng-model="userGroups[<?php echo $key;?>]"  value="<?=$userGroup->usergroupId?>">
										<?php echo encodeQuery($userGroup->name);?>
									</label>
								</div>
								<?php
							}
							?>

						</div>
					</div>
					<?php
				}
				?>


				<div class="form-group text-center">
					<button type="submit" class="btn btn-green" id="chatm_create"><?php echo $this->lang->line('chat_start');?></button>
				</div>
			</form>
			<div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p><?php echo $this->lang->line('general_completeall')?> <strong class="text-danger">*</strong></p>
							<div class="text-right">
								<span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.form.js"></script>
