<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('general_campaigns');?></li>
</ol>

<?php if($this->Identity->Validate('campaigns/create')) { ?>
<p>
	<a href="/<?=FOLDERADD?>/campaigns/create" class="btn btn-sm btn-success "><i class="fa fa-plus"></i><strong> <?=$this->lang->line('administration_create');?></strong></a>
</p>
<?php }
if (isset($_SESSION['flashMessage'])){ ?>
<div class="alert alert-success alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong><i class="fa fa-check"></i></strong> 
	<?php if ($_SESSION['flashMessage'] == 'create'){
		echo $this->lang->line('campaign_successmessage');
	}
	elseif ($_SESSION['flashMessage'] == 'delete'){
		echo $this->lang->line('campaign_deletemessage');
	}
	elseif($_SESSION['flashMessage']=='edit')
	{
		echo $this->lang->line('campaign_editmessage');
	}
	?>
</div>
<?php } ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('general_campaigns');?></strong>
		<span class="badge pull-right bg-success"><?=count($model)?></span>
	</div>
	<table class="table table-hover">
		<thead>
			<tr class="active">
				<th><?=$this->lang->line('campaign_name');?></th>
				<th><?=$this->lang->line('general_section');?></th>
				<th><?=$this->lang->line('general_client');?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($model) > 0) {?>

			<?php foreach ($model as $campaign){ ?>
			<tr class="optionsUser">
				<td><?=encodeQuery($campaign->name)?></td>
				<td>
					<?php 
					echo ($campaign->section == NULL ? $this->lang->line('campaign_nosection'): encodeQuery($campaign->section));
					?>
				</td>
				<td>
					<?php 
					echo ($campaign->client == NULL ? $this->lang->line('campaign_noclient'): encodeQuery($campaign->client));
					?>
				</td>
				<td>
					<?php if($this->Identity->Validate('campaigns/edit')) { ?>
					<a href="/<?=FOLDERADD?>/campaigns/edit/<?=$campaign->campaignId?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp; 
					<?php } ?>
					<?php if($this->Identity->Validate('campaigns/delete')) { ?>
					<a href="/<?=FOLDERADD?>/campaigns/delete/<?=$campaign->campaignId?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
					<?php } ?>
				</td>
			</tr>

			<?php } ?>
			<?php } 
			else { 
				?>
				<tr class="text-center">
					<td colspan="3"><i class="fa fa-bullhorn"></i> <?=$this->lang->line('campaign_empty');?></td>
				</tr>
				<?php
			}?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
$('#nav_campaigns').addClass('active');

</script>