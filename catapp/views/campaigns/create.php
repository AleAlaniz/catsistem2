<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li><a href="/<?=FOLDERADD?>/campaigns"><?=$this->lang->line('general_campaigns');?></a></li>
	<li class="active"><?=$this->lang->line('administration_create');?></li>
</ol>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('campaign_create');?></strong>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="POST" novalidate >
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label"><?=$this->lang->line('campaign_name');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<input type="text" class="form-control input-sm" id="name" name="name" value="<?php echo set_value('name');?>" placeholder="<?=$this->lang->line('campaign_name');?>" required>
					<?php echo form_error('name'); ?>
				</div>
			</div>

			<div class="form-group">
				<label for="section" class="col-sm-2 control-label"><?=$this->lang->line('general_section');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="section" name="section">
						<option value="0"><?php echo $this->lang->line('campaign_nosection'); ?></option>
						<?php
						foreach($sections as $section){
							?>
							<option value="<?=$section->sectionId?>" <?php echo  set_select('section', $section->sectionId); ?> ><?=$section->name?></option>
							<?php
						}
						?>
					</select>
					<?php echo form_error('section'); ?>
				</div>
			</div>


			<div class="form-group">
				<label for="client" class="col-sm-2 control-label"><?=$this->lang->line('general_client');?><span class="text-danger"><strong> *</strong></span></label>
				<div class="col-sm-10">
					<select class="form-control input-sm" id="client" name="client">
						<option value="0"><?php echo $this->lang->line('campaign_noclient'); ?></option>
						<?php 
						foreach($clients as $client)
						{
							?>
							<option value="<?=$client->clientId?>" <?php echo  set_select('client', $client->clientId); ?> ><?=$client->name?></option>
							<?php 
						}
						?>
					</select>
					<?php echo form_error('client'); ?>

				</div>
			</div>

			<hr>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button>
				<a href="/<?=FOLDERADD?>/campaigns" class="btn btn-danger btn-sm"><?=$this->lang->line('general_cancel');?></a>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$('#nav_campaigns').addClass('active');
</script>