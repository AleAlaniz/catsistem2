<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo $this->lang->line('referred_send_referred');?>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <h4><?= $this->lang->line('referred_message_body'); ?></h4> 
            </div>

            <div class="form-group">
                <label for="dni" class="control-label"><?= $this->lang->line('users_dni'); ?><span class="text-danger"><strong> *</strong></span></label>
                <input class="form-control" type="text" name="dni" id="dni" ng-model="referred.dni" maxlength="8" only-numbers="onlyNumbers()">
            </div>

            <div class="form-group">
                <label for="name" class="control-label"><?= $this->lang->line('teaming_form_name'); ?><span class="text-danger"><strong> *</strong></span></label>
                <input class="form-control" type="text" name="name" id="name" ng-model="referred.name" maxlength="50" alpha-input="alphaInput()">
            </div>

            <div class="form-group">
                <label for="numberPhone" class="control-label"><?= $this->lang->line('users_cell_phone'); ?><span class="text-danger"><strong> *</strong></span></label>
                <input class="form-control" type="text" name="numberPhone" id="numberPhone" ng-model="referred.numberPhone" maxlength="15" cel-number="celNumber()">
            </div>

            <div class="form-group">
                <label for="mail" class="control-label"><?= $this->lang->line('administration_users_email'); ?></label>
                <input class="form-control" type="text" name="mail" id="mail" ng-model="referred.mail" maxlength="50">
            </div>
            <div ng-bind-html="errors | HtmlSanitize" style="text-align: left;"></div>
        </div>
    </div>
    <div class="buttons text-center">
        <button type="submit" ng-click="sendReferred()" class="btn btn-default">
            <?=$this->lang->line('referred_send');?>
        </button>
    </div>
    <div class="modal fade" id="no-complete" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    <p><?php echo $this->lang->line('general_completeall')?> <strong class="text-danger">*</strong></p>
                    <div class="text-right">
                        <span class="btn btn-white" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_accept'); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script> $('#referred').replaceWith("<li><?php echo $this->lang->line('referred_title') ?></li>");</script>