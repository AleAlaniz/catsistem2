<link href="/<?php echo APPFOLDERADD; ?>/libraries/css/bootstrap-datepicker.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/<?php echo APPFOLDERADD; ?>/libraries/css/dataTables.bootstrap.min.css">

<div class="col-xs-12 flat-style">

	<div class="ng-hide container-fluid col-xs-12" ng-if="!candidatesList" style="background-color:white; margin:auto; display:block; vertical-align:middle;" ng-show="!loading">
		
		<div class="text-center container-fluid"  ng-if="isEmpty">
			<h1><?php echo $this->lang->line('referred_empty'); ?></h1>
		</div>
	</div>

	<div class="container-fluid"  ng-if="candidatesList">
		<div class="panel panel-default">
			<div class="panel panel-heading">
				<?php echo $this->lang->line('referred_candidates'); ?>
			</div>
			<div class="ng-hide panel-body" ng-show="!loading">
				<table class="datatable table table-responsive">
					<thead>
						<tr>
							<th class="row-md-1">
								&nbsp;
							</th>
							<th class="row-md-1">
								<?php echo $this->lang->line('teaming_form_name'); ?>
							</th>
							<th class="row-md-1">
								<?php echo $this->lang->line('users_dni'); ?>
							</th>
							<th class="row-md-1">
								<?php echo $this->lang->line('administration_users_cellPhone'); ?>
							</th>
							<th class="row-md-1">
								<?php echo $this->lang->line('administration_users_email'); ?>
							</th>
							<th class="row-md-1">
								<?php echo $this->lang->line('referred_by'); ?>
							</th>
							<th class="row-md-1">
								<?php echo $this->lang->line('referred_month'); ?>
							</th>
						</tr>
					</thead>
					<tbody class="table-body">
						<tr ng-if="candidatesList && candidatesList.length == 0">
							<td class="text-center" colspan="7">
								<?php echo $this->lang->line('referred_empty'); ?>
							</td>
						</tr>
						<tr class="table-adm" ng-repeat="candidate in candidatesList" ng-init ="candidate.selected = -1 ">
							<td class="row-md-1">
								<input type="radio" name="candidate.selected" ng-model="$parent.selected" value="{{candidate.referedId}}">
							</td>
							<td class="row-md-1">
								<span ng-hide=" editionEnabled && $parent.selected == candidate.referedId">
									{{candidate.name}}
								</span>
								<input type="text" ng-model="candidate.name" ng-value="candidate.name" maxlength="50" alpha-input="alphaInput()" ng-show="editionEnabled && $parent.selected == candidate.referedId"">
							</td>
							<td class="row-md-1">
								<span ng-hide=" editionEnabled && $parent.selected == candidate.referedId">
									{{candidate.dni}}
								</span>
								<input type="text" ng-model="candidate.dni" only-numbers="onlyNumbers()" ng-value="candidate.dni" maxlength="8" ng-show="editionEnabled && $parent.selected == candidate.referedId"">
							</td>
							<td class="row-md-1">
								<span ng-hide=" editionEnabled && $parent.selected == candidate.referedId">
									{{candidate.numberPhone}}
								</span>
								<input type="text" ng-model="candidate.numberPhone" ng-value="candidate.numberPhone" cel-number="celNumberInput()" maxlength="15" ng-show="editionEnabled && $parent.selected == candidate.referedId"">
							</td>
							<td class="row-md-1">
								<span ng-hide=" editionEnabled && $parent.selected == candidate.referedId">
									{{hasMail(candidate.mail)}}
								</span>
								<input type="text" ng-model="candidate.mail" ng-value="hasMail(candidate.mail)" maxlength="50" ng-show="editionEnabled && $parent.selected == candidate.referedId"">
							</td>
							<td class="row-md-1">
								<span>
									{{candidate.completeName}}
								</span>
							</td>
							<td class="row-md-1">
								<span>
									{{currentMonth(candidate.month)}}
								</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div  class="container-fluid text-center" ng-if="errors"  ng-bind-html="errors | HtmlSanitize" style="text-align: center; background-color: white; color:red">
		</div>

		<div  class="container-fluid text-center" ng-if="successful"  ng-bind-html="successful | HtmlSanitize" style="text-align: center; background-color: white; color:green">
		</div>
		</div>
		

		<div class="ng-hide" ng-show="!loading">
			<div class="btn-toolbar center-block" style="display: table;" ng-show="selected && !editionEnabled" >
				<div class="btn-group" role="group" aria-label="First group">
					<button class="btn btn-primary btn-lg center-block" ng-click="saveState(selected); editionEnabled = !editionEnabled">
						<?php echo $this->lang->line("general_edit")?>
					</button>
				</div>
				<div class="btn-group" role="group" aria-label="First group">
					<button class="btn btn-primary btn-lg center-block" ng-click="savedCandidateData = null; selected = null">
						<?php echo $this->lang->line("general_cancel")?>
					</button>
				</div>
			</div>
			<div class="btn-toolbar center-block" style="display: table;"  ng-hide="!editionEnabled">
				<div class="btn-group" role="group" aria-label="First group">
					<button class="btn btn-primary btn-lg center-block" ng-click="saveEdition(selected); editionEnabled = !editionEnabled; selected = null">
						<?php echo $this->lang->line("general_save")?>
					</button>
				</div>
				<div class="btn-group" role="group" aria-label="First group">
					<button class="btn btn-primary btn-lg center-block" ng-click="restoreData(selected); editionEnabled = !editionEnabled; selected = null">
						<?php echo $this->lang->line("general_cancel")?>
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="text-center" role="loading" ng-show="loading">
		<i class='fa fa-refresh fa-spin fa-4x fa-fw dark' ></i>
	</div>
</div>



<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/bootstrap-datepicker.min.js"></script>
<script src="/<?php echo APPFOLDERADD; ?>/libraries/script/locales/bootstrap-datepicker.es.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="/<?php echo APPFOLDERADD; ?>/libraries/script/dataTables.select.min.js"></script>
<script> $('#referredList').replaceWith("<li><?php echo $this->lang->line('referred_sended') ?></li>");</script>
