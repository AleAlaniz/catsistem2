<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-body">
            <p class="accepted-paragraph">
                <?=$this->lang->line('referred_accepted') ?>
            </p>
        </div>
    </div>
    <div class="buttons text-center">
        <a href="#/referred" class="btn btn-white">
            <?=$this->lang->line('general_goback');?>
        </a>
    </div>
</div>