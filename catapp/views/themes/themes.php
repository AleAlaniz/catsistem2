<ol class="breadcrumb">
	<li><a href="/<?=FOLDERADD?>/administration"><?=$this->lang->line('general_administration');?></a></li>
	<li class="active"><?=$this->lang->line('general_themes');?></li>
</ol>

<?php if($this->Identity->Validate('theme/create')) { ?>
<p>
	<a href="/<?=FOLDERADD?>/themes/create" class="btn btn-sm btn-success "><i class="fa fa-plus"></i><strong> <?=$this->lang->line('administration_create');?></strong></a>
</p>
<?php
}
if (isset($_SESSION['flashMessage']))
{ 
	?>
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong><i class="fa fa-check"></i></strong> 
		<?php 
		if ($_SESSION['flashMessage'] == 'create')
		{
			echo $this->lang->line('administration_themes_successmessage');
		}
		elseif ($_SESSION['flashMessage'] == 'delete')
		{
			echo $this->lang->line('administration_themes_deletemessage');
		}
		elseif ($_SESSION['flashMessage'] == 'save')
		{
			echo $this->lang->line('administration_themes_savemessage');
		}
		?>
	</div>
	<?php
} 
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<strong><?=$this->lang->line('general_themes');?></strong>
		<span class="badge pull-right bg-success"><?=count($model)?></span>
	</div>
	<table class="table table-hover">
		<form class="form-horizontal" method="POST" novalidate >
			<thead>
				<tr class="active">
					<th><?=$this->lang->line('administration_themes_name');?></th>
					<th><?=$this->lang->line('administration_themes_active');?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if (count($model) > 0) 
				{
					?>
					
					<?php
					foreach ($model as $theme)
					{ 
						$selected = '';
						if ($theme->isselected == 'TRUE') {
							$selected = 'checked';
						}
						?>
						<tr class="optionsUser">
							<td><?=encodeQuery($theme->name)?></td>
							<td><input type="radio" name="themeId" value="<?=$theme->themeId?>" <?=$selected?> ></td>
							<td>
								<?php 
								if($this->Identity->Validate('theme/details')) 
								{ 
									?>
									<a href="/<?=FOLDERADD?>/themes/details/<?=$theme->themeId?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>&nbsp; 
									<?php 
								}
								?>
								<?php 
								if($this->Identity->Validate('theme/edit')) 
								{
									?>
									<a href="/<?=FOLDERADD?>/themes/edit/<?=$theme->themeId?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp; 
									<?php 
								}
								?>
								<?php 
								if($this->Identity->Validate('theme/delete')) 
								{
									?>
									<a href="/<?=FOLDERADD?>/themes/delete/<?=$theme->themeId?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
									<?php 
								}
								?>
							</td>
						</tr>
						<?php 
					} 
					?>

					
					<?php
				} 
				else 
				{ 
					?>
					<tr class="text-center">
						<td colspan="3"><i class="fa fa-image"></i> <?=$this->lang->line('administration_themes_empty');?></td>
					</tr>

					<?php
				}
				?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3" class="text-right"><button type="submit" class="btn btn-success btn-sm"><?=$this->lang->line('general_save');?></button></td>
				</tr>
			</tfoot>
		</form>
	</table>
</div>
<script type="text/javascript">
$('#nav_theme').addClass('active');
$('.optionsUser').hover(function(){
	$(this).find('.glyphicon').css('visibility','visible');

},function(){
	$(this).find('.glyphicon').css('visibility','hidden');

});
</script>