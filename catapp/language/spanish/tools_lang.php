<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['tools_analitycs'] 				= 'Analitycs';
$lang['tools_analitycs_date'] 			= 'Fecha';
$lang['tools_analitycs_datestart'] 		= 'Comienzo';
$lang['tools_analitycs_datefinish'] 	= 'Final';
$lang['tools_analitycs_dateto'] 		= 'a';
$lang['tools_analitycs_viewdata'] 		= 'Mostrar grafico';
$lang['tools_analitycs_measures'] 		= 'Medidas';
$lang['tools_analitycs_width'] 			= 'Ancho';
$lang['tools_analitycs_height'] 		= 'Alto';
$lang['tools_analitycs_users'] 			= 'Usuarios';
$lang['tools_analitycs_logins'] 		= 'Entradas totales';
$lang['tools_analitycs_daily'] 			= 'Diario';
$lang['tools_analitycs_perusers'] 		= 'Por usuarios';
$lang['tools_analitycs_newusers'] 		= 'Usuarios nuevos';
$lang['tools_analitycs_oldusers'] 		= 'Usuarios que ya habian entrado';
$lang['tools_analitycs_online'] 		= 'Tiempo real';
$lang['tools_analitycs_loading'] 		= 'Cargando ';
$lang['tools_analitycs_onlineusers']	= 'Usuarios online';
$lang['tools_analitycs_pagesstates']	= 'Paginas visitadas';
$lang['tools_analitycs_page']			= 'Pagina';
$lang['tools_analitycs_visitors']		= 'Visitas';
$lang['tools_analitycs_login']			= 'Login / logout';
$lang['tools_analitycs_userslogin']		= 'Login / logout de usuarios';
$lang['tools_analitycs_show']			= 'Mostrar';
$lang['tools_analitycs_usersempty']		= 'No se encontraron usuarios';


$lang['tools_analitycs_name']			= 'Nombre';
$lang['tools_analitycs_lastName']		= 'Apellido';
$lang['tools_analitycs_userName']		= 'Usuario';
$lang['tools_analitycs_dni']		    = 'Dni';
$lang['tools_analitycs_logintime']		= 'Hora de login';
$lang['tools_analitycs_logouttime']		= 'Hora de logout';
$lang['tools_analitycs_site']			= 'Site';

$lang['tools_input_gamification']		= 'Control gamification';

$lang['tools_prize_validator']			= 'Validación de premios';
$lang['tools_prize_code']				= 'Codigo voucher';
$lang['tools_prize_findprize']			= 'Buscar premio';
$lang['tools_prize_coderequired']		= 'Debe ingresar un codigo voucher';
$lang['tools_prize_voucherempty']		= 'El voucher no existe';
$lang['tools_prize_error']				= 'Ha ocurrido un error, intentelo nuevamente';
$lang['tools_prize_userdata']			= 'Datos del usuario';
$lang['tools_prize_completename']		= 'Nombre y apellido';
$lang['tools_prize_photo']				= 'Foto';
$lang['tools_prize_prizedata']			= 'Datos del premio';
$lang['tools_prize_name'] 				= 'Nombre';
$lang['tools_prize_description']		= 'Descripcion';
$lang['tools_prize_reddem']				= 'Confirmar canje';
$lang['tools_prize_reddemedcomplete']	= 'Premio canjeado correctamente';
$lang['tools_prize_reddemeddata']		= 'Datos del canje';
$lang['tools_prize_reddemeddate']		= 'Fecha que fue canjeado';
$lang['tools_prize_reddemeduser']		= 'Usuario que confirmo el canje';
$lang['tools_prize_reddemconfirm']		= 'Estas segur@ que quieres confirmar este canje?';
$lang['tools_input_export']             = 'Exportar datos de operadores';

$lang['tools_faq']                      = 'Preguntas frecuentes';
$lang['tools_faq_empty']                = 'No hay preguntas frecuentes';
$lang['tools_create_title']             = 'Crear una nueva pregunta';
$lang['tools_title']                    = 'Título';
$lang['tools_message']                  = 'Los datos que se vuelquen en esta herramienta son solamente para un control personal pero que no constituyen la base de liquidación de comisiones ni premios, y solamente se tendrán en cuenta las operaciones efectivamente concretadas conforme las bases de datos de nuestros clientes y bajo la plataforma asignada para tal fin.';
$lang['tools_faq_create']               = 'Crear pregunta';
$lang['tools_faq_areyousure']           = '¿Estas seguro de eliminar esta pregunta?';
$lang['tools_faq_created']              = 'Pregunta creada correctamente';
$lang['tools_faq_edited']               = 'Pregunta editada correctamente';
$lang['tools_faq_deleted']              = 'Pregunta eliminada correctamente';

$lang['tools_analitycs_online_sites']   = 'Usuarios online por sites';
$lang['tools_analitycs_export']         = 'Exportar datos a excel';