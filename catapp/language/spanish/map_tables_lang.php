<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['map_tables_gender_m'] = 'Masculino';
$lang['map_tables_gender_f'] = 'Femenino';

$lang['map_tables_turn_m'] = 'Mañana';
$lang['map_tables_turn_t'] = 'Tarde';
$lang['map_tables_turn_n'] = 'Noche';
$lang['map_tables_turn_f'] = 'Full Time';

$lang['map_tables_civil_state_s'] = 'Soltero/a';
$lang['map_tables_civil_state_c'] = 'Casado/a';
$lang['map_tables_civil_state_d'] = 'Divorciado/a';
$lang['map_tables_civil_state_v'] = 'Viudo/a';
$lang['map_tables_civil_state_o'] = 'Concubino/a';

$lang['map_tables_study_levels_s'] = 'Secundario';
$lang['map_tables_study_levels_t'] = 'Terciario';
$lang['map_tables_study_levels_u'] = 'Universitario';
$lang['map_tables_study_levels_p'] = 'Postgrado';

$lang['map_tables_study_statues_c'] = 'Completo';
$lang['map_tables_study_statues_i'] = 'Incompleto';
$lang['map_tables_study_statues_e'] = 'En curso';
$lang['map_tables_study_statues_n'] = 'No cursado';
