<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['newsletter_file'] 						= 'Archivo Adjunto';
$lang['newsletter_downloadfile'] 				= 'Ver/Descargar Archivo Adjunto';
$lang['newsletter_empty'] 						= 'No hay novedades';
$lang['newsletter_newsletter'] 					= 'Novedad';
$lang['newsletter_categoryempty'] 				= 'No hay categorias';
$lang['newsletter_createcategory'] 				= 'Categoria';
$lang['newsletter_message'] 					= 'Gestionar novedades';
$lang['newsletter_categorysuccessmessage'] 		= 'Categoria creada correctamente';
$lang['newsletter_categorydeletemessage'] 		= 'Categoria eliminada correctamente';
$lang['newsletter_categoryeditmessage'] 		= 'Categoria editada correctamente';
$lang['newsletter_newscreatemessage'] 			= 'Novedad creada correctamente';
$lang['newsletter_newsdeletemessage'] 			= 'Novedad eliminada correctamente';
$lang['newsletter_newseditmessage'] 			= 'Novedad editada correctamente';
$lang['newsletter_name'] 						= 'Nombre';
$lang['newsletter_ntitle'] 						= 'Titulo';
$lang['newsletter_color'] 						= 'Color';
$lang['newsletter_createnewsletter'] 			= 'Crear novedad';
$lang['newsletter_editnewsletter']	 			= 'Editar novedad';
$lang['newsletter_create'] 						= 'Novedad';
$lang['newsletter_ccategory'] 					= 'Crear categoria';
$lang['newsletter_categorydeleteareyousure'] 	= 'Esta seguro que quiere eliminar esta categoria?';
$lang['newsletter_newsdeleteareyousure'] 		= 'Esta seguro que quiere eliminar esta novedad?';
$lang['newsletter_category'] 					= 'Categoria';
$lang['newsletter_editcategory'] 				= 'Editar categoria';
$lang['newsletter_categoryexist'] 				= 'Esta categoria no existe';
$lang['newsletter_link'] 						= 'Quien lo puede ver';
$lang['newsletter_new'] 						= 'Nuevo';
$lang['newsletter_emptyLinks'] 					= 'Nadie lo puede ver';
$lang['newsletter_findusers'] 					= 'Buscar usuarios';
$lang['newsletter_info'] 						= 'Informacion';
$lang['newsletter_file_allowtypes']				= 'Tipos de archivos permitidos: gif, jpg, png, pdf, doc, docx, pptx, ppt, xlsx, xls, txt, html';
$lang['newsletter_completeall']					= 'Complete todos los campos marcados con';


?>