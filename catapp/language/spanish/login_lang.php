<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['login_username'] = 'Nombre de Usuario';
$lang['login_password'] = 'Contrase&ntilde;a';
$lang['login_error_message'] = 'El nombre de usuario y la contrase&ntilde;a no coinciden';
$lang['login_signin'] 		 = 'Entrar';
$lang['login_dont_remember_pass'] = '¿Olvidaste tu contraseña?';