<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['suggestbox_havesuggest'] 	= 'Tienes una sugerencia?';
$lang['suggestbox_suggest'] 		= 'Sugerencia';
$lang['suggestbox_mysuggest'] 		= 'Mis sugerencias';
$lang['suggestbox_empty'] 			= 'No hay sugerencias';
$lang['suggestbox_successmessage'] 	= 'Sugerencia enviada correctamente.';
$lang['suggestbox_viewall'] 		= 'Todas';
$lang['suggestbox_view'] 			= 'Ver buzón';
$lang['suggestbox_suggests'] 		= 'Sugerencias';
$lang['suggestbox_site'] 			= 'Site';
$lang['suggestbox_campaign'] 		= 'Campaña / Área';
$lang['suggestbox_take'] 			= 'Tomar sugerencia';
$lang['suggestbox_takehelp'] 		= 'Haz click aqui para tomar esta sugerencia';
$lang['suggestbox_takeconfirm']		= 'Esta seguro que quiere tomar esta sugerencia?';
$lang['suggestbox_takedby']			= 'Sugerencia tomada por';
$lang['suggestbox_takenok']			= 'Sugerencia tomada correctamente';