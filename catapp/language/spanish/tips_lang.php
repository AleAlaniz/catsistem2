<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['tips_empty']			= 'No hay tips';
$lang['tips_create']		= 'Crear tip';
$lang['tips_tip']			= 'Tip';
$lang['tips_feedback']		= 'Con feedback';
$lang['tips_date']			= 'Fecha y hora';
$lang['tips_createdby']		= 'Creado por';
$lang['tips_deletemessage']	= 'Esta seguro que quiere eliminar este tip?';
$lang['tips_details']		= 'Detalles';
$lang['tips_edit']			= 'Editar tip';
$lang['tips_feedback_w']	= 'Feedback';


?>