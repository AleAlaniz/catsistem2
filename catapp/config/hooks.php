<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['post_controller_constructor'][] = array(
	'class'    => 'Data',
	'function' => 'check_data_complete',
	'filename' => 'Data.php',
	'filepath' => 'hooks');

$hook['post_controller_constructor'][] = array(
	'class'    => 'Surveys_hook',
	'function' => 'complete_surveys',
	'filename' => 'Surveys_hook.php',
	'filepath' => 'hooks');

$hook['post_controller_constructor'][] = array(
	'class'    => 'CorporateDocs_hook',
	'function' => 'read_doc',
	'filename' => 'CorporateDocs_hook.php',
	'filepath' => 'hooks');

$hook['post_controller_constructor'][] = array(
	'class'    => 'Notification_hook',
	'function' => 'show_notification',
	'filename' => 'Notification_hook.php',
	'filepath' => 'hooks');

$hook['post_controller_constructor'][] = array(
	'class'	   => 'Activities_hook',
	'function' => 'complete_activities',
	'filename' => 'Activities_hook.php',
	'filepath' => 'hooks'
);
/* End of file hooks.php */
/* Location: ./application/config/hooks.php */